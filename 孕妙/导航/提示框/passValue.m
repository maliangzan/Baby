//
//  passValue.m
//  有登录功能的多款APP
//
//  Created by sayes on 15/6/25.
//  Copyright (c) 2015年 sayes. All rights reserved.
//

#import "passValue.h"


static passValue *share = nil;
@implementation passValue

+(passValue *)passValue
{
    if(!share)
    {
        share = [[passValue alloc]init];
    }
    return share;
}
@end
