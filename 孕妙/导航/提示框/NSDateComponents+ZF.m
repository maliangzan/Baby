//
//  NSDateComponents+ZF.m
//  BLE体重称
//
//  Created by gjiacheng on 15/7/21.
//  Copyright (c) 2015年 gjiacheng. All rights reserved.
//

#import "NSDateComponents+ZF.h"

@implementation NSDateComponents (ZF)
+ (instancetype)dateComponentsWithNow
{
    NSDate *now = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSUInteger unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitWeekday | NSCalendarUnitWeekOfMonth | NSCalendarUnitWeekOfYear;
    
    return [calendar components:unitFlags fromDate:now];
}
@end
