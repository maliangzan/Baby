//
//  NSDateComponents+ZF.h
//  BLE体重称
//
//  Created by gjiacheng on 15/7/21.
//  Copyright (c) 2015年 gjiacheng. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDateComponents (ZF)
/**
 获得当前的日期：年月日，在当前月的第几个星期，当前年的第几个星期
 */
+ (instancetype)dateComponentsWithNow;
@end
