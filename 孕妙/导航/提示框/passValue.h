//
//  passValue.h
//  有登录功能的多款APP
//
//  Created by sayes on 15/6/25.
//  Copyright (c) 2015年 sayes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "foodExchange.h"


@interface passValue : NSObject
@property (nonatomic,copy)NSString *accountString;
@property (nonatomic,assign)BOOL loginFlag;
@property (nonatomic,assign)BOOL blueToothFlag;
@property (nonatomic,assign)BOOL sleepFlag;
@property (nonatomic,assign)BOOL chengFlag;
@property (nonatomic,assign)BOOL savePWFlag;
@property (nonatomic,copy)NSString *code;
@property (nonatomic,copy)NSString *sssAppVerison;
@property (nonatomic,copy)NSString *userAccountString;
@property (nonatomic,copy)NSString *questionString;
@property (nonatomic,copy)NSString *answerString;
@property (nonatomic,copy)NSString *lycAppVersion;
@property (nonatomic,assign)BOOL sssFlag;
@property (nonatomic,copy)NSString *weixinName;
@property (nonatomic,assign)BOOL backFlag;
@property (nonatomic,assign)BOOL weixinFlag;
@property (nonatomic,assign)BOOL QQFlag;
@property (nonatomic,assign)BOOL superAppFlag;
@property (nonatomic,copy)NSString *picAddrString1;
@property (nonatomic,copy)NSString *picAddrString2;
@property (nonatomic,copy)NSString *picAddrString3;
@property (nonatomic,strong)NSMutableArray *imageArray;
@property (nonatomic,assign)BOOL netFlag;

@property (nonatomic,copy)NSString *accessToken;
@property (nonatomic,copy)NSString *nickname;
@property (nonatomic,copy)NSString *phoneAccountString;
@property (nonatomic,assign)BOOL zidongFlag;
@property (nonatomic,assign)BOOL saveTokenFlag;
@property (nonatomic,assign)BOOL fanhuiFlag;
@property (nonatomic,assign)BOOL backFlag1;

@property (nonatomic,assign)BOOL weixinHiddenFlag;
@property (nonatomic,assign)BOOL getUserInformationFlag;

@property (nonatomic,strong)UITabBarController *control;

@property (nonatomic,assign)BOOL weixinCountFlag;
@property (nonatomic,copy)NSString *passValueName;
@property (nonatomic,assign)BOOL selectCellflag;
@property (nonatomic,copy)NSString *selectCellUserName;
@property (nonatomic,copy)NSString *saveDataUserName;

@property (nonatomic,assign)BOOL bianjiFlag;
@property (nonatomic,copy)NSString *bianjiUserName;
@property (nonatomic,assign)BOOL removeFlag;

@property (nonatomic,assign)BOOL noUserNameInfo;

@property (nonatomic,strong)NSMutableArray *foodaaaaaa;

//产检次数
@property (nonatomic,assign)NSInteger ChanjianCount;
//产检完成次数
@property (nonatomic,strong)NSMutableArray *chanjianCompleteArray;

//判断账号有无
@property (nonatomic,assign)BOOL accountHaveFlag;

@property (nonatomic,assign)BOOL exchangeFoodFlag;
//用户ID
@property (nonatomic, assign)NSString *userID;

//对比食物
@property (nonatomic,strong)foodExchange *selectFood;



+(passValue *)passValue;

@end
