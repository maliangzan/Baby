//
//  UIBarButtonItem+ZFL.m
//  健康监测
//
//  Created by sayes1 on 16/1/11.
//  Copyright © 2016年 sayes1. All rights reserved.
//

#import "UIBarButtonItem+ZFL.h"

@implementation UIBarButtonItem (ZFL)
+ (UIBarButtonItem *)itemWithIcon:(NSString *)icon highIcon:(NSString *)highIcon target:(id)target action:(SEL)action
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    if (icon) {
        [button setImage:[UIImage imageNamed:icon] forState:UIControlStateNormal];
    }
    if (highIcon) {
        [button setBackgroundImage:[UIImage imageNamed:highIcon] forState:UIControlStateHighlighted];
    }

    button.frame = CGRectMake(0, 0, 100, 44);//(CGRect){CGPointZero, button.currentImage.size};
    
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
//    [button setTitle:@"   " forState:UIControlStateNormal];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
//    [button setBackgroundColor:[UIColor blackColor]];
    return [[UIBarButtonItem alloc] initWithCustomView:button];
}

@end
