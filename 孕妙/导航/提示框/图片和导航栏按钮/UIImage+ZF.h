//
//  UIImage+ZF.h
//  BLE体重称
//
//  Created by gjiacheng on 15/6/29.
//  Copyright (c) 2015年 gjiacheng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (ZF)
///**
// *  加载图片
// *
// *  @param name 图片名
// */
//+ (UIImage *)imageWithName:(NSString *)name;

/**
 *  返回一张自由拉伸的图片
 */
+ (UIImage *)resizedImageWithName:(NSString *)name;
/**
 *截图
 */
+ (instancetype)captureWithView:(UIView *)view;
@end
