//
//  UIBarButtonItem+ZFL.h
//  健康监测
//
//  Created by sayes1 on 16/1/11.
//  Copyright © 2016年 sayes1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBarButtonItem (ZFL)

/**
 *  快速创建一个显示图片的item
 *
 *  @param action   监听方法
 */
+ (UIBarButtonItem *)itemWithIcon:(NSString *)icon highIcon:(NSString *)highIcon target:(id)target action:(SEL)action;
@end
