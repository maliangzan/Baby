//
//  AllKindsOfView.m
//  孕妙
//
//  Created by 是源科技 on 16/5/30.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "AllKindsOfView.h"

@implementation AllKindsOfView

//疫苗管理
-(void)yimiaoGuanliAddTitleLabel:(NSString *)titleString andmiaoshuLabel:(NSString *)miaoshuStr
{
    CGFloat w = self.frame.size.width;
    CGFloat h = self.frame.size.height;
    
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(20, h / 5.0, h / 5.0 * 3, h / 5.0 * 3)];
    imageView.image = [UIImage imageNamed:@"assistant_vaccine management"];
    [self addSubview:imageView];
    
    UILabel *titleLabel = [[UILabel alloc]init];
    titleLabel.frame = CGRectMake(40 + h / 5.0 * 3, 0, w / 2.0, h / 2.0);
    titleLabel.font = [UIFont systemFontOfSize:18.0];
    titleLabel.textColor = YMColor(252, 162, 208);
    titleLabel.text = titleString;
    [self addSubview:titleLabel];
    
    UILabel *miaoshuLabel = [[UILabel alloc]init];
    miaoshuLabel.frame = CGRectMake(40 + h / 5.0 * 3, h / 2.0 - 8, w / 2.0, h / 2.0);
    miaoshuLabel.adjustsFontSizeToFitWidth = YES;
    miaoshuLabel.font = [UIFont systemFontOfSize:14.0];
    miaoshuLabel.textColor = YMColor(194, 194, 194);
    miaoshuLabel.numberOfLines = 0;
    miaoshuLabel.text = miaoshuStr;
    [self addSubview:miaoshuLabel];
    
    _completeLabel = [[UILabel alloc]init];
    _completeLabel.frame = CGRectMake(w - 20 - w / 4.0, 0, w / 4.0, h);
    _completeLabel.textColor = YMColor(253, 133, 0);
    _completeLabel.textAlignment = NSTextAlignmentRight;
    _completeLabel.text = @"未完成";
    _completeLabel.font = [UIFont systemFontOfSize:15.0];
    [self addSubview:_completeLabel];
    
    self.yimiaoBtn = [[UIButton alloc]initWithFrame:self.bounds];
    [self addSubview:self.yimiaoBtn];
    self.yimiaoBtn.tag = self.tag;

}

@end
