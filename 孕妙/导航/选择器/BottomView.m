//
//  BottomView.m
//  YunShouHu
//
//  Created by WangQiang on 16/3/24.
//  Copyright © 2016年 WangQiang. All rights reserved.
//

#import "BottomView.h"
@interface BottomView()
@property (weak ,nonatomic) UIButton *confirmBtn;
@property (weak ,nonatomic) UIButton *canleBtn;

@property (weak ,nonatomic) UIView *topLine;
@property (weak ,nonatomic) UIView  *midleLine;
@end
@implementation BottomView
-(instancetype)init{
    if (self = [super init]) {
        UIButton *confirm = [[UIButton alloc] init];
        [confirm setTitle:@"确 定" forState:UIControlStateNormal];
        confirm.titleLabel.font = [UIFont systemFontOfSize:15.0];
        [confirm setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [confirm setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        [confirm addTarget:self action:@selector(confirm) forControlEvents:UIControlEventTouchUpInside];
        confirm.backgroundColor = [UIColor colorWithRed:252 / 255.0 green:160 / 255.0 blue:207 / 255.0  alpha:1.0];
        self.confirmBtn = confirm;
        [self addSubview:confirm];
        
        UIButton *cancle = [[UIButton alloc] init];
        [cancle setTitle:@"取 消" forState:UIControlStateNormal];
        [cancle setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [cancle addTarget:self action:@selector(cancle) forControlEvents:UIControlEventTouchUpInside];
        cancle.titleLabel.font = [UIFont systemFontOfSize:15.0];
        cancle.backgroundColor = [UIColor colorWithRed:252 / 255.0 green:160 / 255.0 blue:207 / 255.0  alpha:1.0];
        self.canleBtn = cancle;
        [self addSubview:cancle];
        
        UIView *topLine = [[UIView alloc] init];
        topLine.backgroundColor = [UIColor clearColor];
        self.topLine = topLine;
        [self addSubview:topLine];
        
        
        UIView *middleLine = [[UIView alloc] init];
        middleLine.backgroundColor = [UIColor whiteColor];
        self.midleLine = middleLine;
        [self addSubview:middleLine];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}
-(void)layoutSubviews{
    [super layoutSubviews];
    CGFloat LineHeight = 1;
    CGFloat lineW = LineHeight;
    self.topLine.frame = CGRectMake(0, 0, self.width, lineW);
    self.midleLine.frame = CGRectMake(self.width*0.5, lineW, lineW, self.height - lineW);
    self.canleBtn.frame = CGRectMake(0, 1.0, self.width*0.5, self.height - 1.0);
    self.confirmBtn.frame = CGRectMake(CGRectGetMaxX(self.canleBtn.frame), self.canleBtn.y, self.canleBtn.width, self.canleBtn.height);
//     self.confirmBtn.frame = CGRectMake(CGRectGetMaxX(self.confirmBtn.frame), self.confirmBtn.y, self.confirmBtn.width, self.confirmBtn.height);
}

-(void)confirm{
    if ([self.delegate respondsToSelector:@selector(bottomViewDidConfirm:)]) {
        [self.delegate bottomViewDidConfirm:self];
    }
}
-(void)cancle{
    if ([self.delegate respondsToSelector:@selector(bottomViewDidCancel:)]) {
        [self.delegate bottomViewDidCancel:self];
    }
}
@end
