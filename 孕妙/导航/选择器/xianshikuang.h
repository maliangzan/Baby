//
//  xianshikuang.h
//  孕妙
//
//  Created by 是源科技 on 16/5/17.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface xianshikuang : UIView
@property (strong, nonatomic)UIButton      *sureBtn;
@property (strong, nonatomic)UIButton      *cancelBtn;
@property (strong, nonatomic)UIPickerView  *selectPickerView;
@property (strong, nonatomic)UILabel *dianL;

- (void)setInterFace:(NSString *)title;

@end
