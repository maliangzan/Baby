//
//  SettingAlertController.h
//  YunShouHu
//
//  Created by WangQiang on 16/3/24.
//  Copyright © 2016年 WangQiang. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger,ALertContentType) {
    ALertContentTypeTableView,
    ALertContentTypePickerView,
};
@interface SettingAlertController : UIViewController
@property (strong ,nonatomic) NSArray *tableViewData;
@property (copy ,nonatomic) void (^completionSelected)(NSArray *resuls,BOOL confirm);
@property (assign ,nonatomic) ALertContentType contentType;
@property (copy ,nonatomic) NSString * alertTitle;
@property (strong ,nonatomic) UIDatePicker *pickerView;
@end
