//
//  BottomView.h
//  YunShouHu
//
//  Created by WangQiang on 16/3/24.
//  Copyright © 2016年 WangQiang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+Extension.h"
@class BottomView;
@protocol BottomViewDelegate<NSObject>
-(void)bottomViewDidCancel:(BottomView *)bottomView;
-(void)bottomViewDidConfirm:(BottomView *)bottomView;
@end
@interface BottomView : UIView
@property (weak ,nonatomic) id<BottomViewDelegate> delegate;
@end
