//
//  SettingAlertController.m
//  YunShouHu
//
//  Created by WangQiang on 16/3/24.
//  Copyright © 2016年 WangQiang. All rights reserved.
//

#import "SettingAlertController.h"
#import "BottomView.h"
#define TitleH 30
#define APP_WIGHT 320

#define kDeviceHeight [UIScreen mainScreen].bounds.size.height
#define kDeviceWidth  [UIScreen mainScreen].bounds.size.width
@interface SettingAlertController ()<BottomViewDelegate,UITableViewDelegate,UITableViewDataSource>
@property (strong ,nonatomic) UIView *centerView;
@property (strong ,nonatomic) BottomView *bottomView;
@property (strong ,nonatomic) UITableView *tableView;
@property (strong ,nonatomic) NSDate *selectedDate;
//@property (copy ,nonatomic) NSString *selectedDate;
@property (strong ,nonatomic) UILabel *alertTitleView;
@end

@implementation SettingAlertController
-(UILabel *)alertTitleView{
    if (!_alertTitleView) {
        _alertTitleView = [[UILabel alloc] init];
        _alertTitleView.backgroundColor = [UIColor colorWithRed:252 / 255.0 green:160 / 255.0 blue:207 / 255.0  alpha:1.0];
        _alertTitleView.font = [UIFont systemFontOfSize:15.0];
        _alertTitleView.textAlignment = NSTextAlignmentCenter;
        _alertTitleView.textColor = [UIColor whiteColor];
        
    }
    return _alertTitleView;
}
-(UIView *)centerView{
    if (!_centerView) {
        _centerView = [[UIView alloc] init];
        _centerView.backgroundColor = [UIColor whiteColor];
//        [_centerView addCornerRadius:5.0];
        [_centerView addSubview:self.bottomView];
    }
    return _centerView;
}
-(UIDatePicker *)pickerView{
    if (!_pickerView) {
        _pickerView = [[UIDatePicker alloc] init];
        _pickerView.backgroundColor = [UIColor whiteColor];
        //设置监听
        [_pickerView addTarget:self action:@selector(birthdayChange:) forControlEvents:UIControlEventValueChanged];
        _pickerView.datePickerMode = UIDatePickerModeTime;
        _pickerView.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"];
//        for (UIView *view01 in _pickerView.subviews) {
//            if ([view01 isKindOfClass:NSClassFromString(@"_UIDatePickerView")]) {
//                for (UIView *view02 in view01.subviews) {
//                    if ([view02 isMemberOfClass:[UIView class]] && view02.height == 0.5) {
//                        view02.hidden = YES;
//                    }
//                }
//            }
//        }
        self.selectedDate = [NSDate date];
        
        [_pickerView setDate:self.selectedDate];
    }
    return _pickerView;
}
-(BottomView *)bottomView{
    if (!_bottomView) {
        _bottomView = [[BottomView alloc] init];
        _bottomView.delegate = self;
        _bottomView.backgroundColor = [UIColor whiteColor];
    }
    return _bottomView;
}

-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] init];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.separatorColor = [UIColor grayColor];
        _tableView.showsHorizontalScrollIndicator = NO;
        if ([_tableView respondsToSelector:@selector(setSeparatorInset:)]) {
            [_tableView setSeparatorInset:UIEdgeInsetsMake(0,0,0,0)];
        }
        
        if ([_tableView respondsToSelector:@selector(setLayoutMargins:)]) {
            [_tableView setLayoutMargins:UIEdgeInsetsMake(0,0,0,0)];
        }
    }
    return _tableView;
}


-(void)setContentType:(ALertContentType)contentType{
    _contentType = contentType;
    if (self.alertTitle && self.alertTitle.length > 0) {
        [self.centerView addSubview:self.alertTitleView];
    }
    if (contentType == ALertContentTypeTableView) {
        [self.centerView addSubview:self.tableView];
    }else{
        [self.centerView addSubview:self.pickerView];
    }
    [self.centerView bringSubviewToFront:self.bottomView];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    [self.view addSubview:self.centerView];
}
-(void)setAlertTitle:(NSString *)alertTitle{
    _alertTitle = alertTitle;
    self.alertTitleView.text = alertTitle;
}
-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    
    self.centerView.frame = CGRectMake(0, 0, APP_WIGHT - 50 , 300);
    self.centerView.center = self.view.center;
    
    CGFloat bottomH = 38.0;
    if (self.alertTitle && self.alertTitle.length > 0) {
        self.alertTitleView.frame = CGRectMake(0, 0, self.centerView.frame.size.width , TitleH);
        if (self.contentType == ALertContentTypeTableView) {
            self.tableView.frame = CGRectMake(0, CGRectGetMaxY(self.alertTitleView.frame), self.centerView.frame.size.width, self.centerView.frame.size.height - bottomH);
        }else{
            self.pickerView.frame = CGRectMake(0, CGRectGetMaxY(self.alertTitleView.frame), self.centerView.frame.size.width, self.centerView.frame.size.height - bottomH);
        }
    }else{
        if (self.contentType == ALertContentTypeTableView) {
            self.tableView.frame = CGRectMake(0, 0, self.centerView.frame.size.width, self.centerView.frame.size.height - bottomH);
        }else{
            self.pickerView.frame = CGRectMake(0, 0, self.centerView.frame.size.width, self.centerView.frame.size.height - bottomH);
        }
    }
    

    self.bottomView.frame = CGRectMake(0, self.centerView.frame.size.height - bottomH, self.centerView.frame.size.width, bottomH);
}


#pragma mark -- tableView
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  self.tableViewData.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *myCell = @"myCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:myCell];
    if (!cell) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:myCell];
        if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
            [cell setSeparatorInset:UIEdgeInsetsZero];
        }
        
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsZero];
        }
        cell.textLabel.font = [UIFont systemFontOfSize:14.0];
        cell.textLabel.textColor = [UIColor colorWithRed:170 / 255.0 green:170 / 255.0 blue:170 / 255.0 alpha:1.0];
        
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.highlightedTextColor = [UIColor colorWithRed:252 / 255.0 green:160 / 255.0 blue:207 / 255.0  alpha:1.0];
        cell.selectedBackgroundView.backgroundColor = [UIColor colorWithRed:252 / 255.0 green:160 / 255.0 blue:207 / 255.0  alpha:1.0];
        
    }
    cell.textLabel.text = self.tableViewData[indexPath.row];
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50.0;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self confirm];
}
#pragma mark BottomViewDelegate
-(void)bottomViewDidCancel:(BottomView *)bottomView{
    [self cancel];
}

-(void)bottomViewDidConfirm:(BottomView *)bottomView{
    [self confirm];
}

-(void)confirm{
    [UIView animateWithDuration:0.25 animations:^{
        self.view.alpha = 0.0;
        self.centerView.alpha = 0.5;
    } completion:^(BOOL finished) {
        
        if (self.completionSelected) {
            
            if (self.contentType == ALertContentTypeTableView) {
              self.completionSelected(@[self.tableViewData[[self.tableView indexPathForSelectedRow].row]],YES);
            }else{
                self.completionSelected(@[self.selectedDate],YES);
            }
            
        }
    }];

}
-(void)cancel{
    [UIView animateWithDuration:0.25 animations:^{
        self.view.alpha = 0.0;
        self.centerView.alpha = 0.5;
    } completion:^(BOOL finished) {
        
        if (self.completionSelected) {
            self.completionSelected([NSArray array],NO);
        }
    }];
}

-(void)birthdayChange:(UIDatePicker *)datePicker{
    self.selectedDate = datePicker.date;
}

-(NSString *)formatDate:(NSDate *)date{
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    [fmt setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    fmt.dateFormat = @"HH:mm";
    //datePicker.date 即为当前选中的时间
    NSString *time = [fmt stringFromDate:date];
    return time;
}
@end
