//
//  ForgetPassWordViewController.m
//  孕妙
//
//  Created by 是源科技 on 16/4/18.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "ForgetPassWordViewController.h"
#import "ResetPasswordViewController.h"

@interface ForgetPassWordViewController ()
@property (nonatomic,strong)UITextField *phoneTF;
@property (nonatomic,strong)UITextField *yanzhengmaTF;
@property (nonatomic,strong)UIButton *yanzhengmaBtn;
@property (nonatomic,strong)UIButton *nextButton;
@property (nonatomic,copy)NSString *yzmString;
@property (nonatomic,strong)NSTimer *yzmTimer;
@property (nonatomic,assign)int yzmTime;

@end

@implementation ForgetPassWordViewController

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.phoneTF resignFirstResponder];
    [self.yanzhengmaTF resignFirstResponder];
}

- (void)viewDidLoad {
    [super viewDidLoad];
   self.title = @"重置密码";
    self.view.backgroundColor = YMColor(245, 245, 245);
    
    CGFloat y = 100;
  
        UIView *view1 = [[UIView alloc]initWithFrame:CGRectMake(0, y, kDeviceWidth, kDeviceHeight / 8.0)];
        view1.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:view1];
        y += kDeviceHeight / 8.0 + 1.0;
    
        UIView *view2 = [[UIView alloc]initWithFrame:CGRectMake(0, y, kDeviceWidth, kDeviceHeight / 8.0)];
        view2.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:view2];
        for(int i = 0;i < 2;i ++)
        {
            CGFloat h = 100;
            UILabel *label2 = [[UILabel alloc]initWithFrame:CGRectMake(0, h + kDeviceHeight / 8.0 * (i + 1) + (i + 1), kDeviceWidth, 1.0)];
            label2.backgroundColor = YMColor(226, 226, 226);
            [self.view addSubview:label2];
            
        }
    y += kDeviceHeight / 8.0 + 1.0;
    
    self.nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.nextButton.frame = CGRectMake(kDeviceWidth / 3.0, y + 40, kDeviceWidth / 3.0, kDeviceWidth / 3.0 / 2.22);
    [self.nextButton setTitle:@"下一步" forState:UIControlStateNormal];
    [self.nextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.nextButton.layer.cornerRadius = 8.0;
    self.nextButton.backgroundColor = YMColor(246, 156, 202);
    [self.view addSubview:self.nextButton];
    
    CGFloat w = view1.frame.size.width;
    CGFloat h = view2.frame.size.height;
    UIImageView *logoImageView = [[UIImageView alloc]initWithFrame:CGRectMake(40, h / 3.0, h / 3.0, h / 3.0)];
    logoImageView.image = [UIImage imageNamed:@"Setting_phone"];
    [view1 addSubview:logoImageView];
    
    UIImageView *yanzhengmaImageView = [[UIImageView alloc]initWithFrame:CGRectMake(40, (h - h / 3.5) / 2.0 , h / 3.5, h / 3.5)];
    yanzhengmaImageView.image = [UIImage imageNamed:@"Setting_Verification code"];
    [view2 addSubview:yanzhengmaImageView];
    
    self.phoneTF = [[UITextField alloc]initWithFrame:CGRectMake(w / 4.0, 0, w / 4.0 * 3, h)];
    self.phoneTF.placeholder = @"请输入原手机号";
    [self.phoneTF setValue:YMColor(192, 192, 192) forKeyPath:@"_placeholderLabel.textColor"];
    self.phoneTF.keyboardType = UIKeyboardTypeDecimalPad;
    [view1 addSubview:self.phoneTF];
    
    self.yanzhengmaTF = [[UITextField alloc]initWithFrame:CGRectMake(w / 4.0, 0, w / 2.0, h)];
    self.yanzhengmaTF.placeholder = @"输入验证码";
    [self.yanzhengmaTF setValue:YMColor(192, 192, 192) forKeyPath:@"_placeholderLabel.textColor"];
    self.yanzhengmaTF.keyboardType = UIKeyboardTypeDecimalPad;
    [view2 addSubview:self.yanzhengmaTF];
    
    self.yanzhengmaBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.yanzhengmaBtn.frame = CGRectMake(w / 3.0 * 2, h / 4.0, h / 2.0 * 2.375, h / 2.0);
    self.yanzhengmaBtn.layer.borderWidth = 1.0;
    self.yanzhengmaBtn.layer.cornerRadius = 5.0;
    self.yanzhengmaBtn.layer.borderColor = [YMColor(246, 156, 202)CGColor];
    [self.yanzhengmaBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    self.yanzhengmaBtn.titleLabel.font = [UIFont systemFontOfSize:13.0];
    [self.yanzhengmaBtn setTitleColor:YMColor(246, 156, 202) forState:UIControlStateNormal];
    [view2 addSubview:self.yanzhengmaBtn];
    
    [self.yanzhengmaBtn addTarget:self action:@selector(HuoQuYanzhengma) forControlEvents:UIControlEventTouchUpInside];
    [self.nextButton addTarget:self action:@selector(clickNextButton) forControlEvents:UIControlEventTouchUpInside];
    
}
-(void)shengchengYanZhengMa
{
    self.yzmString = [[NSString alloc]init];
    
    for(int i = 0;i < 6; i ++)
    {
        int m = arc4random() % 10;
        self.yzmString = [self.yzmString stringByAppendingString:[NSString stringWithFormat:@"%d",m]];
    }
    
    YMLog(@"随机生成的6位数位:%@",self.yzmString);
}


-(void)HuoQuYanzhengma
{
    NSUInteger phoneLength = self.phoneTF.text.length;
    if(phoneLength == 11)
    {
        
        [self shengchengYanZhengMa];
        NSString *urlString = @"http://web.cr6868.com/asmx/smsservice.aspx";
        NSURL *url = [[NSURL alloc]initWithString:urlString];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:2];
        
        request.HTTPMethod = @"POST";
        NSString *str = [NSString stringWithFormat:@"name=13602624302&pwd=5E06798C36C5FA7CE3ACEA8DE233&content=验证码为%@，请在一分钟内输入正确的验证码&mobile=%@&stime=&sign=是源医学&type=pt&extno=",self.yzmString,self.phoneTF.text];
        
        NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:data];
        
        NSOperationQueue *queue = [NSOperationQueue mainQueue];
        
        [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
         {
             
             if (data)
             {
                 // 请求成功
                 NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
                 YMLog(@"dict = %@",dict);
                 [MBProgressHUD showSuccess:@"验证码发送成功!"];
                 
                 self.yzmTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(yamTimeDisplay) userInfo:nil repeats:YES];
                 self.yzmTime = 60;
                 self.yanzhengmaBtn.userInteractionEnabled = NO;
             }
         }];
    }
    else
    {
        [MBProgressHUD showError:@"请输入正确的手机号"];
    }

}

-(void)clickNextButton
{
    
    
    if([self.yanzhengmaTF.text isEqualToString:self.yzmString])
    {
        [self.yzmTimer invalidate];
        self.yzmTimer = nil;
        self.yanzhengmaBtn.backgroundColor = [UIColor clearColor];
        [self.yanzhengmaBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        self.yanzhengmaBtn.userInteractionEnabled = YES;
        [[NSUserDefaults standardUserDefaults]setObject:self.phoneTF.text forKey:@"oldPhoneNO"];
        ResetPasswordViewController *reset = [[ResetPasswordViewController alloc]init];
        [self.navigationController pushViewController:reset animated:YES];
        
    }
    else
    {
        [MBProgressHUD showError:@"验证码错误"];
    }
}



//验证码倒计时
-(void)yamTimeDisplay
{
    self.yzmTime --;
    if(self.yzmTime > 0)
    {
        self.yanzhengmaBtn.backgroundColor = [UIColor whiteColor];
        [self.yanzhengmaBtn setTitle:[NSString stringWithFormat:@"%ds",self.yzmTime] forState:UIControlStateNormal];
        [self.yanzhengmaBtn setTintColor:[UIColor whiteColor]];
        self.yanzhengmaBtn.userInteractionEnabled = NO;
    }
    else
    {
        [self.yanzhengmaBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        [self.yanzhengmaBtn setTitleColor:YMColor(249, 137, 196) forState:UIControlStateNormal];
        self.yanzhengmaBtn.layer.cornerRadius = 5.0;
        [self.yanzhengmaBtn setBackgroundColor:[UIColor clearColor]];
        self.yanzhengmaBtn.userInteractionEnabled = YES;
        [self.yzmTimer invalidate];
        self.yzmTimer = nil;
        self.yzmString = nil;
    }
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    YMLog(@"bdusi  %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"oldPhoneNO"]);
}


@end
