//
//  ResetPasswordViewController.m
//  孕妙
//
//  Created by 是源科技 on 16/4/18.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "ResetPasswordViewController.h"

@interface ResetPasswordViewController ()
@property (nonatomic,strong)UIButton *completeButton;
@property (nonatomic,strong)UITextField *NewPWTF;
@property (nonatomic,strong)UITextField *SureNewPWTF;


@end

@implementation ResetPasswordViewController

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.NewPWTF resignFirstResponder];
    [self.SureNewPWTF resignFirstResponder];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"重置密码";
    self.view.backgroundColor = YMColor(245, 245, 245);
    
    CGFloat y = 100;
    
    UIView *view1 = [[UIView alloc]initWithFrame:CGRectMake(0, y, kDeviceWidth, kDeviceHeight / 8.0)];
    view1.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:view1];
    y += kDeviceHeight / 8.0 + 1.0;
    
    UIView *view2 = [[UIView alloc]initWithFrame:CGRectMake(0, y, kDeviceWidth, kDeviceHeight / 8.0)];
    view2.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:view2];
    
    for(int i = 0;i < 2;i ++)
    {
        CGFloat h = 100;
        UILabel *label2 = [[UILabel alloc]initWithFrame:CGRectMake(0, h + kDeviceHeight / 8.0 * (i + 1) + (i + 1), kDeviceWidth, 1.0)];
        label2.backgroundColor = YMColor(226, 226, 226);
        [self.view addSubview:label2];
        
    }
    CGFloat w = view1.frame.size.width;
    CGFloat h = view1.frame.size.height;
    UIImageView *logoImageView = [[UIImageView alloc]initWithFrame:CGRectMake(40, h / 3.0, h / 3.0, h / 3.0)];
    logoImageView.image = [UIImage imageNamed:@"Setting_pw"];
    [view1 addSubview:logoImageView];
    UIImageView *logoImageView1 = [[UIImageView alloc]initWithFrame:CGRectMake(40, h / 3.0, h / 3.0, h / 3.0)];
    logoImageView1.image = [UIImage imageNamed:@"Setting_pw"];
    [view2 addSubview:logoImageView1];
    
    self.NewPWTF = [[UITextField alloc]initWithFrame:CGRectMake(w / 4.0, 0, w / 4.0 * 3, h)];
    self.NewPWTF.placeholder = @"请输入新密码";
    [self.NewPWTF setValue:YMColor(192, 192, 192) forKeyPath:@"_placeholderLabel.textColor"];
    [view1 addSubview:self.NewPWTF];
    
    self.SureNewPWTF = [[UITextField alloc]initWithFrame:CGRectMake(w / 4.0, 0, w / 4.0 * 3, h)];
    self.SureNewPWTF.placeholder = @"请再次输入密码";
    [self.SureNewPWTF setValue:YMColor(192, 192, 192) forKeyPath:@"_placeholderLabel.textColor"];
    [view2 addSubview:self.SureNewPWTF];
    
    self.NewPWTF.secureTextEntry = YES;
    self.SureNewPWTF.secureTextEntry = YES;
    
    y += kDeviceHeight / 8.0 + 1.0;
    
    self.completeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.completeButton.frame = CGRectMake(kDeviceWidth / 3.0, y + 40, kDeviceWidth / 3.0, kDeviceWidth / 3.0 / 2.22);
    [self.completeButton setTitle:@"完 成" forState:UIControlStateNormal];
    [self.completeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.completeButton.layer.cornerRadius = 8.0;
    self.completeButton.backgroundColor = YMColor(246, 156, 202);
    [self.view addSubview:self.completeButton];
    
    [self.completeButton addTarget:self action:@selector(clickCompleteButton) forControlEvents:UIControlEventTouchUpInside];
    
    YMLog(@"shouhihao  %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"oldPhoneNO"]);
  
}

-(void)clickCompleteButton
{
    NSString *account = [[NSUserDefaults standardUserDefaults]objectForKey:@"oldPhoneNO"];
    NSString *string1 = self.NewPWTF.text;
    NSString *string2 = self.SureNewPWTF.text;
    
    [MBProgressHUD showMessage:@"拼命加载中，请稍后..."];
    if(string1.length != 0 && string2.length != 0)
    {
        if([string1 isEqualToString:string2])
        {
            NSString *urlString = @"http://120.24.178.16:8080/Health/baby/renewPass.do";
            NSURL *url = [[NSURL alloc]initWithString:urlString];
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:2];
            
            request.HTTPMethod = @"POST";
            NSString *str = [NSString stringWithFormat:@"account=%@&answer=0&newPass=%@",account,string2];
            
            NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
            [request setHTTPBody:data];
            
            NSOperationQueue *queue = [NSOperationQueue mainQueue];
            
            [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
             {
                 
                 if (data)
                 {
                     [MBProgressHUD hideHUD];
                     // 请求成功
                     NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
                     YMLog(@"dict = %@",dict);
                     if([[dict objectForKey:@"code"] isEqualToString:@"0"])
                     {
                         [MBProgressHUD showSuccess:@"密码修改成功"];
//                         [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"userAccount"];
                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                             [self.navigationController popToRootViewControllerAnimated:YES];
                         });
                     }
                     if([[dict objectForKey:@"code"] isEqualToString:@"2"])
                     {
                         [MBProgressHUD showSuccess:@"密码长度不能小于6位"];
                     }
                     if([[dict objectForKey:@"code"] isEqualToString:@"3"])
                     {
                         [MBProgressHUD showSuccess:@"账户不存在"];
                     }

                     
                 }
             }];
        }
        else
        {
            [MBProgressHUD showError:@"密码不一致"];
        }
    }
    else
    {
        [MBProgressHUD showError:@"密码长度不一致"];
    }
}




@end
