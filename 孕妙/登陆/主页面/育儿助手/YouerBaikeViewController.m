//
//  YouerBaikeViewController.m
//  孕妙
//
//  Created by 是源科技 on 16/4/21.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "YouerBaikeViewController.h"

@interface YouerBaikeViewController ()

@end

@implementation YouerBaikeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"1-3岁百科";
    self.view.backgroundColor = YMColor(245, 245, 245);
    
    CGFloat y = 75;
    for(int i = 1;i < 6;i ++)
    {
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, y, kDeviceWidth, 60)];
        view.tag = i;
        view.backgroundColor = YMColor(254, 233, 244);
        [self.view addSubview:view];
        
        y += view.frame.size.height + 2;
        if(view.tag == 1)
        {
            [self addTitleNameToView:view andTitleName:@"幼儿喂养"];
        }
        if(view.tag == 2)
        {
            [self addTitleNameToView:view andTitleName:@"幼儿护理"];
        }
        if(view.tag == 3)
        {
            [self addTitleNameToView:view andTitleName:@"幼儿疾病"];
        }
        if(view.tag == 4)
        {
            [self addTitleNameToView:view andTitleName:@"生长发育"];
        }
        if(view.tag == 5)
        {
            [self addTitleNameToView:view andTitleName:@"安全防护"];
        }
        
    }
}

-(void)addTitleNameToView:(UIView *)view andTitleName:(NSString *)titleName
{
    CGFloat w = view.frame.size.width;
    CGFloat h = view.frame.size.height;
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(25, 0, w / 2.0, h)];
    label.text = titleName;
    label.textColor = YMColor(195, 97, 147);
    [view addSubview:label];
    
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(w - h / 4.0 - 30, h / 8.0 * 3, h / 4.0, h / 4.0)];
    imageView.image = [UIImage imageNamed:@"assistant_》small"];
    [view addSubview:imageView];
    
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, w, h)];
    btn.tag = view.tag;
    [view addSubview:btn];
    
}

@end
