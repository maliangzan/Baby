//
//  YimiaoguanliViewController.m
//  孕妙
//
//  Created by 是源科技 on 16/4/21.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "YimiaoguanliViewController.h"
#import "YimiaoInformationViewController.h"

@interface YimiaoguanliViewController ()<UIScrollViewDelegate>
@property (nonatomic,strong)UILabel *completetLabel;
@property (nonatomic,weak)UIScrollView *scrollView;
@property (nonatomic,copy)NSString *jianjieString;

@property (nonatomic,weak)UIButton *babyBirthdayDateBtn;//选择宝宝出生日期按钮
@property (nonatomic,weak)UIButton *jisuanButton;//计算按钮

@property (nonatomic,assign)int count;

@property (nonatomic,strong)SettingAlertController *riqiAlert;



@end


@implementation YimiaoguanliViewController

-(NSString *)jianjieString
{
    if(!_jianjieString)
    {
        _jianjieString = @"        婴幼儿刚出生自身免疫力低下，为了增强婴幼儿自身免疫抵抗能力疫苗的接种是必要而且必须的，但是宝宝要打哪些疫苗？可以预防哪些疾病？是否总是担心错过了宝宝疫苗接种的最佳时机？疫苗接种时间表会告诉您！";
    }
    return _jianjieString;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"疫苗管理";
    self.view.backgroundColor = YMColor(245, 245, 245);
    
    UIScrollView *scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, kDeviceHeight)];
    scrollView.delegate = self;
    self.scrollView = scrollView;
    [self.view addSubview:scrollView];
    
    [self labelInit];
}

-(void)labelInit
{
    self.DateLabel1 = [[UILabel alloc]init];
    self.DateLabel2 = [[UILabel alloc]init];
    self.DateLabel3 = [[UILabel alloc]init];
    self.DateLabel4 = [[UILabel alloc]init];
    self.DateLabel5 = [[UILabel alloc]init];
    self.DateLabel6 = [[UILabel alloc]init];
    self.DateLabel7 = [[UILabel alloc]init];
    self.DateLabel8 = [[UILabel alloc]init];
    self.DateLabel9 = [[UILabel alloc]init];
    self.DateLabel10 = [[UILabel alloc]init];
    self.DateLabel11 = [[UILabel alloc]init];
    self.DateLabel12 = [[UILabel alloc]init];
    self.DateLabel13 = [[UILabel alloc]init];
    self.DateLabel14 = [[UILabel alloc]init];
    self.DateLabel15 = [[UILabel alloc]init];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self yimiaoGuanli];
}

-(void)yimiaoGuanli
{
    CGFloat y = 15;
    UIView *jianjieView = [[UIView alloc]initWithFrame:CGRectMake(0, y, kDeviceWidth, kDeviceHeight)];
    jianjieView.backgroundColor = [UIColor whiteColor];
    [self.scrollView addSubview:jianjieView];
    
    UILabel *qingduLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
    qingduLabel.text = self.jianjieString;
    qingduLabel.textAlignment = NSTextAlignmentLeft;
    qingduLabel.font = [UIFont systemFontOfSize:15.0];
    NSDictionary * attribute2 = @{NSFontAttributeName:[UIFont systemFontOfSize:15.0]};
    CGSize size2 = [qingduLabel.text boundingRectWithSize:CGSizeMake(kDeviceWidth - 20, kDeviceHeight) options:NSStringDrawingUsesLineFragmentOrigin attributes:attribute2 context:nil].size;
    qingduLabel.lineBreakMode = NSLineBreakByWordWrapping;
    qingduLabel.numberOfLines = 0;
    qingduLabel.frame = CGRectMake(10, 10, kDeviceWidth - 20, size2.height);
    qingduLabel.textColor = YMColor(139, 139, 139);
    [jianjieView addSubview:qingduLabel];
    
    jianjieView.frame = CGRectMake(0, y, kDeviceWidth, size2.height + 20);
    y += jianjieView.frame.size.height;
    
    UIView *babyView = [[UIView alloc]initWithFrame:CGRectMake(0, y, kDeviceWidth, 50)];
    [self.scrollView addSubview:babyView];
    y += babyView.frame.size.height;
    
    UILabel *babyBirthdayLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, kDeviceWidth / 3.0, babyView.frame.size.height)];
    babyBirthdayLabel.textColor = YMColor(189, 71, 136);
    babyBirthdayLabel.text = @"宝宝出生日期";
    babyBirthdayLabel.font = [UIFont systemFontOfSize:16.0];
    [babyView addSubview:babyBirthdayLabel];
    
    UIButton *dateButton = [[UIButton alloc]initWithFrame:CGRectMake(kDeviceWidth / 3.0 , 0, kDeviceWidth / 3.0 + 20, babyView.frame.size.height)];
    [dateButton setTitle:[self getSystemTime] forState:UIControlStateNormal];
    [dateButton setTitleColor:YMColor(148, 148, 148) forState:UIControlStateNormal];
    self.babyBirthdayDateBtn = dateButton;
    dateButton.titleLabel.font = [UIFont systemFontOfSize:16.0];
    [babyView addSubview:dateButton];
    
    [self.babyBirthdayDateBtn addTarget:self action:@selector(selectBabyBirthdayDate) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *jiantouImage = [[UIImageView alloc]initWithFrame:CGRectMake(kDeviceWidth / 3.0 * 2 + 25, babyView.frame.size.height / 3.0, babyView.frame.size.height / 3.0, babyView.frame.size.height / 3.0)];
    jiantouImage.image = [UIImage imageNamed:@"assistant_vaccine》》"];
    [babyView addSubview:jiantouImage];
    
    UIButton *jisuanbutton = [[UIButton alloc]initWithFrame:CGRectMake(kDeviceWidth / 3.0 * 2 + 30 + babyView.frame.size.height / 3.0 + (kDeviceWidth - kDeviceWidth / 3.0 * 2 - 30 - babyView.frame.size.height / 3.0 - babyView.frame.size.height / 8.0 * 10) / 2.0, babyView.frame.size.height / 8.0 * 1.5, babyView.frame.size.height / 8.0 * 10 - 3, babyView.frame.size.height / 8.0 * 5)];
    [jisuanbutton setTitle:@"计  算" forState:UIControlStateNormal];
    [jisuanbutton setTitleColor:YMColor(250, 179, 214) forState:UIControlStateNormal];
    jisuanbutton.layer.cornerRadius = 5.0;
    jisuanbutton.layer.borderColor = [YMColor(250, 179, 214)CGColor];
    jisuanbutton.layer.borderWidth = 1.0;
    self.jisuanButton = jisuanbutton;
    [babyView addSubview:jisuanbutton];
    [jisuanbutton addTarget:self action:@selector(clickJiSuanButton) forControlEvents:UIControlEventTouchUpInside];
    
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"data" ofType:@"plist"];
    NSDictionary *plistDic = [[NSDictionary alloc]initWithContentsOfFile:plistPath];
    NSArray *titleArray = [plistDic objectForKey:@"yimiaoTitle"];
    NSArray *introArray = [plistDic objectForKey:@"yimiaoIntro"];
    int tag = 1;
    for(int i = 0;i < 15;i ++)
    {
        UIView *faView = [[UIView alloc]initWithFrame:CGRectMake(0, y + i * 5 , kDeviceWidth, 155)];
        [self.scrollView addSubview:faView];
        faView.tag = i + 1;
        //日期和宝宝年龄
        UIView *titleView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, 50)];
        titleView.backgroundColor = [UIColor whiteColor];
        titleView.tag = i + 101;
        [faView addSubview:titleView];
        
        switch (titleView.tag)
        {
            case 101:
                [self addDate:self.DateLabel1 andbabyAge:@"出生后24小时内" ToView:titleView];
                break;
            case 102:
                [self addDate:self.DateLabel2 andbabyAge:@"1月龄" ToView:titleView];
                break;
            case 103:
                [self addDate:self.DateLabel3 andbabyAge:@"2月龄" ToView:titleView];
                break;
            case 104:
                [self addDate:self.DateLabel4 andbabyAge:@"3月龄" ToView:titleView];
                break;
            case 105:
                [self addDate:self.DateLabel5 andbabyAge:@"4月龄" ToView:titleView];
                break;
            case 106:
                [self addDate:self.DateLabel6 andbabyAge:@"5月龄" ToView:titleView];
                break;
            case 107:
                [self addDate:self.DateLabel7 andbabyAge:@"6月龄" ToView:titleView];
                break;
            case 108:
                [self addDate:self.DateLabel8 andbabyAge:@"8月龄" ToView:titleView];
                break;
            case 109:
                [self addDate:self.DateLabel9 andbabyAge:@"9月龄" ToView:titleView];
                break;
            case 110:
                [self addDate:self.DateLabel9 andbabyAge:@"1岁" ToView:titleView];
                break;
            case 111:
                [self addDate:self.DateLabel10 andbabyAge:@"1岁半" ToView:titleView];
                break;
            case 112:
                [self addDate:self.DateLabel11 andbabyAge:@"2岁" ToView:titleView];
                break;
            case 113:
                [self addDate:self.DateLabel12 andbabyAge:@"3岁" ToView:titleView];
                break;
            case 114:
                [self addDate:self.DateLabel13 andbabyAge:@"4岁" ToView:titleView];
                break;
            case 115:
                [self addDate:self.DateLabel14 andbabyAge:@"6岁" ToView:titleView];
                break;
            default:
                break;
        }
        
        if(i == 0)
        {
            self.count = 2;
        }
        else if (i == 3)
        {
            self.count = 2;
        }
        else if (i == 4)
        {
            self.count = 2;
        }
        else if (i == 6)
        {
            self.count = 2;
        }
        else if (i == 10)
        {
            self.count = 3;
        }
        else if (i == 11)
        {
            self.count = 2;
        }
        else if (i == 14)
        {
            self.count = 3;
        }
        else
        {
            self.count = 1;
        }
        
        for(int j = 0;j < self.count;j ++)
        {
        faView.frame = CGRectMake(0, y + i * 5, kDeviceWidth, 55 + (j + 1) * 100 + j);
        AllKindsOfView *neirongView = [[AllKindsOfView alloc]initWithFrame:CGRectMake(0,j * 101 + 55, kDeviceWidth, 100)];
        neirongView.backgroundColor = [UIColor whiteColor];
        [faView addSubview:neirongView];
        neirongView.tag = tag ++;
        
        [neirongView yimiaoGuanliAddTitleLabel:titleArray[neirongView.tag - 1] andmiaoshuLabel:introArray[neirongView.tag - 1]];
        UILabel *crossLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 55 + (j + 1) * 100 + j, kDeviceWidth, 1)];
        crossLabel.backgroundColor = YMColor(224, 224, 224);
        [faView addSubview:crossLabel];
             [neirongView.yimiaoBtn addTarget:self action:@selector(clickYiMiaoButton:) forControlEvents:UIControlEventTouchUpInside];
        }
        y += faView.frame.size.height;
    }
    self.scrollView.contentSize = CGSizeMake(kDeviceWidth, y + 66);
}

-(void)addDate:(UILabel *)dateLabel andbabyAge:(NSString *)babyAge ToView:(UIView *)view
{
    CGFloat w = view.frame.size.width;
    CGFloat h = view.frame.size.height;
    dateLabel.frame = CGRectMake(10, 0, w / 2.0, h);
    dateLabel.textColor = YMColor(195, 92, 147);
    dateLabel.font = [UIFont systemFontOfSize:16.0];
    [view addSubview:dateLabel];
    
    UILabel *babyAgeLabel = [[UILabel alloc]initWithFrame:CGRectMake(w / 2.0, 0, w / 2.0 - 20, h)];
    babyAgeLabel.textAlignment = NSTextAlignmentRight;
    babyAgeLabel.font = [UIFont systemFontOfSize:16.0];
    babyAgeLabel.textColor = YMColor(158, 158, 158);
    babyAgeLabel.text = babyAge;
    [view addSubview:babyAgeLabel];
    
                    
}
//选择宝宝的出生日期
-(void)selectBabyBirthdayDate
{
    self.riqiAlert = [[SettingAlertController alloc] init];
    self.riqiAlert.contentType = ALertContentTypePickerView;
    self.riqiAlert.pickerView.datePickerMode = UIDatePickerModeDate;
    [self.view addSubview:self.riqiAlert.view];
    [self addChildViewController:self.riqiAlert];
    self.riqiAlert.alertTitle = @"宝宝出生日期";
    typeof (self) weakSelf = self;
    self.riqiAlert.contentType = ALertContentTypePickerView;
    self.riqiAlert.pickerView.datePickerMode = UIDatePickerModeDate;
    self.riqiAlert.completionSelected = ^(NSArray * results,BOOL confirm){
        if (confirm) {
            NSDate *time = (NSDate *)results.firstObject;
            [weakSelf.babyBirthdayDateBtn setTitle:[weakSelf toFormatTime:time] forState:UIControlStateNormal];
        }
    };
}
//点击了计算按钮
-(void)clickJiSuanButton
{
    NSString *dateString = self.babyBirthdayDateBtn.titleLabel.text;
    NSRange yearRang = {0,4};
    NSRange monthRang = {5,2};
    NSRange dayRang = {8,2};
    int year = [[dateString substringWithRange:yearRang]intValue];
    int month = [[dateString substringWithRange:monthRang]intValue];
    int day = [[dateString substringWithRange:dayRang]intValue];
    [self kindOfLabelDisplayYear:year andMonth:month andDay:day];
    
}

//每个titlelabel日期显示
-(void)kindOfLabelDisplayYear:(int)year andMonth:(int)month andDay:(int)day
{
    self.DateLabel1.text = [NSString stringWithFormat:@"%d年%02d月%02d日",year,month,day];
    if(month + 1 > 12)
    {
        int mm = (month + 1) % 12;
        self.DateLabel2.text = [NSString stringWithFormat:@"%d年%02d月%02d日",year + 1,mm,day];
    }
    else
    {
        self.DateLabel2.text = [NSString stringWithFormat:@"%d年%02d月%02d日",year,month + 1,day];
    }
    if(month + 2 > 12)
    {
        int mm = (month + 2) % 12;
        self.DateLabel3.text = [NSString stringWithFormat:@"%d年%02d月%02d日",year + 1,mm,day];
    }
    else
    {
        self.DateLabel3.text = [NSString stringWithFormat:@"%d年%02d月%02d日",year,month + 2,day];
    }
    if(month + 3 > 12)
    {
        int mm = (month + 3) % 12;
        self.DateLabel4.text = [NSString stringWithFormat:@"%d年%02d月%02d日",year + 1,mm,day];
    }
    else
    {
        self.DateLabel4.text = [NSString stringWithFormat:@"%d年%02d月%02d日",year,month + 3,day];
    }
    if(month + 4 > 12)
    {
        int mm = (month + 4) % 12;
        self.DateLabel5.text = [NSString stringWithFormat:@"%d年%02d月%02d日",year + 1,mm,day];
    }
    else
    {
        self.DateLabel5.text = [NSString stringWithFormat:@"%d年%02d月%02d日",year,month + 4,day];
    }
    if(month + 5 > 12)
    {
        int mm = (month + 5) % 12;
        self.DateLabel6.text = [NSString stringWithFormat:@"%d年%02d月%02d日",year + 1,mm,day];
    }
    else
    {
        self.DateLabel6.text = [NSString stringWithFormat:@"%d年%02d月%02d日",year,month + 5,day];
    }
    if(month + 6 > 12)
    {
        int mm = (month + 6) % 12;
        self.DateLabel7.text = [NSString stringWithFormat:@"%d年%02d月%02d日",year + 1,mm,day];
    }
    else
    {
        self.DateLabel7.text = [NSString stringWithFormat:@"%d年%02d月%02d日",year,month + 6,day];
    }
    if(month + 8 > 12)
    {
        int mm = (month + 8) % 12;
        self.DateLabel8.text = [NSString stringWithFormat:@"%d年%02d月%02d日",year + 1,mm,day];
    }
    else
    {
        self.DateLabel8.text = [NSString stringWithFormat:@"%d年%02d月%02d日",year,month + 8,day];
    }
    
    if(month + 9 > 12)
    {
        int mm = (month + 9) % 12;
        self.DateLabel9.text = [NSString stringWithFormat:@"%d年%02d月%02d日",year + 1,mm,day];
    }
    else
    {
        self.DateLabel9.text = [NSString stringWithFormat:@"%d年%02d月%02d日",year,month + 9,day];
    }
    self.DateLabel10.text = [NSString stringWithFormat:@"%d年%02d月%02d日",year + 1,month,day];
    if(month + 6 > 12)
    {
        int mm = (month + 6) % 12;
        self.DateLabel11.text = [NSString stringWithFormat:@"%d年%02d月%02d日",year + 2,mm,day];
    }
    else
    {
        self.DateLabel11.text = [NSString stringWithFormat:@"%d年%02d月%02d日",year + 1,month + 6,day];
    }
    self.DateLabel12.text = [NSString stringWithFormat:@"%d年%02d月%02d日",year + 2,month,day];
    self.DateLabel13.text = [NSString stringWithFormat:@"%d年%02d月%02d日",year + 3,month,day];
    self.DateLabel14.text = [NSString stringWithFormat:@"%d年%02d月%02d日",year + 4,month,day];
    self.DateLabel15.text = [NSString stringWithFormat:@"%d年%02d月%02d日",year + 6,month,day];
}

-(void)clickYiMiaoButton:(UIButton *)btn
{
    YimiaoInformationViewController *yimiao = [[YimiaoInformationViewController alloc]init];
    [self.navigationController pushViewController:yimiao animated:YES];
    yimiao.tagCount = (int)btn.tag - 1;
}
//获取系统时间
-(NSString *)getSystemTime
{
    NSDate *senddate=[NSDate date];
    NSDateFormatter  *dateformatter=[[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"yyyy年MM月dd日"];
    NSString *locationString=[dateformatter stringFromDate:senddate];
    return locationString;
}
//时间转字符串
-(NSString *)toFormatTime:(NSDate *)time
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy年MM月dd日"];
    NSString *strDate = [dateFormatter stringFromDate:time];
    return strDate;
}


@end
