//
//  YimiaoInformationViewController.m
//  孕妙
//
//  Created by 是源科技 on 16/5/31.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "YimiaoInformationViewController.h"

@interface YimiaoInformationViewController ()<UIScrollViewDelegate>
@property (nonatomic,strong)UIScrollView *scrollView;
@property (nonatomic,strong)UILabel *dateLabel;
@property (nonatomic,assign)float y;

@property (nonatomic,strong)SettingAlertController *riqiAlert;


@end

@implementation YimiaoInformationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = YMColor(245, 245, 245);
    //设置右按钮
    UIButton *saveButton = [UIButton buttonWithType:UIButtonTypeCustom];
    saveButton.frame =  CGRectMake(0, 0, 50.0f, 30.0f);
    [saveButton setTitle:@"保存" forState:normal];
    [saveButton setTitleColor:YMColor(255, 218, 245) forState:UIControlStateNormal];
    UIBarButtonItem *saveButtonItem = [[UIBarButtonItem alloc] initWithCustomView:saveButton];
    self.navigationItem.rightBarButtonItem = saveButtonItem;
    [saveButton addTarget:self action:@selector(saveInforMation) forControlEvents:UIControlEventTouchUpInside];
    
    [self everyYiMiaoInformation];
}

-(void)everyYiMiaoInformation
{
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"data" ofType:@"plist"];
    NSDictionary *plistDic = [[NSDictionary alloc]initWithContentsOfFile:plistPath];
    NSArray *titleArray = [plistDic objectForKey:@"yimiaoTitle"]; //疫苗名称
    NSArray *jibingArray = [plistDic objectForKey:@"yufangjibing"]; //预防疾病
    NSArray *buweiArray = [plistDic objectForKey:@"yimiaoBuwei"]; //接种部位
    NSArray *cishuArray = [plistDic objectForKey:@"jiezhongcishu"]; //接种次数
    NSArray *jianjieArray = [plistDic objectForKey:@"YMjianjie"]; //简介
    NSArray *fanyingArray = [plistDic objectForKey:@"YMfanying"]; //接种反应
    NSArray *jinjiArray = [plistDic objectForKey:@"YMjinji"]; //禁忌
    
    
    self.title = titleArray[self.tagCount];
    
    UIScrollView *scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, kDeviceHeight)];
    scrollView.delegate = self;
    self.scrollView = scrollView;
    [self.view addSubview:scrollView];
    float y = 10;
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, y, kDeviceWidth, 180)];
    view.backgroundColor = [UIColor whiteColor];
    [self.scrollView addSubview:view];
    
    CGFloat w = view.frame.size.width;
    CGFloat h = view.frame.size.height;
    
    UILabel *shuLabel = [[UILabel alloc]initWithFrame:CGRectMake(w / 3.0,0, 1,h)];
    shuLabel.backgroundColor = YMColor(212, 212, 212);
    [view addSubview:shuLabel];
    
    for(int i = 0;i < 5;i ++)
    {
        UILabel *titleLbael = [[UILabel alloc]initWithFrame:CGRectMake(10, i * 35 + i, w / 3.0 - 10, 35)];
        titleLbael.tag = i + 1;
        [view addSubview:titleLbael];
        titleLbael.font = [UIFont systemFontOfSize:15.0];
        titleLbael.textColor = YMColor(192, 92, 143);
        switch (titleLbael.tag) {
            case 1:
                titleLbael.text = @"疫苗管理";
                break;
            case 2:
                titleLbael.text = @"预防疾病";
                break;
            case 3:
                titleLbael.text = @"接种方法/部位";
                break;
            case 4:
                titleLbael.text = @"接种次数";
                break;
            case 5:
                titleLbael.text = @"接种时间";
                break;
                
            default:
                break;
        }
        
        UILabel *crossLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, (i + 1) * 35 + i, kDeviceWidth, 1)];
        crossLabel.backgroundColor = YMColor(221, 221, 221);
        [view addSubview:crossLabel];
        
        UILabel *neirongLaebl = [[UILabel alloc]initWithFrame:CGRectMake(w / 3.0 + 11, i * 35 + i, w / 3.0 * 2 - 12, 35)];
        [view addSubview:neirongLaebl];
        neirongLaebl.font = [UIFont systemFontOfSize:15.0];
        neirongLaebl.tag = i + 10;
        neirongLaebl.textColor = YMColor(158, 158, 158);
        switch (neirongLaebl.tag) {
            case 10:
                neirongLaebl.text = titleArray[self.tagCount];
                break;
            case 11:
                neirongLaebl.text = jibingArray[self.tagCount];
                break;
            case 12:
                neirongLaebl.text = buweiArray[self.tagCount];
                neirongLaebl.adjustsFontSizeToFitWidth = YES;
                break;
            case 13:
                neirongLaebl.text = cishuArray[self.tagCount];
                break;
            case 14:
            {
                neirongLaebl.text = @"请选择日期";
                self.dateLabel = neirongLaebl;
                float pianyi = 0;
                if(SYDevice5 || SYDevice4s)
                {
                    pianyi = 15;
                }
                else
                {
                    pianyi = 25;
                }
                UIImageView *jiantouImageView =[[UIImageView alloc]initWithFrame:CGRectMake(w - w / 3.0 - pianyi, h - 26.25, 17.5, 17.5)];
                jiantouImageView.image = [UIImage imageNamed:@"assistant_》gray"];
                [view addSubview:jiantouImageView];
                
                UIButton *selectDateBtn = [[UIButton alloc]initWithFrame:CGRectMake(w / 3.0, h - 35, w / 3.0 + 20, 35)];
                [selectDateBtn addTarget:self action:@selector(clickSelectButton) forControlEvents:UIControlEventTouchUpInside];
                [view addSubview:selectDateBtn];
            }
                break;
                
            default:
                break;
        }
    }
    y += view.frame.size.height + 5;
    self.y = y;
    
    [self everyMokuaiLayoutandTitleString:@"简介" andJianjieString:jianjieArray[self.tagCount]];
    [self everyMokuaiLayoutandTitleString:@"接种反应" andJianjieString:fanyingArray[self.tagCount]];
    [self everyMokuaiLayoutandTitleString:@"禁忌" andJianjieString:jinjiArray[self.tagCount]];
    self.scrollView.contentSize = CGSizeMake(kDeviceWidth, self.y);
}

-(void)everyMokuaiLayoutandTitleString:(NSString *)titleString andJianjieString:(NSString *)jianjieString
{
    UIView *TitleView = [[UIView alloc]initWithFrame:CGRectMake(0, self.y, kDeviceWidth, 40)];
    TitleView.backgroundColor = [UIColor whiteColor];
    [self.scrollView addSubview:TitleView];
    
    UILabel *TitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, kDeviceWidth - 20, TitleView.frame.size.height)];
    TitleLabel.textColor = YMColor(192, 92, 143);
    TitleLabel.text = titleString;
    TitleLabel.font = [UIFont systemFontOfSize:16.0];
    [TitleView addSubview:TitleLabel];
    self.y += TitleView.frame.size.height;
    
    UILabel *titleCross = [[UILabel alloc]initWithFrame:CGRectMake(0, self.y, kDeviceWidth, 1)];
    titleCross.backgroundColor = YMColor(221, 221, 221);
    [self.scrollView addSubview:titleCross];
    self.y += 1;
    
    UIView *neirongView = [[UIView alloc]initWithFrame:CGRectMake(0, self.y, kDeviceWidth, 100)];
    neirongView.backgroundColor = [UIColor whiteColor];
    [self.scrollView addSubview:neirongView];
    
    [self jisuanLabelHeightandString:jianjieString addToView:neirongView andheight:self.y];
    self.y += neirongView.frame.size.height ;
    
    UILabel *croLabel2 = [[UILabel alloc]initWithFrame:CGRectMake(0, self.y, kDeviceWidth, 1)];
    croLabel2.backgroundColor = YMColor(221, 221, 221);
    [self.scrollView addSubview:croLabel2];
    self.y += 6;
}

//计算label高度
-(void)jisuanLabelHeightandString:(NSString *)string addToView:(UIView *)view andheight:(float)y
{
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
    [view addSubview:label];
    NSString *s2 = string;
    label.text = s2;
    label.textAlignment = NSTextAlignmentLeft;
    label.font = [UIFont systemFontOfSize:15.0];
    
    NSDictionary * attribute2 = @{NSFontAttributeName:[UIFont systemFontOfSize:15.0]};
    CGSize size2 = [s2 boundingRectWithSize:CGSizeMake(kDeviceWidth - 20, kDeviceHeight * 5) options:NSStringDrawingUsesLineFragmentOrigin attributes:attribute2 context:nil].size;
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.numberOfLines = 0;
    label.frame = CGRectMake(10, 4, kDeviceWidth - 20, size2.height);
    label.textColor = YMColor(158, 158, 158);
    
    view.frame = CGRectMake(0, y, kDeviceWidth, size2.height + 8);
    
    
}


//选择疫苗接种时间
-(void)clickSelectButton
{
    self.riqiAlert = [[SettingAlertController alloc] init];
    self.riqiAlert.contentType = ALertContentTypePickerView;
    self.riqiAlert.pickerView.datePickerMode = UIDatePickerModeDate;
    [self.view addSubview:self.riqiAlert.view];
    [self addChildViewController:self.riqiAlert];
    self.riqiAlert.alertTitle = @"宝宝疫苗接种时间";
    typeof (self) weakSelf = self;
    self.riqiAlert.contentType = ALertContentTypePickerView;
    self.riqiAlert.pickerView.datePickerMode = UIDatePickerModeDate;
    self.riqiAlert.completionSelected = ^(NSArray * results,BOOL confirm){
        if (confirm) {
            NSDate *time = (NSDate *)results.firstObject;
            weakSelf.dateLabel.text = [weakSelf toFormatTime:time];
        }
    };

}

//点击保存按钮
-(void)saveInforMation
{
    YMLog(@"点击了保存按钮");
}

//时间转字符串
-(NSString *)toFormatTime:(NSDate *)time
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *strDate = [dateFormatter stringFromDate:time];
    return strDate;
}


@end
