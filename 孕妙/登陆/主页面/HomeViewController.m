//
//  HomeViewController.m
//  孕妙
//
//  Created by 是源科技 on 16/4/13.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "HomeViewController.h"
#import "PersonCenterViewController.h"
#import "AttentionManagerViewController.h"
#import "PerfectInformationViewController.h"
#import "BasicViewController.h"
#import "ChanjianJiluViewController.h"
#import "YBHealthControllerNew.h"
#import "foodHomeViewController.h"
#import "passValue.h"


@interface HomeViewController ()<UIScrollViewDelegate>
@property (nonatomic,weak)UIButton *centerButton;
@property (nonatomic,weak)UIButton *erweimaButton;
@property (nonatomic,assign)BOOL erweimaFlag;
@property (nonatomic,weak)UIView *erweimaView;
@property (strong, nonatomic)UILabel *yuchanqiInfoLabel;
@property (nonatomic,strong)UIImageView *babyGrowImageView;

@property (nonatomic,strong)UIButton *dateLeftBtn;
@property (nonatomic,strong)UIButton *dateRightBtn;
@property (nonatomic,strong)UILabel *dateLabel;

@property (nonatomic,strong)UIScrollView *allButtonScrollView;
@property (nonatomic,assign)int i;
@property (nonatomic,strong)UIButton *starButton;

//宝宝详情
@property (nonatomic,strong)UILabel *babyMotherHealthInfor;
@property (nonatomic,strong)UILabel *babyMotherHealthState;
@property (nonatomic,strong)UILabel *babyWeightAndHeight;
@property (nonatomic,strong)UILabel *babyInformationIntroLabel;

//今日关注以及每周必读
@property (nonatomic,strong)UILabel *displaySelectLabel;//今日关注完成情况
@property (nonatomic,strong)UIScrollView *totalScrollView;
@property (nonatomic,strong)UILabel *chanjianWaringLabel;
@property (nonatomic,strong)UILabel *jiankangJianceLabel;
@property (nonatomic,strong)UILabel *shanshiGuanliLabel;
@property (nonatomic,strong)UILabel *everyDayWaterLabel;

@property (nonatomic,strong)UIView *chanjianWaringView;
@property (nonatomic,strong)UIView *jiankangJianceView;
@property (nonatomic,strong)UIView *shanshiGuanliView;
@property (nonatomic,strong)UIView *everyDayWaterView;

@property (nonatomic,assign)CGFloat totalScrollViewHeight;
@property (nonatomic,strong)UIButton *addAttentionBtn;//添加关注

@property (nonatomic,strong)UIButton *addWaterBtn;
@property (nonatomic,strong)UIButton *subWaterBtn;

@property (nonatomic,strong)UIView *babyView;

@property (nonatomic,strong)UIView *addView;
@property (nonatomic,strong)UIView *weekReadView;
@property (nonatomic,strong)UIView *questionView;

@property (nonatomic,assign)float hy;

@property (nonatomic,copy)NSString *switch1;
@property (nonatomic,copy)NSString *switch2;
@property (nonatomic,copy)NSString *switch3;
@property (nonatomic,copy)NSString *switch4;
//今日关注栏需完成的总数
@property (nonatomic,assign)int totalCompleteCount;
//今日关注栏完成数
@property (nonatomic,assign)int alreadyCompleteCount;
//每周问题数组
@property (nonatomic,strong)NSMutableArray *weekInforMationArray;
@property (nonatomic,strong)NSMutableArray *lianjieArray;
//怀孕的周数和天数
@property (nonatomic,assign)int weekCount;
@property (nonatomic,assign)int totalDayCount;
@property (nonatomic,assign)int dayofweek;

@property (nonatomic,weak)UIButton *oneButton1;
@property (nonatomic,weak)UIButton *oneButton2;
@property (nonatomic,weak)UIButton *oneButton3;
@property (nonatomic,weak)UIButton *oneButton4;
@property (nonatomic,weak)UIButton *oneButton5;
@property (nonatomic,weak)UIButton *oneButton6;
@property (nonatomic,weak)UIButton *oneButton7;

@property (nonatomic,assign)BOOL btnFlag;
@property (nonatomic,assign)CGFloat leaveHeight;
@property (nonatomic,assign)BOOL leaveFlag;

@property (nonatomic,copy)NSString *userName;
@property (nonatomic,copy)NSString *weightStr;
@property (nonatomic,copy)NSString *heightStr;
@property (nonatomic,copy)NSString *birthdayStr;

@property (nonatomic,copy)NSString *huaiyunIntro;
@property (nonatomic,copy)NSString *fenmu;//分母
@property (nonatomic,assign)CGFloat totalSheRuliang;//总的摄入量

@property (nonatomic,strong)NSArray *chanjianArray;//产检提醒下的介绍数据
@property (nonatomic,strong)NSMutableArray *chanjianCompleteArray;
@property (nonatomic,copy)NSString *reportNO;

@property (nonatomic,copy)NSString *Sherucalori;//摄入量
@property (nonatomic,assign)BOOL oneBtnFlag; //点击上面的天数，为了防止下面完成情况增加
@property (nonatomic,assign)BOOL netFlag;

@property (nonatomic, assign)NSInteger dayCountInt;//距离预产期天数


@end
@implementation HomeViewController

-(NSArray *)chanjianArray
{
    if(!_chanjianArray)
    {
        NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"data" ofType:@"plist"];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
        _chanjianArray = [dict objectForKey:@"chanjian"];
    }
    return  _chanjianArray;
}
-(NSMutableArray *)weekInforMationArray
{
    if(!_weekInforMationArray)
    {
        _weekInforMationArray = [NSMutableArray array];
    }
    return _weekInforMationArray;
}

-(NSMutableArray *)lianjieArray
{
    if(!_lianjieArray)
    {
        _lianjieArray = [NSMutableArray array];
    }
    return _lianjieArray;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    int suijishu = arc4random() % 10;
    YMLog(@"suijishu = %d",suijishu);
    [passValue passValue].accountHaveFlag = NO;
    
    [self setTitlelabel:@"幸孕儿"];
    self.hy = 0;
    self.totalScrollViewHeight = 0;
    self.babyInformationIntroLabel = [[UILabel alloc]init];
    self.babyWeightAndHeight = [[UILabel alloc]init];
    
    self.totalCompleteCount = 0;
    self.alreadyCompleteCount = 0;
    
    [self netWorkingJianCe];
    
    self.view.backgroundColor = YMColor(242, 242, 242);
    UIButton *centerButton = [[UIButton alloc]initWithFrame:CGRectMake(20, 9, 26, 26)];
    [centerButton setImage:[UIImage imageNamed:@"id_on"] forState:UIControlStateNormal];
    self.centerButton = centerButton;
    [self.navigationController.navigationBar addSubview:centerButton];
    
    UIButton  *erweimaButton = [[UIButton alloc]initWithFrame:CGRectMake(kDeviceWidth - 46, 9, 26, 26)];
    [erweimaButton setImage:[UIImage imageNamed:@"QR_icon"] forState:UIControlStateNormal];
    self.erweimaButton = erweimaButton;
    [self.navigationController.navigationBar addSubview:erweimaButton];
    
    UIView *erweimaView = [[UIView alloc]initWithFrame:CGRectMake(0, 64, kDeviceWidth, kDeviceHeight - 64 - 49)];
    self.erweimaView = erweimaView;
    erweimaView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:erweimaView];
    self.erweimaView.hidden = YES;
    
    UIButton *hideButton = [[UIButton alloc]initWithFrame:erweimaView.frame];
    [hideButton addTarget:self action:@selector(clickHiddenButton) forControlEvents:UIControlEventTouchUpInside];
    [erweimaView addSubview:hideButton];
    
    
    UIImageView *erweimaImageView = [[UIImageView alloc]initWithFrame:CGRectMake(kDeviceWidth / 5.0, (erweimaView.frame.size.height - kDeviceWidth / 5.0 * 3 / 1.04) / 2.0, kDeviceWidth / 5.0 * 3, kDeviceWidth / 5.0 * 3 / 1.04)];
    erweimaImageView.image = [UIImage imageNamed:@"幸孕儿二维码_红白"];
    [erweimaView addSubview:erweimaImageView];
    
    [self.centerButton addTarget:self action:@selector(clickCenterButton) forControlEvents:UIControlEventTouchUpInside];
    [self.erweimaButton addTarget:self action:@selector(clickErweimaButton) forControlEvents:UIControlEventTouchUpInside];
    
    self.chanjianWaringLabel = [[UILabel alloc]init];
    self.chanjianWaringLabel.textColor = YMColor(126, 189, 249);
    self.jiankangJianceLabel = [[UILabel alloc]init];
    self.jiankangJianceLabel.textColor = YMColor(254, 157, 72);
    self.shanshiGuanliLabel = [[UILabel alloc]init];
    self.shanshiGuanliLabel.textColor = YMColor(101, 101, 101);
    self.everyDayWaterLabel = [[UILabel alloc]init];
    self.everyDayWaterLabel.textColor = YMColor(101, 101, 101);

    self.shanshiGuanliLabel.adjustsFontSizeToFitWidth = YES;
    
    self.chanjianWaringLabel.text = @"未完成";
    self.shanshiGuanliLabel.text = @"0/3100kcal";
    self.jiankangJianceLabel.text = @"0/6完成";
    self.everyDayWaterLabel.text = @"0杯";
    
    [self allKindOfControlLayout];
    [self allButtonLayout];
    [self getHuaiYunWeekAndDay];
    
    YMLog(@"保存的账号：%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"userAccount"]);
}
//获取系统时间
-(NSString *)getSystemTime
{
    NSDate *senddate=[NSDate date];
    NSDateFormatter  *dateformatter=[[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"YYYYMMddHHmmss"];
    NSString *locationString=[dateformatter stringFromDate:senddate];
    return locationString;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if(self.netFlag == YES)
    {
        self.centerButton.hidden = NO;
        self.erweimaButton.hidden = NO;
        
        if([[[NSUserDefaults standardUserDefaults]objectForKey:@"changed"] isEqualToString:@"haveChanged"])
        {
            [self.totalScrollView removeFromSuperview];
            self.totalScrollViewHeight = 0;
            self.hy = 0;
            [self getHuaiYunWeekAndDay];
            [self.weekInforMationArray removeAllObjects];
            [self.chanjianCompleteArray removeAllObjects];
            
            self.alreadyCompleteCount = 0;
        }
        
        self.switch1 = [[NSUserDefaults standardUserDefaults]objectForKey:@"switch1"];
        self.switch2 = [[NSUserDefaults standardUserDefaults]objectForKey:@"switch2"];
        self.switch3 = [[NSUserDefaults standardUserDefaults]objectForKey:@"switch3"];
        self.switch4 = [[NSUserDefaults standardUserDefaults]objectForKey:@"switch4"];
        
        if(self.leaveFlag == YES)
        {
            self.totalScrollViewHeight = 0;
            self.hy = self.leaveHeight;
            [self fourthViewDisplayState];
        }
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self getServicesVersion];
        });
    });
}

/**获取服务器的版本号*/
-(void)getServicesVersion
{
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    // 当前应用名称
    NSString *appCurName = [infoDictionary objectForKey:@"CFBundleDisplayName"];
    NSLog(@"当前应用名称：%@",appCurName);
    // 当前应用软件版本  比如：1.0.1
    NSString *appVerison = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    NSLog(@"当前应用软件版本:%@",appVerison);
    
    NSString *urlString = @"http://120.24.178.16:8080/Health/baby/getNewEdition.do";
    NSURL *url = [[NSURL alloc]initWithString:urlString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:2];
    request.HTTPMethod = @"POST";
    NSString *str = [NSString stringWithFormat:@"aPPType=ios"];
    NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:data];
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (data)
        {
            //            [MBProgressHUD hideHUD];
            // 请求成功
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
//            YMLog(@"dict = %@",dict);
            if([[dict objectForKey:@"code"] isEqualToString:@"0"])
            {
                NSString *serviceVersion = [dict objectForKey:@"data"];
                YMLog(@"serviceVersion = %@",serviceVersion);
                
                if ([serviceVersion compare:appVerison options:NSNumericSearch] == NSOrderedDescending)
                {
                    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"亲，此版本不是最新版本" delegate:self cancelButtonTitle:@"不了，谢谢" otherButtonTitles:@"更新", nil];
                    alertView.tag = 1;
                    [alertView show];
                }
            }
            else
            {
                [MBProgressHUD showError:[dict objectForKey:@"data"]];
            }
        }
    }];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 1)
    {
        if(buttonIndex == 1)
        {
            NSURL *URL = [NSURL URLWithString:@"https://itunes.apple.com/us/app/xing-yun-er/id1092462639?l=zh&ls=1&mt=8"];
            [[UIApplication sharedApplication] openURL:URL];
        }
    }
}


//获取产检信息
-(void)huoquChanjianInformation
{
    NSString *urlString = @"http://120.24.178.16:8080/Health/baby/getPregnancyReport.do";
    NSURL *url = [[NSURL alloc]initWithString:urlString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:2];
    
    request.HTTPMethod = @"POST";
    NSString *str = [NSString stringWithFormat:@"account=%@&username=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"userAccount"],self.userName];
    NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:data];
    
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    self.chanjianCompleteArray = [NSMutableArray array];
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        if (data)
        {
            // 请求成功
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            NSArray *arr = [dict objectForKey:@"data"];
            for(NSDictionary *dd in arr)
            {
                [self.chanjianCompleteArray addObject:[dd objectForKey:@"reportNo"]];
            }
            [self panduanChanjianLabelComplete:self.reportNO];
            [passValue passValue].chanjianCompleteArray = self.chanjianCompleteArray;
        }
    }];
}

//获取怀孕的周数与天数
-(void)getHuaiYunWeekAndDay
{
    NSString *urlString = @"http://120.24.178.16:8080/Health/baby/getDevAccountInfo.do";
    NSURL *url = [[NSURL alloc]initWithString:urlString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:2];
    
    request.HTTPMethod = @"POST";
    NSString *str = [NSString stringWithFormat:@"account=%@&userName=*",[[NSUserDefaults standardUserDefaults]objectForKey:@"userAccount"]];
    NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:data];
    
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (data)
        {
            // 请求成功
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
//            YMLog(@"所有信息dict = %@",dict);
            NSArray *array = [dict objectForKey:@"data"];
            [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"haveChanged"];
            if(array.firstObject == nil)
            {
                [[NSUserDefaults standardUserDefaults]setObject:@"noUserName" forKey:@"isUserName"];
                [MBProgressHUD showError:@"请先完善个人信息"];
                [passValue passValue].accountHaveFlag = YES;
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    PerfectInformationViewController *prefect = [[PerfectInformationViewController alloc]init];
                    [self.navigationController pushViewController:prefect animated:YES];
                });
            }
            else
            {
                [[NSUserDefaults standardUserDefaults]setObject:@"haveUserName" forKey:@"isUserName"];
                for(NSDictionary *dictionary in array)
                {
//                    YMLog(@"array = %@",array.firstObject);
                    NSString *weekCountString = [dictionary objectForKey:@"week"];
                    NSString *totalDayCount = [dictionary objectForKey:@"day"];
                    NSString *dayOfWeek = [dictionary objectForKey:@"dayofweek"];
                    self.weekCount = [weekCountString intValue];
                    self.dayofweek = [dayOfWeek intValue];
                    self.totalDayCount = [totalDayCount intValue] + 1;
                    
                    self.userName = [dictionary objectForKey:@"userName"];
                    [passValue passValue].saveDataUserName = self.userName;
                    [passValue passValue].userID = [dictionary objectForKey:@"userId"];
                    
                    self.birthdayStr = [dictionary objectForKey:@"age"];
                    self.weightStr = [dictionary objectForKey:@"weight"];
                    self.heightStr = [dictionary objectForKey:@"height"];
                    [self jisuanTotalSheRuLiangForWorkRank:[dictionary objectForKey:@"workRank"] andYunqi:[dictionary objectForKey:@"week"]];
                    self.dayCountInt = 280 - self.totalDayCount;
                     NSString *yuanshiStr = [NSString stringWithFormat:@"%d 周＋%d 天 \n距离预产期还有%ld 天",self.weekCount,self.dayofweek,(long)self.dayCountInt];
                    
                    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:yuanshiStr];
                    if(self.weekCount <= 9)
                    {
                        [self LabelString:str andrang1:0 andRang2:1];
                        [self LabelString:str andrang1:4 andRang2:1];
                        if(281 - self.totalDayCount - 1 < 10)
                        {
                            [self LabelString:str andrang1:16 andRang2:3];
                        }
                        if(281 - self.totalDayCount - 1 >= 10 && 281 - self.totalDayCount - 1 < 100)
                        {
                            [self LabelString:str andrang1:16 andRang2:4];
                        }
                        if(281 - self.totalDayCount - 1 >= 100)
                        {
                            [self LabelString:str andrang1:16 andRang2:5];
                        }
                    }
                    if(self.weekCount > 9)
                    {
                        [self LabelString:str andrang1:0 andRang2:2];
                        [self LabelString:str andrang1:5 andRang2:1];
                        
                        if(281 - self.totalDayCount - 1 <= 10)
                        {
                             [self LabelString:str andrang1:17 andRang2:3];
                        }
                        if(281 - self.totalDayCount - 1 > 10 && 281 - self.totalDayCount - 1 <= 100)
                        {
                             [self LabelString:str andrang1:17 andRang2:4];
                        }
                        if(281 - self.totalDayCount - 1 > 100)
                        {
                             [self LabelString:str andrang1:17 andRang2:5];
                        }
                    }
                    self.yuchanqiInfoLabel.attributedText = str;
//                    // 调整行间距
//                    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:self.yuchanqiInfoLabel.text];
//                    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
//                    [paragraphStyle setLineSpacing:160];
//                    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [self.yuchanqiInfoLabel.text length])];
                    
                    self.dateLabel.text = [NSString stringWithFormat:@"%d周",self.weekCount];
                    [self getEveryDayBabyGrowInformation:self.totalDayCount];
                    [self duiSevenButtonChushihua];
                    if(self.dayofweek == 1)
                    {
                        [self.oneButton1 setBackgroundImage:[UIImage imageNamed:@"button_已选"] forState:UIControlStateNormal];
                        [self.oneButton1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    }
                    if(self.dayofweek == 2)
                    {
                        [self.oneButton2 setBackgroundImage:[UIImage imageNamed:@"button_已选"] forState:UIControlStateNormal];
                        [self.oneButton2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    }
                    if(self.dayofweek == 3)
                    {
                        [self.oneButton3 setBackgroundImage:[UIImage imageNamed:@"button_已选"] forState:UIControlStateNormal];
                        [self.oneButton3 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    }
                    if(self.dayofweek == 4)
                    {
                        [self.oneButton4 setBackgroundImage:[UIImage imageNamed:@"button_已选"] forState:UIControlStateNormal];
                        [self.oneButton4 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    }
                    if(self.dayofweek == 5)
                    {
                        [self.oneButton5 setBackgroundImage:[UIImage imageNamed:@"button_已选"] forState:UIControlStateNormal];
                        [self.oneButton5 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    }
                    if(self.dayofweek == 6)
                    {
                        [self.oneButton6 setBackgroundImage:[UIImage imageNamed:@"button_已选"] forState:UIControlStateNormal];
                        [self.oneButton6 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    }
                    if(self.dayofweek == 7)
                    {
                        [self.oneButton7 setBackgroundImage:[UIImage imageNamed:@"button_已选"] forState:UIControlStateNormal];
                        [self.oneButton7 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    }

                }
                [self getWeekInformationAndBabyImage];
                [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"changed"];
                [[NSUserDefaults standardUserDefaults]setObject:self.userName forKey:@"userName"];
                
            }
            
            
        }
    }];

}
//计算总的摄入量
-(void)jisuanTotalSheRuLiangForWorkRank:(NSString *)workRankStr andYunqi:(NSString *)yunqiStr
{
    int workRank = [workRankStr intValue];//1.轻度  2.中度  3.重度
    int yunqi = [yunqiStr intValue];
    NSInteger tempInt = 0;
    //轻度劳动
    if(workRank == 1)
    {
        //孕早期
        if(yunqi <= 12)
        {
            self.totalSheRuliang = 1800;
        }
        //孕中期
        if(yunqi > 12 && yunqi <= 27)
        {
            self.totalSheRuliang = 2100;
        }
        //孕晚期
        if(yunqi > 27)
        {
            self.totalSheRuliang = 2250;
        }
    }
    //中度劳动
    if(workRank == 2)
    {
        //孕早期
        if(yunqi <= 12)
        {
            self.totalSheRuliang = 2100;
        }
        //孕中期
        if(yunqi > 12 && yunqi <= 27)
        {
            self.totalSheRuliang = 2400;
        }
        //孕晚期
        if(yunqi > 27)
        {
            self.totalSheRuliang = 2550;
        }
    }

    //重度劳动
    if(workRank == 3)
    {
        //孕早期
        if(yunqi <= 12)
        {
            self.totalSheRuliang = 2400;
        }
        //孕中期
        if(yunqi > 12 && yunqi <= 27)
        {
            self.totalSheRuliang = 2700;
        }
        //孕晚期
        if(yunqi > 27)
        {
            self.totalSheRuliang = 2850;
        }
    }
    //孕早期
    if(yunqi <= 12)
    {
        tempInt = 1;
    }
    //孕中期
    if(yunqi > 12 && yunqi <= 27)
    {
        tempInt = 2;
    }
    //孕晚期
    if(yunqi > 27)
    {
        tempInt = 3;
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%ld", (long)tempInt] forKey:@"yunqiValue"];
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%f", self.totalSheRuliang] forKey:@"totalNengliang"];
    
     dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
         
     });
    
    
}

//label字符串处理，中间某部分字符串变颜色
-(void)LabelString:(NSMutableAttributedString *)str andrang1:(int)i andRang2:(int)j
{
    [str addAttribute:NSForegroundColorAttributeName value:YMColor(54, 195, 152) range:NSMakeRange(i,j)];
    [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:19.0] range:NSMakeRange(i, j)];
}

//获取每周下面的问题及链接还有对应的图片
-(void)getWeekInformationAndBabyImage
{

    int imageWeekCount = self.weekCount + 1;
    if(imageWeekCount >= 40)
    {
        imageWeekCount = 40;
    }
    NSString *urlString = @"http://120.24.178.16:8080/Health/baby/getMainInfo.do";
    NSURL *url = [[NSURL alloc]initWithString:urlString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:2];
    request.HTTPMethod = @"POST";
    NSString *str = [NSString stringWithFormat:@"week=%d",imageWeekCount];
    NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:data];
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        if (data)
        {
            NSMutableArray *arr = [NSMutableArray array];
            // 请求成功
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            
            NSArray *weekArray = [dict objectForKey:@"data"];
            if(weekArray.firstObject != nil)
            {
                for(NSDictionary *dd in weekArray)
                {
                    
                    NSString *str = [dd objectForKey:@"babypic"];
                    [arr addObject:str];

                    [self.lianjieArray addObject:[dd objectForKey:@"addata"]];
                    [self.weekInforMationArray addObject:[dd objectForKey:@"healthdata"]];
                }
                [self downLoadBabyImageFromInternet:arr[0]];
            }
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.05 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self babyInformationLayout];
            [self loginEveryDayAndReadEveryWeek];
            [self huoquChanjianInformation];
            });
            
        }
    }];
    
}

//从网络下载婴儿生长图
-(void)downLoadBabyImageFromInternet:(NSString *)str
{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSURL *url1 = [NSURL URLWithString:str];
        NSData *resultData1 = [NSData dataWithContentsOfURL:url1];
        UIImage *img1 = [UIImage imageWithData:resultData1];
        
        dispatch_async(dispatch_get_main_queue(), ^{
               self.babyGrowImageView.image = img1;
        });
    });
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.centerButton.hidden = YES;
    self.erweimaButton.hidden = YES;
    self.erweimaView.hidden = YES;
    self.erweimaFlag = NO;
    self.btnFlag = NO;
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"changed"];
    self.oneBtnFlag = NO;

}
//进入个人中心
-(void)clickCenterButton
{
    PersonCenterViewController *personCenter = [[PersonCenterViewController alloc]init];
    [self.navigationController pushViewController:personCenter animated:YES];
    
    personCenter.weekCount = self.weekCount;
    personCenter.dayCount = self.dayofweek;
    personCenter.userName = self.userName;
    personCenter.birthdayString = self.birthdayStr;
    personCenter.weightString = self.weightStr;
    personCenter.heightString = self.heightStr;
}
//点击二维码
-(void)clickErweimaButton
{
    [self.view bringSubviewToFront:self.erweimaView];
    if(self.erweimaFlag)
    {
        self.erweimaView.hidden = YES;
        self.erweimaFlag = NO;
    }
    else
    {
        self.erweimaFlag = YES;
        self.erweimaView.hidden = NO;
    }
}

//隐藏二维码
-(void)clickHiddenButton
{
    self.erweimaView.hidden = YES;
    self.erweimaFlag = NO;
}

//设置导航栏title
-(void)setTitlelabel:(NSString *)title
{
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(kDeviceWidth / 4.0, 0, kDeviceWidth / 2.0, 44)];
    titleLabel.text = title;
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.font = [UIFont systemFontOfSize:20.0];
    self.navigationItem.titleView = titleLabel;
}

//各种控件的布局
-(void)allKindOfControlLayout
{
    self.yuchanqiInfoLabel = [[UILabel alloc]initWithFrame:CGRectMake(20, 64, kDeviceWidth / 2.0 - 20, kDeviceHeight / 4.5)];
    self.yuchanqiInfoLabel.backgroundColor = [UIColor clearColor];
    self.yuchanqiInfoLabel.textColor = YMColor(181, 181, 181);
    self.yuchanqiInfoLabel.numberOfLines = 0;
    self.yuchanqiInfoLabel.font = [UIFont systemFontOfSize:16.0];
    [self.view addSubview:self.yuchanqiInfoLabel];
    
    self.babyGrowImageView = [[UIImageView alloc]initWithFrame:CGRectMake(kDeviceWidth / 2.0 +(kDeviceWidth / 2.0 - kDeviceWidth / 2.69) / 2.0, 64 + (kDeviceHeight / 4.5 - kDeviceWidth / 2.69 / 1.4) / 2.0, kDeviceWidth / 2.69, kDeviceWidth / 2.69 / 1.4)];
//    self.babyGrowImageView.clipsToBounds = YES;
    
    self.babyGrowImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:self.babyGrowImageView];
}

//280个按钮布局
-(void)allButtonLayout
{
    UIView *faview = [[UIView alloc]initWithFrame:CGRectMake(0, 64 + kDeviceHeight / 4.5 , kDeviceWidth, kDeviceHeight / 14.2)];
//    UIView *faview = [[UIView alloc]initWithFrame:CGRectMake(0, 64 + kDeviceHeight / 4.5 , kDeviceWidth, 60)];
    faview.backgroundColor = YMColor(254, 207, 231);
    [self.view addSubview:faview];
    CGFloat h = faview.frame.size.height;
    CGFloat w = faview.frame.size.width;
    
    self.dateLeftBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, h / 3.0, h / 3.0, h / 3.0)];
    [self.dateLeftBtn setImage:[UIImage imageNamed:@"assistant_》small2"] forState:UIControlStateNormal];
    [faview addSubview:self.dateLeftBtn];
    
    self.dateRightBtn = [[UIButton alloc]initWithFrame:CGRectMake(w - 10 - h / 3.0, h / 3.0, h / 3.0, h / 3.0)];
    [self.dateRightBtn setImage:[UIImage imageNamed:@"assistant_》small"] forState:UIControlStateNormal];
    [faview addSubview:self.dateRightBtn];
    
    self.dateLabel = [[UILabel alloc]initWithFrame:CGRectMake(15 + h / 3.0, 0, w / 9.0, h)];
    self.dateLabel.textColor = YMColor(190, 83, 139);
    self.dateLabel.font = [UIFont systemFontOfSize:15.0];
    self.dateLabel.textAlignment = NSTextAlignmentCenter;
    [faview addSubview:self.dateLabel];
    
    self.allButtonScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(20 + h / 3.0 + w / 10.0, 0, w - (20 + h / 3.0) * 2 - w / 10.0 + 5, h)];
    self.allButtonScrollView.backgroundColor = [UIColor clearColor];
    [faview addSubview:self.allButtonScrollView];
    
    CGFloat weekButtonW = w - (20 + h / 3.0) * 2 - w / 10.0 + 5;
    [self buttonLayoutOfOne:weekButtonW and:h toFaView:self.allButtonScrollView];
    
    self.allButtonScrollView.contentSize =CGSizeMake(weekButtonW, h);
    self.allButtonScrollView.scrollEnabled = NO;
    
    [self.dateRightBtn addTarget:self action:@selector(clickRightButton) forControlEvents:UIControlEventTouchUpInside];
    [self.dateLeftBtn addTarget:self action:@selector(clickLeftButton) forControlEvents:UIControlEventTouchUpInside];
}

//胎儿详细情况布局
-(void)babyInformationLayout
{
    CGFloat y = 64 + kDeviceHeight / 4.5 + kDeviceHeight / 14.2;
 
    self.totalScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, y, kDeviceWidth, kDeviceHeight - y - 49)];
    self.totalScrollView.backgroundColor = [UIColor clearColor];
    self.totalScrollView.showsHorizontalScrollIndicator = YES;
    self.totalScrollView.delegate = self;
    // 去掉弹簧效果
    self.totalScrollView.bounces = NO;
    [self.view addSubview:self.totalScrollView];
    
    self.babyView = [[UIView alloc]initWithFrame:CGRectMake(0, self.hy, kDeviceWidth, kDeviceHeight)];
    self.babyView.backgroundColor = [UIColor clearColor];
    self.view.clipsToBounds = NO;
    [self.totalScrollView addSubview:self.babyView];
    
    CGFloat chushiY = self.hy;
    CGFloat babyViewHeight = 0;
    
    CGFloat h = kDeviceHeight / 3.65;
    
    UIView *view1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, h / 4.0 - 1)];
    view1.backgroundColor = [UIColor whiteColor];
    [self.babyView addSubview:view1];
    
    UIView *view2 = [[UIView alloc]initWithFrame:CGRectMake(0, h / 4.0, kDeviceWidth, h / 4.0 - 1)];
    view2.backgroundColor = [UIColor whiteColor];
    [self.babyView addSubview:view2];
    
    UIView *view3 = [[UIView alloc]initWithFrame:CGRectMake(0, h / 2.0, kDeviceWidth, h / 2.0 - 1)];
    view3.backgroundColor = [UIColor whiteColor];
    [self.babyView addSubview:view3];
    
    self.babyMotherHealthInfor = [[UILabel alloc]initWithFrame:CGRectMake(20, 0, kDeviceWidth / 4.0 * 3, h / 4.0 - 1)];

    //BMI=体重(千克)/身高(米)*身高(米)
    float BMI = [self.weightStr floatValue] / ([self.heightStr floatValue] / 100.0 * [self.heightStr floatValue] / 100.0);
    self.babyMotherHealthInfor.text = [NSString stringWithFormat:@"宝妈孕前身体质量指数：%.1f",BMI];
    
    self.babyMotherHealthInfor.font = [UIFont systemFontOfSize:15.0];
    self.babyMotherHealthInfor.textColor = YMColor(153, 153, 153);
    [self.babyView addSubview:self.babyMotherHealthInfor];
    babyViewHeight += self.babyMotherHealthInfor.frame.size.height;

    self.babyMotherHealthState = [[UILabel alloc]initWithFrame:CGRectMake(kDeviceWidth - 30 - kDeviceWidth / 4.0, 0, kDeviceWidth / 4.0, h / 4.0 - 1)];
    self.babyMotherHealthState.text = @"标准";
    self.babyMotherHealthState.textColor = YMColor(93, 210, 179);
    self.babyMotherHealthState.font = [UIFont systemFontOfSize:15.0];
    self.babyMotherHealthState.textAlignment = NSTextAlignmentRight;
    [self.babyView addSubview:self.babyMotherHealthState];
    
    if(BMI < 18.5)
    {
        self.babyMotherHealthState.text = @"偏瘦";
    }
    if(BMI >= 18.5 && BMI < 24.9)
    {
        self.babyMotherHealthState.text = @"标准";
    }
    if(BMI >= 24.9 && BMI <= 29.9)
    {
        self.babyMotherHealthState.text = @"超重";
    }
    if(BMI > 29.9)
    {
        self.babyMotherHealthState.text = @"肥胖";
    }
    
    self.babyWeightAndHeight.frame = CGRectMake(20, h / 4.0, kDeviceWidth - 40, h / 4.0 - 1);
    self.babyWeightAndHeight.font = [UIFont systemFontOfSize:15.0];
    self.babyWeightAndHeight.textColor = YMColor(153, 153, 153);
    [self.babyView addSubview:self.babyWeightAndHeight];
    babyViewHeight += self.babyWeightAndHeight.frame.size.height;

    self.babyInformationIntroLabel.frame = CGRectMake(0, 0, 0, 0);
    self.babyInformationIntroLabel.textAlignment = NSTextAlignmentLeft;
    self.babyInformationIntroLabel.font = [UIFont systemFontOfSize:15.0];
    NSDictionary *attribute2 = @{NSFontAttributeName:[UIFont systemFontOfSize:15.0]};
    CGSize size2 = [self.babyInformationIntroLabel.text boundingRectWithSize:CGSizeMake(kDeviceWidth - 40, kDeviceHeight) options:NSStringDrawingUsesLineFragmentOrigin attributes:attribute2 context:nil].size;
    self.babyInformationIntroLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.babyInformationIntroLabel.numberOfLines = 0;
    self.babyInformationIntroLabel.frame = CGRectMake(20, h / 2.0 + 3, kDeviceWidth - 40, size2.height);
    self.babyInformationIntroLabel.textColor = YMColor(153, 153, 153);
    [self.babyView addSubview:self.babyInformationIntroLabel];
    babyViewHeight += size2.height + 5;
    self.babyView.frame = CGRectMake(0, chushiY, kDeviceWidth, babyViewHeight);
    view3.frame = CGRectMake(0, h / 2.0, kDeviceWidth, self.babyView.frame.size.height - (h / 4.0 - 1) * 2);
     self.hy += self.babyView.frame.size.height + 7;
    
    self.leaveHeight = self.hy;
}

//每个按钮的布局
-(void)buttonLayoutOfOne:(CGFloat)w and:(CGFloat)h toFaView:(UIScrollView *)scrollView
{
    CGFloat kongxiW = w / 29.0;
    CGFloat wbutton = w / 29.0 * 3;
    for(int i = 0;i < 7;i ++)
    {
        UIButton *oneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        oneBtn.frame = CGRectMake(kongxiW * (i + 1) + wbutton * i, (h - wbutton) / 2.0, wbutton, wbutton);
        oneBtn.highlighted = NO;
        [oneBtn setBackgroundImage:[UIImage imageNamed:@"button_未选"] forState:UIControlStateNormal];
        [oneBtn setTitleColor:YMColor(252, 177, 214) forState:UIControlStateNormal];
        oneBtn.tag = i + 1;
        [oneBtn setTitle:[NSString stringWithFormat:@"%d",i + 1] forState:UIControlStateNormal];
        oneBtn.titleLabel.font = [UIFont boldSystemFontOfSize:12.0];
        [scrollView addSubview:oneBtn];
        [oneBtn addTarget:self action:@selector(clickOneButton:) forControlEvents:UIControlEventTouchUpInside];
        if(oneBtn.tag == 1)
        {
            self.oneButton1 = oneBtn;
        }
        if(oneBtn.tag == 2)
        {
            self.oneButton2 = oneBtn;
        }
        if(oneBtn.tag == 3)
        {
            self.oneButton3 = oneBtn;
        }
        if(oneBtn.tag == 4)
        {
            self.oneButton4 = oneBtn;
        }
        if(oneBtn.tag == 5)
        {
            self.oneButton5 = oneBtn;
        }
        if(oneBtn.tag == 6)
        {
            self.oneButton6 = oneBtn;
        }
        if(oneBtn.tag == 7)
        {
            self.oneButton7 = oneBtn;
        }
    }
    
    
}

//初始化所有button
-(void)duiSevenButtonChushihua
{
    [self.oneButton1 setBackgroundImage:[UIImage imageNamed:@"button_未选"] forState:UIControlStateNormal];
    [self.oneButton2 setBackgroundImage:[UIImage imageNamed:@"button_未选"] forState:UIControlStateNormal];
    [self.oneButton3 setBackgroundImage:[UIImage imageNamed:@"button_未选"] forState:UIControlStateNormal];
    [self.oneButton4 setBackgroundImage:[UIImage imageNamed:@"button_未选"] forState:UIControlStateNormal];
    [self.oneButton5 setBackgroundImage:[UIImage imageNamed:@"button_未选"] forState:UIControlStateNormal];
    [self.oneButton6 setBackgroundImage:[UIImage imageNamed:@"button_未选"] forState:UIControlStateNormal];
    [self.oneButton7 setBackgroundImage:[UIImage imageNamed:@"button_未选"] forState:UIControlStateNormal];
    [self.oneButton1 setTitleColor:YMColor(252, 177, 214) forState:UIControlStateNormal];
    [self.oneButton2 setTitleColor:YMColor(252, 177, 214) forState:UIControlStateNormal];
    [self.oneButton3 setTitleColor:YMColor(252, 177, 214) forState:UIControlStateNormal];
    [self.oneButton4 setTitleColor:YMColor(252, 177, 214) forState:UIControlStateNormal];
    [self.oneButton5 setTitleColor:YMColor(252, 177, 214) forState:UIControlStateNormal];
    [self.oneButton6 setTitleColor:YMColor(252, 177, 214) forState:UIControlStateNormal];
    [self.oneButton7 setTitleColor:YMColor(252, 177, 214) forState:UIControlStateNormal];
}

-(void)clickOneButton:(UIButton *)btn
{
    [self duiSevenButtonChushihua];
    if(btn != self.starButton)
    {
        self.starButton.selected = NO;
        [self.starButton setBackgroundImage:[UIImage imageNamed:@"button_未选"] forState:UIControlStateNormal];
        [self.starButton setTitleColor:YMColor(252, 177, 214) forState:UIControlStateNormal];
        self.starButton = btn;
        [btn setBackgroundImage:[UIImage imageNamed:@"button_已选"] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    self.starButton.selected = YES;
    int day = (int)(btn.tag + self.weekCount * 7);
    if(self.netFlag == YES)
    {
        [self getEveryDayBabyGrowInformation:day];
    }
    else
    {
        [MBProgressHUD showError:@"亲，网络连了吗?"];
    }
    self.btnFlag = YES;
    self.oneBtnFlag = YES;
}

//获取每天宝宝的生长信息
-(void)getEveryDayBabyGrowInformation:(int)day
{
    [MBProgressHUD showMessage:@"正在拼命加载，请稍后..."];
    NSString *urlString = @"http://120.24.178.16:8080/Health/baby/getMainDayInfo.do";
    NSURL *url = [[NSURL alloc]initWithString:urlString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:2];
    
    request.HTTPMethod = @"POST";
    NSString *str = [NSString stringWithFormat:@"day=%d&state=1",day];
    
    NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:data];
    
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        if (data)
        {
            [MBProgressHUD hideHUD];
            
            // 请求成功
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            //            YMLog(@"dict = %@",dict);
            if ([[dict objectForKey:@"response"]intValue] != 0)
            {
                NSDictionary *arr = [dict objectForKey:@"data"];
                self.babyWeightAndHeight.text = [arr objectForKey:@"babyweight"];
                self.babyInformationIntroLabel.text = [arr objectForKey:@"data"];
                if(self.btnFlag == YES)
                {
                    self.totalScrollViewHeight = 0;
                    self.hy = 0;
                    [self.babyView removeFromSuperview];
                    [self.chanjianWaringView removeFromSuperview];
                    for (UIView * subview in [self.totalScrollView subviews])
                    {
                        [subview removeFromSuperview];
                    }
                    [self.totalScrollView removeFromSuperview];
                    [self babyInformationLayout];
                    [self loginEveryDayAndReadEveryWeek];
                    
                }
            }
            else
            {
                [MBProgressHUD showError:@"数据获取错误"];
            }
        }
    }];
}

-(void)clickRightButton
{
    self.weekCount ++;

    if(self.weekCount >= 40)
    {
        self.dateRightBtn.userInteractionEnabled = NO;
        [MBProgressHUD showError:@"已到最大值"];
    }
    else
    {
        self.dateRightBtn.userInteractionEnabled = YES;
        
    }
    self.dateLeftBtn.userInteractionEnabled = YES;
    self.dateLabel.text = [NSString stringWithFormat:@"%d周",self.weekCount];
}

-(void)clickLeftButton
{
    self.weekCount --;
    if(self.weekCount <= 1)
    {
        self.dateLeftBtn.userInteractionEnabled = NO;
        [MBProgressHUD showError:@"已是最小值"];
        
    }
    else
    {
        self.dateLeftBtn.userInteractionEnabled = YES;
        
    }
    self.dateRightBtn.userInteractionEnabled = YES;
    self.dateLabel.text = [NSString stringWithFormat:@"%d周",self.weekCount];
}
//今日关注和每周必读布局
-(void)loginEveryDayAndReadEveryWeek
{
    self.chanjianWaringView = [[UIView alloc]init];
    self.jiankangJianceView = [[UIView alloc]init];
    self.shanshiGuanliView = [[UIView alloc]init];
    self.everyDayWaterView = [[UIView alloc]init];
    
//    self.leaveHeight = self.totalScrollViewHeight;
    
    [self fourthViewDisplayState];
    
    
    [self addControllerToView:self.chanjianWaringView andTitleString:@"产检提醒" andDescrpString:[self genjuweekCountHuoquInformation] andResultLabel:self.chanjianWaringLabel andImageName:@"gz_产检提醒icon"];
    [self addControllerToView:self.jiankangJianceView andTitleString:@"健康监测" andDescrpString:@"建议您每天都进行监测" andResultLabel:self.jiankangJianceLabel andImageName:@"gz_健康监测icon"];
    [self addControllerToView:self.shanshiGuanliView andTitleString:@"膳食管理" andDescrpString:@"建议您每天都记录饮食" andResultLabel:self.shanshiGuanliLabel andImageName:@"gz_膳食管理icon"];
    [self addControllerToView:self.everyDayWaterView andTitleString:@"每日饮水" andDescrpString:@"建议每天喝8杯水，约1.7L" andResultLabel:self.everyDayWaterLabel andImageName:@"gz_每日饮水icon"];
   
    //从服务器上获取今日关注下的内容
    [self getjiankangSelectCountFenmuFromSevices];
    
    
    [self getEveryDayWaterCountFromServices];
   
    self.addView = [[UIView alloc]initWithFrame:CGRectMake(0, self.totalScrollViewHeight, kDeviceWidth, 40)];
    self.addView.backgroundColor = [UIColor whiteColor];
    [self.totalScrollView addSubview:self.addView];
    
    UIImageView *addImageView = [[UIImageView alloc]initWithFrame:CGRectMake(kDeviceWidth / 2.0 - 39, 13, 14, 14)];
    addImageView.image = [UIImage imageNamed:@"health_add"];
    [self.addView addSubview:addImageView];
    
    self.addAttentionBtn = [[UIButton alloc]initWithFrame:CGRectMake(kDeviceWidth / 2.0 - 19, 0, 60, 40)];
    [self.addAttentionBtn setTitle:@"添加关注" forState:UIControlStateNormal];
    self.addAttentionBtn.titleLabel.font = [UIFont systemFontOfSize:14.0];
    [self.addAttentionBtn setTitleColor:YMColor(120, 120, 120) forState:UIControlStateNormal];
    [self.addView addSubview:self.addAttentionBtn];
    
    self.totalScrollViewHeight += self.addView.frame.size.height + 5;
    self.weekReadView = [[UIView alloc]initWithFrame:CGRectMake(0, self.totalScrollViewHeight, kDeviceWidth, 40)];
    self.weekReadView.backgroundColor = [UIColor whiteColor];
    [self.totalScrollView addSubview:self.weekReadView];
    UILabel *weekReadLabel = [[UILabel alloc]initWithFrame:CGRectMake(20, 0, kDeviceWidth / 2.0, 40)];
    weekReadLabel.text = @"每周必读";
    weekReadLabel.textColor = YMColor(190, 69, 137);
    [self.weekReadView addSubview:weekReadLabel];
    self.totalScrollViewHeight += self.weekReadView.frame.size.height + 1;
    
    self.questionView = [[UIView alloc]initWithFrame:CGRectMake(0, self.totalScrollViewHeight, kDeviceWidth, 30 * self.weekInforMationArray.count)];
    [self.totalScrollView addSubview:self.questionView];
    
    //每周必读下的问题
    for(int i = 0;i < self.weekInforMationArray.count;i++)
    {
        UIView *questionView = [[UIView alloc]initWithFrame:CGRectMake(0, i * 30, kDeviceWidth, 30)];
        [self.questionView addSubview:questionView];
        questionView.tag = i + 1;
        self.totalScrollViewHeight += questionView.frame.size.height;
        [self addAllofControllerToView:questionView andQuestionString:self.weekInforMationArray[i]];
    }

    self.totalScrollView.contentSize = CGSizeMake(kDeviceWidth, self.totalScrollViewHeight + 5);
    [self.addAttentionBtn addTarget:self action:@selector(clickAddAttentionButton) forControlEvents:UIControlEventTouchUpInside];
}

//根据怀孕周数显示产检提醒下的介绍信息
-(NSString *)genjuweekCountHuoquInformation
{
    NSString *chanjinStr = nil;
    NSString *reportNO = nil;
    if(self.weekCount <= 12)
    {
        chanjinStr = self.chanjianArray[0];
        reportNO = @"1";
    }
    if(self.weekCount <= 16 && self.weekCount >= 13)
    {
        chanjinStr = self.chanjianArray[1];
        reportNO = @"2";
        
    }
    if(self.weekCount <= 20 && self.weekCount >= 17)
    {
        chanjinStr = self.chanjianArray[2];
        reportNO = @"3";
    }
    if(self.weekCount <= 24 && self.weekCount >= 21)
    {
        chanjinStr = self.chanjianArray[3];
        reportNO = @"4";
    }
    if(self.weekCount <= 28 && self.weekCount >= 25)
    {
        chanjinStr = self.chanjianArray[4];
        reportNO = @"5";
    }
    if(self.weekCount <= 32 && self.weekCount >= 29)
    {
        chanjinStr = self.chanjianArray[5];
        reportNO = @"6";
    }
    if(self.weekCount <= 35 && self.weekCount >= 33)
    {
        chanjinStr = self.chanjianArray[6];
        reportNO = @"7";
    }
    if(self.weekCount == 36)
    {
        chanjinStr = self.chanjianArray[7];
        reportNO = @"8";
    }
    if(self.weekCount == 37)
    {
        chanjinStr = self.chanjianArray[8];
        reportNO = @"9";
    }
    if(self.weekCount >= 38)
    {
        chanjinStr = self.chanjianArray[9];
        reportNO = @"10";
    }
    
    self.reportNO = reportNO;
    return  chanjinStr;
}

//判断产检提醒有没有完成
-(void)panduanChanjianLabelComplete:(NSString *)reportNO
{
    int i = 0;
    if(self.chanjianCompleteArray.firstObject != nil)
    {
        while (1) {
            NSString *str = [NSString stringWithFormat:@"%@",self.chanjianCompleteArray[i]];
            if([str isEqualToString:reportNO])
            {
                [self chanjianWaringLabelComplete];
                if(![self.switch1 isEqualToString:@"No"])
                {
                    if(self.oneBtnFlag == YES)
                    {}
                    else
                    {
                    self.alreadyCompleteCount ++;
                    }
                }
                break;
            }
            else
            {
                i ++;
                if(i == self.chanjianCompleteArray.count)
                {
                    [self chanjinaWaringNotComplete];
                    break;
                }
            }
        }
    }
}
//产检提醒栏已完成
-(void)chanjianWaringLabelComplete
{
    self.chanjianWaringLabel.text = @"完成";
    self.chanjianWaringLabel.textColor = YMColor(126, 189, 249);
}

//产检提醒栏未完成
-(void)chanjinaWaringNotComplete
{
    self.chanjianWaringLabel.text = @"未完成";
    self.chanjianWaringLabel.textColor = YMColor(254, 157, 72);

}

//从服务器上获取健康监测中选了几项  分子
-(void)getjiankangSelectCountFromSevices
{
    NSString *urlString = @"http://120.24.178.16:8080/Health/baby/getNumberOfTests.do";
    NSURL *url = [[NSURL alloc]initWithString:urlString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:2];
    request.HTTPMethod = @"POST";
    NSString *str = [NSString stringWithFormat:@"account=%@&username=%@&date=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"userAccount"],self.userName,[self getSystemTime]];
    NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:data];
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        if (data)
        {
            
            // 请求成功
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
//                        YMLog(@"dict = %@",dict);
            NSArray *arr = [dict objectForKey:@"data"];
            if(arr.firstObject != nil)
            {
                for(NSDictionary *dd in arr)
                {
                    NSString *fenziD = [dd objectForKey:@"choosenumber"];
                    if(![self.switch2 isEqualToString:@"No"])
                    {
                        if([fenziD intValue] >= [self.fenmu intValue])
                        {
                            fenziD = self.fenmu;
                            if(self.oneBtnFlag == YES)
                            {}
                            else
                            {
                                self.alreadyCompleteCount ++;
                            }
                        }
                    }
                    self.jiankangJianceLabel.text = [NSString stringWithFormat:@"%@/%@完成",fenziD,self.fenmu];
                }
            }
            else
            {
                self.jiankangJianceLabel.text = [NSString stringWithFormat:@"0/%@完成",self.fenmu];
            }
            
        }
    }];
    
}

//从服务器上获取健康监测中选了几项  分母
-(void)getjiankangSelectCountFenmuFromSevices
{
    NSString *urlString = @"http://120.24.178.16:8080/Health/baby/getNumberOfChoice.do";
    NSURL *url = [[NSURL alloc]initWithString:urlString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:2];
    request.HTTPMethod = @"POST";
    NSString *str = [NSString stringWithFormat:@"account=%@&username=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"userAccount"],self.userName];
    
    NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:data];
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (data)
        {
            // 请求成功
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            NSArray *arr = [dict objectForKey:@"data"];
            for(NSDictionary *dd in arr)
            {
                self.fenmu = [NSString stringWithFormat:@"%@",[dd objectForKey:@"totalnumber"]];
                [self getjiankangSelectCountFromSevices];
            }
        }
    }];
}

//判断今日关注下那四个显示的状态
-(void)fourthViewDisplayState
{
    UIView *titleView = [[UIView alloc]initWithFrame:CGRectMake(0, self.hy, kDeviceWidth, 40)];
    titleView.backgroundColor = [UIColor whiteColor];
    [self.totalScrollView addSubview:titleView];
    
    UILabel *currentDaylabel = [[UILabel alloc]initWithFrame:CGRectMake(20, 0, kDeviceWidth / 2.0, 40)];
    currentDaylabel.text = @"今日关注";
    currentDaylabel.textColor = YMColor(190, 69, 137);
    [titleView addSubview:currentDaylabel];
    
    self.displaySelectLabel = [[UILabel alloc]initWithFrame:CGRectMake(kDeviceWidth - 30 - kDeviceWidth / 4.0, 0, kDeviceWidth / 4.0, 40)];
    self.displaySelectLabel.textColor = YMColor(252, 178, 215);
    
    self.displaySelectLabel.textAlignment = NSTextAlignmentRight;
    [titleView addSubview:self.displaySelectLabel];
    
    self.totalScrollViewHeight += titleView.frame.size.height + 1 + self.hy;
    self.totalCompleteCount = 0;
    
 
    if([self.switch1 isEqualToString:@"No"])
    {
        self.chanjianWaringView.frame = CGRectZero;
        self.chanjianWaringView.clipsToBounds = YES;
        self.totalScrollViewHeight +=  -1;
       
    }
    else
    {
        
        self.chanjianWaringView.frame  = CGRectMake(0, self.totalScrollViewHeight, kDeviceWidth, 60);
        self.totalCompleteCount ++;
       
    }

    self.chanjianWaringView.backgroundColor = [UIColor whiteColor];
    [self.totalScrollView addSubview:self.chanjianWaringView];
    self.chanjianWaringView.tag = 1;
    self.totalScrollViewHeight += self.chanjianWaringView.frame.size.height + 1;
    
    if([self.switch2 isEqualToString:@"No"])
    {
        self.jiankangJianceView.frame = CGRectZero;
        self.jiankangJianceView.clipsToBounds = YES;
        self.totalScrollViewHeight +=  -1;
    }
    else
    {
        self.jiankangJianceView.frame  = CGRectMake(0, self.totalScrollViewHeight, kDeviceWidth, 60);
        self.totalCompleteCount ++;
    }
    
    self.jiankangJianceView.backgroundColor = [UIColor whiteColor];
    [self.totalScrollView addSubview:self.jiankangJianceView];
    self.jiankangJianceView.tag = 2;
    self.totalScrollViewHeight += self.jiankangJianceView.frame.size.height + 1;
    
    if([self.switch3 isEqualToString:@"No"])
    {
        self.shanshiGuanliView.frame = CGRectZero;
        self.shanshiGuanliView.clipsToBounds = YES;
        self.totalScrollViewHeight +=  -1;
    }
    else
    {
        self.shanshiGuanliView.frame  = CGRectMake(0, self.totalScrollViewHeight, kDeviceWidth, 60);
        self.totalCompleteCount ++;
    }
    self.shanshiGuanliView.backgroundColor = [UIColor whiteColor];
    [self.totalScrollView addSubview:self.shanshiGuanliView];
    self.shanshiGuanliView.tag = 3;
    self.totalScrollViewHeight += self.shanshiGuanliView.frame.size.height + 1;
    
    if([self.switch4 isEqualToString:@"No"])
    {
        self.everyDayWaterView.frame = CGRectZero;
        self.everyDayWaterView.clipsToBounds = YES;
        self.totalScrollViewHeight +=  -1;
       
    }
    else
    {
        self.everyDayWaterView.frame  = CGRectMake(0, self.totalScrollViewHeight, kDeviceWidth, 60);
        self.totalCompleteCount ++;
        
    }
    self.everyDayWaterView.backgroundColor = [UIColor whiteColor];
    [self.totalScrollView addSubview:self.everyDayWaterView];
    self.everyDayWaterView.tag = 4;
    self.totalScrollViewHeight += self.everyDayWaterView.frame.size.height + 1;
    
    NSString *guanzhu = [[NSUserDefaults standardUserDefaults]objectForKey:@"guanzhu"];
    NSString *fenzi = [[self.jiankangJianceLabel.text componentsSeparatedByString:@"/"]firstObject];
    
    if(self.oneBtnFlag == YES)
    {
    }
    else
    {
        
        if([guanzhu isEqualToString:@"1close"])
        {
            
            if([self.chanjianWaringLabel.text isEqualToString:@"完成"])
            {
                self.alreadyCompleteCount --;
            }
           
        }
        if([guanzhu isEqualToString:@"1open"])
        {
            
            if([self.chanjianWaringLabel.text isEqualToString:@"完成"])
            {
                self.alreadyCompleteCount ++;
            }
        }
        if([guanzhu isEqualToString:@"2close"])
        {
            
            if([fenzi intValue] >= [self.fenmu intValue])
            {
                self.alreadyCompleteCount --;
            }
            
        }
        if([guanzhu isEqualToString:@"2open"])
        {
            
            if([fenzi intValue] >= [self.fenmu intValue])
            {
                self.alreadyCompleteCount ++;
            }
        }
        if([guanzhu isEqualToString:@"3close"])
        {
            if([self.Sherucalori floatValue] >= self.totalSheRuliang)
            {
                self.alreadyCompleteCount --;
            }
        }
        if([guanzhu isEqualToString:@"3open"])
        {
            if([self.Sherucalori floatValue] >= self.totalSheRuliang)
            {
                self.alreadyCompleteCount ++;
            }
        }
        if([guanzhu isEqualToString:@"4close"])
        {
            if([self.everyDayWaterLabel.text intValue] >= 8)
            {
                self.alreadyCompleteCount --;
            }
        }
        if([guanzhu isEqualToString:@"4open"])
        {
            if([self.everyDayWaterLabel.text intValue] >= 8)
            {
                self.alreadyCompleteCount ++;
            }
        }
    }
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"guanzhu"];
    if(self.alreadyCompleteCount >= self.totalCompleteCount)
    {
        self.alreadyCompleteCount = self.totalCompleteCount;
    }
    self.displaySelectLabel.text = [NSString stringWithFormat:@"%d/%d",self.alreadyCompleteCount,self.totalCompleteCount];
    
    if(self.leaveFlag)
    {
        self.addView.frame = CGRectMake(0, self.totalScrollViewHeight, kDeviceWidth, 40);
        self.totalScrollViewHeight += self.addView.frame.size.height + 5;
        self.weekReadView.frame = CGRectMake(0, self.totalScrollViewHeight, kDeviceWidth, 40);
        self.totalScrollViewHeight += self.weekReadView.frame.size.height + 1;
        self.questionView.frame = CGRectMake(0, self.totalScrollViewHeight, kDeviceWidth, 30 * self.weekInforMationArray.count);
        self.totalScrollViewHeight += self.questionView.frame.size.height + 5;
        self.totalScrollView.contentSize = CGSizeMake(kDeviceWidth, self.totalScrollViewHeight + 5);
    }
    self.leaveFlag = NO;
}
//每周必读下的问题
-(void)addAllofControllerToView:(UIView *)view andQuestionString:(NSString *)questionStr
{
    CGFloat h = view.frame.size.height;
    UIView *littleView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, h)];
    [view addSubview:littleView];
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, littleView.frame.size.width, littleView.frame.size.height)];
    btn.tag = view.tag;
    [littleView addSubview:btn];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(20, 0, kDeviceWidth / 4.0 * 3, h)];
    label.textColor = YMColor(170, 170, 170);
    label.font = [UIFont systemFontOfSize:14.0];
    label.text = questionStr;
    [view addSubview:label];
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(kDeviceWidth - 20 - 14, 8, 14, 14)];
    imageView.image = [UIImage imageNamed:@"assistant_》gray"];
    [view addSubview:imageView];
    [btn addTarget:self action:@selector(clickWeekReadButton:) forControlEvents:UIControlEventTouchUpInside];
}
//点击了每周必读下的内容
-(void)clickWeekReadButton:(UIButton *)btn
{
    weekShouldReadViewController *read = [[weekShouldReadViewController alloc]init];
    [self.navigationController pushViewController:read animated:YES];
    read.weakReadString = self.lianjieArray[btn.tag - 1];
}
//点击了添加关注按钮
-(void)clickAddAttentionButton
{
    AttentionManagerViewController *attention = [[AttentionManagerViewController alloc]init];
    [self.navigationController pushViewController:attention animated:YES];
    self.leaveFlag = YES;
    self.totalCompleteCount = 0;
}

//今日关注下的提醒添加信息
-(void)addControllerToView:(UIView *)view andTitleString:(NSString *)titleStr andDescrpString:(NSString *)descrString andResultLabel:(UILabel *)resultLabel andImageName:(NSString *)imageName
{
    CGFloat h = view.frame.size.height;
    UIImageView *chanjianWaringImageView = [[UIImageView alloc]initWithFrame:CGRectMake(20, 15, 30, 30)];
    chanjianWaringImageView.image = [UIImage imageNamed:imageName];
    [view addSubview:chanjianWaringImageView];
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(70, 5, 100, 30)];
    titleLabel.text = titleStr;
    titleLabel.textColor = YMColor(193, 87, 144);
    [view addSubview:titleLabel];
    
    UILabel *descripLabel = [[UILabel alloc]initWithFrame:CGRectMake(70, 25, kDeviceWidth / 2.0, 30)];
    descripLabel.text = descrString;
    descripLabel.textColor = YMColor(166, 166, 166);
    descripLabel.font = [UIFont systemFontOfSize:12.0];
    [view addSubview:descripLabel];
    
     resultLabel.frame = CGRectMake(kDeviceWidth - 30 - kDeviceWidth / 3.0, 0, kDeviceWidth / 3.0, 60);
    [view addSubview:resultLabel];
    if(view.tag == 4)
    {
        resultLabel.textAlignment = NSTextAlignmentCenter;
        resultLabel.frame = CGRectMake(kDeviceWidth - 15 - kDeviceWidth / 3.0, 0, kDeviceWidth / 3.0, 60);
        self.addWaterBtn = [[UIButton alloc]initWithFrame:CGRectMake(kDeviceWidth - kDeviceWidth / 20.0 - h / 2.0 , h / 4.0, h / 2.0, h / 2.0)];
        [self.addWaterBtn setImage:[UIImage imageNamed:@"gz_jiahao"] forState:UIControlStateNormal];
        [view addSubview:self.addWaterBtn];
        
        self.subWaterBtn = [[UIButton alloc]initWithFrame:CGRectMake(kDeviceWidth / 3.0 * 2 - kDeviceWidth / 35.0, h / 4.0, h / 2.0, h / 2.0)];
        [self.subWaterBtn setImage:[UIImage imageNamed:@"gz_jianhao"] forState:UIControlStateNormal];
        [view addSubview:self.subWaterBtn];
        
        [self.addWaterBtn addTarget:self action:@selector(addWaterLiang) forControlEvents:UIControlEventTouchUpInside];
        [self.subWaterBtn addTarget:self action:@selector(subWaterLiang) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        resultLabel.textAlignment = NSTextAlignmentRight;
    }
    if(view.tag != 4)
    {
        UIButton *btn = [[UIButton alloc]initWithFrame:view.bounds];
        btn.tag = view.tag;
        [view addSubview:btn];
        [btn addTarget:self action:@selector(clickEveryDaylogir:) forControlEvents:UIControlEventTouchUpInside];
    }
}

//点击今日关注下的按钮
-(void)clickEveryDaylogir:(UIButton *)btn
{
    //产检提醒
    if(btn.tag == 1)
    {
        ChanjianJiluViewController *chanjian = [[ChanjianJiluViewController alloc]init];
        [self.navigationController pushViewController:chanjian animated:YES];
        chanjian.userName = self.userName;
//        chanjian.chanjianCompleteArray = self.chanjianCompleteArray;
        
    }
    //健康监测
    if(btn.tag == 2)
    {
        YBHealthControllerNew *ybhealth = [[YBHealthControllerNew alloc]init];
        ybhealth.dayCountInt = self.dayCountInt;
        [self.navigationController pushViewController:ybhealth animated:YES];
        
        [passValue passValue].saveDataUserName = self.userName;
        [passValue passValue].passValueName = [[NSUserDefaults standardUserDefaults]objectForKey:@"userAccount"];
        
    }
    //膳食管理
    if(btn.tag == 3)
    {
        foodHomeViewController *food = [[foodHomeViewController alloc]init];
        [self.navigationController pushViewController:food animated:YES];
        
        [passValue passValue].saveDataUserName = self.userName;
        [passValue passValue].passValueName = [[NSUserDefaults standardUserDefaults]objectForKey:@"userAccount"];
//        food.totalCalor = self.totalSheRuliang;
    }
}

//喝水多少的按钮
-(void)addWaterLiang
{
    int liang = [self.everyDayWaterLabel.text intValue];
    liang ++;
    self.everyDayWaterLabel.text = [NSString stringWithFormat:@"%d杯",liang];
    self.subWaterBtn.userInteractionEnabled = YES;
    [self postWaterInformationToServices];
    if(liang == 8)
    {
        if(self.oneBtnFlag == YES)
        {}
        else
        {
        self.alreadyCompleteCount ++;
        self.displaySelectLabel.text = [NSString stringWithFormat:@"%d/%d",self.alreadyCompleteCount,self.totalCompleteCount];
        }
    }
}

-(void)subWaterLiang
{
    int liang = [self.everyDayWaterLabel.text intValue];
    liang --;
    self.everyDayWaterLabel.text = [NSString stringWithFormat:@"%d杯",liang];
    if(liang <= 0)
    {
        [MBProgressHUD showError:@"已是最小值"];
        self.subWaterBtn.userInteractionEnabled = NO;
        self.everyDayWaterLabel.text = @"0杯";
    }
    else
    {
        self.subWaterBtn.userInteractionEnabled = YES;
    }
  
    if(liang == 7)
    {
        if(self.oneBtnFlag == YES)
        {}
        else
        {
        self.alreadyCompleteCount --;
        self.displaySelectLabel.text = [NSString stringWithFormat:@"%d/%d",self.alreadyCompleteCount,self.totalCompleteCount];
        }
    }
    
    [self postWaterInformationToServices];
}
//上传喝水的信息
-(void)postWaterInformationToServices
{
    [MBProgressHUD showMessage:@"正在拼命的上传中，请稍后..."];
    NSString *urlString = @"http://120.24.178.16:8080/Health/baby/setWaterDrinking.do";
    NSURL *url = [[NSURL alloc]initWithString:urlString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:2];
    request.HTTPMethod = @"POST";
    NSString *str = [NSString stringWithFormat:@"account=%@&username=%@&amount=%@&date=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"userAccount"],self.userName,[NSString stringWithFormat:@"%d",[self.everyDayWaterLabel.text intValue]],[self getSystemTime]];
    
    NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:data];
    
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        if (data)
        {
            [MBProgressHUD hideHUD];
            // 请求成功
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
//            YMLog(@"dict = %@",dict);
            if([[dict objectForKey:@"code"] isEqualToString:@"0"])
            {
                [MBProgressHUD showSuccess:@"上传成功"];
            }
            else
            {
                [MBProgressHUD showError:@"上传失败"];
            }
        }
    }];

}

//从服务器上获取每天喝了多少水
-(void)getEveryDayWaterCountFromServices
{
//    [MBProgressHUD showMessage:@"正在拼命加载，请稍后..."];
    NSString *urlString = @"http://120.24.178.16:8080/Health/baby/getWaterDrinking.do";
    NSURL *url = [[NSURL alloc]initWithString:urlString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:2];
    request.HTTPMethod = @"POST";
    NSString *str = [NSString stringWithFormat:@"account=%@&username=%@&date=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"userAccount"],self.userName,[self getSystemTime]];
    
    NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:data];
    
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        if (data)
        {
            [MBProgressHUD hideHUD];
            
            // 请求成功
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
//            YMLog(@"fuwuqi water %@",dict);
            NSArray *arr = [dict objectForKey:@"data"];
            if(arr.firstObject != nil)
            {
                for(NSDictionary *dd in arr)
                {
                    self.everyDayWaterLabel.text = [NSString stringWithFormat:@"%@杯",[dd objectForKey:@"amount"]];
                    
                    if([[dd objectForKey:@"amount"] intValue] >= 8)
                    {
                        if(self.oneBtnFlag == YES)
                        {}
                        else
                        {
                        if(![self.switch4 isEqualToString:@"No"])
                        {
                            self.alreadyCompleteCount ++;
                        }
                        }
                    }
                }
            }
            YMLog(@"%@",[self getSystemTime]);
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.001 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self getShanShiTotalLiangFromServices];
            });
        }
    }];
    
}

//从服务器上获取每天摄入的营养量
-(void)getShanShiTotalLiangFromServices
{
    NSString *urlString = @"http://120.24.178.16:8080/Health/baby/getFoodanalysisresultinfo.do";
    NSURL *url = [[NSURL alloc]initWithString:urlString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:2];
    request.HTTPMethod = @"POST";
    NSString *str = [NSString stringWithFormat:@"account=%@&username=%@&date=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"userAccount"],self.userName,[self getSystemTime]];
    NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:data];
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (data)
        {
            [MBProgressHUD hideHUD];
           
//            YMLog(@"sheruliang = %f",self.totalSheRuliang);
            // 请求成功
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            if([dict objectForKey:@"data1"] != nil)
            {
            self.totalSheRuliang = [[dict objectForKey:@"data1"]floatValue];
            }
            NSArray *arr = [dict objectForKey:@"data"];
            if(arr.firstObject != nil)
            {
                for(NSDictionary *dd in arr)
                {
                    self.Sherucalori = [dd objectForKey:@"calori"];
                }
            }
            else
            {
                self.Sherucalori = @"0";
            }
            self.shanshiGuanliLabel.text = [NSString stringWithFormat:@"%.f/%.fkcal",[self.Sherucalori floatValue],self.totalSheRuliang];
            if([self.Sherucalori floatValue] >= self.totalSheRuliang)
            {
                if(![self.switch3 isEqualToString:@"No"])
                {
                    if(self.oneBtnFlag == YES)
                    {}
                    else
                    {
                        self.alreadyCompleteCount += 1;
                    }
                    
                }
            }
            self.displaySelectLabel.text = [NSString stringWithFormat:@"%d/%d",self.alreadyCompleteCount,self.totalCompleteCount];
             self.oneBtnFlag = NO;
        }
    }];
}

-(void)netWorkingJianCe
{
    // 1.获得网络监控的管理者
    AFNetworkReachabilityManager *mgr = [AFNetworkReachabilityManager sharedManager];
    
    // 2.设置网络状态改变后的处理
    [mgr setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        // 当网络状态改变了, 就会调用这个block
        switch (status) {
            case AFNetworkReachabilityStatusUnknown: // 未知网络
                break;
                
            case AFNetworkReachabilityStatusNotReachable: // 没有网络(断网)
                [MBProgressHUD showError:@"亲，网络连了吗？"];
                self.netFlag = NO;
                break;
                
            case AFNetworkReachabilityStatusReachableViaWWAN: // 手机自带网络
                [MBProgressHUD showSuccess:@"手机自带网络"];
                self.netFlag = YES;
                break;
                
            case AFNetworkReachabilityStatusReachableViaWiFi: // WIFI
                [MBProgressHUD showSuccess:@"wifi"];
                self.netFlag = YES;
                break;
        }
    }];
    
    // 3.开始监控
    [mgr startMonitoring];//这里开始监控了才会回调上面的Block
    //    mgr.isReachable//检查是否有网络
    //    mgr.isReachableViaWiFi
    //    mgr.isReachableViaWWAN
}



@end
