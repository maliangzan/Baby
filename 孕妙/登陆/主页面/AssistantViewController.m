//
//  AssistantViewController.m
//  孕妙
//
//  Created by 是源科技 on 16/4/13.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "AssistantViewController.h"
#import "ChanjianJiluViewController.h"
#import "ShiyongToolViewController.h"
#import "YunqiBaikeViewController.h"
#import "YunyuKetangViewController.h"
#import "FenMianbaikeViewController.h"
#import "YueZiBaikeViewController.h"
#import "XinshengerViewController.h"
#import "ErtongBaikeViewController.h"
#import "YinerbaikeViewController.h"
#import "YouerBaikeViewController.h"
#import "YimiaoguanliViewController.h"
#import "BeiYunBaiKeViewController.h"

@interface AssistantViewController ()<UIScrollViewDelegate>
@property (nonatomic,strong)UILabel *welcomeLabel;
@property (nonatomic,strong)UIScrollView *imageScrollView;
@property (nonatomic,strong)UIScrollView *toolScrollView;


@end

@implementation AssistantViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = YMColor(245, 245, 245);
    [self viewControllerLayout];
    
}

-(void)viewControllerLayout
{
    CGFloat y = 64;
    self.welcomeLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, y, kDeviceWidth, 30)];
    self.welcomeLabel.backgroundColor = [UIColor whiteColor];
    self.welcomeLabel.textColor = YMColor(149, 149, 149);
    self.welcomeLabel.font = [UIFont systemFontOfSize:14.0];
    self.welcomeLabel.text = [NSString stringWithFormat:@"  %@,  关爱您的孕期每一天",[passValue passValue].saveDataUserName];
    [self.view addSubview:self.welcomeLabel];
    y += self.welcomeLabel.frame.size.height;
    
    self.imageScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, y, kDeviceWidth, kDeviceWidth / 2.13)];
    self.imageScrollView.delegate = self;
    self.imageScrollView.tag = 1;
    [self.view addSubview:self.imageScrollView];
    self.imageScrollView.contentSize = CGSizeMake(kDeviceWidth, kDeviceWidth / 2.13);
    
    UIImageView *displayImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, self.imageScrollView.frame.size.height)];
    displayImageView.image = [UIImage imageNamed:@"img_bg_assistant"];
    [self.imageScrollView addSubview:displayImageView];
    y += self.imageScrollView.frame.size.height;
    
    UILabel *backGroundColor = [[UILabel alloc]initWithFrame:CGRectMake(0, y, kDeviceWidth, 8)];
    backGroundColor.backgroundColor = YMColor(216, 216, 216);
    [self.view addSubview:backGroundColor];
    
    y += backGroundColor.frame.size.height;
    self.toolScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, y, kDeviceWidth, kDeviceHeight - y)];
    self.toolScrollView.delegate = self;
    self.toolScrollView.tag = 2;
    [self.view addSubview:self.toolScrollView];
    
    CGFloat toolY = 40 + (kDeviceWidth - 2) / 3.0 * 2 + 2;
    CGFloat toolYY = toolY + 40 + (kDeviceWidth - 2) / 3.0;
    
    [self kindOfLayoutToViewandHeight:0 andtitle:@"孕期助手" andxhangshu:3 andyhangshu:2 andViewTag:1];
    [self kindOfLayoutToViewandHeight:toolY  andtitle:@"育儿助手" andxhangshu:3 andyhangshu:1 andViewTag:7];
    [self kindOfLayoutToViewandHeight:toolYY andtitle:@"备孕助手" andxhangshu:3 andyhangshu:1 andViewTag:13];
    float addH = 0;
    if(SYDevice5 || SYDevice4s)
    {
        addH = kDeviceWidth / 15.0;
    }
    else
    {
        addH = kDeviceWidth / 30.0;
    }
    
    self.toolScrollView.contentSize = CGSizeMake(kDeviceWidth, toolY * 2.5 - (kDeviceWidth - 2) / 5.5 + addH);
    
    
}

-(void)kindOfLayoutToViewandHeight:(CGFloat)h andtitle:(NSString *)str andxhangshu:(int)hangshuX andyhangshu:(int)hangshuY andViewTag:(int)viewTag
{
    UILabel *yunqiLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, h, kDeviceWidth, 40)];
    yunqiLabel.textColor = YMColor(191, 97, 145);
    yunqiLabel.text = str;
    yunqiLabel.textAlignment = NSTextAlignmentCenter;
    yunqiLabel.backgroundColor = [UIColor whiteColor];
    [self.toolScrollView addSubview:yunqiLabel];
    
    h += yunqiLabel.frame.size.height;
    
    CGFloat viewW = (kDeviceWidth - 2) / 3.0;
    //列
    for(int j = 0;j < hangshuY;j ++)
    {
        //行
        for(int i = 0;i < hangshuX;i ++)
        {
            UIView *view = [[UIView alloc]initWithFrame:CGRectMake(viewW * i + i, viewW * j + j + h, viewW, viewW)];
            view.tag = viewTag ++;
            [self.toolScrollView addSubview:view];
            
            if(view.tag == 1)
            {
                [self addButtonToView:view andImageName:@"assistant_Check" andTitleName:@"产检手册"];
            }
            if(view.tag == 2)
            {
                [self addButtonToView:view andImageName:@"assistant_Pregnancy" andTitleName:@"孕期百科"];
            }
            if(view.tag == 3)
            {
                [self addButtonToView:view andImageName:@"assistant_childbirth" andTitleName:@"分娩百科"];
            }
            if(view.tag == 4)
            {
//                [self addButtonToView:view andImageName:@"assistant_Class" andTitleName:@"孕育课堂"];
                [self addButtonToView:view andImageName:@"assistant_Tool" andTitleName:@"实用工具"];
                
            }
            if(view.tag == 5)
            {
//                [self addButtonToView:view andImageName:@"assistant_Tool" andTitleName:@"实用工具"];
            }
            if(view.tag == 7)
            {
//                [self addButtonToView:view andImageName:@"assistant_vaccine" andTitleName:@"疫苗管理"];
                 [self addButtonToView:view andImageName:@"assistant_yuezi" andTitleName:@"月子百科"];
               
            }
            if(view.tag == 8)
            {
//                [self addButtonToView:view andImageName:@"assistant_yuezi" andTitleName:@"月子百科"];
                [self addButtonToView:view andImageName:@"assistant_baby" andTitleName:@"新生儿百科"];
            }
            if(view.tag == 9)
            {
//                [self addButtonToView:view andImageName:@"assistant_baby" andTitleName:@"新生儿百科"];
            }
            if(view.tag == 10)
            {
//                [self addButtonToView:view andImageName:@"assistant_0-1" andTitleName:@"0-1岁百科"];
            }
            if(view.tag == 11)
            {
//                [self addButtonToView:view andImageName:@"assistant_1-3" andTitleName:@"1-3岁百科"];
            }
            if(view.tag == 12)
            {
//                [self addButtonToView:view andImageName:@"assistant_3-6" andTitleName:@"3-6岁百科"];
            }
            if(view.tag == 13)
            {
                [self addButtonToView:view andImageName:@"assistant_Prepare pregnant" andTitleName:@"备孕百科"];
            }
           
            
        }
    }
    for(int i = 1;i < hangshuY + 1;i++)
    {
        UILabel *crossLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, viewW * i + h, kDeviceWidth, 1)];
        crossLabel.backgroundColor = YMColor(228, 228, 228);
        [self.toolScrollView addSubview:crossLabel];
    }
    
    for(int i = 0;i < hangshuX;i ++)
    {
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(viewW * i, h, 1, viewW * hangshuY + 1)];
        label.backgroundColor = YMColor(228, 228, 228);
        [self.toolScrollView addSubview:label];
    }

}

-(void)addButtonToView:(UIView *)view andImageName:(NSString *)imageName andTitleName:(NSString *)titleName
{
    CGFloat w = view.frame.size.width;
    CGFloat h = view.frame.size.height;
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, w, h)];
    btn.tag = view.tag;
    [view addSubview:btn];
    [btn addTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(w / 3.0, w / 4.2, w / 3.0, w / 3.0)];
    imageView.image = [UIImage imageNamed:imageName];
    [view addSubview:imageView];
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, w / 4.2 + w / 3.0 + 3, w, 30)];
    titleLabel.text = titleName;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = YMColor(129, 129, 129);
    titleLabel.font = [UIFont systemFontOfSize:14.0];
    [view addSubview:titleLabel];
    
}
-(void)clickButton:(UIButton *)btn
{
    //产检手册
    if(btn.tag == 1)
    {
        ChanjianJiluViewController *chanjian = [[ChanjianJiluViewController alloc]init];
        [self.navigationController pushViewController:chanjian animated:YES];
    }
    //孕期百科
    if(btn.tag == 2)
    {
        YunqiBaikeViewController *yunqi = [[YunqiBaikeViewController alloc]init];
        [self.navigationController pushViewController:yunqi animated:YES];
    }
    //分娩百科
    if(btn.tag == 3)
    {
        FenMianbaikeViewController *fenmian = [[FenMianbaikeViewController alloc]init];
        [self.navigationController pushViewController:fenmian animated:YES];
    }
    //孕育课堂
    if(btn.tag == 4)
    {
//        YunyuKetangViewController *ketang = [[YunyuKetangViewController alloc]init];
//        [self.navigationController pushViewController:ketang animated:YES];
        ShiyongToolViewController *shiyongTool = [[ShiyongToolViewController alloc]init];
        [self.navigationController pushViewController:shiyongTool animated:YES];
    }
    //实用工具
    if(btn.tag == 5)
    {
//        ShiyongToolViewController *shiyongTool = [[ShiyongToolViewController alloc]init];
//        [self.navigationController pushViewController:shiyongTool animated:YES];
    }
    //疫苗管理
    if(btn.tag == 7)
    {
//        YimiaoguanliViewController *yimiao = [[YimiaoguanliViewController alloc]init];
//        [self.navigationController pushViewController:yimiao animated:YES];
        YueZiBaikeViewController *yuezi = [[YueZiBaikeViewController alloc]init];
        [self.navigationController pushViewController:yuezi animated:YES];
    }
    //月子百科
    if(btn.tag == 8)
    {
//        YueZiBaikeViewController *yuezi = [[YueZiBaikeViewController alloc]init];
//        [self.navigationController pushViewController:yuezi animated:YES];
        XinshengerViewController *xinshenger = [[XinshengerViewController alloc]init];
        [self.navigationController pushViewController:xinshenger animated:YES];

    }
    //新生儿百科
    if(btn.tag == 9)
    {
//        XinshengerViewController *xinshenger = [[XinshengerViewController alloc]init];
//        [self.navigationController pushViewController:xinshenger animated:YES];
    }
    //0-1岁百科
    if(btn.tag == 10)
    {
//        YinerbaikeViewController *yiner = [[YinerbaikeViewController alloc]init];
//        [self.navigationController pushViewController:yiner animated:YES];
    }
    //1-3岁百科
    if(btn.tag == 11)
    {
//        YouerBaikeViewController *yiner = [[YouerBaikeViewController alloc]init];
//        [self.navigationController pushViewController:yiner animated:YES];
    }
    //3-6岁百科
    if(btn.tag == 12)
    {
//        ErtongBaikeViewController *yiner = [[ErtongBaikeViewController alloc]init];
//        [self.navigationController pushViewController:yiner animated:YES];
    }
    //备孕百科
    if(btn.tag == 13)
    {
        BeiYunBaiKeViewController *beiyun = [[BeiYunBaiKeViewController alloc]init];
        [self.navigationController pushViewController:beiyun animated:YES];
    }
}
@end
