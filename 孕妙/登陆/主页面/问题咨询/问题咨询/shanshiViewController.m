//
//  shanshiViewController.m
//  孕妙
//
//  Created by 是源科技 on 16/5/4.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "shanshiViewController.h"

@interface shanshiViewController ()
@property (nonatomic,assign)CGFloat height;
@property (nonatomic,weak)UIScrollView *scrollView;

@end

@implementation shanshiViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.height = 0;
    UIScrollView *scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0 , kDeviceWidth, kDeviceHeight - 5)];
    scrollView.backgroundColor = [UIColor clearColor];
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.showsVerticalScrollIndicator = YES;
    self.scrollView = scrollView;
    
    self.view.backgroundColor = [UIColor whiteColor];
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, 60)];
    titleLabel.text = @"【膳食】怎样知道我的饮食是否合适？";
    titleLabel.textColor = YMColor(107, 107, 107);
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.font = [UIFont systemFontOfSize:18.0];
    [self.scrollView addSubview:titleLabel];
    self.height += titleLabel.frame.size.height;
    [self jisuanLabelHeightandString:@"1.在哪里可以对膳食进行管理？"];
    [self jisuanLabelHeightandString:@"在首页的【今日关注】中添加膳食管理条目，可对每天的饮食摄入进行管理和分析，系统会对一天6餐的营养摄入量进行统计，并给出专业的建议。"];
    
    self.scrollView.contentSize = CGSizeMake(kDeviceWidth, self.height + 5);
    [self.view addSubview:self.scrollView];
}

-(void)jisuanLabelHeightandString:(NSString *)string
{
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
    [self.scrollView addSubview:label];
    NSString *s2 = string;
    label.text = s2;
    label.textAlignment = NSTextAlignmentLeft;
    label.font = [UIFont systemFontOfSize:15.0];
    
    NSDictionary * attribute2 = @{NSFontAttributeName:[UIFont systemFontOfSize:15.0]};
    CGSize size2 = [s2 boundingRectWithSize:CGSizeMake(kDeviceWidth - 20, kDeviceHeight) options:NSStringDrawingUsesLineFragmentOrigin attributes:attribute2 context:nil].size;
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.numberOfLines = 0;
    label.frame = CGRectMake(10, self.height, kDeviceWidth - 20, size2.height);
    label.textColor = YMColor(107, 107, 107);
    self.height += size2.height+ 5;
}


@end
