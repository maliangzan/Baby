//
//  jiankangViewController.m
//  孕妙
//
//  Created by 是源科技 on 16/5/4.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "jiankangViewController.h"

@interface jiankangViewController ()
@property (nonatomic,assign)CGFloat height;
@property (nonatomic,weak)UIScrollView *scrollView;

@end

@implementation jiankangViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.height = 0;
    UIScrollView *scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0 , kDeviceWidth, kDeviceHeight - 5)];
    scrollView.backgroundColor = [UIColor clearColor];
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.showsVerticalScrollIndicator = YES;
    self.scrollView = scrollView;
    
    self.view.backgroundColor = [UIColor whiteColor];
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, 60)];
    titleLabel.text = @"【健康】如何监测身体健康？";
    titleLabel.textColor = YMColor(107, 107, 107);
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.font = [UIFont systemFontOfSize:18.0];
    [self.scrollView addSubview:titleLabel];
    self.height += titleLabel.frame.size.height;
    [self jisuanLabelHeightandString:@"1.如何在健康监测界面显示孕妇所测得的健康数据？"];
    [self jisuanLabelHeightandString:@"您可以在APP首页中的【今日关注】选择【健康监测】选项，进入到健康监测主界面，点击【管理设备】，进入到管理界面，选择您需要在健康监测界面上显示的健康数据选项。"];
    [self jisuanLabelHeightandString:@"关键字：健康数据，健康监测，管理设备。"];
    self.height += 10;
    [self jisuanLabelHeightandString:@"2.如何进行数据测量？"];
    [self jisuanLabelHeightandString:@"您可以在健康监测界面—点击【开始记录】，选择您要测量的项目，点击【添加】，数据测量有两种方式。系统会根据各账户的情况判断适合的数据测量方式，孕妇可以根据页面提示进行操作。"];
    [self jisuanLabelHeightandString:@"关键字：数据测量，记录数据。"];
    self.height += 10;
    [self jisuanLabelHeightandString:@"3.如何购买相关设备？"];
    [self jisuanLabelHeightandString:@"您可以在健康监测界面—点击【开始记录】，选择相对应的测量项目，点击【添加】，进入到数据测量界面，点击右上角的【购买】，进入到相关产品微店，即可购买。"];
    [self jisuanLabelHeightandString:@"关键字：购买设备，设备。"];
    self.scrollView.contentSize = CGSizeMake(kDeviceWidth, self.height + 5);
    [self.view addSubview:self.scrollView];
}

-(void)jisuanLabelHeightandString:(NSString *)string
{
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
    [self.scrollView addSubview:label];
    NSString *s2 = string;
    label.text = s2;
    label.textAlignment = NSTextAlignmentLeft;
    label.font = [UIFont systemFontOfSize:15.0];
    
    NSDictionary * attribute2 = @{NSFontAttributeName:[UIFont systemFontOfSize:15.0]};
    CGSize size2 = [s2 boundingRectWithSize:CGSizeMake(kDeviceWidth - 20, kDeviceHeight) options:NSStringDrawingUsesLineFragmentOrigin attributes:attribute2 context:nil].size;
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.numberOfLines = 0;
    label.frame = CGRectMake(10, self.height, kDeviceWidth - 20, size2.height);
    label.textColor = YMColor(107, 107, 107);
    self.height += size2.height+ 5;
}


@end
