//
//  questionZixunViewController.m
//  孕妙
//
//  Created by 是源科技 on 16/5/4.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "questionZixunViewController.h"

@interface questionZixunViewController ()<UIWebViewDelegate>
@property (nonatomic,strong)UIWebView *webView;

@end
static dispatch_once_t onceToken;
@implementation questionZixunViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"问题详情";
    self.webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, kDeviceHeight)];
    self.webView.backgroundColor = [UIColor whiteColor];
    [self.webView setUserInteractionEnabled:YES];//是否支持交互
    self.webView.delegate = self;
    self.webView.dataDetectorTypes = UIDataDetectorTypeAll;
    [self.webView setOpaque:NO];//opaque是不透明的意思
    [self.webView setScalesPageToFit:YES];//自动缩放以适应屏幕
    [self.view addSubview:self.webView];
    
    NSURL *url = [NSURL URLWithString:self.questionStr];
    [self.webView loadRequest:[NSURLRequest requestWithURL:url]];
}

//几个代理方法

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    dispatch_once(&onceToken, ^{
        [MBProgressHUD showMessage:@"正在拼命加载，请耐心等待"];
    });
    
}

- (void)webViewDidFinishLoad:(UIWebView *)web
{
//    if(web.isLoading)
//    {
//        return;
//    }
    [MBProgressHUD hideHUD];
}

-(void)webView:(UIWebView*)webView  DidFailLoadWithError:(NSError*)error
{
    [MBProgressHUD showError:@"网页加载出错,请稍后再试！"];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    onceToken = 0;
}


@end
