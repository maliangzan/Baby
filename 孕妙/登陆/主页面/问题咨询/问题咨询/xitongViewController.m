//
//  xitongViewController.m
//  孕妙
//
//  Created by 是源科技 on 16/5/4.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "xitongViewController.h"

@interface xitongViewController ()<UIScrollViewDelegate>
@property (nonatomic,assign)CGFloat height;
@property (nonatomic,weak)UIScrollView *scrollView;

@end

@implementation xitongViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.height = 0;
     UIScrollView *scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0 , kDeviceWidth, kDeviceHeight - 5)];
    scrollView.backgroundColor = [UIColor clearColor];
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.showsVerticalScrollIndicator = YES;
    self.scrollView = scrollView;
    
    self.view.backgroundColor = [UIColor whiteColor];
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, 60)];
    titleLabel.text = @"【系统】忘记密码怎么办？";
    titleLabel.textColor = YMColor(107, 107, 107);
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.font = [UIFont systemFontOfSize:18.0];
    [self.scrollView addSubview:titleLabel];
    self.height += titleLabel.frame.size.height;
    [self jisuanLabelHeightandString:@"1.忘记幸孕儿账号密码，怎么找回？"];
    [self jisuanLabelHeightandString:@"请您在幸孕儿登录页面点击【找回密码】，根据页面提示的方式进行操作（如图示）。"];
    UIImageView *image1 = [[UIImageView alloc]initWithFrame:CGRectMake(kDeviceWidth / 3.0, self.height, kDeviceWidth / 3.0, kDeviceWidth / 3.0 * 1.778)];
    image1.image = [UIImage imageNamed:@"image1.jpg"];
    [self.scrollView addSubview:image1];
    self.height += image1.frame.size.height + 2;
    [self jisuanLabelHeightandString:@"关键字：密码，忘记密码，找回密码。"];
    self.height += 10;
    [self jisuanLabelHeightandString:@"2.怎样修改密码？"];
    [self jisuanLabelHeightandString:@"请您登陆幸孕儿账号后，进入到个人中心界面，点击【设置】，进入到设置界面后再点击【修改密码】，根据页面提示的方式进行操作（如图示）。"];
    UIImageView *image2 = [[UIImageView alloc]initWithFrame:CGRectMake(kDeviceWidth / 9.0, self.height, kDeviceWidth / 3.0, kDeviceWidth / 3.0 * 1.778)];
    image2.image = [UIImage imageNamed:@"image2.jpg"];
    [self.scrollView addSubview:image2];
    UIImageView *image3 = [[UIImageView alloc]initWithFrame:CGRectMake(kDeviceWidth / 9.0 * 2 + kDeviceWidth / 3.0, self.height, kDeviceWidth / 3.0, kDeviceWidth / 3.0 * 1.778)];
    image3.image = [UIImage imageNamed:@"image3.jpg"];
    [self.scrollView addSubview:image3];
    self.height += image2.frame.size.height + 2;
    [self jisuanLabelHeightandString:@"关键字：密码，修改密码。"];
    self.height += 10;
    [self jisuanLabelHeightandString:@"3.如何修改绑定的手机号码？"];
    [self jisuanLabelHeightandString:@"您可以登录幸孕儿账号，直接进入到个人中心【设置】界面，点击【绑定手机】，进入到【更换手机号码】界面进行修改。如图示。"];
    UIImageView *image4 = [[UIImageView alloc]initWithFrame:CGRectMake(kDeviceWidth / 9.0, self.height, kDeviceWidth / 3.0, kDeviceWidth / 3.0 * 1.778)];
    image4.image = [UIImage imageNamed:@"image2.jpg"];
    [self.scrollView addSubview:image4];
    UIImageView *image5 = [[UIImageView alloc]initWithFrame:CGRectMake(kDeviceWidth / 9.0 * 2 + kDeviceWidth / 3.0, self.height, kDeviceWidth / 3.0, kDeviceWidth / 3.0 * 1.778)];
    image5.image = [UIImage imageNamed:@"image3.jpg"];
    [self.scrollView addSubview:image5];
    self.height += image4.frame.size.height + 2;
    [self jisuanLabelHeightandString:@"关键字：手机号码，修改手机号，更换手机号码。"];
    self.height += 10;
    [self jisuanLabelHeightandString:@"4.如何修改我的状态？"];
    [self jisuanLabelHeightandString:@"您可以登录幸孕儿账号，直接进入到个人中心界面，点击【我的状态】，修改您目前的状态"];
    [self jisuanLabelHeightandString:@"关键字：状态，我的状态。"];
    self.height += 10;
    [self jisuanLabelHeightandString:@"5.如何设置每日关注提醒？"];
    [self jisuanLabelHeightandString:@"您可以在APP主界面—今日关注—点击【添加关注】，进入到关注管理界面，选择相对应的关注项目，开启关注后，会出现在主界面的今日关注中，会在对应的时间提醒孕妇。"];
    [self jisuanLabelHeightandString:@"关键字：每日关注，关注提醒，添加关注。"];
    
    self.scrollView.contentSize = CGSizeMake(kDeviceWidth, self.height + 5);
    [self.view addSubview:self.scrollView];
}

-(void)jisuanLabelHeightandString:(NSString *)string
{
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
    [self.scrollView addSubview:label];
    NSString *s2 = string;
    label.text = s2;
    label.textAlignment = NSTextAlignmentLeft;
    label.font = [UIFont systemFontOfSize:15.0];
    
    NSDictionary * attribute2 = @{NSFontAttributeName:[UIFont systemFontOfSize:15.0]};
    CGSize size2 = [s2 boundingRectWithSize:CGSizeMake(kDeviceWidth - 20, kDeviceHeight) options:NSStringDrawingUsesLineFragmentOrigin attributes:attribute2 context:nil].size;
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.numberOfLines = 0;
    label.frame = CGRectMake(10, self.height, kDeviceWidth - 20, size2.height);
    label.textColor = YMColor(107, 107, 107);
    self.height += size2.height+ 5;
}

@end
