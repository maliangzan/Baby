//
//  baikeViewController.m
//  孕妙
//
//  Created by 是源科技 on 16/5/4.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "baikeViewController.h"

@interface baikeViewController ()
@property (nonatomic,assign)CGFloat height;
@property (nonatomic,weak)UIScrollView *scrollView;

@end

@implementation baikeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.height = 0;
    UIScrollView *scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0 , kDeviceWidth, kDeviceHeight - 5)];
    scrollView.backgroundColor = [UIColor clearColor];
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.showsVerticalScrollIndicator = YES;
    self.scrollView = scrollView;
    
    self.view.backgroundColor = [UIColor whiteColor];
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, 60)];
    titleLabel.text = @"【百科】怎样分享百科内容给朋友？";
    titleLabel.textColor = YMColor(107, 107, 107);
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.font = [UIFont systemFontOfSize:18.0];
    [self.scrollView addSubview:titleLabel];
    self.height += titleLabel.frame.size.height;
    [self jisuanLabelHeightandString:@"1.如何分享百科内容？"];
    [self jisuanLabelHeightandString:@"进入百科内容页面，点击右上角分享标志，选择分享应用。"];
    self.height += 10;
    [self jisuanLabelHeightandString:@"2.百科内容在哪里可以找到？"];
    [self jisuanLabelHeightandString:@"首页点击进入【百科助手】，里面有备孕、怀孕、育儿三大部分百科，请选择你关心的百科内容进行浏览，或者回复关键字进行搜索。"];
    self.scrollView.contentSize = CGSizeMake(kDeviceWidth, self.height + 5);
    [self.view addSubview:self.scrollView];
}

-(void)jisuanLabelHeightandString:(NSString *)string
{
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
    [self.scrollView addSubview:label];
    NSString *s2 = string;
    label.text = s2;
    label.textAlignment = NSTextAlignmentLeft;
    label.font = [UIFont systemFontOfSize:15.0];
    
    NSDictionary * attribute2 = @{NSFontAttributeName:[UIFont systemFontOfSize:15.0]};
    CGSize size2 = [s2 boundingRectWithSize:CGSizeMake(kDeviceWidth - 20, kDeviceHeight) options:NSStringDrawingUsesLineFragmentOrigin attributes:attribute2 context:nil].size;
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.numberOfLines = 0;
    label.frame = CGRectMake(10, self.height, kDeviceWidth - 20, size2.height);
    label.textColor = YMColor(107, 107, 107);
    self.height += size2.height+ 5;
}


@end
