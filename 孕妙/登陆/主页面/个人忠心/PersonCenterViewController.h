//
//  PersonCenterViewController.h
//  孕妙
//
//  Created by 是源科技 on 16/4/14.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PersonCenterViewController : UIViewController
@property (nonatomic,assign)int weekCount;
@property (nonatomic,assign)int dayCount;
@property (nonatomic,copy)NSString *userName;
@property (nonatomic,copy)NSString *weightString;
@property (nonatomic,copy)NSString *heightString;
@property (nonatomic,copy)NSString *birthdayString;


@end
