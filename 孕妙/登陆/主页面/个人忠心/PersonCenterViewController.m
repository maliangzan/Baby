//
//  PersonCenterViewController.m
//  孕妙
//
//  Created by 是源科技 on 16/4/14.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "PersonCenterViewController.h"
#import "BasicViewController.h"
#import "LoginViewController.h"
#import "PerfectInformationViewController.h"
#import "SettingViewController.h"
#import "HelpAndFeedbackViewController.h"



@interface PersonCenterViewController ()
@property (nonatomic,strong)UIImageView *touxiangImageView;
@property (nonatomic,strong)UILabel *userNameLabel;
@property (nonatomic,strong)UILabel *userIDlabel;
@property (nonatomic,strong)UILabel *weekLabel;
@property (nonatomic,strong)UILabel *dayLabel;
@property (nonatomic,strong)UILabel *yunqiLabel;

@property (nonatomic,strong)UILabel *yunfuState;
@property (nonatomic,strong)UIButton *exixButton;
@property (nonatomic,strong)UILabel *infoLabel;


@end

@implementation PersonCenterViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"个人中心";
    [self allKindOfControllerLayout];
    if(self.userName != nil)
    {
        [self getTouxiangImageFromServices];
    }
}

//页面布局
-(void)allKindOfControllerLayout
{
    CGFloat h = 64;
    UIView *firstView = [[UIView alloc]initWithFrame:CGRectMake(0, h, kDeviceWidth, kDeviceHeight / 5.0)];
    firstView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:firstView];
    CGFloat firstH = firstView.frame.size.height;
    
    self.touxiangImageView = [[UIImageView alloc]initWithFrame:CGRectMake(20, firstH / 6.0, firstH / 3.0 * 2, firstH / 3.0 * 2)];
    self.touxiangImageView.clipsToBounds = YES;
    self.touxiangImageView.contentMode = UIViewContentModeScaleAspectFit;
    self.touxiangImageView.image = [UIImage imageNamed:@"personal_Portrait"];
    self.touxiangImageView.layer.cornerRadius = self.touxiangImageView.frame.size.width / 2.0;
    [firstView addSubview:self.touxiangImageView];
    
    self.userNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(50 + firstH / 3.0 * 2, 25 , kDeviceWidth - 10 - (50 + firstH / 3.0 * 2), firstH / 3.0)];
    self.userNameLabel.text = self.userName;
//    self.userNameLabel.font = [UIFont systemFontOfSize:15.0];
    self.userNameLabel.textColor = YMColor(101, 101, 101);
    self.userNameLabel.numberOfLines = 0;
//    self.userNameLabel.textAlignment = NSTextAlignmentCenter;
    self.userIDlabel.adjustsFontSizeToFitWidth = YES;
    [firstView addSubview:self.userNameLabel];
    
    
//    UILabel *IdLabel = [[UILabel alloc]initWithFrame:CGRectMake(25 + firstH / 3.0 * 2, firstH / 2.0, 100, 30)];
//    IdLabel.textColor = YMColor(169, 169, 169);
//    IdLabel.text = @"用户ID:";
//    IdLabel.font = [UIFont systemFontOfSize:15.0];
//    [firstView addSubview:IdLabel];
//    
//    self.userIDlabel = [[UILabel alloc]initWithFrame:CGRectMake(25 + firstH / 3.0 * 2, firstH / 2.0 + 18, 200, 30)];
//    self.userIDlabel.text = @"sz20100122213";
//    self.userIDlabel.textColor = YMColor(2, 198, 157);
//    self.userIDlabel.font = [UIFont systemFontOfSize:14.0];
//    [firstView addSubview:self.userIDlabel];
    
    self.infoLabel = [[UILabel alloc]initWithFrame:CGRectMake(50 + firstH / 3.0 * 2, firstH / 3.0 + 18, kDeviceWidth / 2.0, firstH / 2.0)];
    self.infoLabel.numberOfLines = 0;
    self.infoLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.infoLabel.textColor = YMColor(179, 179, 179);
    self.infoLabel.font = [UIFont systemFontOfSize:15.0];
    [firstView addSubview:self.infoLabel];
    
    NSString *yunqiString = nil;
    if(self.weekCount <= 12)
    {
        yunqiString = @"孕早期";
    }
    if(self.weekCount > 12 && self.weekCount <= 27)
    {
        yunqiString = @"孕中期";
    }
    if(self.weekCount > 27)
    {
        yunqiString = @"孕晚期";
    }
    [self displayLabel:self.infoLabel andWeekCount:self.weekCount andDayCount:self.dayCount andYunqi:yunqiString];
    
    h += firstView.frame.size.height + 8;
    for(int i = 1;i < 5; i ++)
    {
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, h, kDeviceWidth, kDeviceHeight / 10.0)];
        view.backgroundColor = [UIColor whiteColor];
        view.tag = i;
        [self.view addSubview:view];
        h += view.frame.size.height + 8;
        
        switch (view.tag) {
            case 1:
                [self addkindofControllerToView:view andimageName:@"personal_mystate" andmiaoshuString:@"我的状态"];
                self.yunfuState = [[UILabel alloc]initWithFrame:CGRectMake(view.frame.size.width / 2.0, 0, view.frame.size.width / 3.0, view.frame.size.height)];
                self.yunfuState.textColor = YMColor(252, 165, 72);
                self.yunfuState.text = @"怀孕状态";
                self.yunfuState.textAlignment = NSTextAlignmentCenter;
                [view addSubview:self.yunfuState];
                break;
            case 2:
                [self addkindofControllerToView:view andimageName:@"personal_basicinfo" andmiaoshuString:@"基本信息"];
                break;
            case 3:
                [self addkindofControllerToView:view andimageName:@"personal_help&Fb" andmiaoshuString:@"帮助与反馈"];
                break;
            case 4:
                [self addkindofControllerToView:view andimageName:@"personal_setting" andmiaoshuString:@"设置"];
                break;
                
            default:
                break;
        }
    }
    
    h += (kDeviceHeight - 64 - h) / 5.0 * 2;
    
    self.exixButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.exixButton.frame = CGRectMake(kDeviceWidth / 3.0, h, kDeviceWidth / 3.0, kDeviceWidth / 3.0 / 2.22);
    self.exixButton.backgroundColor = YMColor(252, 160, 207);
    [self.exixButton setTitle:@"退出登录" forState:UIControlStateNormal];
    [self.exixButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.exixButton.layer.cornerRadius = 8.0;
    [self.view addSubview:self.exixButton];
    
    [self.exixButton addTarget:self action:@selector(tuichuLogin) forControlEvents:UIControlEventTouchUpInside];
}

//从服务器获取照片
-(void)getTouxiangImageFromServices
{
//    YMLog(@"账号  %@  用户名  %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"userAccount"],self.userName);
    
    NSString *urlString = @"http://120.24.178.16:8080/Health/baby/getPhoto.do";
    NSURL *url = [[NSURL alloc]initWithString:urlString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:2];
    
    request.HTTPMethod = @"POST";
    NSString *str = [NSString stringWithFormat:@"username=%@&account=%@",self.userName,[[NSUserDefaults standardUserDefaults]objectForKey:@"userAccount"]];
    
    NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:data];
    
    //        NSOperationQueue *queue = [NSOperationQueue mainQueue];
    NSOperationQueue *queue = [[NSOperationQueue alloc]init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (data)
        {
            // 请求成功
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
//            YMLog(@"dict = %@",dict);
            NSArray *array = [dict objectForKey:@"data"];
            if(array.count != 0)
            {
                for(NSDictionary *dd in array)
                {
                    NSString *imageUrl = [dd objectForKey:@"imageurl"];
                    [self downLoadBabyImageFromInternet:imageUrl];
                }
            }
        }
    }];
}

//从网络下载用户头像
-(void)downLoadBabyImageFromInternet:(NSString *)str
{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *str1 = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url1 = [NSURL URLWithString:str1];
        NSData *resultData1 = [NSData dataWithContentsOfURL:url1];
        UIImage *img1 = [UIImage imageWithData:resultData1];
        dispatch_async(dispatch_get_main_queue(), ^{
            self.touxiangImageView.image = img1;
            });
        });
}


//主页显示怀孕周数和天数 以及处于什么期
-(void)displayLabel:(UILabel *)label andWeekCount:(int)weekCount andDayCount:(int)dayCount andYunqi:(NSString *)yunqiStr
{
    NSString *yuanshiStr = [NSString stringWithFormat:@"您已孕 %d 周 %d 天\n目前处于%@",weekCount,dayCount,yunqiStr];
    if(weekCount <= 9)
    {
        NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:yuanshiStr];
        [str addAttribute:NSForegroundColorAttributeName value:YMColor(54, 195, 152) range:NSMakeRange(4,1)];
        [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:19.0] range:NSMakeRange(4, 1)];
        [str addAttribute:NSForegroundColorAttributeName value:YMColor(54, 195, 152) range:NSMakeRange(8,1)];
        [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:19.0] range:NSMakeRange(8, 1)];
        [str addAttribute:NSForegroundColorAttributeName value:YMColor(54, 195, 152) range:NSMakeRange(16,3)];
        [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:19.0] range:NSMakeRange(16, 3)];
        label.attributedText = str;
    }
    if(weekCount > 9)
    {
        NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:yuanshiStr];
        [str addAttribute:NSForegroundColorAttributeName value:YMColor(54, 195, 152) range:NSMakeRange(4,2)];
        [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:19.0] range:NSMakeRange(4, 2)];
        [str addAttribute:NSForegroundColorAttributeName value:YMColor(54, 195, 152) range:NSMakeRange(9,1)];
        [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:19.0] range:NSMakeRange(9, 1)];
        [str addAttribute:NSForegroundColorAttributeName value:YMColor(54, 195, 152) range:NSMakeRange(17,3)];
        [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:19.0] range:NSMakeRange(17, 3)];
        label.attributedText = str;
    }
}

-(void)addkindofControllerToView:(UIView *)view andimageName:(NSString *)imageName andmiaoshuString:(NSString *)miaoshuStr
{
    CGFloat h = view.frame.size.height;
    CGFloat w = view.frame.size.width;
    
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, w, h)];
    btn.tag = view.tag;
    [view addSubview:btn];
    
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(30, h / 4.0, h / 2.0, h / 2.0)];
    imageView.image = [UIImage imageNamed:imageName];
    [view addSubview:imageView];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(50 + h / 2.0, 0, w / 2.0, h)];
    label.text = miaoshuStr;
    label.textColor = YMColor(195, 99, 148);
    [view addSubview:label];
    UIImageView *jiantouImageView = [[UIImageView alloc]initWithFrame:CGRectMake(w - h / 4.0 - 30, (h - h / 4.0) / 2.0, h / 4.0, h / 4.0)];
    jiantouImageView.image = [UIImage imageNamed:@"personal_》small"];
    [view addSubview:jiantouImageView];
    [btn addTarget:self action:@selector(clickDifferentButton:) forControlEvents:UIControlEventTouchUpInside];
    
}
-(void)clickDifferentButton:(UIButton *)btn
{
    if(btn.tag == 1)
    {
        PerfectInformationViewController *prefect = [[PerfectInformationViewController alloc]init];
        [self.navigationController pushViewController:prefect animated:YES];
        prefect.touxiangImage = self.touxiangImageView.image;
    }
    if(btn.tag == 2)
    {
        BasicViewController *basci = [[BasicViewController alloc]init];
        [self.navigationController pushViewController:basci animated:YES];
        basci.nameString = self.userName;
        basci.birthdayString = self.birthdayString;
        basci.weightString = self.weightString;
        basci.heightString = self.heightString;
    }
    if(btn.tag == 4)
    {
       SettingViewController *setting = [[SettingViewController alloc]init];
        [self.navigationController pushViewController:setting animated:YES];
    }
    if(btn.tag == 3)
    {
        HelpAndFeedbackViewController *help = [[HelpAndFeedbackViewController alloc]init];
        [self.navigationController pushViewController:help animated:YES];
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    [self getTouxiangImageFromServices];
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
}

-(void)tuichuLogin
{
    AppDelegate *appde = (AppDelegate *)[UIApplication sharedApplication].delegate;
    UIWindow *window = appde.window;
    sayesTabbarViewController *loginN = [[sayesTabbarViewController alloc]init];
    LoginViewController *login = [[LoginViewController alloc]init];
    [loginN addOneChlildVc:login title:@"" imageName:@"" selectedImageName:@""];
    window.rootViewController = loginN;
    
    [[NSUserDefaults standardUserDefaults]setObject:@"notSave" forKey:@"saveFlag"];
}

@end
