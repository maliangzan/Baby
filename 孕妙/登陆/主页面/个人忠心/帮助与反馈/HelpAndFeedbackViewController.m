//
//  HelpAndFeedbackViewController.m
//  孕妙
//
//  Created by 是源科技 on 16/4/15.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "HelpAndFeedbackViewController.h"
#import "GuanyuYunmiaoViewController.h"
#import "UserXieyiViewController.h"
#import "YijianFankuiViewController.h"
#import "ContactUSViewController.h"

@interface HelpAndFeedbackViewController ()

@end

@implementation HelpAndFeedbackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"帮助与反馈";
    self.view.backgroundColor = YMColor(245, 245, 245);;
    CGFloat totalH = kDeviceHeight - 64;
    CGFloat oneh = (totalH - 5) / 6.0;
    for(int i = 0;i < 6;i ++)
    {
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 64 + oneh * i + 1, kDeviceWidth, oneh)];
        view.tag = i;
        [self.view addSubview:view];
        
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 64 + oneh * i, kDeviceWidth, 1)];
        label.backgroundColor = YMColor(225, 225, 225);
        [self.view addSubview:label];
        
        if(view.tag == 0)
        {
            [self addButtonToView:view andImageName:@"help_联系我们icon" andTitleName:@"联系我们"];
        }
        if(view.tag == 1)
        {
            [self addButtonToView:view andImageName:@"help_意见反馈icon" andTitleName:@"意见反馈"];
        }
        if(view.tag == 2)
        {
            [self addButtonToView:view andImageName:@"help_给个好评icon" andTitleName:@"给个好评"];
        }
        if(view.tag == 3)
        {
            [self addButtonToView:view andImageName:@"help_用户协议icon" andTitleName:@"用户协议"];
        }
        if(view.tag == 4)
        {
            [self addButtonToView:view andImageName:@"icon_help_guanyu" andTitleName:@"关于幸孕儿"];
        }
        if(view.tag == 5)
        {
            [self addButtonToView:view andImageName:@"help_客服热线icon" andTitleName:@"400-800-4046"];
        }
    }
}

-(void)addButtonToView:(UIView *)view andImageName:(NSString *)imageName andTitleName:(NSString *)titleName
{
    CGFloat w = view.frame.size.width;
    CGFloat h = view.frame.size.height;
    
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(30, h / 6.0, h / 3.0 * 2, h / 3.0 * 2)];
    imageView.image = [UIImage imageNamed:imageName];
    [view addSubview:imageView];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(w / 4.0, 0, w / 2.0, h)];
    label.text = titleName;
    label.textColor = YMColor(140, 140, 140);
    label.textAlignment = NSTextAlignmentCenter;
    [view addSubview:label];
    
    UIImageView *jiantouImageView = [[UIImageView alloc]initWithFrame:CGRectMake(w - 30 - h / 3.0, h / 3.0, h / 3.0, h / 3.0)];
    jiantouImageView.image = [UIImage imageNamed:@"help_》"];
    [view addSubview:jiantouImageView];
    
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, w, h)];
    [view addSubview:btn];
    btn.tag = view.tag;
    [btn addTarget:self action:@selector(checkComplaneInforMation:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)checkComplaneInforMation:(UIButton *)btn
{
    if(btn.tag == 0)
    {
        ContactUSViewController *contact = [[ContactUSViewController alloc]init];
        [self.navigationController pushViewController:contact animated:YES];
    }
    if(btn.tag == 1)
    {
//        YijianFankuiViewController *yijian = [[YijianFankuiViewController alloc]init];
//        [self.navigationController pushViewController:yijian animated:YES];
    }
    if(btn.tag == 2)
    {
        NSString *urlStr = @"https://itunes.apple.com/us/app/yun-miao/id1092462639?mt=8";
        NSURL *url = [NSURL URLWithString:urlStr];
        [[UIApplication sharedApplication]openURL:url];
    }
    if(btn.tag == 3)
    {
        UserXieyiViewController *user = [[UserXieyiViewController alloc]init];
        [self.navigationController pushViewController:user animated:YES];
    }
    if(btn.tag == 4)
    {
        GuanyuYunmiaoViewController *guanyu = [[GuanyuYunmiaoViewController alloc]init];
        [self.navigationController pushViewController:guanyu animated:YES];
    }
    if(btn.tag == 5)
    {
        NSString *number = @"4008004046";// 此处读入电话号码
        // NSString *num = [[NSString alloc]initWithFormat:@"tel://%@",number]; //number为号码字符串 如果使用这个方法结束电话之后会进入联系人列表
        NSString *num = [[NSString alloc]initWithFormat:@"telprompt://%@",number]; //而这个方法则打电话前先弹框 是否打电话 然后打完电话之后回到程序中 网上说这个方法可能不合法 无法通过审核
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:num]]; //拨号
    }
}
@end
