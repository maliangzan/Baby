//
//  GuanyuYunmiaoViewController.m
//  孕妙
//
//  Created by 是源科技 on 16/4/16.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "GuanyuYunmiaoViewController.h"

@interface GuanyuYunmiaoViewController ()

@end

@implementation GuanyuYunmiaoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"关于幸孕儿";
    self.view.backgroundColor = YMColor(245, 245, 245);
    UIImageView *logoImageView = [[UIImageView alloc]initWithFrame:CGRectMake(kDeviceWidth / 4.0, kDeviceHeight / 3.2, kDeviceWidth / 2.0, kDeviceWidth / 2.0)];
    logoImageView.image = [UIImage imageNamed:@"help_孕妙icon"];
    [self.view addSubview:logoImageView];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, kDeviceHeight / 3.2 + kDeviceWidth / 2.0 + 5, kDeviceWidth, 30)];
    label.text = @"幸孕儿V1.2.2";
    label.textColor = YMColor(144, 144, 144);
    label.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:label];
    
    UILabel *jianjieLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, kDeviceHeight - 60, kDeviceWidth, 40)];
    jianjieLabel.textColor = YMColor(126, 126, 126);
    jianjieLabel.textAlignment = NSTextAlignmentCenter;
    jianjieLabel.text = @"深圳市是源医学科技有限公司版权所有";
    [self.view addSubview:jianjieLabel];
    
}


@end
