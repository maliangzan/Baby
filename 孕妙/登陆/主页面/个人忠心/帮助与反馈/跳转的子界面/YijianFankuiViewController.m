//
//  YijianFankuiViewController.m
//  孕妙
//
//  Created by 是源科技 on 16/4/16.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "YijianFankuiViewController.h"

@interface YijianFankuiViewController ()
@property (nonatomic,strong)UITextField *questionTF;
@property (nonatomic,strong)UIButton *sendButton;

@end

@implementation YijianFankuiViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"意见反馈";
    self.view.backgroundColor = YMColor(245, 245, 245);
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(20, 80, kDeviceWidth, 30)];
    label.textColor = YMColor(156, 156, 156);
    label.font = [UIFont systemFontOfSize:15.0];
    label.text = @"您好，有什么意见和建议，欢迎向我吐槽~";
    [self.view addSubview:label];
    
    _questionTF = [[UITextField alloc]initWithFrame:CGRectMake(10, kDeviceHeight - 50, kDeviceWidth - 100, 35)];
    _questionTF.layer.cornerRadius = 8.0;
    _questionTF.layer.borderWidth = 1.0;
    _questionTF.layer.borderColor = [YMColor(187, 187, 187)CGColor];
    _questionTF.font = [UIFont systemFontOfSize:15.0];
    _questionTF.placeholder = @"点击输入您的问题";
    _questionTF.borderStyle = UITextBorderStyleRoundedRect;
    [self.view addSubview:_questionTF];
    
    self.sendButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.sendButton.frame = CGRectMake(kDeviceWidth - 85, kDeviceHeight - 50, 75, 35);
    self.sendButton.layer.cornerRadius = 8.0;
    self.sendButton.backgroundColor = YMColor(252, 160, 207);
    [self.sendButton setTitle:@"发  送" forState:UIControlStateNormal];
    [self.sendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.view addSubview:self.sendButton];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.questionTF resignFirstResponder];
}

@end
