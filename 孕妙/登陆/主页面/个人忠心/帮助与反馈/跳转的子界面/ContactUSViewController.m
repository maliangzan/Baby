//
//  ContactUSViewController.m
//  孕妙
//
//  Created by 是源科技 on 16/4/16.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "ContactUSViewController.h"

@interface ContactUSViewController ()

@end

@implementation ContactUSViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"联系我们";
    self.view.backgroundColor = YMColor(245, 245, 245);
    CGFloat y = 110;
    
    UIImageView *logoImageView = [[UIImageView alloc]initWithFrame:CGRectMake(kDeviceWidth / 3.0, y, kDeviceWidth / 3.0, kDeviceWidth / 3.0 / 4.7)];
    logoImageView.image = [UIImage imageNamed:@"help_是源logo"];
    [self.view addSubview:logoImageView];
    y += logoImageView.frame.size.height + 15;
    
    UILabel *logoTitle = [[UILabel alloc]initWithFrame:CGRectMake(0, y, kDeviceWidth, 50)];
    logoTitle.text = @"深圳市是源医学科技有限公司";
    logoTitle.textAlignment = NSTextAlignmentCenter;
    logoTitle.textColor = YMColor(153, 153, 153);
    logoTitle.font = [UIFont systemFontOfSize:18.0];
    [self.view addSubview:logoTitle];
    y += 30;
    
    UILabel *englishLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, y, kDeviceWidth , 50)];
    englishLabel.textColor = YMColor(153, 153, 153);
    englishLabel.textAlignment = NSTextAlignmentCenter;
    englishLabel.text = @"Sayes Medical Technology Co.,Ltd";
    englishLabel.font = [UIFont systemFontOfSize:15.0];
    [self.view addSubview:englishLabel];
    y += 40;
//    UILabel *addressLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, y, kDeviceWidth, 30)];
//    addressLabel.textColor = YMColor(153, 153, 153);
//    addressLabel.text = @"地址：深圳市龙华新区民清路光辉科技园2栋2单元2楼";
//    addressLabel.textAlignment = NSTextAlignmentCenter;
//    addressLabel.font = [UIFont systemFontOfSize:14.0];
//    [self.view addSubview:addressLabel];
//    y += 20;
    
    UILabel *addressLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
    [self.view addSubview:addressLabel];
    NSString *s2 = @"地址：深圳市龙华新区民清路光辉科技园2栋2单元2楼";
    addressLabel.text = s2;
    addressLabel.textAlignment = NSTextAlignmentLeft;
    addressLabel.font = [UIFont systemFontOfSize:14.0];
    
    NSDictionary * attribute2 = @{NSFontAttributeName:[UIFont systemFontOfSize:14.0]};
    CGSize size2 = [s2 boundingRectWithSize:CGSizeMake(kDeviceWidth / 7.0 * 5, kDeviceHeight * 5) options:NSStringDrawingUsesLineFragmentOrigin attributes:attribute2 context:nil].size;
    addressLabel.lineBreakMode = NSLineBreakByWordWrapping;
    addressLabel.numberOfLines = 0;
    addressLabel.textColor = YMColor(153, 153, 153);
    addressLabel.frame = CGRectMake(kDeviceWidth / 7.0, y, kDeviceWidth / 7.0 * 6 - 20, size2.height);
    y += size2.height - 4;
    
    
    for(int i = 0;i < 4;i++)
    {
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(kDeviceWidth / 7.0, y + i * 20, kDeviceWidth, 30)];
        label.textColor = YMColor(153, 153, 153);
        label.font = [UIFont systemFontOfSize:14.0];
        label.tag = i + 1;
        [self.view addSubview:label];
        if(label.tag == 1)
        {
            label.text = @"传真：0755-29822478";
        }
        if(label.tag == 2)
        {
            label.text = @"电话：0755-83934040";
        }
        if(label.tag == 3)
        {
            label.text = @"客服热线：400-800-4060";
        }
        if(label.tag == 4)
        {
            label.text = @"网址：www.sayesmed.com";
        }
        
    }
    
}


@end
