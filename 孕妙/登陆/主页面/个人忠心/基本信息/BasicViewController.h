//
//  BasicViewController.h
//  孕妙
//
//  Created by 是源科技 on 16/4/14.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BasicViewController : UIViewController
@property (nonatomic,copy)NSString *nameString;
@property (nonatomic,copy)NSString *birthdayString;
@property (nonatomic,copy)NSString *heightString;
@property (nonatomic,copy)NSString *weightString;
@end
