//
//  BasicViewController.m
//  孕妙
//
//  Created by 是源科技 on 16/4/14.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "BasicViewController.h"


@interface BasicViewController ()



@end

@implementation BasicViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"基本信息";
    self.weightString = [NSString stringWithFormat:@"%@ kg",self.weightString];
    self.heightString = [NSString stringWithFormat:@"%@ cm",self.heightString];
    self.birthdayString = [NSString stringWithFormat:@"%@",self.birthdayString];
    
    CGFloat h = 80;
    for(int i = 1;i < 5;i ++)
    {
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, h, kDeviceWidth, kDeviceWidth / 6.4)];
        view.backgroundColor = [UIColor whiteColor];
        view.tag = i;
        [self.view addSubview:view];
        h += view.frame.size.height + 1.5;
        switch (view.tag) {
            case 1:
                [self addname:@"姓       名" andInfor:self.nameString toView:view];
                break;
                break;
            case 2:
                [self addname:@"生       日" andInfor:self.birthdayString toView:view];
                break;
            case 3:
                [self addname:@"身       高" andInfor:self.heightString toView:view];
                break;
            case 4:
                [self addname:@"当前体重" andInfor:self.weightString toView:view];
                break;
                
            default:
                break;
        }
        
    }
    
   
  
}

-(void)addname:(NSString *)titleName andInfor:(NSString *)inforStr toView:(UIView *)view
{
    CGFloat h = view.frame.size.height;
    CGFloat w = view.frame.size.width;
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(30, 0, w / 3.0, h)];
    label.text = titleName;
    label.textColor = YMColor(188, 56, 132);
    [view addSubview:label];
    
    UILabel *infoLabel = [[UILabel alloc]initWithFrame:CGRectMake(30 + w / 3.0, 0, w / 2.0, h)];
    infoLabel.text = inforStr;
    infoLabel.textColor = YMColor(140, 140, 140);
    [view addSubview:infoLabel];
    
}

@end
