//
//  AttentionManagerViewController.m
//  孕妙
//
//  Created by 是源科技 on 16/4/20.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "AttentionManagerViewController.h"

@interface AttentionManagerViewController ()
@property (nonatomic,strong)UISwitch *switchBtn1;
@property (nonatomic,strong)UISwitch *switchBtn2;
@property (nonatomic,strong)UISwitch *switchBtn3;
@property (nonatomic,strong)UISwitch *switchBtn4;

@end

@implementation AttentionManagerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"关注管理";
    self.view.backgroundColor = YMColor(245, 245, 245);
    self.switchBtn1 = [[UISwitch alloc]init];
    self.switchBtn2 = [[UISwitch alloc]init];
    self.switchBtn3 = [[UISwitch alloc]init];
    self.switchBtn4 = [[UISwitch alloc]init];
    self.switchBtn1.tag = 1;
    self.switchBtn2.tag = 2;
    self.switchBtn3.tag = 3;
    self.switchBtn4.tag = 4;
    
//    [[NSUserDefaults standardUserDefaults]setObject:@"haveChanged" forKey:@"changed"];
    CGFloat y = 80;
    
    UIView *tipsView = [[UIView alloc]initWithFrame:CGRectMake(0, y, kDeviceWidth, 60)];
    tipsView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:tipsView];
    
    UILabel *tipsLabel = [[UILabel alloc]initWithFrame:CGRectMake(20, 0, kDeviceWidth - 40, 60)];
    tipsLabel.numberOfLines = 0;
    tipsLabel.lineBreakMode = NSLineBreakByWordWrapping;
    tipsLabel.text = @"    开启关注后会在对应的时间提醒你，并会出现在首页的今日关注中。";
    tipsLabel.textColor = YMColor(172, 172, 172);
    tipsLabel.font = [UIFont systemFontOfSize:16.0];
    [tipsView addSubview:tipsLabel];
    
    y += 76;
    
    for(int i = 1;i < 5;i ++)
    {
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, y, kDeviceWidth, 60)];
        view.tag = i;
        view.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:view];
        
        y += view.frame.size.height;
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, y, kDeviceWidth, 1)];
        label.backgroundColor = YMColor(219, 219, 219);
        [self.view addSubview:label];
        y += 1;
        
        if(view.tag == 1)
        {
            [self addSwitchButtonToView:view andImageName:@"gz_产检提醒icon" andtitleName:@"产检提醒" andSwitch:self.switchBtn1];
            
        }
        if(view.tag == 2)
        {
            [self addSwitchButtonToView:view andImageName:@"gz_健康监测icon" andtitleName:@"健康监测" andSwitch:self.switchBtn2];
        }
        if(view.tag == 3)
        {
            [self addSwitchButtonToView:view andImageName:@"gz_膳食管理icon" andtitleName:@"膳食管理" andSwitch:self.switchBtn3];
        }
        if(view.tag == 4)
        {
            [self addSwitchButtonToView:view andImageName:@"gz_每日饮水icon" andtitleName:@"每日饮水" andSwitch:self.switchBtn4];
        }
    }
   
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
   
    if([[[NSUserDefaults standardUserDefaults]objectForKey:@"switch1"]isEqualToString:@"No"])
    {
        [self.switchBtn1 setOn:NO];
       
    }
    else
    {
        [self.switchBtn1 setOn:YES];
        
    }
    if([[[NSUserDefaults standardUserDefaults]objectForKey:@"switch2"]isEqualToString:@"No"])
    {
        [self.switchBtn2 setOn:NO];
        
    }
    else
    {
        [self.switchBtn2 setOn:YES];
       
    }
    if([[[NSUserDefaults standardUserDefaults]objectForKey:@"switch3"]isEqualToString:@"No"])
    {
        [self.switchBtn3 setOn:NO];
       
    }
    else
    {
        [self.switchBtn3 setOn:YES];
        
    }

    if([[[NSUserDefaults standardUserDefaults]objectForKey:@"switch4"]isEqualToString:@"No"])
    {
        [self.switchBtn4 setOn:NO];
        
    }
    else
    {
        [self.switchBtn4 setOn:YES];
        
    }


}

-(void)addSwitchButtonToView:(UIView *)view andImageName:(NSString *)imageName andtitleName:(NSString *)titleName andSwitch:(UISwitch *)switchBtn
{
    CGFloat w = view.frame.size.width;
    CGFloat h = view.frame.size.height;
    
    UIImageView *logoImageView = [[UIImageView alloc]initWithFrame:CGRectMake(30, h / 4.0, h / 2.0, h / 2.0)];
    logoImageView.image = [UIImage imageNamed:imageName];
    [view addSubview:logoImageView];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(90, 0, kDeviceWidth / 2.0, h)];
    label.text = titleName;
    label.textColor = YMColor(196, 102, 149);
    [view addSubview:label];
    
    switchBtn.frame = CGRectMake(w - 30 - h * 0.8, h * 0.3, h * 0.8, h * 0.4);
    [switchBtn setOn:NO];
    switchBtn.onTintColor = YMColor(252, 160, 207);
//    switchBtn.tintColor = YMColor(201, 201, 201);
    switchBtn.tag = view.tag;
    [switchBtn addTarget:self action:@selector(clickSwitchButton:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:switchBtn];
}

-(void)clickSwitchButton:(UISwitch *)SwitchBtn
{
    if(SwitchBtn.tag == 1)
    {
        if(SwitchBtn.isOn)
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"Yes"forKey:@"switch1"];
            [[NSUserDefaults standardUserDefaults]setObject:@"1open" forKey:@"guanzhu"];
        }
        else
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"No" forKey:@"switch1"];
             [[NSUserDefaults standardUserDefaults]setObject:@"1close" forKey:@"guanzhu"];
        }
    }
    if(SwitchBtn.tag == 2)
    {
        if(SwitchBtn.isOn)
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"Yes"forKey:@"switch2"];
             [[NSUserDefaults standardUserDefaults]setObject:@"2open" forKey:@"guanzhu"];
        }
        else
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"No" forKey:@"switch2"];
            [[NSUserDefaults standardUserDefaults]setObject:@"2close" forKey:@"guanzhu"];
        }
    }

    if(SwitchBtn.tag == 3)
    {
        if(SwitchBtn.isOn)
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"Yes"forKey:@"switch3"];
            [[NSUserDefaults standardUserDefaults]setObject:@"3open" forKey:@"guanzhu"];
        }
        else
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"No" forKey:@"switch3"];
            [[NSUserDefaults standardUserDefaults]setObject:@"3close" forKey:@"guanzhu"];
        }
    }

    if(SwitchBtn.tag == 4)
    {
        if(SwitchBtn.isOn)
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"Yes"forKey:@"switch4"];
            [[NSUserDefaults standardUserDefaults]setObject:@"4open" forKey:@"guanzhu"];
        }
        else
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"No" forKey:@"switch4"];
            [[NSUserDefaults standardUserDefaults]setObject:@"4close" forKey:@"guanzhu"];
        }
    }

}
@end
