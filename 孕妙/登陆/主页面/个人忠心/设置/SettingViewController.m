//
//  SettingViewController.m
//  孕妙
//
//  Created by 是源科技 on 16/4/15.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "SettingViewController.h"
#import "changPasswordViewController.h"
#import "changPhoneNOViewController.h"
#import "changeMailViewController.h"

@interface SettingViewController ()
@property (nonatomic,strong)UILabel *phoneLabel;
@property (nonatomic,strong)UILabel *mailLabel;
@property (nonatomic,strong)UILabel *huancunLabel;


@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"设置";
    self.view.backgroundColor = YMColor(245, 245, 245);
    
    self.phoneLabel = [[UILabel alloc]init];
    self.mailLabel = [[UILabel alloc]init];
    self.huancunLabel = [[UILabel alloc]init];
    
    [self settingControlLayout];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    NSString *phoneString = [[NSUserDefaults standardUserDefaults]objectForKey:@"userAccount"];
//    NSRange rang = {3,5};
//    phoneString = [phoneString stringByReplacingCharactersInRange:rang withString:@"*****"];
//    self.phoneLabel.text = phoneString;
//    self.mailLabel.text = @"longdiao***@qq.com";
//    self.huancunLabel.text = @"当前缓存1.22M";
}

-(void)settingControlLayout
{
    CGFloat y = 64;
    CGFloat h = (kDeviceHeight - 120) / 4.0;
    
    for(int i = 0;i < 4;i++)
    {
        
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, y , kDeviceWidth, h)];
        view.tag = i + 1;
        [self.view addSubview:view];
        if(3 != i)
        {
            UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, y + h, kDeviceWidth, 1)];
            label.backgroundColor = YMColor(226, 226, 226);
            [self.view addSubview:label];
            y += h + 1;
        }
        
        if(view.tag == 1)
        {
            [self addtitleToView:view andTitleString:@"修改密码" andLabel:nil andImageName:@"Setting_change pw"];
        }
        if(view.tag == 2)
        {
            [self addtitleToView:view andTitleString:@"绑定手机" andLabel:self.phoneLabel andImageName:@"Setting_bound phone"];
        }
        if(view.tag == 3)
        {
            [self addtitleToView:view andTitleString:@"绑定邮箱" andLabel:self.mailLabel andImageName:@"Setting_bound mail"];
        }
        if(view.tag == 4)
        {
//            [self addtitleToView:view andTitleString:@"清空缓存" andLabel:self.huancunLabel andImageName:@"Setting_clear"];
        }
    }
}

-(void)addtitleToView:(UIView *)view andTitleString:(NSString *)str andLabel:(UILabel *)label andImageName:(NSString *)imageName
{
    CGFloat h = view.frame.size.height;
    CGFloat w = view.frame.size.width;
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, w, h)];
    btn.tag = view.tag;
    [view addSubview:btn];
    
    UIImageView *logoImageView = [[UIImageView alloc]initWithFrame:CGRectMake(25, h / 4.0, h / 2.0, h / 2.0)];
    logoImageView.image = [UIImage imageNamed:imageName];
    [view addSubview:logoImageView];
    
    UILabel *titleLabel = [[UILabel alloc]init];
    if(view.tag == 1)
    {
        titleLabel.frame = CGRectMake(w / 4.0, 0, w / 2.0, h);
    }
    else
    {
        titleLabel.frame = CGRectMake(w / 4.0, h / 9.0, w / 2.0, h / 4.0 * 3);
        
        label.frame = CGRectMake(w / 4.0, h / 3.0, w / 2.0, h / 3.0 * 2);
        label.textColor = YMColor(116, 192, 245);
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont systemFontOfSize:15.0];
        [view addSubview:label];
    }
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = str;
    titleLabel.textColor = YMColor(169, 169, 169);
    [view addSubview:titleLabel];
    
    UIImageView *jiantouImage = [[UIImageView alloc]initWithFrame:CGRectMake(w - h / 8.0 * 3 - 25, h / 8.0 * 3, h / 4.0, h / 4.0)];
    jiantouImage.image = [UIImage imageNamed:@"personal_》small"];
    [view addSubview:jiantouImage];
    
    [btn addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)clickBtn:(UIButton *)btn
{
    
    if(btn.tag == 1)
    {
        changPasswordViewController *password = [[changPasswordViewController alloc]init];
        [self.navigationController pushViewController:password animated:YES];
    }
    if(btn.tag == 2)
    {
        changPhoneNOViewController *phone = [[changPhoneNOViewController alloc]init];
        [self.navigationController pushViewController:phone animated:YES];
    }
    if(btn.tag == 3)
    {
        changeMailViewController *mail = [[changeMailViewController alloc]init];
        [self.navigationController pushViewController:mail animated:YES];
    }
    
}
@end
