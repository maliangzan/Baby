//
//  changPasswordViewController.m
//  孕妙
//
//  Created by 是源科技 on 16/4/20.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "changPasswordViewController.h"

@interface changPasswordViewController ()<UITextFieldDelegate>
@property (nonatomic,strong)UITextField *oldPassWord;
@property (nonatomic,strong)UITextField *NewPassWord;
@property (nonatomic,strong)UITextField *againNewPassWord;
@property (nonatomic,strong)UIButton *sureButton;


@end

@implementation changPasswordViewController

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.oldPassWord resignFirstResponder];
    [self.NewPassWord resignFirstResponder];
    [self.againNewPassWord resignFirstResponder];
    
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"修改密码";
    self.view.backgroundColor = YMColor(245, 245, 245);
    YMLog(@"保存的密码：%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"userPassWord"]);
    
    self.oldPassWord = [[UITextField alloc]init];
    self.NewPassWord = [[UITextField alloc]init];
    self.againNewPassWord = [[UITextField alloc]init];
    
    self.oldPassWord.secureTextEntry = YES;
    self.NewPassWord.secureTextEntry = YES;
    self.againNewPassWord.secureTextEntry = YES;
    self.oldPassWord.delegate = self;
    
    CGFloat y = 85;
    
    for(int i = 1;i < 4;i ++)
    {
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, y, kDeviceWidth, 80)];
        view.backgroundColor = [UIColor whiteColor];
        view.tag = i;
        [self.view addSubview:view];
        
        y += view.frame.size.height;
        
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, y, kDeviceWidth, 1)];
        label.backgroundColor = YMColor(219, 219, 219);
        [self.view addSubview:label];
        y += 1;
        
        if(view.tag == 1)
        {
            [self addImageToView:view andImageName:@"Setting_pw" andTextField:self.oldPassWord andTFstr:@"请输入原始密码"];
        }
        if(view.tag == 2)
        {
            [self addImageToView:view andImageName:@"Setting_pw" andTextField:self.NewPassWord andTFstr:@"请输入新密码"];
        }
        if(view.tag == 3)
        {
            [self addImageToView:view andImageName:@"Setting_pw" andTextField:self.againNewPassWord andTFstr:@"请再次输入新密码"];
        }
    }
    
    y += 30;
    self.sureButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.sureButton.frame = CGRectMake(kDeviceWidth / 3.0, y, kDeviceWidth / 3.0, kDeviceWidth / 3.0 / 2.22);
    [self.sureButton setTitle:@"确  定" forState:UIControlStateNormal];
    [self.sureButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.sureButton.backgroundColor = YMColor(252, 160, 207);
    self.sureButton.layer.cornerRadius = 8.0;
    [self.view addSubview:self.sureButton];
    
    [self.sureButton addTarget:self action:@selector(clickSureButton) forControlEvents:UIControlEventTouchUpInside];
    
}

-(void)addImageToView:(UIView *)view andImageName:(NSString *)imageName andTextField:(UITextField *)TF andTFstr:(NSString *)tfStr
{
    CGFloat h = view.frame.size.height;
    UIImageView *logoImage = [[UIImageView alloc]initWithFrame:CGRectMake(35, h / 3.0, h / 3.0, h / 3.0)];
    logoImage.image = [UIImage imageNamed:imageName];
    [view addSubview:logoImage];
    
    TF.frame = CGRectMake(kDeviceWidth / 4.0, 0, kDeviceWidth / 4.0 * 3, h);
    TF.placeholder = tfStr;
    [TF setValue:YMColor(201, 201, 201) forKeyPath:@"_placeholderLabel.textColor"];
    [view addSubview:TF];
}
//完成按钮
-(void)clickSureButton
{

    if(self.NewPassWord.text != nil && self.againNewPassWord.text != nil)
    {
        if([self.NewPassWord.text isEqualToString:self.againNewPassWord.text])
        {
            [self changePassWordToServices];
        }
        else
        {
            [MBProgressHUD showError:@"两次密码不一致，请重新输入!"];
        }
    }
    else
    {
        [MBProgressHUD showError:@"密码不能为空!"];
    }
}

-(void)changePassWordToServices
{
    
    NSString *urlString = @"http://120.24.178.16:8080/Health/baby/updateUserInfo.do";
    NSURL *url = [[NSURL alloc]initWithString:urlString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:2];
    
    request.HTTPMethod = @"POST";
    NSString *str = [NSString stringWithFormat:@"account=%@&type=2&info=%@&oldpwd=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"userAccount"],self.againNewPassWord.text,self.oldPassWord.text];
    NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:data];
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
     {
         if (data)
         {
             // 请求成功
             NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
//             YMLog(@"dict = %@",dict);
             if([[dict objectForKey:@"code"] isEqualToString:@"0"])
             {
                 [MBProgressHUD showSuccess:@"密码修改成功"];
//                 [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"userAccount"];
                 [[NSUserDefaults standardUserDefaults]setObject:self.againNewPassWord.text forKey:@"userPassWord"];
                 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                     UIWindow *window = [[UIApplication sharedApplication].delegate window];
                     sayesTabbarViewController *homeN = [[sayesTabbarViewController alloc]init];
                     LoginViewController *login = [[LoginViewController alloc]init];
                     [homeN addOneChlildVc:login title:@"" imageName:@"" selectedImageName:@""];
                     window.rootViewController = homeN;
                 });
             }
             else
             {
                 [MBProgressHUD showError:[dict objectForKey:@"message"]];
             }
         }
     }];
}

@end
