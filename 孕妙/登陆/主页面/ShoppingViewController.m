
//
//  ShoppingViewController.m
//  孕妙
//
//  Created by 是源科技 on 16/4/13.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "ShoppingViewController.h"

@interface ShoppingViewController ()<UIWebViewDelegate>
@property (nonatomic,strong)UIWebView *webView;

@property (nonatomic,weak)UIButton *shuaxinButton;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSURLCache *urlCache;
@property (strong, nonatomic) NSURL *url;
@property (strong, nonatomic) NSMutableURLRequest *request;

@end

static dispatch_once_t onceToken;
@implementation ShoppingViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, kDeviceHeight)];
    self.webView.backgroundColor = [UIColor whiteColor];
    [self.webView setUserInteractionEnabled:YES];//是否支持交互
    self.webView.delegate = self;
    self.webView.dataDetectorTypes = UIDataDetectorTypeAll;
    [self.webView setOpaque:NO];//opaque是不透明的意思
    [self.webView setScalesPageToFit:YES];//自动缩放以适应屏幕
    [self.view addSubview:self.webView];
    
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(20, 0, 100, 44)];
    [btn setTitle:@"回到主页" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.shuaxinButton = btn;
    [self.navigationController.navigationBar addSubview:btn];
    [btn addTarget:self action:@selector(clickShuaxinButton) forControlEvents:UIControlEventTouchUpInside];
    
//    NSString *paramURLAsString= @"http://weidian.com/?userid=429415601&wfr=qfriend";
//    self.urlCache = [NSURLCache sharedURLCache];
//    
//    [self.urlCache setMemoryCapacity:1*1024*1024];
//    //创建一个nsurl
//    self.url = [NSURL URLWithString:paramURLAsString];
//    //创建一个请求
//    self.request=[NSMutableURLRequest requestWithURL:self.url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0f];
//    [self.webView loadRequest:self.request];
    
    [self jiazaiNetwork];
}

//加载网页链接
-(void)jiazaiNetwork
{
    NSString *path = @"http://weidian.com/s/429415601?wfr=qfriend";
    NSURL *url = [NSURL URLWithString:path];
    [self.webView loadRequest:[NSURLRequest requestWithURL:url]];
    
//    //从请求中获取缓存输出
//    NSCachedURLResponse *response =[self.urlCache cachedResponseForRequest:self.request];
//    //判断是否有缓存
//    if (response != nil){
//        NSLog(@"如果有缓存输出，从缓存中获取数据");
//        [self.request setCachePolicy:NSURLRequestReturnCacheDataDontLoad];
//    }
//    [self.webView loadRequest:self.request];
//    
//    self.connection = nil;
//    
//    NSURLConnection *newConnection = [[NSURLConnection alloc] initWithRequest:self.request delegate:self startImmediately:YES];
//    self.connection = newConnection;
}

-(void)clickShuaxinButton
{
    [self jiazaiNetwork];
}

//几个代理方法

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    dispatch_once(&onceToken, ^{
        [MBProgressHUD showMessage:@"正在拼命加载，请耐心等待"];
    });
    
}

- (void)webViewDidFinishLoad:(UIWebView *)web
{
    [MBProgressHUD hideHUD];
    
}

-(void)webView:(UIWebView*)webView  DidFailLoadWithError:(NSError*)error
{
    [MBProgressHUD showError:@"网页加载出错,请稍后再试！"];
}


@end
