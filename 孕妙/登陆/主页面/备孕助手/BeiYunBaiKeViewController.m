//
//  BeiYunBaiKeViewController.m
//  孕妙
//
//  Created by 是源科技 on 16/4/21.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "BeiYunBaiKeViewController.h"

@interface BeiYunBaiKeViewController ()<UIWebViewDelegate>
@property (nonatomic,strong)UIWebView *webView;


@end

@implementation BeiYunBaiKeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"备孕百科";
    self.view.backgroundColor = YMColor(245, 245, 245);
    
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(SYMargin10, SYMargin5 + 64, 65, 30)];
    [btn setTitle:@"返回上一级" forState:UIControlStateNormal];
    btn.backgroundColor = YMColor(249, 137, 196);
    btn.layer.cornerRadius = 5;
    btn.layer.masksToBounds = YES;
    btn.titleLabel.font = [UIFont systemFontOfSize:11];
    [btn addTarget:self action:@selector(gobackUp) forControlEvents:UIControlEventTouchUpInside];
    [btn setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    [self.view addSubview:btn];
    
    self.webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(btn.frame) + SYMargin5, kDeviceWidth, kDeviceHeight - (CGRectGetMaxY(btn.frame) + SYMargin5))];
    self.webView.backgroundColor = [UIColor whiteColor];
    [self.webView setUserInteractionEnabled:YES];//是否支持交互
    self.webView.delegate = self;
    self.webView.dataDetectorTypes = UIDataDetectorTypeAll;
    [self.webView setOpaque:NO];// opaque是不透明的意思
    [self.webView setScalesPageToFit:YES];//自动缩放以适应屏幕
    [self.view addSubview:self.webView];
    
    NSString *path = @"http://120.24.178.16:8080/Health/beiYunBaike/beiYunBaike.html";
    NSURL *url = [NSURL URLWithString:path];
    [self.webView loadRequest:[NSURLRequest requestWithURL:url]];
    
    
}

- (void)gobackUp{
    if (self.webView.canGoBack) {
        [self.webView goBack];
    } else {
        [MBProgressHUD showError:@"已是第一页"];
    }
}

//几个代理方法

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [MBProgressHUD showMessage:@"正在拼命加载，请耐心等待"];
    YMLog(@"start");
}

- (void)webViewDidFinishLoad:(UIWebView *)web
{
    [MBProgressHUD hideHUD];
    YMLog(@"end");
}

-(void)webView:(UIWebView*)webView  DidFailLoadWithError:(NSError*)error
{
    [MBProgressHUD showError:@"网页加载出错,请稍后再试！"];
}
@end
