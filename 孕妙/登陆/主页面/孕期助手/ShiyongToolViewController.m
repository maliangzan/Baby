//
//  ShiyongToolViewController.m
//  孕妙
//
//  Created by 是源科技 on 16/4/19.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "ShiyongToolViewController.h"
#import "babyBloodViewController.h"
#import "YuchanqiViewController.h"
#import "babyHeightViewController.h"
#import "babyWeightViewController.h"

@interface ShiyongToolViewController ()

@end

@implementation ShiyongToolViewController

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    self.view.backgroundColor = YMColor(245, 245, 245);
    self.title = @"实用工具";
    
    CGFloat viewW = (kDeviceWidth - 2) / 3.0;
    int viewTag = 1;
    //列
    for(int j = 0;j < 6;j ++)
    {
        //行
        for(int i = 0;i < 3;i ++)
        {
            UIView *view = [[UIView alloc]initWithFrame:CGRectMake(viewW * i + i, 64 + viewW * j + j, viewW, viewW)];
            view.tag = viewTag ++;
            [self.view addSubview:view];
            
            if(view.tag == 1)
            {
                [self addButtonToView:view andImageName:@"tool_babyborn" andTitleName:@"预产期计算器"];
            }
            if(view.tag == 2)
            {
                [self addButtonToView:view andImageName:@"tool_babyheight" andTitleName:@"宝宝身高预测"];
            }
            if(view.tag == 3)
            {
                [self addButtonToView:view andImageName:@"tool_babyweight" andTitleName:@"胎儿重量预测"];
            }
            if(view.tag == 4)
            {
                [self addButtonToView:view andImageName:@"tool_babyABO" andTitleName:@"宝宝血型预测"];
            }
        }
    }
    
    for(int i = 0;i < 3;i ++)
    {
        UILabel *shuLabel = [[UILabel alloc]initWithFrame:CGRectMake(viewW * i, 0, 1, kDeviceHeight)];
        shuLabel.backgroundColor = YMColor(219, 219, 219);
    
        [self.view addSubview:shuLabel];
    }
    
    for(int j = 0;j < 6;j ++)
    {
        UILabel *crossLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, viewW * j + 64, kDeviceWidth, 1)];
        crossLabel.backgroundColor = YMColor(219, 219, 219);
        [self.view addSubview:crossLabel];
    }
    
}


-(void)addButtonToView:(UIView *)view andImageName:(NSString *)imageName andTitleName:(NSString *)titleName
{
    CGFloat w = view.frame.size.width;
    
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, w, w)];
    btn.tag = view.tag;
    [view addSubview:btn];
    
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(w / 3.0, w / 4.0, w / 3.0, w / 3.0)];
    imageView.image = [UIImage imageNamed:imageName];
    [view addSubview:imageView];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, w / 4.0 + w / 3.0, w, 40)];
    label.text = titleName;
    label.font = [UIFont systemFontOfSize:15.0];
    label.textColor = YMColor(105, 105, 105);
    label.textAlignment = NSTextAlignmentCenter;
    [view addSubview:label];
    
    [btn addTarget:self action:@selector(clickBtnCheckBabyInformation:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)clickBtnCheckBabyInformation:(UIButton *)btn
{
    if(btn.tag == 1)
    {
        YuchanqiViewController *yuchanqi = [[YuchanqiViewController alloc]init];
        [self.navigationController pushViewController:yuchanqi animated:YES];
    }
    if(btn.tag == 2)
    {
        babyHeightViewController *yuchanqi = [[babyHeightViewController alloc]init];
        [self.navigationController pushViewController:yuchanqi animated:YES];
    }
    if(btn.tag == 3)
    {
        babyWeightViewController *yuchanqi = [[babyWeightViewController alloc]init];
        [self.navigationController pushViewController:yuchanqi animated:YES];
    }
    if(btn.tag == 4)
    {
        babyBloodViewController *yuchanqi = [[babyBloodViewController alloc]init];
        [self.navigationController pushViewController:yuchanqi animated:YES];
    }
}
@end
