//
//  YunyuKetangViewController.m
//  孕妙
//
//  Created by 是源科技 on 16/4/20.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "YunyuKetangViewController.h"
#import "TaijiaoInportViewController.h"

@interface YunyuKetangViewController ()

@end

@implementation YunyuKetangViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"孕育课堂";
    self.view.backgroundColor = YMColor(245, 245, 245);
    
    CGFloat y = 64;
    int count = (kDeviceHeight - 64) / 60 + 1;
    for(int i = 1;i < count;i ++)
    {
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, y, kDeviceWidth, 60)];
        view.tag = i;
        if(view.tag % 2 == 1)
        {
            view.backgroundColor = [UIColor whiteColor];
        }
        [self.view addSubview:view];
        y += view.frame.size.height;
        
        if(view.tag == 1)
        {
            [self addkindOfControllerToView:view andImageName:@"assistant_pic01" andtitleName:@"01  胎教的重要性"];
        }
        if(view.tag == 2)
        {
            [self addkindOfControllerToView:view andImageName:@"assistant_pic02" andtitleName:@"02  孕妇体操"];
        }
        if(view.tag == 3)
        {
            [self addkindOfControllerToView:view andImageName:@"assistant_pic03" andtitleName:@"03  孕妇合理用药"];
        }
        if(view.tag == 4)
        {
            [self addkindOfControllerToView:view andImageName:@"assistant_pic04" andtitleName:@"04  孕妇特有疾病防治"];
        }
        if(view.tag == 5)
        {
            [self addkindOfControllerToView:view andImageName:@"assistant_pic05" andtitleName:@"05  分娩方式的选择"];
        }
        if(view.tag == 6)
        {
            [self addkindOfControllerToView:view andImageName:@"assistant_pic06" andtitleName:@"06  新生儿喂养方式"];
        }
    }
    
}

-(void)addkindOfControllerToView:(UIView *)view andImageName:(NSString *)imageName andtitleName:(NSString *)titleName
{
    CGFloat w = view.frame.size.width;
    CGFloat h = view.frame.size.height;
    UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(25, h / 6.0, h / 3.0 * 2, h / 3.0 * 2)];
    image.image = [UIImage imageNamed:imageName];
    [view addSubview:image];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(w / 4.0, 0, w / 2.0, h)];
    label.text = titleName;
    label.textColor = YMColor(119, 119, 119);
    [view addSubview:label];
    
    UIImageView *jiantou = [[UIImageView alloc]initWithFrame:CGRectMake(w - 30 - h / 3.0, h / 3.0, h / 3.0, h / 3.0)];
    jiantou.image = [UIImage imageNamed:@"personal_》small"];
    [view addSubview:jiantou];
    
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, w, h)];
    btn.tag = view.tag;
    [view addSubview:btn];
    [btn addTarget:self action:@selector(checkKeTangInformation:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)checkKeTangInformation:(UIButton *)btn
{
    if(btn.tag == 1)
    {
        TaijiaoInportViewController *taijiao = [[TaijiaoInportViewController alloc]init];
        [self.navigationController pushViewController:taijiao animated:YES];
    }
}




@end
