//
//  chanJianInformationViewController.m
//  孕妙
//
//  Created by 是源科技 on 16/5/5.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "chanJianInformationViewController.h"

@interface chanJianInformationViewController ()
@property (nonatomic,weak)UIScrollView *scrollView;
@property (nonatomic,assign)float height;
@property (nonatomic,weak)UILabel *countLabel;
@property (nonatomic,weak)UIButton *selectButton;
@property (nonatomic,weak)UILabel *jianchaLabel;
@property (nonatomic,assign)BOOL selectFlag;
@end
@implementation chanJianInformationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"产检记录";
    self.view.backgroundColor = [UIColor whiteColor];
    UIView *firstView = [[UIView alloc]initWithFrame:CGRectMake(0, 64, kDeviceWidth, 60)];
    firstView.backgroundColor = YMColor(245, 245, 245);
    [self.view addSubview:firstView];
    
    CGFloat h = firstView.frame.size.height;
    UILabel *coutLabel = [[UILabel alloc]initWithFrame:CGRectMake(25, 0, kDeviceWidth / 2.0, h)];
    coutLabel.textColor = YMColor(102, 102, 102);
    coutLabel.font = [UIFont systemFontOfSize:20.0];
    self.countLabel = coutLabel;
    [firstView addSubview:coutLabel];
    
    UILabel *jianchaLabel = [[UILabel alloc]initWithFrame:CGRectMake(kDeviceWidth - 90, 0, 100, h)];
    self.jianchaLabel = jianchaLabel;
    jianchaLabel.textColor = YMColor(102, 102, 102);
    [firstView addSubview:jianchaLabel];
    
    UIButton *selectBtn = [[UIButton alloc]initWithFrame:CGRectMake(kDeviceWidth - 95 - h / 3.0, h / 3.0, h / 3.0, h / 3.0)];
    [selectBtn setImage:[UIImage imageNamed:@"dl_Unselected"] forState:UIControlStateNormal];
    self.selectButton = selectBtn;
    [firstView addSubview:selectBtn];
    [selectBtn addTarget:self action:@selector(selectshangchuan) forControlEvents:UIControlEventTouchUpInside];
    [self setTitleLabelInformation];
    self.jianchaLabel.text = @"未检查";
    self.height = 0;
    UIScrollView *scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64 + firstView.frame.size.height + 10 , kDeviceWidth, kDeviceHeight - (64 + firstView.frame.size.height + 10) - 15)];
    scrollView.backgroundColor = [UIColor clearColor];
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.showsVerticalScrollIndicator = YES;
    self.scrollView = scrollView;
    [self.view addSubview:self.scrollView];
    
    [self getDisplayNeirong];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if([self.haveCheck isEqualToString:@"have"])
    {
        [self.selectButton setImage:[UIImage imageNamed:@"dl_Selected"] forState:UIControlStateNormal];
        self.selectFlag = YES;
        self.jianchaLabel.text = @"已检查";
        self.selectButton.userInteractionEnabled = NO;
        self.jianchaLabel.textColor = YMColor(91, 178, 250);
    }
    else
    {
        self.selectButton.userInteractionEnabled = YES;
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}


-(void)getDisplayNeirong
{
    NSString *urlString = @"http://120.24.178.16:8080/Health/baby/getAntenataladvise.do";
    NSURL *url = [[NSURL alloc]initWithString:urlString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:2];
    request.HTTPMethod = @"POST";
    NSString *str = [NSString stringWithFormat:@"times=%lu",(long)self.count];
    NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:data];
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (data)
        {
            // 请求成功
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            NSArray *dataArray = [dict objectForKey:@"data"];
            for(NSDictionary *dd in dataArray)
            {

                [self jisuanLabelHeightandString:[dd objectForKey:@"keypoint"]];
                self.height += 10;
                [self jisuanLabelHeightandString:[dd objectForKey:@"content"]];
                self.scrollView.contentSize = CGSizeMake(kDeviceWidth, self.height + 5);
            }
        }
    }];
}

-(void)jisuanLabelHeightandString:(NSString *)string
{
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
    [self.scrollView addSubview:label];
    NSString *s2 = string;
    label.text = s2;
    label.textAlignment = NSTextAlignmentLeft;
    label.font = [UIFont systemFontOfSize:15.0];
    
    NSDictionary * attribute2 = @{NSFontAttributeName:[UIFont systemFontOfSize:15.0]};
    CGSize size2 = [s2 boundingRectWithSize:CGSizeMake(kDeviceWidth - 20, kDeviceHeight * 5) options:NSStringDrawingUsesLineFragmentOrigin attributes:attribute2 context:nil].size;
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.numberOfLines = 0;
    label.frame = CGRectMake(10, self.height, kDeviceWidth - 20, size2.height);
    label.textColor = YMColor(107, 107, 107);
    self.height += size2.height+ 5;
}

-(void)selectshangchuan
{
    if(self.selectFlag)
    {
        [self.selectButton setImage:[UIImage imageNamed:@"dl_Unselected"] forState:UIControlStateNormal];
        self.selectFlag = NO;
        self.jianchaLabel.text = @"未检查";
        self.jianchaLabel.textColor = YMColor(102, 102, 102);
    }
    else
    {
        [self.selectButton setImage:[UIImage imageNamed:@"dl_Selected"] forState:UIControlStateNormal];
        self.selectFlag = YES;
        self.jianchaLabel.text = @"已检查";
        self.jianchaLabel.textColor = YMColor(91, 178, 250);
        [self postChanjianInformationToServices];
    }
}

//上传产检信息到服务器
-(void)postChanjianInformationToServices
{
    [MBProgressHUD showMessage:@"正在拼命加载，请耐心等待"];
    NSString *urlString = @"http://120.24.178.16:8080/Health/baby/savePregnancyReport.do";
    NSURL *url = [[NSURL alloc]initWithString:urlString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:2];
    request.HTTPMethod = @"POST";
    NSString *str = [NSString stringWithFormat:@"account=%@&username=%@&reportNo=%@&describe=0&checkdate=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"userAccount"],self.userName,[NSString stringWithFormat:@"%lu",(long)self.count],[self getSystemTime]];
    YMLog(@"str = %@",str);
    NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:data];
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        if (data)
        {
            [MBProgressHUD hideHUD];
            // 请求成功
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            YMLog(@"dict = %@",dict);
            if([[dict objectForKey:@"code"]isEqualToString:@"0"])
            {
                [MBProgressHUD showSuccess:@"上传成功"];
                self.selectButton.userInteractionEnabled = NO;
                [[NSUserDefaults standardUserDefaults]setObject:@"haveChanged" forKey:@"changed"];
                [passValue passValue].ChanjianCount = self.count;
            }
            else
            {
                [MBProgressHUD showError:@"上传失败"];
                [self.selectButton setImage:[UIImage imageNamed:@"dl_Unselected"] forState:UIControlStateNormal];
                self.selectFlag = NO;
                self.jianchaLabel.text = @"未检查";
                self.jianchaLabel.textColor = YMColor(102, 102, 102);
            }
        }
    }];

}

-(void)setTitleLabelInformation
{
    switch (self.count) {
        case 1:
            self.countLabel.text = @"第一次产检 ";
            break;
        case 2:
            self.countLabel.text = @"第二次产检 ";
            break;
        case 3:
            self.countLabel.text = @"第三次产检 ";
            break;
        case 4:
            self.countLabel.text = @"第四次产检 ";
            break;
        case 5:
            self.countLabel.text = @"第五次产检 ";
            break;
        case 6:
            self.countLabel.text = @"第六次产检 ";
            break;
        case 7:
            self.countLabel.text = @"第七次产检 ";
            break;
        case 8:
            self.countLabel.text = @"第八次产检 ";
            break;
        case 9:
            self.countLabel.text = @"第九次产检 ";
            break;
        case 10:
            self.countLabel.text = @"第十次产检 ";
            break;
        case 11:
            self.countLabel.text = @"第十一次产检 ";
            break;
        case 12:
            self.countLabel.text = @"第十二次产检 ";
            break;
        default:
            break;
    }
}

//获取系统时间
-(NSString *)getSystemTime
{
    NSDate *senddate=[NSDate date];
    NSDateFormatter  *dateformatter=[[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"YYYYMMddHHmmss"];
    NSString *locationString=[dateformatter stringFromDate:senddate];
    return locationString;
}





@end
