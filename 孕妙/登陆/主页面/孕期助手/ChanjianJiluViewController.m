//
//  ChanjianJiluViewController.m
//  孕妙
//
//  Created by 是源科技 on 16/4/18.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "ChanjianJiluViewController.h"
#import "chanJianInformationViewController.h"

@interface ChanjianJiluViewController ()<UIScrollViewDelegate>
@property (nonatomic,strong)UIScrollView *chanjianScrollView;
@property (nonatomic,strong)UILabel *firstLabel;
@property (nonatomic,strong)UILabel *secondLabel;
@property (nonatomic,strong)UILabel *thirdLabel;
@property (nonatomic,strong)UILabel *fouthLabel;
@property (nonatomic,strong)UILabel *fifthLabel;
@property (nonatomic,strong)UILabel *sixthLabel;
@property (nonatomic,strong)UILabel *seventhLabel;
@property (nonatomic,strong)UILabel *eightthLabel;
@property (nonatomic,strong)UILabel *ninethLabel;
@property (nonatomic,strong)UILabel *tenthLabel;
@property (nonatomic,strong)UILabel *elevenLabel;
@property (nonatomic,strong)UILabel *twelveLabel;

@end

@implementation ChanjianJiluViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"产检纪录";
    self.view.backgroundColor = YMColor(245, 245, 245);
    
    self.chanjianScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 16, kDeviceWidth, kDeviceHeight - 16)];
    self.chanjianScrollView.delegate = self;
    [self.view addSubview:self.chanjianScrollView];
    
    self.firstLabel = [[UILabel alloc]init];
    self.secondLabel = [[UILabel alloc]init];
    self.thirdLabel = [[UILabel alloc]init];
    self.fouthLabel = [[UILabel alloc]init];
    self.fifthLabel = [[UILabel alloc]init];
    self.sixthLabel = [[UILabel alloc]init];
    self.seventhLabel = [[UILabel alloc]init];
    self.eightthLabel = [[UILabel alloc]init];
    self.ninethLabel = [[UILabel alloc]init];
    self.tenthLabel = [[UILabel alloc]init];
    self.elevenLabel = [[UILabel alloc]init];
    self.twelveLabel = [[UILabel alloc]init];
    
    CGFloat h = (kDeviceHeight - 80) / 7.0;
    for(int i = 0;i < 12;i ++)
    {
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, (h + 1) * i, kDeviceWidth, h)];
        view.backgroundColor = [UIColor whiteColor];
        view.tag = i;
        [self.chanjianScrollView addSubview:view];
    
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, h * i + i - 1, kDeviceWidth, 1)];
        label.backgroundColor = YMColor(220, 220, 220);
        [self.chanjianScrollView addSubview:label];
        
        if(view.tag == 0)
        {
            [self addallControllerToView:view andDateString:@"怀孕13周前" andmiaoshuStr:@"建档 常规检查" andmiaoshustring:@"NT彩超 早期糖筛"andcompleteLabel:self.firstLabel andcishuString:@"第一次"];
        }
        if(view.tag == 1)
        {
            [self addallControllerToView:view andDateString:@"怀孕14-20周" andmiaoshuStr:@"产前检查 唐氏筛查" andmiaoshustring:@"尿常规 基因筛查" andcompleteLabel:self.secondLabel andcishuString:@"第二次"];
        }
        if(view.tag == 2)
        {
            [self addallControllerToView:view andDateString:@"怀孕21-24周" andmiaoshuStr:@"产前检查 尿常规" andmiaoshustring:@"B超排畸" andcompleteLabel:self.thirdLabel andcishuString:@"第三次"];
        }
        if(view.tag == 3)
        {
            [self addallControllerToView:view andDateString:@"怀孕24-28周" andmiaoshuStr:@"产前检查 尿常规" andmiaoshustring:@"血常规 建议糖筛" andcompleteLabel:self.fouthLabel andcishuString:@"第四次"];
        }
        if(view.tag == 4)
        {
            [self addallControllerToView:view andDateString:@"怀孕28-30周" andmiaoshuStr:@"产前检查 尿常规" andmiaoshustring:@"建议ABO抗体检查 胎动" andcompleteLabel:self.fifthLabel andcishuString:@"第五次"];
        }
        if(view.tag == 5)
        {
            [self addallControllerToView:view andDateString:@"怀孕30-32周" andmiaoshuStr:@"产前检查 尿常规" andmiaoshustring:@"血常规 建议B超" andcompleteLabel:self.sixthLabel andcishuString:@"第六次"];
        }
        if(view.tag == 6)
        {
            [self addallControllerToView:view andDateString:@"怀孕32-34周" andmiaoshuStr:@"产前检查 尿常规" andmiaoshustring:@"胎心监测" andcompleteLabel:self.seventhLabel andcishuString:@"第七次"];
        }
        if(view.tag == 7)
        {
            [self addallControllerToView:view andDateString:@"怀孕32-34周" andmiaoshuStr:@"产前检查 尿常规" andmiaoshustring:@"胎心监测" andcompleteLabel:self.eightthLabel andcishuString:@"第八次"];
        }
        if(view.tag == 8)
        {
            [self addallControllerToView:view andDateString:@"怀孕37周" andmiaoshuStr:@"产前检查 尿常规"  andmiaoshustring:@"血常规 胎心监测" andcompleteLabel:self.ninethLabel andcishuString:@"第九次"];
        }
        if(view.tag == 9)
        {
            [self addallControllerToView:view andDateString:@"怀孕38周" andmiaoshuStr:@"产前检查 尿常规" andmiaoshustring:@"胎心监测" andcompleteLabel:self.tenthLabel andcishuString:@"第十次"];
        }
        if(view.tag == 10)
        {
            [self addallControllerToView:view andDateString:@"怀孕39周" andmiaoshuStr:@"产前检查 尿常规" andmiaoshustring:@"胎心监测 B超" andcompleteLabel:self.elevenLabel andcishuString:@"第十一次"];
        }
        if(view.tag == 11)
        {
            [self addallControllerToView:view andDateString:@"怀孕40周" andmiaoshuStr:@"产前检查 尿常规" andmiaoshustring:@"胎心监测" andcompleteLabel:self.twelveLabel andcishuString:@"第十二次"];
        }
    }
    self.chanjianScrollView.contentSize = CGSizeMake(kDeviceWidth,h * 12 + 10);
  
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.chanjianCompleteArray = [passValue passValue].chanjianCompleteArray;
    if([[[NSUserDefaults standardUserDefaults]objectForKey:@"changed"] isEqualToString:@"haveChanged"])
    {
        [self.chanjianCompleteArray addObject:[NSString stringWithFormat:@"%lu",(long)[passValue passValue].ChanjianCount]];
//        [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"changed"];
    }
      [self chanjianCompleteInformation];
}

-(void)chanjianCompleteInformation
{
    for(int i = 0;i < self.chanjianCompleteArray.count;i ++)
    {
        NSString *str = [NSString stringWithFormat:@"%@",self.chanjianCompleteArray[i]];
        if([str isEqualToString:@"1"])
        {
            self.firstLabel.text = @"完成";
            self.firstLabel.textColor = YMColor(88, 176, 250);
        }
        if([str isEqualToString:@"2"])
        {
            self.secondLabel.text = @"完成";
            self.secondLabel.textColor = YMColor(88, 176, 250);
        }
        if([str isEqualToString:@"3"])
        {
            self.thirdLabel.text = @"完成";
            self.thirdLabel.textColor = YMColor(88, 176, 250);
        }
        if([str isEqualToString:@"4"])
        {
            self.fouthLabel.text = @"完成";
            self.fouthLabel.textColor = YMColor(88, 176, 250);
        }
        if([str isEqualToString:@"5"])
        {
            self.fifthLabel.text = @"完成";
            self.fifthLabel.textColor = YMColor(88, 176, 250);
        }
        if([str isEqualToString:@"6"])
        {
            self.sixthLabel.text = @"完成";
            self.sixthLabel.textColor = YMColor(88, 176, 250);
        }
        if([str isEqualToString:@"7"])
        {
            self.seventhLabel.text = @"完成";
            self.seventhLabel.textColor = YMColor(88, 176, 250);
        }
        if([str isEqualToString:@"8"])
        {
            self.eightthLabel.text = @"完成";
            self.eightthLabel.textColor = YMColor(88, 176, 250);
        }
        if([str isEqualToString:@"9"])
        {
            self.ninethLabel.text = @"完成";
            self.ninethLabel.textColor = YMColor(88, 176, 250);
        }
        if([str isEqualToString:@"10"])
        {
            self.tenthLabel.text = @"完成";
            self.tenthLabel.textColor = YMColor(88, 176, 250);
        }
        if([str isEqualToString:@"11"])
        {
            self.elevenLabel.text = @"完成";
            self.elevenLabel.textColor = YMColor(88, 176, 250);
        }
        if([str isEqualToString:@"12"])
        {
            self.twelveLabel.text = @"完成";
            self.twelveLabel.textColor = YMColor(88, 176, 250);
        }
    }
}

-(void)addallControllerToView:(UIView *)view andDateString:(NSString *)dateString andmiaoshuStr:(NSString *)str1 andmiaoshustring:(NSString *)str2 andcompleteLabel:(UILabel *)completeLabel andcishuString:(NSString *)cishuStr
{
    CGFloat h = view.frame.size.height;
    CGFloat w = view.frame.size.width;
    UIImageView *yuanImageView = [[UIImageView alloc]initWithFrame:CGRectMake(30, h / 8.0, h / 4.0 * 3, h / 4.0 * 3)];
    yuanImageView.image = [UIImage imageNamed:@"assistant_checkdate"];
    [view addSubview:yuanImageView];
    
    UIButton *butt = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, w, h)];
    [view addSubview:butt];
    butt.tag = view.tag;
    [butt addTarget:self action:@selector(checkChanjianInformation:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(30, h / 3.2, yuanImageView.frame.size.width, yuanImageView.frame.size.height / 3.0)];
    label1.text = cishuStr;
    label1.font = [UIFont systemFontOfSize:14.0];
    label1.textAlignment = NSTextAlignmentCenter;
    label1.textColor = YMColor(200, 118, 159);
    [view addSubview:label1];
    
    UILabel *label2 = [[UILabel alloc]initWithFrame:CGRectMake(30, h / 8.0 + yuanImageView.frame.size.height / 2.0, yuanImageView.frame.size.width, yuanImageView.frame.size.height / 3.0)];
    label2.text = @"产检";
    label2.font = [UIFont systemFontOfSize:14.0];
    label2.textAlignment = NSTextAlignmentCenter;
    label2.textColor = YMColor(200, 118, 159);
    [view addSubview:label2];
    
    UILabel *label3 = [[UILabel alloc]initWithFrame:CGRectMake(w / 3.0, 5, w / 3.0, h / 3.0)];
    label3.text = dateString;
    label3.textColor = YMColor(252, 151, 204);
    [view addSubview:label3];
    
    UILabel *label4 = [[UILabel alloc]initWithFrame:CGRectMake(w / 3.0, 5 + h / 3.0, w / 2.0, (h - 10 - h / 3.0) / 2.0)];
    label4.text = str1;
    label4.textColor = YMColor(187, 187, 187);
    label4.font = [UIFont systemFontOfSize:14.0];
    [view addSubview:label4];
    
    UILabel *label5 = [[UILabel alloc]init];
    label5.frame = CGRectMake(w / 3.0, 5 + h / 3.5 + (h - 12 - h / 3.0) / 2.0, w / 2.0, (h - 10 - h / 3.0) / 2.0);
    label5.font = [UIFont systemFontOfSize:14.0];
    label5.text = str2;
    label5.textColor = YMColor(187, 187, 187);
    [view addSubview:label5];
    
    completeLabel.frame = CGRectMake(kDeviceWidth / 4.0 * 3, 0, kDeviceWidth / 4.0, h);
    completeLabel.textAlignment = NSTextAlignmentCenter;
    completeLabel.textColor = YMColor(253, 118, 0);
    completeLabel.text = @"未完成";
    [view addSubview:completeLabel];
}

-(void)checkChanjianInformation:(UIButton *)butt
{
    chanJianInformationViewController *chanjian = [[chanJianInformationViewController alloc]init];
    [self.navigationController pushViewController:chanjian animated:YES];
    chanjian.count = butt.tag + 1;
    chanjian.userName = self.userName;
    
    for(int i = 0;i < self.chanjianCompleteArray.count;i ++)
    {
        NSString *str = [NSString stringWithFormat:@"%@",self.chanjianCompleteArray[i]];
        if([str intValue] == chanjian.count)
        {
            chanjian.haveCheck = @"have";
        }
    }
}

@end
