//
//  TaijiaoInportViewController.m
//  孕妙
//
//  Created by 是源科技 on 16/4/20.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "TaijiaoInportViewController.h"

@interface TaijiaoInportViewController ()

@end

@implementation TaijiaoInportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"孕育课堂";
    self.view.backgroundColor = YMColor(245, 245, 245);
    
    CGFloat y = 64;
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, y, kDeviceWidth, 80)];
    titleLabel.text = @"胎教的重要性";
    titleLabel.backgroundColor = [UIColor whiteColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = YMColor(102, 102, 102);
    titleLabel.font = [UIFont systemFontOfSize:18.0];
    [self.view addSubview:titleLabel];
    
    y += titleLabel.frame.size.height;
    
    int count = (kDeviceHeight - y) / 60 + 1;
    
    for(int i = 1;i < count + 1;i ++)
    {
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, y, kDeviceWidth, 60)];
        view.tag = i;
        if(view.tag % 2 == 0)
        {
            view.backgroundColor = [UIColor whiteColor];
        }
        [self.view addSubview:view];
        y += view.frame.size.height;
        if(view.tag == 1)
        {
            [self addView:view andTitleName:@"不要盲目胎教，正确的胎教很重要"];
        }
        if(view.tag == 2)
        {
            [self addView:view andTitleName:@"告诉妈妈胎教的N个好处"];
        }
        if(view.tag == 3)
        {
            [self addView:view andTitleName:@"解读胎教的重要性"];
        }
        if(view.tag == 4)
        {
            [self addView:view andTitleName:@"胎教的现实意义"];
        }
    }
    
}

-(void)addView:(UIView *)view andTitleName:(NSString *)titleName
{
    CGFloat w = view.frame.size.width;
    CGFloat h = view.frame.size.height;
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(20, 0, kDeviceWidth, h)];
    label.textColor = YMColor(102, 102, 102);
    label.text = titleName;
    [view addSubview:label];
    
    UIImageView *jiantou = [[UIImageView alloc]initWithFrame:CGRectMake(w - 20 - h / 3.0, h / 3.0, h / 3.0, h / 3.0)];
    jiantou.image = [UIImage imageNamed:@"personal_》small"];
    [view addSubview:jiantou];
}



@end
