//
//  babyHeightViewController.m
//  孕妙
//
//  Created by 是源科技 on 16/4/19.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "babyHeightViewController.h"


@interface babyHeightViewController ()<UIScrollViewDelegate,UIImagePickerControllerDelegate,UIPickerViewDelegate, UIPickerViewDataSource>
@property (nonatomic,strong)UILabel *sexLabel;
@property (nonatomic,strong)UILabel *matherLabel;
@property (nonatomic,strong)UILabel *fatherLabel;

@property (nonatomic,strong)UIButton *sureButton;
@property (nonatomic,strong)UILabel *resultLabel;

@property (nonatomic,strong)SettingAlertController *dataAlert;
//选择器
@property (nonatomic,strong)xianshikuang *pickerWindow;
@property (nonatomic,weak)UIView *xuanzeqiView;

@property (nonatomic,strong)NSArray *heightIntArray; //身高整数部分
@property (nonatomic,strong)NSArray *babySexArray;


@end

@implementation babyHeightViewController
{
    NSString *selectIntStr;    //获取整数值
    NSString *selectFloatStr;  //获取小数值
    NSString *valueStr;     //设置string格式
}
//宝宝性别
-(NSArray *)babySexArray
{
    if(!_babySexArray)
    {
        _babySexArray = @[@"男",@"女"];
    }
    return _babySexArray;
}

//身高的整数部分
-(NSArray *)heightIntArray
{
    if(!_heightIntArray)
    {
        NSMutableArray *tempAaary = [NSMutableArray array];
        for (NSInteger index = 30; index <= 250; index++) {
            NSString *str = [NSString stringWithFormat:@"%ld", (long)index];
            [tempAaary addObject:str];
        }
        _heightIntArray = [NSArray arrayWithArray:tempAaary];
    }
    return _heightIntArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"宝宝身高预测";
    self.view.backgroundColor = YMColor(245, 245, 245);
    
    self.sexLabel = [[UILabel alloc]init];
    self.matherLabel = [[UILabel alloc]init];
    self.fatherLabel = [[UILabel alloc]init];
    
    self.sexLabel.text = @"男";
    self.matherLabel.text = @"160 cm";
    self.fatherLabel.text = @"175 cm";
    
    UIScrollView *scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, kDeviceHeight - 20 )];
    scrollView.delegate = self;
    [self.view addSubview:scrollView];
    
    CGFloat y = 75;
    
    for(int i = 1;i < 4;i ++)
    {
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, y, kDeviceWidth, 80)];
        view.tag = i;
        [scrollView addSubview:view];
        y += 80;
        
        if(view.tag == 1)
        {
            [self addControllerToView:view andImageName:@"tool_babysex" andLabel:self.sexLabel andmiaoshuString:@"宝宝性别"];
        }
        if(view.tag == 2)
        {
            [self addControllerToView:view andImageName:@"tool_mom" andLabel:self.matherLabel andmiaoshuString:@"妈妈身高"];
        }
        if(view.tag == 3)
        {
            [self addControllerToView:view andImageName:@"tool_dad" andLabel:self.fatherLabel andmiaoshuString:@"爸爸身高"];
        }
    }
    y += 30;
    
    self.sureButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.sureButton.frame = CGRectMake(kDeviceWidth / 3.0, y, kDeviceWidth / 3.0, kDeviceWidth / 3.0 / 2.22);
    self.sureButton.backgroundColor = YMColor(252, 160, 207);
    self.sureButton.layer.cornerRadius = 8.0;
    [self.sureButton setTitle:@"确  定" forState:UIControlStateNormal];
    [self.sureButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [scrollView addSubview:self.sureButton];
    
    [self.sureButton addTarget:self action:@selector(clickSureButton) forControlEvents:UIControlEventTouchUpInside];
    
    y += self.sureButton.frame.size.height + 10;
    
    UILabel *resultLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, y, kDeviceWidth, 30)];
    resultLabel.text = @"宝宝成年后的身高预测结果";
    resultLabel.textColor = YMColor(199, 107, 156);
    resultLabel.textAlignment = NSTextAlignmentCenter;
    [scrollView addSubview:resultLabel];
    y += resultLabel.frame.size.height;
    
    self.resultLabel = [[UILabel alloc]initWithFrame:CGRectMake(kDeviceWidth / 6.0, y, kDeviceWidth / 3.0 * 2, kDeviceWidth / 3.0 * 2 / 2.0)];
    self.resultLabel.layer.borderColor = [YMColor(252, 160, 207)CGColor];
    self.resultLabel.layer.borderWidth = 1.0;
    self.resultLabel.textAlignment = NSTextAlignmentCenter;
    self.resultLabel.textColor = YMColor(0, 202, 189);
    [scrollView addSubview:self.resultLabel];
    
    
    y += self.resultLabel.frame.size.height;
    scrollView.contentSize = CGSizeMake(kDeviceWidth, y);
    
    UIView *view1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, kDeviceHeight)];
    view1.backgroundColor = [UIColor blackColor];
    self.xuanzeqiView = view1;
    self.xuanzeqiView.hidden = YES;
    self.xuanzeqiView.alpha = 0.5;
    [self.view addSubview:view1];
    
    _pickerWindow = [[xianshikuang alloc] init];
    [self.view addSubview:_pickerWindow];
    self.pickerWindow.hidden = YES;
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.pickerWindow = nil;
}

-(void)addControllerToView:(UIView *)view andImageName:(NSString *)imageName andLabel:(UILabel *)label andmiaoshuString:(NSString *)str
{
    UILabel *lastLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, 0, kDeviceWidth, 30)];
    lastLabel.text = str;
    lastLabel.textColor = YMColor(153, 153, 153);
    lastLabel.font = [UIFont systemFontOfSize:15.0];
    [view addSubview:lastLabel];
    
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 30, kDeviceWidth, 50)];
    btn.tag = view.tag;
    btn.backgroundColor = [UIColor whiteColor];
    [view addSubview:btn];
    
    UIImageView *image1 = [[UIImageView alloc]initWithFrame:CGRectMake(25, 42.5, 25, 25)];
    image1.image = [UIImage imageNamed:imageName];
    [view addSubview:image1];
    
    label.frame = CGRectMake(kDeviceWidth / 4.0, 30, kDeviceWidth / 2.0, 50);
    label.textColor = YMColor(107, 190, 246);
    label.textAlignment = NSTextAlignmentCenter;
    [view addSubview:label];
    
    UIImageView *jiantou1 = [[UIImageView alloc]initWithFrame:CGRectMake(kDeviceWidth - 50, 42.5, 25, 25)];
    jiantou1.image = [UIImage imageNamed:@"personal_》small"];
    [view addSubview:jiantou1];
    
   
    [btn addTarget:self action:@selector(presectInformation:) forControlEvents:UIControlEventTouchUpInside];
}

//点击了选择器的确定按钮
- (void)clickSureButton:(UIButton *)btn
{
    valueStr = nil;
    //    _pickerWindow.dianL.hidden = YES;
    _pickerWindow.hidden = YES;
    _pickerWindow.selectPickerView.delegate = nil;
    _pickerWindow.selectPickerView.dataSource = nil;
    self.xuanzeqiView.hidden = YES;
    
    //宝宝性别
    if(btn.tag == 1)
    {
        valueStr = [NSString stringWithFormat:@"%@", selectIntStr];
        self.sexLabel.text = valueStr;
    }
    //妈妈身高
    if(btn.tag == 2)
    {
        valueStr = [NSString stringWithFormat:@"%@ cm", selectIntStr];
        self.matherLabel.text = valueStr;
    }
    //爸爸身高
    if(btn.tag == 3)
    {
        valueStr = [NSString stringWithFormat:@"%@ cm", selectIntStr];
        self.fatherLabel.text = valueStr;
    }
}
//点击了选择器的取消按钮
-(void)clickCancelButton
{
    self.xuanzeqiView.hidden = YES;
    self.pickerWindow.hidden = YES;
    _pickerWindow.selectPickerView.delegate = nil;
    _pickerWindow.selectPickerView.dataSource = nil;
    [_pickerWindow.dianL removeFromSuperview];
}


-(void)presectInformation:(UIButton *)btn
{
    if(btn.tag == 1)
    {
        self.xuanzeqiView.hidden = NO;
        _pickerWindow.hidden = NO;
        [_pickerWindow setInterFace:@"宝宝性别"];
        [_pickerWindow addSubview:_pickerWindow.dianL];
        _pickerWindow.dianL.hidden = YES;
        _pickerWindow.selectPickerView.delegate = self;
        _pickerWindow.selectPickerView.dataSource = self;
        _pickerWindow.selectPickerView.tag = btn.tag;
        [_pickerWindow.sureBtn addTarget:self action:@selector(clickSureButton:) forControlEvents:UIControlEventTouchUpInside];
        [_pickerWindow.cancelBtn addTarget:self action:@selector(clickCancelButton) forControlEvents:UIControlEventTouchUpInside];
        _pickerWindow.sureBtn.tag = btn.tag;
        [_pickerWindow.selectPickerView selectRow:0  inComponent:0 animated:YES];
        selectIntStr = self.babySexArray[0];
    }
    if(btn.tag == 2)
    {
        self.xuanzeqiView.hidden = NO;
        _pickerWindow.hidden = NO;
        [_pickerWindow setInterFace:@"妈妈身高"];
        [_pickerWindow addSubview:_pickerWindow.dianL];
        _pickerWindow.dianL.hidden = YES;
        _pickerWindow.selectPickerView.delegate = self;
        _pickerWindow.selectPickerView.dataSource = self;
        _pickerWindow.selectPickerView.tag = btn.tag;
        [_pickerWindow.sureBtn addTarget:self action:@selector(clickSureButton:) forControlEvents:UIControlEventTouchUpInside];
        [_pickerWindow.cancelBtn addTarget:self action:@selector(clickCancelButton) forControlEvents:UIControlEventTouchUpInside];
        _pickerWindow.sureBtn.tag = btn.tag;
        [_pickerWindow.selectPickerView selectRow:125  inComponent:0 animated:YES];
        selectIntStr = self.heightIntArray[125];
    }
    if(btn.tag == 3)
    {
        self.xuanzeqiView.hidden = NO;
        _pickerWindow.hidden = NO;
        [_pickerWindow setInterFace:@"爸爸身高"];
        [_pickerWindow addSubview:_pickerWindow.dianL];
        _pickerWindow.dianL.hidden = YES;
        _pickerWindow.selectPickerView.delegate = self;
        _pickerWindow.selectPickerView.dataSource = self;
        _pickerWindow.selectPickerView.tag = btn.tag;
        [_pickerWindow.sureBtn addTarget:self action:@selector(clickSureButton:) forControlEvents:UIControlEventTouchUpInside];
        [_pickerWindow.cancelBtn addTarget:self action:@selector(clickCancelButton) forControlEvents:UIControlEventTouchUpInside];
        _pickerWindow.sureBtn.tag = btn.tag;
        [_pickerWindow.selectPickerView selectRow:125  inComponent:0 animated:YES];
        selectIntStr = self.heightIntArray[125];
        
    }
}

-(void)clickSureButton
{
    //男孩
    if([self.sexLabel.text isEqualToString:@"男"])
    {
        float motherHeight = [self.matherLabel.text floatValue];
        float fatherHeight = [self.fatherLabel.text floatValue];
        float sonHeight = (motherHeight + fatherHeight) * 1.08 / 2.0;
        self.resultLabel.text = [NSString stringWithFormat:@"%.f cm",sonHeight];
        
    }
    else  //女孩
    {
        float motherHeight = (float)[self.matherLabel.text intValue];
        float fatherHeight = (float)[self.fatherLabel.text intValue];
        float sonHeight = (fatherHeight * 0.923 + motherHeight) / 2.0;
        self.resultLabel.text = [NSString stringWithFormat:@"%.f cm",sonHeight];
    }

}

#pragma mark - UIPickerView
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSInteger number = 0;
    if(pickerView.tag == 1)
    {
        number = self.babySexArray.count;
    }
    else
    {
        number = self.heightIntArray.count;
    }
    return number;
    
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *dataStr = nil;
    if(pickerView.tag == 1)
    {
        dataStr = self.babySexArray[row];
    }
    else
    {
        dataStr = self.heightIntArray[row];
    }
    return dataStr;
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if(pickerView.tag == 1)
    {
        selectIntStr = self.babySexArray[row];
    }
    else
    {
        selectIntStr = self.heightIntArray[row];
    }
}
@end
