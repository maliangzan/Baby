//
//  babyBloodViewController.m
//  孕妙
//
//  Created by 是源科技 on 16/4/19.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "babyBloodViewController.h"

@interface babyBloodViewController ()<UIScrollViewDelegate,UIImagePickerControllerDelegate,UIPickerViewDelegate, UIPickerViewDataSource>
@property (nonatomic,strong)UILabel *matherLabel;
@property (nonatomic,strong)UILabel *fatherLabel;
@property (nonatomic,strong)UIButton *sureButton;

//四种血型结果
@property (nonatomic,weak)UILabel *labelA;
@property (nonatomic,weak)UILabel *labelB;
@property (nonatomic,weak)UILabel *labelAB;
@property (nonatomic,weak)UILabel *labelO;

//选择器
@property (nonatomic,strong)xianshikuang *pickerWindow;
@property (nonatomic,weak)UIView *xuanzeqiView;
@property (nonatomic,strong)NSArray *BloodArray; //身高整数部分

@end

@implementation babyBloodViewController
{
    NSString *selectIntStr;    //获取整数值
    NSString *selectFloatStr;  //获取小数值
    NSString *valueStr;     //设置string格式
}

-(NSArray *)BloodArray
{
    if(!_BloodArray)
    {
        _BloodArray = @[@"A",@"B",@"AB",@"O"];
    }
    return _BloodArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"宝宝血型预测";
    self.view.backgroundColor = YMColor(245, 245, 245);
    
    self.matherLabel = [[UILabel alloc]init];
    self.fatherLabel = [[UILabel alloc]init];

    self.matherLabel.text = @"A";
    self.fatherLabel.text = @"A";
    
    UIScrollView *scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, kDeviceHeight - 20 )];
    scrollView.delegate = self;
    [self.view addSubview:scrollView];
    
    CGFloat y = 75;
    
    for(int i = 1;i < 3;i ++)
    {
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, y, kDeviceWidth, 80)];
        view.tag = i;
        [scrollView addSubview:view];
        y += 80;
        
        if(view.tag == 1)
        {
            [self addControllerToView:view andImageName:@"tool_mom" andLabel:self.matherLabel andmiaoshuString:@"妈妈血型"];
        }
        if(view.tag == 2)
        {
            [self addControllerToView:view andImageName:@"tool_dad" andLabel:self.fatherLabel andmiaoshuString:@"爸爸血型"];
        }
    }
    y += 30;
    
    self.sureButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.sureButton.frame = CGRectMake(kDeviceWidth / 3.0, y, kDeviceWidth / 3.0, kDeviceWidth / 3.0 / 2.22);
    self.sureButton.backgroundColor = YMColor(252, 160, 207);
    self.sureButton.layer.cornerRadius = 8.0;
    [self.sureButton setTitle:@"确  定" forState:UIControlStateNormal];
    [self.sureButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [scrollView addSubview:self.sureButton];
    [self.sureButton addTarget:self action:@selector(clickSureButton) forControlEvents:UIControlEventTouchUpInside];
    
    y += self.sureButton.frame.size.height + 10;
    
    UILabel *resultLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, y, kDeviceWidth, 30)];
    resultLabel.text = @"宝宝可能血型及概率如下";
    resultLabel.textColor = YMColor(199, 107, 156);
    resultLabel.textAlignment = NSTextAlignmentCenter;
    [scrollView addSubview:resultLabel];
    y += resultLabel.frame.size.height;
    
    UIView *resultView = [[UIView alloc]initWithFrame:CGRectMake(kDeviceWidth / 6.0, y, kDeviceWidth / 3.0 * 2, kDeviceWidth / 3.0 * 2 / 2.0)];
    resultView.layer.borderColor = [YMColor(252, 160, 207)CGColor];
    resultView.layer.borderWidth = 1.0;
    [scrollView addSubview:resultView];
    
    CGFloat w = resultView.frame.size.width;
    CGFloat h = (resultView.frame.size.height - 10) / 4.0;
    CGFloat labelY = 5;
    for(int i = 1;i < 5;i ++)
    {
        UILabel *labelA = [[UILabel alloc]initWithFrame:CGRectMake(0, labelY, w, h)];
        labelA.textAlignment = NSTextAlignmentCenter;
        labelA.textColor = YMColor(0, 202, 189);
        labelA.tag = i;
        [resultView addSubview:labelA];
        labelY += h;
        if(labelA.tag == 1)
        {
            self.labelA = labelA;
        }
        if(labelA.tag == 2)
        {
            self.labelB = labelA;
        }
        if(labelA.tag == 3)
        {
            self.labelAB = labelA;
        }
        if(labelA.tag == 4)
        {
            self.labelO = labelA;
        }
    }
    
    
    y += resultView.frame.size.height;
    scrollView.contentSize = CGSizeMake(kDeviceWidth, y);
    
    UIView *view1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, kDeviceHeight)];
    view1.backgroundColor = [UIColor blackColor];
    self.xuanzeqiView = view1;
    self.xuanzeqiView.hidden = YES;
    self.xuanzeqiView.alpha = 0.5;
    [self.view addSubview:view1];
    
    _pickerWindow = [[xianshikuang alloc] init];
    [self.view addSubview:_pickerWindow];
    self.pickerWindow.hidden = YES;
    
}


-(void)addControllerToView:(UIView *)view andImageName:(NSString *)imageName andLabel:(UILabel *)label andmiaoshuString:(NSString *)str
{
    UILabel *lastLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, 0, kDeviceWidth, 30)];
    lastLabel.text = str;
    lastLabel.textColor = YMColor(153, 153, 153);
    lastLabel.font = [UIFont systemFontOfSize:15.0];
    [view addSubview:lastLabel];
    
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 30, kDeviceWidth, 50)];
    btn.tag = view.tag;
    btn.backgroundColor = [UIColor whiteColor];
    [view addSubview:btn];
    
    UIImageView *image1 = [[UIImageView alloc]initWithFrame:CGRectMake(25, 42.5, 25, 25)];
    image1.image = [UIImage imageNamed:imageName];
    [view addSubview:image1];
    
    label.frame = CGRectMake(kDeviceWidth / 4.0, 30, kDeviceWidth / 2.0, 50);
    label.textColor = YMColor(107, 190, 246);
    label.textAlignment = NSTextAlignmentCenter;
    [view addSubview:label];
    
    UIImageView *jiantou1 = [[UIImageView alloc]initWithFrame:CGRectMake(kDeviceWidth - 50, 42.5, 25, 25)];
    jiantou1.image = [UIImage imageNamed:@"personal_》small"];
    [view addSubview:jiantou1];
    
    
    [btn addTarget:self action:@selector(presectInformation:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)presectInformation:(UIButton *)btn
{
    if(btn.tag == 1)
    {
        self.xuanzeqiView.hidden = NO;
        _pickerWindow.hidden = NO;
        [_pickerWindow setInterFace:@"妈妈血型"];
        [_pickerWindow addSubview:_pickerWindow.dianL];
        _pickerWindow.dianL.hidden = YES;
        _pickerWindow.selectPickerView.delegate = self;
        _pickerWindow.selectPickerView.dataSource = self;
        _pickerWindow.selectPickerView.tag = btn.tag;
        [_pickerWindow.sureBtn addTarget:self action:@selector(clickSureButton:) forControlEvents:UIControlEventTouchUpInside];
        [_pickerWindow.cancelBtn addTarget:self action:@selector(clickCancelButton) forControlEvents:UIControlEventTouchUpInside];
        _pickerWindow.sureBtn.tag = btn.tag;
        [_pickerWindow.selectPickerView selectRow:0  inComponent:0 animated:YES];
        selectIntStr = self.BloodArray[0];
    }
    if(btn.tag == 2)
    {
        self.xuanzeqiView.hidden = NO;
        _pickerWindow.hidden = NO;
        [_pickerWindow setInterFace:@"爸爸血型"];
        [_pickerWindow addSubview:_pickerWindow.dianL];
        _pickerWindow.dianL.hidden = YES;
        _pickerWindow.selectPickerView.delegate = self;
        _pickerWindow.selectPickerView.dataSource = self;
        _pickerWindow.selectPickerView.tag = btn.tag;
        [_pickerWindow.sureBtn addTarget:self action:@selector(clickSureButton:) forControlEvents:UIControlEventTouchUpInside];
        [_pickerWindow.cancelBtn addTarget:self action:@selector(clickCancelButton) forControlEvents:UIControlEventTouchUpInside];
        _pickerWindow.sureBtn.tag = btn.tag;
        [_pickerWindow.selectPickerView selectRow:0  inComponent:0 animated:YES];
        selectIntStr = self.BloodArray[0];
    }
}

//点击了选择器的确定按钮
- (void)clickSureButton:(UIButton *)btn
{
    valueStr = nil;
    //    _pickerWindow.dianL.hidden = YES;
    _pickerWindow.hidden = YES;
    _pickerWindow.selectPickerView.delegate = nil;
    _pickerWindow.selectPickerView.dataSource = nil;
    self.xuanzeqiView.hidden = YES;
    
    //妈妈血型
    if(btn.tag == 1)
    {
        valueStr = [NSString stringWithFormat:@"%@", selectIntStr];
        self.matherLabel.text = valueStr;
    }
    //爸爸血型
    if(btn.tag == 2)
    {
        valueStr = [NSString stringWithFormat:@"%@", selectIntStr];
        self.fatherLabel.text = valueStr;
    }
}
//点击了选择器的取消按钮
-(void)clickCancelButton
{
    self.xuanzeqiView.hidden = YES;
    self.pickerWindow.hidden = YES;
    _pickerWindow.selectPickerView.delegate = nil;
    _pickerWindow.selectPickerView.dataSource = nil;
    [_pickerWindow.dianL removeFromSuperview];
}


-(void)clickSureButton
{
    if([self.matherLabel.text isEqualToString:@"A"])
    {
        if([self.fatherLabel.text isEqualToString:@"A"])
        {
            self.labelA.text = @"A型血概率为94%";
            self.labelB.text = @"B型血概率为0";
            self.labelAB.text = @"AB型血概率为0";
            self.labelO.text = @"O型血概率为6%";
            
        }
        if([self.fatherLabel.text isEqualToString:@"B"])
        {
            self.labelA.text = @"A型血概率为19%";
            self.labelB.text = @"B型血概率为19%";
            self.labelAB.text = @"AB型血概率为56%";
            self.labelO.text = @"O型血概率为6%";
        }
        if([self.fatherLabel.text isEqualToString:@"AB"])
        {
            self.labelA.text = @"A型血概率为50%";
            self.labelB.text = @"B型血概率为12%";
            self.labelAB.text = @"AB型血概率为38%";
             self.labelO.text = @"O型血概率为0";
        }
        if([self.fatherLabel.text isEqualToString:@"O"])
        {
            self.labelA.text = @"A型血概率为75%";
             self.labelB.text = @"B型血概率为0";
             self.labelAB.text = @"AB型血概率为0";
            self.labelO.text = @"O型血概率为25%";
        }
    }
    
    if([self.matherLabel.text isEqualToString:@"B"])
    {
        if([self.fatherLabel.text isEqualToString:@"A"])
        {
            self.labelA.text = @"A型血概率为19%";
            self.labelB.text = @"B型血概率为19%";
            self.labelAB.text = @"AB型血概率为56%";
            self.labelO.text = @"O型血概率为6%";
            
        }
        if([self.fatherLabel.text isEqualToString:@"B"])
        {
             self.labelA.text = @"A型血概率为0";
            self.labelB.text = @"B型血概率为94%";
             self.labelAB.text = @"AB型血概率为0";
            self.labelO.text = @"O型血概率为6%";
        }
        if([self.fatherLabel.text isEqualToString:@"AB"])
        {
            self.labelA.text = @"A型血概率为12%";
            self.labelB.text = @"B型血概率为50%";
            self.labelAB.text = @"AB型血概率为38%";
             self.labelO.text = @"O型血概率为0";
        }
        if([self.fatherLabel.text isEqualToString:@"O"])
        {
             self.labelA.text = @"AB型血概率为0";
            self.labelB.text = @"B型血概率为75%";
             self.labelAB.text = @"AB型血概率为0";
            self.labelO.text = @"O型血概率为25%";
        }
    }
    if([self.matherLabel.text isEqualToString:@"AB"])
    {
        if([self.fatherLabel.text isEqualToString:@"A"])
        {
            self.labelA.text = @"A型血概率为50%";
            self.labelB.text = @"B型血概率为12%";
            self.labelAB.text = @"AB型血概率为38%";
             self.labelO.text = @"O型血概率为0";
            
        }
        if([self.fatherLabel.text isEqualToString:@"B"])
        {
            self.labelA.text = @"A型血概率为12%";
            self.labelB.text = @"B型血概率为50%";
            self.labelAB.text = @"AB型血概率为38%";
             self.labelO.text = @"O型血概率为0";
        }
        if([self.fatherLabel.text isEqualToString:@"AB"])
        {
            self.labelA.text = @"A型血概率为25%";
            self.labelB.text = @"B型血概率为25%";
            self.labelAB.text = @"AB型血概率为50%";
             self.labelO.text = @"O型血概率为0";
        }
        if([self.fatherLabel.text isEqualToString:@"O"])
        {
            self.labelA.text = @"A型血概率为50%";
            self.labelB.text = @"B型血概率为50%";
             self.labelAB.text = @"AB型血概率为0";
             self.labelO.text = @"O型血概率为0";
        }
    }
    if([self.matherLabel.text isEqualToString:@"O"])
    {
        if([self.fatherLabel.text isEqualToString:@"A"])
        {
            self.labelA.text = @"A型血概率为75%";
             self.labelB.text = @"B型血概率为0";
             self.labelAB.text = @"AB型血概率为0";
            self.labelO.text = @"O型血概率为25%";
        }
        if([self.fatherLabel.text isEqualToString:@"B"])
        {
             self.labelA.text = @"A型血概率为0";
            self.labelB.text = @"B型血概率为75%";
             self.labelAB.text = @"AB型血概率为0";
            self.labelO.text = @"O型血概率为25%";
            
        }
        if([self.fatherLabel.text isEqualToString:@"AB"])
        {
            self.labelA.text = @"A型血概率为50%";
            self.labelB.text = @"B型血概率为50%";
             self.labelAB.text = @"AB型血概率为0";
             self.labelO.text = @"O型血概率为0";
        }
        if([self.fatherLabel.text isEqualToString:@"O"])
        {
             self.labelA.text = @"A型血概率为0";
             self.labelB.text = @"B型血概率为0";
             self.labelAB.text = @"AB型血概率为0";
            self.labelO.text = @"O型血概率为100%";
        }
    }

}

#pragma mark - UIPickerView
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.BloodArray.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return self.BloodArray[row];
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if(pickerView.tag == 1 || pickerView.tag == 2)
    {
        selectIntStr = self.BloodArray[row];
    }
}



@end
