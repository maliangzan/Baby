//
//  YuchanqiViewController.m
//  孕妙
//
//  Created by 是源科技 on 16/4/19.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "YuchanqiViewController.h"

@interface YuchanqiViewController ()<UIScrollViewDelegate>
@property (nonatomic,strong)UILabel *lastYuejingLabel;
@property (nonatomic,strong)UILabel *yuejingDateLabel;
@property (nonatomic,strong)UIButton *lastYuejingButton;
@property (nonatomic,strong)UIButton *yuejingDateButton;
@property (nonatomic,strong)UIButton *sureButton;
@property (nonatomic,strong)UILabel *yuchanqiTimeLabel;
@property (nonatomic,strong)UILabel *timeLabel;

@property (nonatomic,strong)SettingAlertController *riqiAlert;
@property (nonatomic,strong)SettingAlertController *dataAlert;

@property (nonatomic,strong)NSArray *dateArray;
@property (nonatomic,copy)NSString *currentTime;
@end

@implementation YuchanqiViewController

-(NSArray *)dateArray
{
    if(!_dateArray)
    {
        _dateArray = @[@"0",@"31",@"28",@"31",@"30",@"31",@"30",@"31",@"31",@"30",@"31",@"30",@"31"];
    }
    return _dateArray;
}

-(void)getSystemTime
{
    //获取系统时间
    NSDate *senddate=[NSDate date];
    NSDateFormatter  *dateformatter=[[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"YYYYMMdd"];
    self.currentTime = [dateformatter stringFromDate:senddate];
    YMLog(@"locationString:%@",self.currentTime);
}

//判断是否是闰年
-(BOOL)panduanrunnian:(int)year
{
    BOOL runnian;
    if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0)
    {
        runnian = YES;
    }
    else
    {
        runnian = NO;
    }
    
    return runnian;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"预产期计算器";
    self.view.backgroundColor = YMColor(245, 245, 245);
    [self getSystemTime];
    
    UIScrollView *scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, kDeviceHeight - 20 )];
    scrollView.delegate = self;
    [self.view addSubview:scrollView];
    
    CGFloat y = 70;
    UILabel *jianjieLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, y, kDeviceWidth, 30)];
    jianjieLabel.textColor = YMColor(153, 153, 153);
    jianjieLabel.text = @"请填写月经详情，幸孕儿将为您计算出预产期";
    jianjieLabel.font = [UIFont systemFontOfSize:16.0];
    [scrollView addSubview:jianjieLabel];
    y += 30;
    
    UILabel *lastLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, y, kDeviceWidth, 30)];
    lastLabel.text = @"末次月经日期";
    lastLabel.textColor = YMColor(153, 153, 153);
    lastLabel.font = [UIFont systemFontOfSize:15.0];
    [scrollView addSubview:lastLabel];
    y += 30;
    
    UIView *view1 = [[UIView alloc]initWithFrame:CGRectMake(0, y, kDeviceWidth, 50)];
    view1.backgroundColor = [UIColor whiteColor];
    [scrollView addSubview:view1];
    
    self.lastYuejingButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, view1.frame.size.width, view1.frame.size.height)];
    [view1 addSubview:self.lastYuejingButton];
    
    UIImageView *image1 = [[UIImageView alloc]initWithFrame:CGRectMake(25, 12.5, 25, 25)];
    image1.image = [UIImage imageNamed:@"tool_date"];
    [view1 addSubview:image1];
    
    self.lastYuejingLabel = [[UILabel alloc]initWithFrame:CGRectMake(kDeviceWidth / 4.0, 0, kDeviceWidth / 2.0, 50)];
    self.lastYuejingLabel.textColor = YMColor(107, 190, 246);
    self.lastYuejingLabel.text = @"2016-01-01";
    self.lastYuejingLabel.textAlignment = NSTextAlignmentCenter;
    [view1 addSubview:self.lastYuejingLabel];
    
    UIImageView *jiantou1 = [[UIImageView alloc]initWithFrame:CGRectMake(kDeviceWidth - 50, 12.5, 25, 25)];
    jiantou1.image = [UIImage imageNamed:@"personal_》small"];
    [view1 addSubview:jiantou1];
    
    y += 50;
    UILabel *lastLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(15, y, kDeviceWidth, 30)];
    lastLabel1.text = @"月经周期天数";
    lastLabel1.textColor = YMColor(153, 153, 153);
    lastLabel1.font = [UIFont systemFontOfSize:15.0];
    [scrollView addSubview:lastLabel1];
    y += 30;
    
    UIView *view2 = [[UIView alloc]initWithFrame:CGRectMake(0, y, kDeviceWidth, 50)];
    view2.backgroundColor = [UIColor whiteColor];
    [scrollView addSubview:view2];
    
    self.yuejingDateButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, view1.frame.size.width, view1.frame.size.height)];
    [view2 addSubview:self.yuejingDateButton];
    
    UIImageView *image2 = [[UIImageView alloc]initWithFrame:CGRectMake(25, 12.5, 25, 25)];
    image2.image = [UIImage imageNamed:@"tool_day"];
    [view2 addSubview:image2];
    
    self.yuejingDateLabel = [[UILabel alloc]initWithFrame:CGRectMake(kDeviceWidth / 4.0, 0, kDeviceWidth / 2.0, 50)];
    self.yuejingDateLabel.textColor = YMColor(107, 190, 246);
    self.yuejingDateLabel.text = @"0天";
    self.yuejingDateLabel.textAlignment = NSTextAlignmentCenter;
    [view2 addSubview:self.yuejingDateLabel];
    
    UIImageView *jiantou2 = [[UIImageView alloc]initWithFrame:CGRectMake(kDeviceWidth - 50, 12.5, 25, 25)];
    jiantou2.image = [UIImage imageNamed:@"personal_》small"];
    [view2 addSubview:jiantou2];
    y += 80;
    
    self.sureButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.sureButton.frame = CGRectMake(kDeviceWidth / 3.0, y, kDeviceWidth / 3.0, kDeviceWidth / 3.0 / 2.22);
    self.sureButton.backgroundColor = YMColor(252, 160, 207);
    self.sureButton.layer.cornerRadius = 8.0;
    [self.sureButton setTitle:@"确  定" forState:UIControlStateNormal];
    [self.sureButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [scrollView addSubview:self.sureButton];
    
    [self.sureButton addTarget:self action:@selector(clickSureButton) forControlEvents:UIControlEventTouchUpInside];
    
    y += self.sureButton.frame.size.height + 10;
    
    UILabel *resultLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, y, kDeviceWidth, 30)];
    resultLabel.text = @"计算结果";
    resultLabel.textColor = YMColor(199, 107, 156);
    resultLabel.textAlignment = NSTextAlignmentCenter;
    [scrollView addSubview:resultLabel];
    y += resultLabel.frame.size.height;
    
    UIView *resultView = [[UIView alloc]initWithFrame:CGRectMake(kDeviceWidth / 6.0, y, kDeviceWidth / 3.0 * 2, kDeviceWidth / 3.0 * 2 / 2.0)];
    resultView.layer.borderColor = [YMColor(252, 160, 207)CGColor];
    resultView.layer.borderWidth = 1.0;
    [scrollView addSubview:resultView];
    
    CGFloat w = resultView.frame.size.width;
    CGFloat h = resultView.frame.size.height / 4.0;
    
    UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, w, h)];
    label1.textAlignment = NSTextAlignmentCenter;
    label1.textColor = YMColor(112, 112, 112);
    label1.text = @"您的预产期时间";
    [resultView addSubview:label1];
    
    self.yuchanqiTimeLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, h, w, h)];
    self.yuchanqiTimeLabel.textAlignment = NSTextAlignmentCenter;
    self.yuchanqiTimeLabel.textColor = YMColor(0, 202, 189);
//    self.yuchanqiTimeLabel.text = @"2016年9月10日";
    [resultView addSubview:self.yuchanqiTimeLabel];
    
    UILabel *label2 = [[UILabel alloc]initWithFrame:CGRectMake(0, h * 2, w, h)];
    label2.textAlignment = NSTextAlignmentCenter;
    label2.textColor = YMColor(112, 112, 112);
    label2.text = @"小宝宝出生还有";
    [resultView addSubview:label2];
    
    self.timeLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, h * 3, w, h)];
    self.timeLabel.textAlignment = NSTextAlignmentCenter;
    self.timeLabel.textColor = YMColor(0, 202, 189);
//    self.timeLabel.text = @"200天";
    [resultView addSubview:self.timeLabel];
    
    
    y += resultView.frame.size.height;
    scrollView.contentSize = CGSizeMake(kDeviceWidth, y);
    
    //选择器设置
    self.dataAlert = [[SettingAlertController alloc] init];
    self.dataAlert.alertTitle = @"数据设置";
    [self.view addSubview:self.dataAlert.view];
    [self addChildViewController:self.dataAlert];
    self.dataAlert.view.alpha = 0;
    
    self.riqiAlert = [[SettingAlertController alloc] init];
    self.riqiAlert.alertTitle = @"提醒时间设置";
    self.riqiAlert.contentType = ALertContentTypePickerView;
    self.riqiAlert.pickerView.datePickerMode = UIDatePickerModeDate;
    [self.view addSubview:self.riqiAlert.view];
    [self addChildViewController:self.riqiAlert];
    self.riqiAlert.view.alpha = 0;

    
    //点击末次月经按钮
    [self.lastYuejingButton addTarget:self action:@selector(lastYuejingButtonClick) forControlEvents:UIControlEventTouchUpInside];
    //点击天数按钮
    [self.yuejingDateButton addTarget:self action:@selector(yuejingDateButtonClick) forControlEvents:UIControlEventTouchUpInside];
}

-(void)lastYuejingButtonClick
{
    self.riqiAlert.view.alpha = 1.0;
    self.riqiAlert.alertTitle = @"末次月经日期";
    typeof (self) weakSelf = self;
    self.riqiAlert.contentType = ALertContentTypePickerView;
    self.riqiAlert.pickerView.datePickerMode = UIDatePickerModeDate;
    self.riqiAlert.completionSelected = ^(NSArray * results,BOOL confirm){
        if (confirm) {
            NSDate *time = (NSDate *)results.firstObject;
            weakSelf.lastYuejingLabel.text = [weakSelf toFormatTime:time];
            
        }
    };

}

-(void)yuejingDateButtonClick
{
    self.dataAlert.view.alpha = 1.0;
    self.dataAlert.alertTitle = @"月经周期天数";
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"data" ofType:@"plist"];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
    self.dataAlert.tableViewData = [dict objectForKey:@"days"];
    [self.dataAlert.tableView reloadData];
    self.dataAlert.contentType = ALertContentTypeTableView;
    typeof (self) weakSelf = self;
    self.dataAlert.completionSelected = ^(NSArray * results,BOOL confirm){
        if (confirm) {
            NSString *time = (NSString *)results.firstObject;
            weakSelf.yuejingDateLabel.text = [NSString stringWithFormat:@"%@天",time];
            
        }
    };
}

//点击确定按钮
-(void)clickSureButton
{
    
    NSString *riqiDate = self.lastYuejingLabel.text;
    NSRange yearRang = {0,4};
    NSRange yueRang = {5,2};
    NSRange riRang = {8,2};
    
    NSString *yearStr = [riqiDate substringWithRange:yearRang];
    NSString *yueStr = [riqiDate substringWithRange:yueRang];
    NSString *riStr = [riqiDate substringWithRange:riRang];
    YMLog(@"year = %@  yue = %@   ri = %@",yearStr,yueStr,riStr);
    
    
    
    
    int tian,yuefen,year;
    BOOL runnian = [self panduanrunnian:[yearStr intValue]];
    if([yueStr intValue] + 9 <= 12)
    {
        yuefen = [yueStr intValue] + 9;
        year = [yearStr intValue];
    }
    else
    {
        yuefen = [yueStr intValue] - 3;
        year = [yearStr intValue] + 1;
    }
    int mon = [self.dateArray[[yueStr intValue]]intValue];
    if(runnian == 1)
    {
        if([yueStr intValue] == 2)
        {
            mon = mon + 1;
        }
    }
    
    
    if([riStr intValue] + 7 <= mon)
    {
        tian = [riStr intValue] + 7;
    }
    else
    {
        tian = (([riStr intValue] + 7) % mon);
        yuefen += 1;
        if(yuefen >= 13)
        {
            yuefen = yuefen % 12;
            year += 1;
        }
    }
    self.yuchanqiTimeLabel.text = [NSString stringWithFormat:@"%d年%d月%d日",year,yuefen,tian];
    
    NSRange yearRange = {0,4};
    NSRange monthRange = {4,2};
    NSRange dayRange = {6,2};
    NSString *currentYear = [self.currentTime substringWithRange:yearRange];
    NSString *currentmonth = [self.currentTime substringWithRange:monthRange];
    NSString *currentDay = [self.currentTime substringWithRange:dayRange];
    int allDays = 0;
    //同一年
    if([yearStr isEqualToString:currentYear])
    {
        //同一yue
        if([yueStr intValue] == [currentmonth intValue])
        {
            allDays += [currentDay intValue] - [riStr intValue] - 1;
        }
        else
        {
            for(int i = [yueStr intValue] + 1;i < [currentmonth intValue];i ++)
            {
                allDays += [self.dateArray[i]intValue];
            }
            allDays += [self.dateArray[[yueStr intValue]]intValue] - [riStr intValue] + 1;
            allDays += [currentDay intValue] - 1;
        }
        
        
    }
    else //不同年
    {
        allDays = ([currentYear intValue] - [yearStr intValue] - 1) * 365;
        for(int i = [yueStr intValue] + 1;i <= 12;i ++)
        {
            allDays += [self.dateArray[i]intValue];
        }
        
        for(int j = 0; j < [currentmonth intValue];j ++)
        {
            allDays += [self.dateArray[j]intValue];
        }
        
        allDays += [self.dateArray[[yueStr intValue]]intValue] - [riStr intValue] + 1;
        allDays += [currentDay intValue] - 1;
    }
    
    if([self panduanrunnian:[yearStr intValue]] == 1)
    {
        allDays += 1;
    }
        YMLog(@"%d",allDays);
    self.timeLabel.text = [NSString stringWithFormat:@"%d天",280 - allDays];
    if(allDays > 280)
    {
        
    }
    
}


//时间转字符串
-(NSString *)toFormatTime:(NSDate *)time
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *strDate = [dateFormatter stringFromDate:time];
    return strDate;
}

@end
