//
//  babyWeightViewController.m
//  孕妙
//
//  Created by 是源科技 on 16/4/19.
//  Copyright © 2016年 是源科技. All rights reserved.
//


// 计算公式：1.07*BDP*BDP*BDP+0.3*AC*AC*FL  输入单位都是厘米(cm)  BDP 双顶径  AC 腹围  FL 股骨长

#import "babyWeightViewController.h"

@interface babyWeightViewController ()<UIScrollViewDelegate,UIImagePickerControllerDelegate,UIPickerViewDelegate, UIPickerViewDataSource>
@property (nonatomic,strong)UILabel *shuandingjingLabel;
@property (nonatomic,strong)UILabel *fuweiLabel;
@property (nonatomic,strong)UILabel *guguchangLabel;
@property (nonatomic,strong)UILabel *resultLabel;
@property (nonatomic,strong)UIButton *sureButton;

//选择器
@property (nonatomic,strong)xianshikuang *pickerWindow;
@property (nonatomic,weak)UIView *xuanzeqiView;

@property (nonatomic,strong)NSArray *SDJIntArray; //双顶径整数部分
@property (nonatomic,strong)NSArray *FWIntArray; //腹围整数部分
@property (nonatomic,strong)NSArray *GGCIntArray; //股骨长整数部分
@property (nonatomic,strong)NSArray *FloattArray; //小数部分

@end

@implementation babyWeightViewController
{
    NSString *selectIntStr;    //获取整数值
    NSString *selectFloatStr1;  //获取小数值1
    NSString *selectFloatStr2;  //获取小数值2
    NSString *valueStr;     //设置string格式
}

//双顶径整数部分
-(NSArray *)SDJIntArray
{
    if(!_SDJIntArray)
    {
        _SDJIntArray = @[@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"11",@"12",@"13",@"14",@"15",@"16",@"17",@"18",@"19"];
    }
    return _SDJIntArray;
}

//腹围整数部分
-(NSArray *)FWIntArray
{
    if(!_FWIntArray)
    {
        NSMutableArray *tempAaary = [NSMutableArray array];
        for (NSInteger index = 5; index <= 60; index++) {
            NSString *str = [NSString stringWithFormat:@"%ld", (long)index];
            [tempAaary addObject:str];
        }
        _FWIntArray = [NSArray arrayWithArray:tempAaary];
    }
    return _FWIntArray;
}

//股骨长整数部分
-(NSArray *)GGCIntArray
{
    if(!_GGCIntArray)
    {
        _GGCIntArray = @[@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"11",@"12",@"13",@"14",@"15",@"16",@"17",@"18",@"19"];
    }
    return _GGCIntArray;
}

//小数部分
-(NSArray *)FloattArray
{
    if(!_FloattArray)
    {
        _FloattArray = @[@"0", @"1", @"2", @"3", @"4",@"5", @"6", @"7",@"8", @"9"];
    }
    return _FloattArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"胎儿重量预测";
    self.view.backgroundColor = YMColor(245, 245, 245);
    self.shuandingjingLabel = [[UILabel alloc]init];
    self.fuweiLabel = [[UILabel alloc]init];
    self.guguchangLabel = [[UILabel alloc]init];
    
    
    UIScrollView *scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, kDeviceWidth, kDeviceHeight - 20 )];
    scrollView.delegate = self;
    [self.view addSubview:scrollView];
    
    CGFloat y = 5;
    UILabel *jianjieLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, y, kDeviceWidth, 30)];
    jianjieLabel.textColor = YMColor(153, 153, 153);
    jianjieLabel.text = @"请根据B超结果填写以下项";
    jianjieLabel.font = [UIFont systemFontOfSize:16.0];
    [scrollView addSubview:jianjieLabel];
    y += 30;
    
    
    
    for(int i = 1;i < 4;i ++)
    {
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, y, kDeviceWidth, 80)];
        view.tag = i;
        [scrollView addSubview:view];
        y += 80;
        
        if(view.tag == 1)
        {
            [self addControllerToView:view andImageName:@"tool_Biparietal diameter" andLabel:self.shuandingjingLabel andmiaoshuString:@"双顶径"];
        }
        if(view.tag == 2)
        {
            [self addControllerToView:view andImageName:@"tool_babyAc" andLabel:self.fuweiLabel andmiaoshuString:@"腹围"];
        }
        if(view.tag == 3)
        {
            [self addControllerToView:view andImageName:@"tool_babyFl" andLabel:self.guguchangLabel andmiaoshuString:@"股骨长"];
        }
    }
    y += 30;
    
    self.sureButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.sureButton.frame = CGRectMake(kDeviceWidth / 3.0, y, kDeviceWidth / 3.0, kDeviceWidth / 3.0 / 2.22);
    self.sureButton.backgroundColor = YMColor(252, 160, 207);
    self.sureButton.layer.cornerRadius = 8.0;
    [self.sureButton setTitle:@"确  定" forState:UIControlStateNormal];
    [self.sureButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [scrollView addSubview:self.sureButton];
    
    [self.sureButton addTarget:self action:@selector(jisuanBabyWeight) forControlEvents:UIControlEventTouchUpInside];
    
    
    y += self.sureButton.frame.size.height + 10;
    
    UILabel *resultLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, y, kDeviceWidth, 30)];
    resultLabel.text = @"胎儿重量的预测结果";
    resultLabel.textColor = YMColor(199, 107, 156);
    resultLabel.textAlignment = NSTextAlignmentCenter;
    [scrollView addSubview:resultLabel];
    y += resultLabel.frame.size.height;
    
    self.resultLabel = [[UILabel alloc]initWithFrame:CGRectMake(kDeviceWidth / 6.0, y, kDeviceWidth / 3.0 * 2, kDeviceWidth / 3.0 * 2 / 2.0)];
    self.resultLabel.layer.borderColor = [YMColor(252, 160, 207)CGColor];
    self.resultLabel.layer.borderWidth = 1.0;
    self.resultLabel.textAlignment = NSTextAlignmentCenter;
    self.resultLabel.textColor = YMColor(0, 202, 189);
    [scrollView addSubview:self.resultLabel];
    
    
    y += self.resultLabel.frame.size.height;
    scrollView.contentSize = CGSizeMake(kDeviceWidth, y + 64);
    
    UIView *view1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, kDeviceHeight)];
    view1.backgroundColor = [UIColor blackColor];
    self.xuanzeqiView = view1;
    self.xuanzeqiView.hidden = YES;
    self.xuanzeqiView.alpha = 0.5;
    [self.view addSubview:view1];
    
    _pickerWindow = [[xianshikuang alloc] init];
    [self.view addSubview:_pickerWindow];
    self.pickerWindow.hidden = YES;
    
}

-(void)addControllerToView:(UIView *)view andImageName:(NSString *)imageName andLabel:(UILabel *)label andmiaoshuString:(NSString *)str
{
    UILabel *lastLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, 0, kDeviceWidth, 30)];
    lastLabel.text = str;
    lastLabel.textColor = YMColor(153, 153, 153);
    lastLabel.font = [UIFont systemFontOfSize:15.0];
    [view addSubview:lastLabel];
    
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 30, kDeviceWidth, 50)];
    btn.tag = view.tag;
    btn.backgroundColor = [UIColor whiteColor];
    [view addSubview:btn];
    
    UIImageView *image1 = [[UIImageView alloc]initWithFrame:CGRectMake(25, 42.5, 25, 25)];
    image1.image = [UIImage imageNamed:imageName];
    [view addSubview:image1];
    
    label.frame = CGRectMake(kDeviceWidth / 4.0, 30, kDeviceWidth / 2.0, 50);
    label.textColor = YMColor(107, 190, 246);
    label.textAlignment = NSTextAlignmentCenter;
    [view addSubview:label];
    
    UIImageView *jiantou1 = [[UIImageView alloc]initWithFrame:CGRectMake(kDeviceWidth - 50, 42.5, 25, 25)];
    jiantou1.image = [UIImage imageNamed:@"personal_》small"];
    [view addSubview:jiantou1];
    [btn addTarget:self action:@selector(presectInformation:) forControlEvents:UIControlEventTouchUpInside];
}
//点击了计算按钮
-(void)jisuanBabyWeight
{
    float BDP = [self.shuandingjingLabel.text floatValue];
    float AC = [self.fuweiLabel.text floatValue];
    float FL = [self.guguchangLabel.text floatValue];

    // 计算公式：1.07*BDP*BDP*BDP+0.3*AC*AC*FL
    self.resultLabel.text = [NSString stringWithFormat:@"%.2f kg",(1.07 * BDP * BDP * BDP + 0.3 * AC * AC * FL) / 1000.0];
}

-(void)presectInformation:(UIButton *)btn
{
    if(btn.tag == 1)
    {
        self.xuanzeqiView.hidden = NO;
        _pickerWindow.hidden = NO;
        [_pickerWindow setInterFace:@"双顶径"];
        [_pickerWindow addSubview:_pickerWindow.dianL];
        _pickerWindow.dianL.hidden = YES;
        _pickerWindow.selectPickerView.delegate = self;
        _pickerWindow.selectPickerView.dataSource = self;
        _pickerWindow.selectPickerView.tag = btn.tag;
        [_pickerWindow.sureBtn addTarget:self action:@selector(clickSureButton:) forControlEvents:UIControlEventTouchUpInside];
        [_pickerWindow.cancelBtn addTarget:self action:@selector(clickCancelButton) forControlEvents:UIControlEventTouchUpInside];
        _pickerWindow.sureBtn.tag = btn.tag;
        [_pickerWindow.selectPickerView selectRow:3  inComponent:0 animated:YES];
        [_pickerWindow.selectPickerView selectRow:5  inComponent:1 animated:YES];
        [_pickerWindow.selectPickerView selectRow:5  inComponent:2 animated:YES];
        selectIntStr = self.SDJIntArray[3];
        selectFloatStr1 = self.FloattArray[5];
        selectFloatStr2 = self.FloattArray[5];
    }
    if(btn.tag == 2)
    {
        self.xuanzeqiView.hidden = NO;
        _pickerWindow.hidden = NO;
        [_pickerWindow setInterFace:@"腹围"];
        [_pickerWindow addSubview:_pickerWindow.dianL];
        _pickerWindow.dianL.hidden = YES;
        _pickerWindow.selectPickerView.delegate = self;
        _pickerWindow.selectPickerView.dataSource = self;
        _pickerWindow.selectPickerView.tag = btn.tag;
        [_pickerWindow.sureBtn addTarget:self action:@selector(clickSureButton:) forControlEvents:UIControlEventTouchUpInside];
        [_pickerWindow.cancelBtn addTarget:self action:@selector(clickCancelButton) forControlEvents:UIControlEventTouchUpInside];
        _pickerWindow.sureBtn.tag = btn.tag;
        [_pickerWindow.selectPickerView selectRow:15  inComponent:0 animated:YES];
        [_pickerWindow.selectPickerView selectRow:5  inComponent:1 animated:YES];
        [_pickerWindow.selectPickerView selectRow:5  inComponent:2 animated:YES];
        selectIntStr = self.FWIntArray[15];
        selectFloatStr1 = self.FloattArray[5];
        selectFloatStr2 = self.FloattArray[5];
    }
    if(btn.tag == 3)
    {
        self.xuanzeqiView.hidden = NO;
        _pickerWindow.hidden = NO;
        [_pickerWindow setInterFace:@"股骨长"];
        [_pickerWindow addSubview:_pickerWindow.dianL];
        _pickerWindow.dianL.hidden = YES;
        _pickerWindow.selectPickerView.delegate = self;
        _pickerWindow.selectPickerView.dataSource = self;
        _pickerWindow.selectPickerView.tag = btn.tag;
        [_pickerWindow.sureBtn addTarget:self action:@selector(clickSureButton:) forControlEvents:UIControlEventTouchUpInside];
        [_pickerWindow.cancelBtn addTarget:self action:@selector(clickCancelButton) forControlEvents:UIControlEventTouchUpInside];
        _pickerWindow.sureBtn.tag = btn.tag;
        [_pickerWindow.selectPickerView selectRow:4  inComponent:0 animated:YES];
        [_pickerWindow.selectPickerView selectRow:5  inComponent:1 animated:YES];
        [_pickerWindow.selectPickerView selectRow:5  inComponent:2 animated:YES];
        selectIntStr = self.GGCIntArray[4];
        selectFloatStr1 = self.FloattArray[5];
        selectFloatStr2 = self.FloattArray[5];
    }
}

//点击了选择器的确定按钮
- (void)clickSureButton:(UIButton *)btn
{
    valueStr = nil;
    //    _pickerWindow.dianL.hidden = YES;
    _pickerWindow.hidden = YES;
    _pickerWindow.selectPickerView.delegate = nil;
    _pickerWindow.selectPickerView.dataSource = nil;
    self.xuanzeqiView.hidden = YES;
    
    //双顶径
    if(btn.tag == 1)
    {
        valueStr = [NSString stringWithFormat:@"%@.%@%@ cm", selectIntStr,selectFloatStr1,selectFloatStr2];
        self.shuandingjingLabel.text = valueStr;
    }
    //腹围
    if(btn.tag == 2)
    {
       valueStr = [NSString stringWithFormat:@"%@.%@%@ cm", selectIntStr,selectFloatStr1,selectFloatStr2];
        self.fuweiLabel.text = valueStr;
    }
    //股骨长
    if(btn.tag == 3)
    {
        valueStr = [NSString stringWithFormat:@"%@.%@%@ cm", selectIntStr,selectFloatStr1,selectFloatStr2];
        self.guguchangLabel.text = valueStr;
    }
}
//点击了选择器的取消按钮
-(void)clickCancelButton
{
    self.xuanzeqiView.hidden = YES;
    self.pickerWindow.hidden = YES;
    _pickerWindow.selectPickerView.delegate = nil;
    _pickerWindow.selectPickerView.dataSource = nil;
    [_pickerWindow.dianL removeFromSuperview];
}

#pragma mark - UIPickerView
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSInteger number = 0;
    if(pickerView.tag == 1)
    {
        if(component == 0)
        {
            number = self.SDJIntArray.count;
        }
        else
        {
            number = self.FloattArray.count;
        }
    }
    if(pickerView.tag == 2)
    {
        if(component == 0)
        {
            number = self.FWIntArray.count;
        }
        else
        {
            number = self.FloattArray.count;
        }
    }

    if(pickerView.tag == 3)
    {
        if(component == 0)
        {
            number = self.GGCIntArray.count;
        }
        else
        {
            number = self.FloattArray.count;
        }
    }

    
    return number;
    
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *dataStr = nil;
    if(pickerView.tag == 1)
    {
        if(component == 0)
        {
            dataStr = self.SDJIntArray[row];
        }
        else if (component == 1)
        {
            dataStr = self.FloattArray[row];
        }
        else
        {
            dataStr = self.FloattArray[row];
        }
    }
    if(pickerView.tag == 2)
    {
        if(component == 0)
        {
            dataStr = self.FWIntArray[row];
        }
        else if (component == 1)
        {
            dataStr = self.FloattArray[row];
        }
        else
        {
            dataStr = self.FloattArray[row];
        }
    }
    if(pickerView.tag == 3)
    {
        if(component == 0)
        {
            dataStr = self.GGCIntArray[row];
        }
        else if (component == 1)
        {
            dataStr = self.FloattArray[row];
        }
        else
        {
            dataStr = self.FloattArray[row];
        }
    }

    return dataStr;
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if(pickerView.tag == 1)
    {
        if(component == 0)
        {
            selectIntStr = self.SDJIntArray[row];
        }
        else if (component == 1)
        {
            selectFloatStr1 = self.FloattArray[row];
        }
        else
        {
            selectFloatStr2 = self.FloattArray[row];
        }
    }
    if(pickerView.tag == 2)
    {
        if(component == 0)
        {
            selectIntStr = self.FWIntArray[row];
        }
        else if (component == 1)
        {
            selectFloatStr1 = self.FloattArray[row];
        }
        else
        {
            selectFloatStr2 = self.FloattArray[row];
        }
    }
    if(pickerView.tag == 3)
    {
        if(component == 0)
        {
            selectIntStr = self.GGCIntArray[row];
        }
        else if (component == 1)
        {
            selectFloatStr1 = self.FloattArray[row];
        }
        else
        {
            selectFloatStr2 = self.FloattArray[row];
        }
    }

}



@end
