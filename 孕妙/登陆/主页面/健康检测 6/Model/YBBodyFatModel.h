//
//  YBBodyFatModel.h
//  孕妙
//
//  Created by Raoy on 16/6/12.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@class YBCreateTime;
@interface YBBodyFatModel : NSObject


@property (nonatomic, assign) float bmi;
@property (nonatomic, assign) float bone;
@property (nonatomic, assign) float fat;
@property (nonatomic, assign) NSInteger kcal;
@property (nonatomic, assign) float muscle;
@property (nonatomic, assign) float viscerafat;
@property (nonatomic, assign) float water;

@property (nonatomic, strong) YBCreateTime *date;
@property (nonatomic, strong) NSDateComponents *parmaterDateCom;
@end
