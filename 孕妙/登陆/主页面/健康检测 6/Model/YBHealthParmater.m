//
//  YBHealthParmater.m
//  孕妙
//
//  Created by sayes1 on 16/1/24.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "YBHealthParmater.h"
//#import "YBCreateTime.h"

@implementation YBHealthParmater
- (void)setCreateTime:(NSString *)createTime
{
    _createTime = createTime;
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"yyyyMMddHHMMss"];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:[createTime doubleValue] / 1000];//NSTimeInterval
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSUInteger unit = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekOfMonth | NSCalendarUnitWeekday;
    _parmaterDateCom = [cal components:unit fromDate:date];
    NSLog(@"date == %@", _parmaterDateCom);
    NSLog(@"dateStr == %@", [formatter stringFromDate:date]);
    
}

@end
