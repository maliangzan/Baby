//
//  YBPamaterList.h
//  孕妙
//
//  Created by sayes1 on 16/1/22.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YBPamaterList : NSObject

/**
 * 2016年01月22日
 */
@property (nonatomic, copy) NSString *dateStr;
/**
 * 参数值字符串
 */
@property (nonatomic, copy) NSString *valueStr;
@end
