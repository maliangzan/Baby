//
//  YBHealthParmater.h
//  孕妙
//
//  Created by sayes1 on 16/1/24.
//  Copyright © 2016年 是源科技. All rights reserved.
// 从服务器返回的健康监测数据

#import <Foundation/Foundation.h>



@interface YBHealthParmater : NSObject
/**
 * 血压高
 */
@property (nonatomic, assign) NSInteger dbp;
/**
 * 血压低
 */
@property (nonatomic, assign) NSInteger sbp;
/**
 * 血糖
 */
@property (nonatomic, assign) float bloodG;
/**
 * 心率
 */
@property (nonatomic, assign) NSInteger heartRate;
/**
 * 体重
 */
@property (nonatomic, assign) float weight;

//体温
@property (nonatomic, assign)float tem;

//胎心
@property (nonatomic, assign)NSInteger TaiHeartRate;
/**
 * 数据上传的时间
 */
@property (nonatomic, strong) NSString *createTime;
/**
 * 年月日
 */
@property (nonatomic, strong) NSDateComponents *parmaterDateCom;

@end
