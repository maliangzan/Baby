//
//  ScanerQRcodeVC.m
//  孕妙
//
//  Created by Raoy on 16/9/14.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "ScanerQRcodeVC.h"
#import "ScanerVC.h"
#import "passValue.h"


@interface ScanerQRcodeVC ()<QRCodeValueDelegate>

@property (nonatomic, strong)UITextField *textField;
@property (nonatomic, strong)UIButton *btn2;
@end

@implementation ScanerQRcodeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _textField = [[UITextField alloc] initWithFrame:CGRectMake(10,
                                                                 110,
                                                                 DeviceWidth * 0.7,
                                                                 40)];
    _textField.layer.cornerRadius = 10.0;
    _textField.layer.borderColor = SYColor(255, 150, 200).CGColor;
    _textField.layer.borderWidth = 2;
    _textField.placeholder = @"请扫描设备二维码获取序列号";
    _textField.backgroundColor = [UIColor whiteColor];
    _textField.enabled = NO;
    _textField.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:_textField];
    
    UILabel *templlb2 = [[UILabel alloc] initWithFrame:CGRectMake(10,
                                                                  155,
                                                                  DeviceWidth * 0.7,
                                                                  40)];
    
    templlb2.textAlignment = NSTextAlignmentLeft;
    templlb2.numberOfLines = 0;
    templlb2.textColor = [UIColor grayColor];
    templlb2.font = [UIFont systemFontOfSize:13];
    templlb2.text = @"二维码位置请查看包装或附带的操作说明书";
    [self.view addSubview:templlb2];
    
    UIButton *btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn1 setImage:[UIImage imageNamed:@"scanQRcode"] forState:UIControlStateNormal];
    CGFloat btn1Width =  DeviceWidth - CGRectGetMaxX(_textField.frame) - 10;
    btn1.frame = CGRectMake(CGRectGetMaxX(_textField.frame),
                            90,
                            btn1Width,
                            btn1Width);
    [self.view addSubview:btn1];
    [btn1 addTarget:self action:@selector(btn1ButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *templlb3 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_textField.frame),
                                                                  CGRectGetMaxY(btn1.frame),
                                                                  btn1Width,
                                                                  30)];
    templlb3.textColor = SYColor(255, 150, 200);
    templlb3.text = @"点击扫描";
    templlb3.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:templlb3];
    
    _btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    _btn2.frame = CGRectMake(DeviceWidth * 0.5 - 50,
                            CGRectGetMaxY(btn1.frame) + 80,
                            100,
                            40);
    _btn2.backgroundColor = SYColor(255, 150, 200);
    [_btn2 setTitle:@"激活" forState:UIControlStateNormal];
    _btn2.layer.cornerRadius = 10.0;
    _btn2.enabled = NO;
    [self.view addSubview:_btn2];
    [_btn2 addTarget:self action:@selector(btn2ButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [_btn2 setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    
}

- (void)dealloc
{
    
}

- (void)btn1ButtonClick:(UIButton *)btn
{
    ScanerVC *scanVC = [[ScanerVC alloc] init];
    scanVC.qrCodedelegate = self;
    [self.navigationController pushViewController:scanVC animated:YES];
}

- (void)btn2ButtonClick:(UIButton *)btn
{
    [self PostAvtivateCode];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)qrCodeValueDelegate:(NSString *)qrCode
{
    _btn2.enabled = YES;
    self.textField.text = qrCode;
}

//上传激活码
- (void)PostAvtivateCode
{
    //创建请求管理对象
    AFHTTPRequestOperationManager *mgr = [AFHTTPRequestOperationManager manager];
    //设置请求参数
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:[passValue passValue].passValueName forKey:@"activationAccount"];
    [params setObject:self.textField.text forKey:@"productCode"];
    [mgr POST:URL_Health_Hardware_Sys_ProductSales_Activate parameters:params success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        NSLog(@"responseObject = %@", responseObject);
        if ([responseObject objectForKey:@"response"]) {
            if ([[responseObject objectForKey:@"code"] integerValue]) {
                [MBProgressHUD showError:[responseObject objectForKey:@"msg"]];
            }else {
                [MBProgressHUD showSuccess:@"激活成功"];
                [self.navigationController popViewControllerAnimated:YES];
            }
        } else {
            [MBProgressHUD showError:[responseObject objectForKey:@"msg"]];
        }
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [MBProgressHUD showError:@"请求网络失败，请检查网络连接"];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
