//
//  ScanerPhoto.h
//  Photo
//
//  Created by Raoy on 16/9/13.
//  Copyright © 2016年 Raoy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScanerPhoto : UIView

// 扫描区域边长
@property(nonatomic, assign)CGFloat scanAreaEdgeLength;

//扫描区域，用与修正边长
@property(nonatomic, assign, readonly) CGRect scanAreaRect;


@end
