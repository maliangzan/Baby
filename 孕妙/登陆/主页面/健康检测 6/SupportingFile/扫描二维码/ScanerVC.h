//
//  ScanerVC.h
//  Photo
//
//  Created by Raoy on 16/9/13.
//  Copyright © 2016年 Raoy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@protocol QRCodeValueDelegate<NSObject>
@optional
- (void)qrCodeValueDelegate:(NSString *)qrCode;
@end

@interface ScanerVC : UIViewController
@property (nonatomic, weak) id<QRCodeValueDelegate> qrCodedelegate;
@end
