//
//  YBHealthTableViewCell.m
//  孕妙
//
//  Created by Raoy on 16/4/7.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "YBHealthTableViewCell.h"

@implementation YBHealthTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.HealthValueLabel setTextColor:SYColor(80, 80, 80)];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
