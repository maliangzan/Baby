//
//  YBHealthControllerNew.h
//  孕妙
//
//  Created by Raoy on 16/4/7.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YBHealthControllerNew : UIViewController

//距离预产期天数
@property(assign, nonatomic)NSInteger dayCountInt;
@end
