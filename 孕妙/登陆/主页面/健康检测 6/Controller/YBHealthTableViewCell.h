//
//  YBHealthTableViewCell.h
//  孕妙
//
//  Created by Raoy on 16/4/7.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YBHealthTableViewCell : UITableViewCell
@property(weak, nonatomic)  IBOutlet UIImageView *Healthimage;
@property(weak, nonatomic) IBOutlet UILabel *HealthLable;
@property (weak, nonatomic)IBOutlet UILabel *HealthValueLabel;
    

@end
