//
//  TestItemManagerController.m
//  孕妙
//
//  Created by Raoy on 16/4/7.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "TestItemManagerController.h"
#import "TestItemTableViewCell.h"
#import "AFNetworking.h"
#import "MJExtension.h"
#import "passValue.h"

#define cellHeight (50 * (DeviceHeight / 480))

@interface TestItemManagerController ()<UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic)UITableView *testTableView;
@property (strong, nonatomic)UITextView  *adviceTV;
@property (copy, nonatomic)NSArray *arrayItem;
@property (strong, nonatomic)NSArray *imageItemArray;
@property (strong, nonatomic)UIButton *buyBtn;
@property (copy, nonatomic)NSMutableArray *itemarray;
@end

@implementation TestItemManagerController


- (NSMutableArray *)itemarray
{
    if (_itemarray == nil) {
        _itemarray = [NSMutableArray array];
    }
    return _itemarray;
}

- (NSArray *)arrayItem
{
    if (_arrayItem == nil) {
        _arrayItem = [NSArray arrayWithObjects:@"体成分", @"体温", @"血糖", @"血压", @"胎心", @"心率", nil];
        
    }
    return _arrayItem;
}

- (NSArray *)imageItemArray
{
    if (_imageItemArray == nil) {
        _imageItemArray = [NSArray arrayWithObjects:@"health_weight", @"health_temperature", @"health_bloodsugar", @"health_mmHg", @"health_FHR", @"health_bpm", nil];
    }
    return _imageItemArray;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"设备管理";
    self.view.backgroundColor = SYColor(245, 245, 245);
    self.automaticallyAdjustsScrollViewInsets = NO;
//    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemWithIcon:@"YB_backToSupper" highIcon:nil target:self action:@selector(clickLeftButtonToSupper:)];
    //提示文字
    [self loadtextView];
    //控制开关的表视图
    [self loadTableView];
    //提示购买的button
    
    [self buyView];
    // Do any additional setup after loading the view.
}

- (void)clickLeftButtonToSupper:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)loadtextView
{
    _adviceTV = [[UITextView alloc] initWithFrame:CGRectMake(0, SYNavigateHeight + 5, DeviceWidth, 55)];
    _adviceTV.text = @"    开启管理后，健康监测页面将会显示您的健康数据，提醒您每天关注。";
    _adviceTV.textColor = SYColor(110, 110, 110);
    _adviceTV.editable = YES;
    _adviceTV.backgroundColor = [UIColor whiteColor];
    _adviceTV.textAlignment = NSTextAlignmentLeft;
    _adviceTV.selectable = NO;
    if (SYDevice4s) {
        _adviceTV.font = [UIFont systemFontOfSize:16];
    } else if (SYDevice5) {
        _adviceTV.font = [UIFont systemFontOfSize:16];
    } else if (SYDevice6) {
        _adviceTV.font = [UIFont systemFontOfSize:18];
    } else if (SYDevice6p) {
        _adviceTV.font = [UIFont systemFontOfSize:20];
    }
    [self.view addSubview:self.adviceTV];
    
}

- (void)loadTableView
{
    _testTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.adviceTV.frame)+ 10, DeviceWidth , self.arrayItem.count * cellHeight) style:UITableViewStylePlain];
    _testTableView.backgroundColor = [UIColor whiteColor];
    _testTableView.bounces = NO;
    _testTableView.delegate = self;
    _testTableView.dataSource = self;
    [self.view addSubview:self.testTableView];
}

- (void)buyView
{
    NSString *aaText = @"还没有幸孕儿设备？ 立即前往购买";
    _buyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _buyBtn.frame = CGRectMake(0, CGRectGetMaxY(self.testTableView.frame) , DeviceWidth, DeviceHeight - CGRectGetMaxY(self.testTableView.frame) - 10);
    
    [_buyBtn setTitle:aaText forState:UIControlStateNormal];
    _buyBtn.backgroundColor = SYColor(245, 245, 245);
//    _buyBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    [_buyBtn setTitleColor:SYColor(248, 152, 202)forState:UIControlStateNormal];
   
//    _buyBtn.backgroundColor = SYColor(245, 245, 245);
    [_buyBtn addTarget:self action:@selector(buyBtnclick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_buyBtn];
    UIView *hongseLabel = [[UIView alloc] initWithFrame:CGRectMake(0, DeviceHeight - 10, DeviceWidth, 10)];
    hongseLabel.backgroundColor = SYColor(253, 207, 230);
    [self.view addSubview:hongseLabel];
}

- (void)buyBtnclick:(UIButton *)btn
{
    UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, SYNavigateHeight, DeviceWidth, DeviceHeight)];
   
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:weixin_url]];
    [webView loadRequest:request];
    [self.view addSubview:webView];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark TableDelegate

//分行数section
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return  1;
}

//每个section对应的行数
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 6;
}


//行高
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return cellHeight;
}

//返回cell


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellid = @"TestItemCellid";
//    static bool isYES = YES;
    TestItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
    
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"TestItemTableViewCell" owner:self options:nil] lastObject];
        cell.LabelCell.text = [self.arrayItem objectAtIndex:indexPath.row];
//        [cell.LabelCell setTintColor:SYColor(248, 152, 202)];
        cell.ImageViewCell.frame = CGRectMake(0, 5, cellHeight, cellHeight);
        [cell.ImageViewCell setImage:[UIImage imageNamed:[self.imageItemArray objectAtIndex:indexPath.row]]];
        cell.switchCell.onTintColor = SYColor(248, 152, 202);
        cell.switchCell.tintColor = SYColor(200, 200, 200);
        cell.switchCell.tag = indexPath.row;
        cell.backgroundColor = SYColor(245, 245, 245);
        if (self.itemNameMgr) {
        //NSString 转NSData
        NSData *tempData = [self.itemNameMgr dataUsingEncoding:NSUTF8StringEncoding];
        //NSData 转Byte数组
        Byte *tempByte = (Byte*)[tempData bytes];
        [self.itemarray addObject:[NSString stringWithFormat:@"%c", (unsigned char)tempByte[indexPath.row]]];
        if (tempByte[indexPath.row] == '0') {
             cell.switchCell.on = NO;
        }
        
        [cell.switchCell addTarget:self action:@selector(swithValue:) forControlEvents:UIControlEventValueChanged];
    }
    }
    
    return cell;
}


- (void)swithValue:(UISwitch *)sw
{
    if (sw.isOn) {
        [self.itemarray replaceObjectAtIndex:sw.tag withObject:@"1"];
    }else{
    
        [self.itemarray replaceObjectAtIndex:sw.tag withObject:@"0"];
    }
    
    [self PostUserSetNumberselected];
    sleep(0.5);
}

//上传设备管理数据
- (void)PostUserSetNumberselected
{
    NSString *tempStr = @"";
    NSInteger tempInt = 0;
    for (NSInteger i = 0; i < self.itemarray.count; i ++) {
        tempStr = [NSString stringWithFormat:@"%@%@", tempStr, [self.itemarray objectAtIndex:i]];
        if ([[self.itemarray objectAtIndex:i] isEqualToString:@"1"]) {
            tempInt++;
        }
    }
    NSLog(@"%@", tempStr);
    
    AFHTTPRequestOperationManager *mgr = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:[passValue passValue].passValueName forKey:@"account"];
    [params setObject:[passValue passValue].saveDataUserName forKey:@"username"];
    [params setObject:[NSString stringWithFormat:@"%ld", (long)tempInt] forKey:@"totalnumber"];
    [params setObject:tempStr forKey:@"selectednumber"];
    
    [mgr POST:URL_Health_baby_setNumberOfChoice parameters:params success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        NSLog(@"%@", responseObject);
        if ([responseObject objectForKey:@"response"]) {
//            [MBProgressHUD showSuccess:@"数据上传成功"];
        } else {
            [MBProgressHUD showError:[responseObject objectForKey:@"message"]];
        }
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [MBProgressHUD showError:@"上传失败"];
    }];
    
}


@end
