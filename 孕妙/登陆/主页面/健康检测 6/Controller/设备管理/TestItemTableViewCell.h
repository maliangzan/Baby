//
//  TestItemTableViewCell.h
//  孕妙
//
//  Created by Raoy on 16/4/7.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestItemTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *ImageViewCell;
@property (weak, nonatomic) IBOutlet UILabel *LabelCell;
@property (weak, nonatomic) IBOutlet UISwitch *switchCell;
@end
