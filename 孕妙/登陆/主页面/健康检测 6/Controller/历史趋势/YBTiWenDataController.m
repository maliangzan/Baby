//
//  YBTiWenDataController.m
//  孕妙
//
//  Created by Raoy on 16/4/5.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "YBTiWenDataController.h"

@interface YBTiWenDataController ()


@end

@implementation YBTiWenDataController

- (void)setTiWenF:(float)tiWenF
{
    if (!_tiWenF) {
        _tiWenF = tiWenF;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"体温趋势";
//    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemWithIcon:@"YB_backToSupper"  highIcon:nil target:self action:@selector(clickLeftButtonToSupper3:)];
    self.automaticallyAdjustsScrollViewInsets = NO;
   
    NSArray *tempArray = @[@0, @32, @34, @36, @38, @40, @42, @44, @0];
    [self setupChartView:tempArray flagIndex:1];
    
    NSString *adviceStr = @"女性怀孕时的基础体温是呈高温状态，一般体温要稍高一点，大约37度半到38度就是正常的，且会持续16周左右，这是因为孕激素具体升温作用。因此，导致女性怀孕后体温上升";
//    [self getUserParmaterData2:1];
    [self setCalendarDateL:1 adviceText:adviceStr];
    
    [self setHistoryList:@"体温历史记录" listTable:@"摄氏度 °C"];
     [self getUserParmaterData:@"*"andBmi:nil];
    // Do any additional setup after loading the view.
}

- (void)clickLeftButtonToSupper3:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
