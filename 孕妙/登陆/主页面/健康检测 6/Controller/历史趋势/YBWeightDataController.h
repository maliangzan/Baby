//
//  YBWeightDataController.h
//  孕妙
//
//  Created by Raoy on 16/4/11.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YBHistoryDataController.h"

@interface YBWeightDataController : YBHistoryDataController

@property(assign, nonatomic)float weightValue; //体重值
/**
 *
 */
@property (nonatomic, assign) NSString * bmiStr;
@end
