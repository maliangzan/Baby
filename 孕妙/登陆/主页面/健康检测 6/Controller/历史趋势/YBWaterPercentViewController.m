//
//  YBWaterPercentViewController.m
//  孕妙
//
//  Created by Raoy on 16/6/9.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "YBWaterPercentViewController.h"

@interface YBWaterPercentViewController ()

@end

@implementation YBWaterPercentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"水分含量";
//    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemWithIcon:@"YB_backToSupper" highIcon:nil target:self action:@selector(clickLeftButtonToSupper:)];
    NSArray *tempArry = @[@0, @40, @50, @60, @70, @80, @90, @100, @0];
    [self setupChartView:tempArry flagIndex:9];
    NSString *adviceStr = @"";
    [self setCalendarDateL:9 adviceText:adviceStr];
    [self setHistoryList:@"水分率历史记录" listTable:@"百分比" fatIndex:4];
    [self getBodyFatData];

}

- (void)clickLeftButtonToSupper:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
