//
//  YBXueyaDataController.m
//  孕妙
//
//  Created by Raoy on 16/4/11.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "YBXueyaDataController.h"

@interface YBXueyaDataController ()

@end

@implementation YBXueyaDataController



- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"血压趋势";
    self.view.backgroundColor = [UIColor whiteColor];
//    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemWithIcon:@"YB_backToSupper"  highIcon:nil target:self action:@selector(clickLeftButtonToSupper3:)];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    
    [self setupChartXueYaView];
    
    NSString *adviceStr = @"血压正常范围收缩压90~139mmHg，舒张压61~89mmHg。高于这个范围可能是高血压或临界高血压，低于这个范围就可能是低血压。";
    _xueyaValueH = 121;
    _xueyaValuL = 89;
    
//    [self getUserParmaterData2:3];
    [self setCalendarDateL:3 adviceText:adviceStr];
    
    [self setHistoryList:@"血压历史记录" listTable:@"高／低血压 mmHg"];
    [self getUserParmaterData:@"2" andBmi:nil];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)clickLeftButtonToSupper3:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


@end
