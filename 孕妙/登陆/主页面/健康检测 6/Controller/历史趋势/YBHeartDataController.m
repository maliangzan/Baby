//
//  YBHeartDataController.m
//  孕妙
//
//  Created by Raoy on 16/4/11.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "YBHeartDataController.h"

@interface YBHeartDataController ()

@end

@implementation YBHeartDataController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"心率趋势";
    self.view.backgroundColor = [UIColor whiteColor];
//    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemWithIcon:@"YB_backToSupper"  highIcon:nil target:self action:@selector(clickLeftButtonToSupper3:)];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    
    _heartRateValue = 0;
    NSArray *tempArray = @[@0, @40, @60, @80, @100, @120, @140, @160,@0];
    [self setupChartView:tempArray flagIndex:4];
    NSString *adviceStr = @"成人正常心率60～100次/分，大多数为60～80次/分；女生稍快。";
//    [self getUserParmaterData2:5];
    [self setCalendarDateL:5 adviceText:adviceStr];

    [self setHistoryList:@"心率历史记录" listTable:@"心率 次/min"];
    [self getUserParmaterData:@"*" andBmi:nil];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)clickLeftButtonToSupper3:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
