//
//  YBBoneViewController.m
//  孕妙
//
//  Created by Raoy on 16/6/12.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "YBBoneViewController.h"

@interface YBBoneViewController ()

@end

@implementation YBBoneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"骨骼含量";
//    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemWithIcon:@"YB_backToSupper" highIcon:nil target:self action:@selector(clickLeftButtonToSupper:)];
    NSArray *tempArry = @[@0, @1, @2, @3, @4, @5];
    [self setupChartView:tempArry flagIndex:11];
    NSString *adviceStr = @"";
    [self setCalendarDateL:11 adviceText:adviceStr];
    [self setHistoryList:@"骨骼历史记录" listTable:@"kg" fatIndex:6];
    [self getBodyFatData];
}
- (void)clickLeftButtonToSupper:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
