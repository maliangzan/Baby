//
//  YBTiWenDataController.h
//  孕妙
//
//  Created by Raoy on 16/4/5.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YBHistoryDataController.h"

@interface YBTiWenDataController : YBHistoryDataController

@property (assign, nonatomic) float tiWenF;          //体温值
@property(copy, nonatomic) NSString    *dateStr1;    //测试日期

@end
