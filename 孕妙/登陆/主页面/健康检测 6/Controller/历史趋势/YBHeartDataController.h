//
//  YBHeartDataController.h
//  孕妙
//
//  Created by Raoy on 16/4/11.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YBHistoryDataController.h"

@interface YBHeartDataController : YBHistoryDataController

@property(assign, nonatomic)NSInteger heartRateValue; //心率值
@end
