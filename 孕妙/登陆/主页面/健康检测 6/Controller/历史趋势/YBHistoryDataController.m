//
//  YBHistoryDataControlle.m
//  孕妙
//
//  Created by Raoy on 16/4/5.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "YBHistoryDataController.h"
#import "passValue.h"
#import "AFNetworking.h"
#import "MJExtension.h"
#import "YBParmaterListView.h"
#import "YBHealthParmater.h"

#import "YBBodyFatModel.h"
#define lineH (LayoutHeight * 0.297 - SYMargin20)

//体重增长
#import "DVLineChartView.h"

@interface YBHistoryDataController() <DVLineChartViewDelegate>

@property(strong, nonatomic)UIView *adviewView;
@property (nonatomic, strong) YBChartScrollView *chartScrollView;
@property (nonatomic, assign) CGFloat lastScale;
@property (nonatomic, assign) BOOL isWeekChart;
@property (nonatomic, assign) BOOL isMonthChart;
@property (nonatomic, assign) BOOL isYearChart;
@property (nonatomic, strong) NSArray *data;
/**
 捏合
 */
@property (nonatomic, assign) BOOL isReduceChart;
/**
 拉伸
 */
@property (nonatomic, assign) BOOL isEnlargeChart;
/**
 向右滑动
 */
@property (nonatomic, assign) BOOL isSlideToRight;
/**
 向左滑动
 */
@property (nonatomic, assign) BOOL isSlideToLeft;
/**
 纵坐标view
 */
@property (nonatomic, weak) YBYChartView *chartView;
//日期
@property(strong, nonatomic)UILabel *calendarDateL;


//建议view
@property(strong, nonatomic)UIView *adviceView;

//目标value
@property(strong, nonatomic)UILabel *objectValue;

//数据模型
@property(strong, nonatomic)YBParmaterListView *parmaterListView;
//当前日起
@property(copy, nonatomic) NSString    *dateStr;
//测试结果
@property (nonatomic, strong) UILabel *measureResultL;
@property float BMI;
@end

@implementation YBHistoryDataController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];

}

- (void)setData:(NSArray *)data
{
    _data = data;
}


- (NSString *)dateStr
{
    if (!_dateStr) {
        NSDateComponents *iphoneDateComp = [self convertFromDateToDateComp:[NSDate date]];
        _dateStr = [NSString stringWithFormat:@"%ld年%02ld月%02ld日",(long)iphoneDateComp.year, (long)iphoneDateComp.month, (long)iphoneDateComp.day];
    }
    
    return _dateStr;
}

- (void)setupChartView:(NSArray *)array flagIndex:(NSInteger)index
{
    self.isWeekChart = YES;
    self.isMonthChart = NO;
    self.isYearChart = NO;
    self.isReduceChart = NO;
    self.isEnlargeChart = NO;
    self.isSlideToLeft = NO;
    self.isSlideToRight = NO;
    self.lastScale = 1.0;
    //健康数值范围
    UIView *view1 = [[UIView alloc] init];
    [self.view addSubview:view1];
    
    YBChartScrollView *chartScrollView = [[YBChartScrollView alloc] init];
    self.chartScrollView = chartScrollView;
    self.chartScrollView.YBDelegate = self;
    chartScrollView.measureResult = self.data;
    self.automaticallyAdjustsScrollViewInsets = NO;
    chartScrollView.directionalLockEnabled = YES;
    [self.view addSubview:chartScrollView];
    
    
    //设置画图类别
    self.chartScrollView.isWeekChart = self.isWeekChart;
    self.chartScrollView.isMonthChart = self.isMonthChart;
//    self.chartScrollView.isYearChart = self.isYearChart;
    self.chartScrollView.weightLine = YES;
    
    //为scrollView添加捏合手势
    UIPinchGestureRecognizer *zoomGestRec = [[UIPinchGestureRecognizer alloc] init];
    [self.chartScrollView addGestureRecognizer:zoomGestRec];
    zoomGestRec.scale = 1;
    [zoomGestRec addTarget:self action:@selector(chartScrollviewZoomGestRec:)];
    
    //为scrollView添加滑动手势
    UISwipeGestureRecognizer *slideRecongizer = [[UISwipeGestureRecognizer alloc] init];
    [slideRecongizer setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.chartScrollView addGestureRecognizer:slideRecongizer];
    [slideRecongizer addTarget:self action:@selector(handleSwipeFrom:)];
    
    slideRecongizer = [[UISwipeGestureRecognizer alloc] init];
    [slideRecongizer setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self.chartScrollView addGestureRecognizer:slideRecongizer];
    [slideRecongizer addTarget:self action:@selector(handleSwipeFrom:)];
    
    CGFloat chartScrollviewX = SYMargin20 + SYMargin10;
    CGFloat chartScrollviewY = SYNavigateHeight;
    CGFloat chartScrollviewW = DeviceWidth * 10;
    CGFloat chartScrollviewH = LayoutHeight * 0.297;
    chartScrollView.frame = CGRectMake(chartScrollviewX, chartScrollviewY, chartScrollviewW, chartScrollviewH);
//    chartScrollView.backgroundColor = SYColor(254, 231, 243);
    chartScrollView.backgroundColor = [UIColor clearColor];
    chartScrollView.weightLine = NO;
    chartScrollView.xueyaLine = NO;
    chartScrollView.xuetangLine = NO;
    chartScrollView.heartLine = NO;
    chartScrollView.tiwenLine = NO;
    
    switch (index) {
        case 1:
            chartScrollView.tiwenLine = YES;
            break;
        case 2:
            chartScrollView.weightLine = YES;
            
            break;
        case 3:
            chartScrollView.xuetangLine = YES;
            view1.frame =CGRectMake(SYMargin30, SYNavigateHeight + lineH / 7 * 4.5, DeviceWidth - SYMargin30,  lineH / 7);
            view1.backgroundColor = SYColor(200, 230, 200);
            break;
        case 4:
            chartScrollView.heartLine = YES;
            view1.frame = CGRectMake(SYMargin30, SYNavigateHeight + lineH / 8 * 4, DeviceWidth - SYMargin30, lineH / 8 * 2);
            view1.backgroundColor = SYColor(200, 230, 200);
            break;
        case 5:
            break;
        case 6:
            chartScrollView.fatLine = YES;
            chartScrollView.yNumber = index;
            break;
        case 7:
            chartScrollView.fatLine = YES;
            chartScrollView.yNumber = index;
            break;
        case 8:
            chartScrollView.fatLine = YES;
            chartScrollView.yNumber = index;
            break;
        case 9:
            chartScrollView.fatLine = YES;
            chartScrollView.yNumber = index;
            break;
        case 10:
            chartScrollView.fatLine = YES;
            chartScrollView.yNumber = index;
            break;
        case 11:
            chartScrollView.fatLine = YES;
            chartScrollView.yNumber = index;
            break;
        case 12:
            chartScrollView.fatLine = YES;
            chartScrollView.yNumber = index;
            break;
        default:
            break;
    }
    
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, SYNavigateHeight, SYMargin10, chartScrollviewH)];
//    leftView.backgroundColor = SYColor(254, 231, 243);
    leftView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:leftView];
    
    //纵坐标
    YBYChartView *chartView = [[YBYChartView alloc] init];
    CGFloat chartViewX = SYMargin10;
    CGFloat chartViewY = SYNavigateHeight;
    CGFloat chartViewW = SYMargin20;
    CGFloat chartViewH = chartScrollviewH;
    chartView.frame = CGRectMake(chartViewX, chartViewY, chartViewW, chartViewH);
    self.chartView = chartView;
//    chartView.backgroundColor = SYColor(254, 231, 243);
    chartView.backgroundColor = [UIColor clearColor];
    chartView.canshuValueArray = [NSArray arrayWithArray:array];
    [self.view addSubview:chartView];
    
    UILabel *YLL = [[UILabel alloc] initWithFrame:CGRectMake(0, chartViewY, SYMargin30, 20)];
    YLL.text = @"Y轴";
    YLL.font = [UIFont systemFontOfSize:9];
    YLL.textColor = SYColor(83, 83, 83);
    YLL.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:YLL];

}


- (void)setupChartXueYaView
{
    self.isWeekChart = YES;
    self.isMonthChart = NO;
    self.isYearChart = NO;
    self.isReduceChart = NO;
    self.isEnlargeChart = NO;
    self.isSlideToLeft = NO;
    self.isSlideToRight = NO;
    self.lastScale = 1.0;
    //健康数值范围
    UIView *view1 = [[UIView alloc] init];
    view1.frame = CGRectMake(SYMargin30, SYNavigateHeight + lineH / 6 * 3.5, DeviceWidth - SYMargin30, lineH / 6 );
    view1.backgroundColor = SYColor(210, 240, 250);
    [self.view addSubview:view1];
    UIView *view2 = [[UIView alloc] init];
    view2.frame = CGRectMake(SYMargin30, CGRectGetMaxY(view1.frame), DeviceWidth - SYMargin30, lineH / 6 * 0.5);
    view2.backgroundColor = SYColor(250, 210, 200);
    [self.view addSubview:view2];
    
    YBChartScrollView *chartScrollView = [[YBChartScrollView alloc] init];
    self.chartScrollView = chartScrollView;
//    self.chartScrollView.YBDelegate = self;
    chartScrollView.measureResult = self.data;
    self.automaticallyAdjustsScrollViewInsets = NO;
    chartScrollView.directionalLockEnabled = YES;
    [self.view addSubview:chartScrollView];
    
    //设置画图类别
    self.chartScrollView.isWeekChart = self.isWeekChart;
    self.chartScrollView.isMonthChart = self.isMonthChart;
//    self.chartScrollView.isYearChart = self.isYearChart;
    self.chartScrollView.weightLine = YES;
    
    //为scrollView添加捏合手势
    UIPinchGestureRecognizer *zoomGestRec = [[UIPinchGestureRecognizer alloc] init];
    [self.chartScrollView addGestureRecognizer:zoomGestRec];
    zoomGestRec.scale = 1;
    [zoomGestRec addTarget:self action:@selector(chartScrollviewZoomGestRec:)];
    
    //为scrollView添加滑动手势
    UISwipeGestureRecognizer *slideRecongizer = [[UISwipeGestureRecognizer alloc] init];
    [slideRecongizer setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.chartScrollView addGestureRecognizer:slideRecongizer];
    [slideRecongizer addTarget:self action:@selector(handleSwipeFrom:)];
    
    slideRecongizer = [[UISwipeGestureRecognizer alloc] init];
    [slideRecongizer setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self.chartScrollView addGestureRecognizer:slideRecongizer];
    [slideRecongizer addTarget:self action:@selector(handleSwipeFrom:)];
    
    CGFloat chartScrollviewX = SYMargin20 + SYMargin10;
    CGFloat chartScrollviewY = SYNavigateHeight;
    CGFloat chartScrollviewW = DeviceWidth * 10;
    CGFloat chartScrollviewH = LayoutHeight * 0.297;
    chartScrollView.frame = CGRectMake(chartScrollviewX, chartScrollviewY, chartScrollviewW, chartScrollviewH);
//    chartScrollView.backgroundColor = SYColor(254, 231, 243);
    chartScrollView.backgroundColor = [UIColor clearColor];
    
    chartScrollView.weightLine = NO;
    chartScrollView.xueyaLine = YES;
    chartScrollView.xuetangLine = NO;
    chartScrollView.heartLine = NO;
    chartScrollView.tiwenLine = NO;
    
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, SYNavigateHeight, SYMargin10, chartScrollviewH)];
//    leftView.backgroundColor = SYColor(254, 231, 243);
    leftView.backgroundColor = [UIColor clearColor];
//    [self.view addSubview:leftView];
    
    //纵坐标
    YBYChartView *chartView = [[YBYChartView alloc] init];
    CGFloat chartViewX = SYMargin10;
    CGFloat chartViewY = SYNavigateHeight;
    CGFloat chartViewW = SYMargin20;
    CGFloat chartViewH = chartScrollviewH;
    chartView.frame = CGRectMake(chartViewX, chartViewY, chartViewW, chartViewH);
    self.chartView = chartView;
    chartView.backgroundColor = SYColor(254, 231, 243);
    chartView.backgroundColor = [UIColor clearColor];

//    chartView.canshuValueArray = @[@0, @40, @60, @80, @100, @120, @140, @160,@180, @200, @220,@0];
    chartView.canshuValueArray = @[@0, @40, @60, @80, @100, @120, @140, @160,@180, @200, @220,@0];
    [self.view addSubview:chartView];
    
    UILabel *YLL = [[UILabel alloc] initWithFrame:CGRectMake(0, chartViewY, SYMargin30, 20)];
    YLL.text = @"Y轴";
    YLL.font = [UIFont systemFontOfSize:9];
    YLL.textColor = SYColor(83, 83, 83);
    YLL.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:YLL];
    
}

- (void)setCalendarDateL:(NSInteger)index  adviceText:(NSString *)comText
{
//    self.calendarDateL = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.chartScrollView.frame), DeviceWidth, LayoutHeight * 0.0495)];
    self.calendarDateL = [[UILabel alloc] initWithFrame:CGRectMake(0, SYNavigateHeight + LayoutHeight * 0.3, DeviceWidth, LayoutHeight * 0.0495)];
    self.calendarDateL.text = self.dateStr;
    self.calendarDateL.textColor = SYColor(106, 66, 102);
    self.calendarDateL.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.calendarDateL];
    
    self.adviceView = [[UIView alloc] init];
    self.adviceView.backgroundColor = SYColor(241, 241, 241);
    [self.view addSubview:self.adviceView];
    
    NSInteger objectFont = 0;
    NSInteger commonFont = 0;
    if (SYDevice6p) {
        objectFont = 30;
        commonFont = 18;
    } else if (SYDevice6) {
        objectFont = 30;
        commonFont = 17;
    } else if (SYDevice5) {
        objectFont = 27;
        commonFont = 15;
    } else if (SYDevice4s) {
        objectFont = 24;
        commonFont = 13;
    }
    
    NSString *textStr = @"200.8kg";
    CGSize weightStrSize = [textStr sizeWithAttributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:objectFont]}];
    _objectValue = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, weightStrSize.height + SYMargin10)];
    
    _objectValue.textColor = SYColor(249, 130, 186);
    _objectValue.font = [UIFont boldSystemFontOfSize:objectFont];
    _objectValue.textAlignment = NSTextAlignmentCenter;
    [self reloadView:index];
    [self.adviceView addSubview:self.objectValue];
    
    
    NSString *measureStr = @"测试状态";
    CGSize measureStrSize = [measureStr sizeWithAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:commonFont]}];
    UILabel *measureStateL = [[UILabel alloc] initWithFrame:CGRectMake(SYMargin20, CGRectGetMaxY(self.objectValue.frame), DeviceWidth * 0.5 - SYMargin20, measureStrSize.height + SYMargin5)];
    measureStateL.text = @"测试状态   静止";
    measureStateL.textAlignment = NSTextAlignmentCenter;
    measureStateL.textColor = SYColor(106, 66, 102);
    measureStateL.font = [UIFont systemFontOfSize:commonFont];
    [self.adviceView addSubview:measureStateL];
    
    UILabel *measureL = [[UILabel alloc] initWithFrame:CGRectMake(DeviceWidth * 0.5, CGRectGetMaxY(self.objectValue.frame), measureStrSize.width, measureStrSize.height + SYMargin5)];
    measureL.text = @"测试结果";
    measureL.textColor = SYColor(106, 66, 102);
    measureL.font = [UIFont systemFontOfSize:commonFont];
    [self.adviceView addSubview:measureL];
    if (index <= 5) {
    _measureResultL = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(measureL.frame) + SYMargin20, CGRectGetMaxY(self.objectValue.frame), DeviceWidth - CGRectGetMaxX(measureL.frame) - SYMargin20, measureStrSize.height + SYMargin5)];
    _measureResultL.text = @"正常";
    }
    [self reloadView:index];
    self.measureResultL.textColor = SYColor(54, 181, 13);
    self.measureResultL.font = [UIFont systemFontOfSize:commonFont];
    [self.adviceView addSubview:self.measureResultL];

    
    
    NSDictionary *commonStrAttr = @{NSFontAttributeName:[UIFont boldSystemFontOfSize:commonFont]};
    CGSize commonStrSize = [comText boundingRectWithSize:CGSizeMake(DeviceWidth - 2 * SYMargin20, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:commonStrAttr context:nil].size;
    UILabel *commonAdviceL = [[UILabel alloc] initWithFrame:CGRectMake(SYMargin20, CGRectGetMaxY(self.measureResultL.frame), commonStrSize.width, commonStrSize.height)];
    commonAdviceL.text = comText;
    commonAdviceL.textColor = SYColor(83, 83, 83);
    commonAdviceL.numberOfLines = 0;
    commonAdviceL.font = [UIFont systemFontOfSize:commonFont];
    [self.adviceView addSubview:commonAdviceL];
    self.adviceView.frame= CGRectMake(0,CGRectGetMaxY(self.calendarDateL.frame), DeviceWidth, CGRectGetMaxY(commonAdviceL.frame) + SYMargin10);
    
}

- (void)setHistoryList:(NSString *)HistoryListStr listTable:(NSString *)listTableStr
{
    self.parmaterListView = [YBParmaterListView parmaterListViewWithframe:CGRectMake(0, CGRectGetMaxY(self.adviceView.frame), DeviceWidth,  DeviceHeight - CGRectGetMaxY(self.adviceView.frame)) titleStr:HistoryListStr parmaterName:listTableStr];
    //    parmaterListView.backgroundColor = [UIColor redColor];
    [self.view addSubview:self.parmaterListView];
    
}

- (void)setHistoryList:(NSString *)HistoryListStr listTable:(NSString *)listTableStr fatIndex:(NSInteger)index
{
    
    self.parmaterListView = [YBParmaterListView parmaterListViewWithframe:CGRectMake(0, CGRectGetMaxY(self.adviceView.frame), DeviceWidth,  DeviceHeight - CGRectGetMaxY(self.adviceView.frame)) titleStr:HistoryListStr parmaterName:listTableStr index:index];
   
    //    parmaterListView.backgroundColor = [UIColor redColor];
    [self.view addSubview:self.parmaterListView];
}
//收缩手势实现方法
- (void)chartScrollviewZoomGestRec:(UIPinchGestureRecognizer *)recognizer
{
    if ([recognizer state] == UIGestureRecognizerStateBegan) {
        SYLog(@"手势开始");
        _lastScale = 1.0;
    }
    
    if (recognizer.scale < 1.0){
    
        self.isReduceChart = YES;
        self.isEnlargeChart = NO;
    }else if (recognizer.scale > 1.0){
    
        self.isReduceChart = NO;
        self.isEnlargeChart = YES;
    }else{
    
        self.isEnlargeChart = NO;
        self.isReduceChart = NO;
    }
    
    if (self.chartScrollView.frame.size.width <= DeviceWidth && self.isReduceChart){
    
        return;
    }
    
//    防止星期的图表缩小
    if (self.isYearChart && self.isReduceChart) {
        return;
    }
//    if (self.isReduceChart) {
//        return;
//    }
    
    //防止星期的图表放大
    if (self.isWeekChart && self.isEnlargeChart) {
        return;
    }
    
    //结束手势
    if ([recognizer state] == UIGestureRecognizerStateEnded) {
        SYLog(@"手势结束");
        if (self.isWeekChart) {
            //周
            if (self.isReduceChart) {
                //缩小，换到月图
                self.isWeekChart = NO;
                self.isMonthChart = YES;
//                self.isYearChart = NO;
                self.chartScrollView.isWeekChart = NO;
                self.chartScrollView.isMonthChart = YES;
//                self.chartScrollView.isYearChart = NO;
            }else{
            
                return;
            }
        }else
//            if(self.isMonthChart)
        {
        
            if (self.isReduceChart) {
                // 缩小，换到年图
//                self.isWeekChart = NO;
//                self.isMonthChart = NO;
//                self.isYearChart = YES;
//                self.chartScrollView.isWeekChart = NO;
//                self.chartScrollView.isMonthChart = NO;
//                self.chartScrollView.isYearChart = YES;
            }else{
            
                // 放大，换到周图
                self.isWeekChart = YES;
                self.isMonthChart = NO;
//                self.isYearChart = NO;
                self.chartScrollView.isWeekChart = YES;
                self.chartScrollView.isMonthChart = NO;
//                self.chartScrollView.isYearChart = NO;
            }
//        }else if (self.isReduceChart){
//        
//            //年
//            if (self.isReduceChart) {
//                //缩小
//                return;
//            } else {
//                //放大，换到月图
//                self.isWeekChart = NO;
//                self.isMonthChart = YES;
////                self.isYearChart = NO;
//                self.chartScrollView.isWeekChart = NO;
//                self.chartScrollView.isMonthChart = YES;
////                self.chartScrollView.isYearChart = NO;
//            }
        }
    }
    
    CGRect currentFrame = self.chartScrollView.frame;
    CGRect newFrame = CGRectMake(CGRectGetMaxX(self.chartView.frame), self.chartView.frame.origin.y, currentFrame.size.width, currentFrame.size.height);
    self.chartScrollView.frame = newFrame;
    _lastScale = [recognizer scale];
}

//滑动手势处理
- (void)handleSwipeFrom:(UISwipeGestureRecognizer *)reconginzer
{
    if (reconginzer.direction == UISwipeGestureRecognizerDirectionRight) {
        SYLog(@"向右滑动");
        self.isSlideToRight = YES;
        self.isSlideToLeft = NO;
        self.chartScrollView.isSlideToLeft = NO;
        self.chartScrollView.isSlideToRight = YES;
    }else
        if(reconginzer.direction == UISwipeGestureRecognizerDirectionLeft)
    {
    
        NSLog(@"向左滑动");
        self.isSlideToRight = NO;
        self.isSlideToLeft = YES;
        self.chartScrollView.isSlideToLeft = YES;
        self.chartScrollView.isSlideToRight = NO;
        
    }
}

//请求体脂数据
- (void)getBodyFatData
{
    
    
    AFHTTPRequestOperationManager *mgr = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:[passValue passValue].passValueName forKey:@"account"];
    [params setObject:[passValue passValue].saveDataUserName forKey:@"username"];
    
    NSDateComponents *tempComp  = [self convertFromDateToDateComp:[NSDate date]];
    params[@"date"] = [NSString stringWithFormat:@"201601%02ld000000",(long)tempComp.day];
    params[@"date1"] = [NSString stringWithFormat:@"%ld%02ld%02ld000000", (long)tempComp.year, (long)tempComp.month, (long)tempComp.day + 1];
    
    params[@"devicename"] = @"zfc";
    [mgr GET: URL_Health_baby_getDevUseInfo parameters:params success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        NSLog(@"responseObject:%@", responseObject);
        if ([responseObject objectForKey:@"response"]) {
            if ([[responseObject objectForKey:@"data"] firstObject]) {
                
                
                NSArray *tempdic = [responseObject objectForKey:@"data"];
                self.data = [YBBodyFatModel mj_objectArrayWithKeyValuesArray:tempdic];
                NSLog(@"data = %@", self.data);
                self.parmaterListView.pamaterArray = self.data;
                self.chartScrollView.measureResult = self.data;
                [self.view setNeedsDisplay];
            }
            
            
        }else{
            [MBProgressHUD showError:[NSString stringWithFormat:@"%@", [responseObject objectForKey:@"message"] ]];
        }
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [MBProgressHUD showError:@"请求数据失败,请检查网络"];
    }];
}


- (void)getUserParmaterData:(NSString *)dateStr andBmi:(NSString *)bmi
{
    //1.创建请求管理对象
    AFHTTPRequestOperationManager *magr = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:[passValue passValue].passValueName forKey:@"account"];
    [params setObject:[passValue passValue].saveDataUserName forKey:@"username"];
    [params setObject:@"phealth" forKey:@"devicename"];
    
    if ([dateStr isEqualToString:@"weight"]) {
        [params setObject:@"*" forKey:@"date"];
    }else{
        [params setObject:dateStr forKey:@"date"];
    }
    
    [magr GET:URL_Health_baby_getHistoryData parameters:params success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        SYLog(@"responseObject:%@", magr);
        NSLog(@"11111111222222222223333%@", responseObject);
        if (responseObject[@"response"]) {
            NSArray *dataDictArray = [responseObject objectForKey:@"data"];
            self.data = [YBHealthParmater mj_objectArrayWithKeyValuesArray:dataDictArray];
            self.parmaterListView.pamaterArray = self.data;
            self.chartScrollView.measureResult = self.data;
            
            
            
            if ([dateStr isEqualToString:@"weight"]) {
                NSArray *data1Array = [responseObject objectForKey:@"data1"];
                NSArray *data2Array = [responseObject objectForKey:@"data2"];
                
                [self weightAddChartView:data1Array data2:data2Array andbmiStr:bmi];
            }
            [self.view setNeedsDisplay];
            SYLog(@"请求成功");
        } else {
            [MBProgressHUD showError:@"请求数据失败,请检查网络"];
        }
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [MBProgressHUD showError:@"请求数据失败,请检查网络"];
    }];
}

/**
 * 获得手机时间(年月日时分秒)
 */
- (NSDateComponents *)convertFromDateToDateComp:(NSDate *)tempDate
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    int unit = NSCalendarUnitDay | NSCalendarUnitMonth |  NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitWeekday;
    
    // 1.获得当前时间的年月日
    NSDateComponents *nowCmps = [calendar components:unit fromDate:tempDate];
    
    return nowCmps;
}

- (void)reloadView:(NSInteger)index
{
    switch (index) {
        case 1:
            _objectValue.text = [NSString stringWithFormat:@"%0.1f °C", self.itemIntvalue3 ];
            if (self.itemIntvalue3 < 37) {
                _measureResultL.text = @"偏低";
                _measureResultL.textColor = SYColor(160, 188, 233);
            }else if (self.itemIntvalue3 > 38)
            {
                _measureResultL.text = @"偏高";
                _measureResultL.textColor = [UIColor redColor];
            }
            break;
        case 2:
            _objectValue.text = [NSString stringWithFormat:@"%0.1f kg", self.itemIntvalue3];
            
            break;
        case 3:
            _objectValue.text = [NSString stringWithFormat:@"%ld mmHg/%ld mmHg", (long)self.itemIntvalue1, (long)self.itemIntvalue2];
            if (self.itemIntvalue1 < 90 || self.itemIntvalue2 < 61) {
                _measureResultL.text = @"偏低";
                _measureResultL.textColor = SYColor(140, 130, 200);
            }else if (self.itemIntvalue1 > 139 || self.itemIntvalue2 > 89)
            {
                _measureResultL.text = @"偏高";
                _measureResultL.textColor = [UIColor redColor];
            }
            break;
        case 4:
            _objectValue.text = [NSString stringWithFormat:@"%0.1f mmol/L", self.itemIntvalue3 ];
            if (self.itemIntvalue3 < 3.3) {
                _measureResultL.text = @"偏低";
                _measureResultL.textColor = SYColor(140, 130, 200);
            }else if (self.itemIntvalue3 > 5.3)
            {
                _measureResultL.text = @"偏高";
                _measureResultL.textColor = [UIColor redColor];
            }
            break;
        case 5:
            _objectValue.text = [NSString stringWithFormat:@"%ld 次/min", (long)self.itemIntvalue1];
            if (self.itemIntvalue1 < 60) {
                _measureResultL.text = @"偏低";
                _measureResultL.textColor = SYColor(140, 130, 200);
            }else if (self.itemIntvalue1 > 100)
            {
                _measureResultL.text = @"偏高";
                _measureResultL.textColor = [UIColor redColor];
            }
            break;
        case 6:
            _objectValue.text = [NSString stringWithFormat:@""];
            
            break;
        default:
            break;
    }
}


//体重增长图
- (void)weightAddChartView:(NSArray *)data1 data2:(NSArray *)data2 andbmiStr:(NSString *)bmiStr
{

    DVLineChartView *ccc = [[DVLineChartView alloc] init];
    [self.view addSubview:ccc];
    ccc.weightData1 = data1;
    ccc.dayData2 = data2;
    
    ccc.yAxisViewWidth = 52;
    ccc.delegate = self;
    ccc.pointUserInteractionEnabled = YES;
    ccc.x = 0;
    ccc.y = SYNavigateHeight;
    ccc.height = LayoutHeight * 0.297;
    ccc.width = self.view.width;
    [ccc setLineDraw:bmiStr];
    
    UILabel *temp1LL = [[UILabel alloc] initWithFrame:CGRectMake(0, SYNavigateHeight, 40, 15)];
    temp1LL.text = @"Y轴(kg)";
    temp1LL.textAlignment = NSTextAlignmentRight;
    temp1LL.font = [UIFont systemFontOfSize:10];
    temp1LL.textColor = SYColor(150, 180, 200);
    [self.view addSubview:temp1LL];
    UILabel *temp2LL = [[UILabel alloc] initWithFrame:CGRectMake(0, SYNavigateHeight + 0.3 * DeviceHeight - 35, 40, 15)];
    temp2LL.text = @"X轴(周)";
    temp2LL.textAlignment = NSTextAlignmentRight;
    temp2LL.font = [UIFont systemFontOfSize:10];
    [self.view addSubview:temp2LL];
    temp2LL.textColor = SYColor(150, 180, 200);
}

- (void)lineChartView:(DVLineChartView *)lineChartView DidClickPointAtIndex:(NSInteger)index {
    
    NSLog(@"%ld", (long)index);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end
