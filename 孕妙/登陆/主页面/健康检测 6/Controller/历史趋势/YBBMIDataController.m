//
//  YBBMIDataController.m
//  孕妙
//
//  Created by Raoy on 16/6/9.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "YBBMIDataController.h"

@interface YBBMIDataController ()

@end

@implementation YBBMIDataController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"BMI趋势";
//    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemWithIcon:@"YB_backToSupper" highIcon:nil target:self action:@selector(clickLeftButtonToSupper:)];
    NSArray *tempArry = @[@0, @20, @25, @30, @35, @0];
    [self setupChartView:tempArry flagIndex:6];
    NSString *adviceStr = @"";
    [self setCalendarDateL:6 adviceText:adviceStr];
    [self setHistoryList:@"BMI历史记录" listTable:@"指数" fatIndex:1];
    [self getBodyFatData];
    
    
    // Do any additional setup after loading the view.
}

- (void)clickLeftButtonToSupper:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
