//
//  YBWeightDataController.m
//  孕妙
//
//  Created by Raoy on 16/4/11.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "YBWeightDataController.h"

@interface YBWeightDataController ()
@property float BMI;
@end

@implementation YBWeightDataController

- (void)setWeightValue:(float)weightValue
{
    _weightValue = weightValue = 50;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"体重增长趋势";
//    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemWithIcon:@"YB_backToSupper"  highIcon:nil target:self action:@selector(clickLeftButtonToSupper3:)];
    self.automaticallyAdjustsScrollViewInsets = NO;

    
//    NSArray *tempArray = @[@0, @40, @60, @80, @100, @0];
//    [self setupChartView:tempArray flagIndex:2];
    
    
    NSString *adviceStr = @"合适的体重增加，有利于确保胎儿和母体组织的最佳发育。孕期体重增长过快或过慢都会对胎儿的正常发育产生不利的影响。";
//    [self getUserParmaterData2:2];
    [self setCalendarDateL:2 adviceText:adviceStr];
    [self setHistoryList:@"体重历史记录" listTable:@"体重 kg"];
    [self getUserParmaterData:@"weight" andBmi:_bmiStr];
//    [self weightAddChartView];
    
    
    
    
}

- (void)clickLeftButtonToSupper3:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
