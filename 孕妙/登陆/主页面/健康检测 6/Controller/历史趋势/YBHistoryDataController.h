//
//  YBHistoryDataView.h
//  孕妙
//
//  Created by Raoy on 16/4/5.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YBChartScrollView.h"
#import "YBYChartView.h"


@interface YBHistoryDataController : UIViewController<YBChartScrollViewDelegate>

@property(assign, nonatomic)NSInteger itemIntvalue1;
@property(assign, nonatomic)NSInteger itemIntvalue2;
@property(assign, nonatomic)float itemIntvalue3;

//设置当前数据和建议
- (void)setCalendarDateL:(NSInteger)index  adviceText:(NSString *)comText;

//设置趋势图
- (void)setupChartView:(NSArray *)array flagIndex:(NSInteger)index;
- (void)setupChartXueYaView;
//设置体重历史列表明和格式
- (void)setHistoryList:(NSString *)HistoryListStr listTable:(NSString *)listTableStr;
- (void)getUserParmaterData:(NSString *)dateStr andBmi:(NSString *)bmi;
- (void)getBodyFatData;
- (void)setHistoryList:(NSString *)HistoryListStr listTable:(NSString *)listTableStr fatIndex:(NSInteger)index;
//- (void)getUserParmaterData2:(NSInteger)index;

//加载体重增长视图
- (void)weightAddChartView;
@end
