//
//  YBXueyaDataController.h
//  孕妙
//
//  Created by Raoy on 16/4/11.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YBHistoryDataController.h"

@interface YBXueyaDataController : YBHistoryDataController

@property(assign, nonatomic)NSInteger xueyaValueH; //收缩压
@property(assign, nonatomic)NSInteger xueyaValuL; //舒张压
@end
