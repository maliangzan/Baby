//
//  YBNengLiangViewController.m
//  孕妙
//
//  Created by Raoy on 16/6/12.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "YBNengLiangViewController.h"

@interface YBNengLiangViewController ()

@end

@implementation YBNengLiangViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"基础代谢";
//    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemWithIcon:@"YB_backToSupper" highIcon:nil target:self action:@selector(clickLeftButtonToSupper:)];
    NSArray *tempArry = @[@0, @1000, @1500, @2000, @2500, @3000, @3500, @4000, @0];
    [self setupChartView:tempArry flagIndex:12];
    NSString *adviceStr = @"";
    [self setCalendarDateL:12 adviceText:adviceStr];
    [self setHistoryList:@"基础代谢历史记录" listTable:@"kcal" fatIndex:7];
    [self getBodyFatData];
}

- (void)clickLeftButtonToSupper:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
