//
//  YBXueTangDataController.m
//  孕妙
//
//  Created by Raoy on 16/4/11.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "YBXueTangDataController.h"

@interface YBXueTangDataController ()

@end

@implementation YBXueTangDataController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"血糖趋势";
    self.view.backgroundColor = [UIColor whiteColor];
//    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemWithIcon:@"YB_backToSupper"  highIcon:nil target:self action:@selector(clickLeftButtonToSupper3:)];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    
    NSArray *tempArray = @[@0, @2, @4, @6, @8, @10, @12, @0];
    [self setupChartView:tempArray flagIndex:3];
    _xuetangValue = 5.8;
    NSString *adviceStr = @"基于空腹（FPG）、任意时间或OGTT中2小时血糖值（2h PG）。孕妇正常血糖范围：空腹血糖3.3~5.3mmol/L，餐后一小时不超过7.8mmol/L，餐后二小时不超过6.7mmol/L。测试结果基于空腹标准.";
//    [self getUserParmaterData2:4];
    [self setCalendarDateL:4 adviceText:adviceStr];
    [self setHistoryList:@"血糖历史记录" listTable:@"血糖 mmol/L"];
    [self getUserParmaterData:@"*" andBmi:nil];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)clickLeftButtonToSupper3:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
