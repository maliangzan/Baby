//
//  YBHealthNavController.m
//  健康监测
//
//  Created by sayes1 on 16/1/11.
//  Copyright © 2016年 sayes1. All rights reserved.
//

// 2.获得RGB颜色
#define SYColor(r, g, b) ([UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1.0])
#import "YBHealthNavController.h"

@interface YBHealthNavController ()

@end

@implementation YBHealthNavController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationBar.translucent = YES;
    self.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    self.navigationBar.barTintColor = SYColor(249, 130, 186);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
@end
