//
//  YBChildBaseController.m
//  TTT
//
//  Created by Raoy on 16/3/28.
//  Copyright © 2016年 Raoy. All rights reserved.
//

#import "YBChildBaseController.h"

//#import "YBLastParmaterData.h"

#import "passValue.h"

#import "YBHealthParmater.h"
#import "MJExtension.h"
#import "AFNetworking.h"

#define heightScale (DeviceHeight / 480)
@interface YBChildBaseController ()

@property(strong, nonatomic)UIView *textview;
@property(strong, nonatomic)UIImageView *instructionsImage;
@property(strong, nonatomic)UILabel *middleLabel;
@property(strong, nonatomic)UIView *buyView;
@property(strong, nonatomic)NSString *dateStr;
@property(strong, nonatomic)UIView *bluetoothView;
@property(strong, nonatomic)NSArray *theDayValue;
@property(copy, nonatomic)NSString *itemChooseStr;

@end

@implementation YBChildBaseController
{
    NSString * dbp ;
    NSString * sbp;
    NSString * bloodG;
    NSString * heartRate;
    NSString * weight;
    NSString * tem;
}
- (UIButton *)rightBtn
{
    if (_rightBtn == nil) {
        _rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//        [_rightBtn setTitle:@"手动输入" forState:UIControlStateNormal];
        [_rightBtn setImage:[UIImage imageNamed:@"record_byhand"] forState:UIControlStateNormal];
        _rightBtn.frame = CGRectMake(0, 10, 30, 30);
        _rightBtn.titleLabel.textAlignment = NSTextAlignmentRight;
    }
    return _rightBtn;
}


- (UIButton *)startbtn2
{
    if (_startbtn2 == nil) {
        _startbtn2 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        
    }
    return _startbtn2;
}

- (UIView *)fuGaiView
{
    if (_fuGaiView == nil) {
        //显示选择器的时候设置一层透明的覆盖View
        UIView *tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight)];
        tempView.backgroundColor = [UIColor blackColor];
        self.fuGaiView = tempView;
        self.fuGaiView.hidden = YES;
        self.fuGaiView.alpha = 0.5;
        [self.view addSubview:self.fuGaiView];
    }
    return _fuGaiView;
}

- (UIImageView *)buletoothImageView
{
    if (_buletoothImageView == nil) {
        _buletoothImageView = [[UIImageView alloc] initWithFrame:CGRectMake(DeviceWidth / 2 - 25, 2, 50, 25)];
    }
    return _buletoothImageView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _textview = [[UIView  alloc]initWithFrame:CGRectMake(0, SYNavigateHeight + 40 * heightScale,  DeviceWidth, 60 * heightScale)];
    _textview.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_textview];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    [self getDate];
    [self getItemTests];
    // Do any additional setup after loading the view.
}

- (void)getDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    _dateStr = [dateFormatter stringFromDate:[NSDate date]];

}

- (void)setTitle:(NSString *)titleName buyTitle:(NSString *)buytitle indexImage:(NSInteger)index
{
    _buyView =  [[UIView alloc] initWithFrame:CGRectMake(0, SYNavigateHeight, DeviceWidth, 40 * heightScale)];
//    _buyView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.buyView];
    UILabel *timeStr = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth - 100, 25 * heightScale)];
    timeStr.text = [NSString stringWithFormat:@"测量时间:%@",self.dateStr];
    timeStr.textAlignment = NSTextAlignmentLeft;
    timeStr.textColor = [UIColor grayColor];
//    timeStr.font = [UIFont systemFontOfSize:15];
    [self.buyView addSubview:timeStr];
    
    //购买button
    _buyBtn = [[UIButton alloc] initWithFrame:CGRectMake(DeviceWidth - 110, 0, 110, 40 * heightScale)];
    [_buyBtn setTitle:buytitle forState:UIControlStateNormal];
    [_buyBtn setTitleColor:SYColor(249, 130, 186) forState:UIControlStateNormal];
    _buyBtn.tag = 10 + index;
    [_buyBtn addTarget:self action:@selector(buyBtnclick:) forControlEvents:UIControlEventTouchUpInside];
    if(_buyBtn.tag == 11 || _buyBtn.tag == 12 || _buyBtn.tag == 14)
    {
        [self.buyView addSubview:_buyBtn];
    }
    
    UILabel *inputL = [[UILabel alloc] initWithFrame:CGRectMake(0, 0,  110, 60 * heightScale)];
    inputL.text = titleName;
//    inputL.font = [UIFont systemFontOfSize:15];
    inputL.numberOfLines = 0;
    inputL.textColor = SYColor(249, 130, 186);
    [self.textview addSubview:inputL];
    
    UIView *useaddviceView2 = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.textview.frame) + 5, DeviceWidth, 200 * heightScale)];
    useaddviceView2.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:useaddviceView2];
    _instructionsImage = [[UIImageView alloc] initWithFrame:CGRectMake(30, 20, DeviceWidth - 60, 160 * heightScale)];
    _instructionsImage.backgroundColor = [UIColor whiteColor];
    [useaddviceView2 addSubview:_instructionsImage];
   
    //蓝牙连接图标
    _bluetoothView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(useaddviceView2.frame) + 5, DeviceWidth, 30 * heightScale)];
    _bluetoothView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_bluetoothView];
    
    [self.buletoothImageView setImage:[UIImage imageNamed:@"蓝牙断开"]];
    [self.bluetoothView addSubview:_buletoothImageView];
    
    self.startbtn2.frame = CGRectMake(DeviceWidth / 2 - 40, CGRectGetMaxY(self.bluetoothView.frame) + 15 * heightScale, 80, 40);
    [self.startbtn2 setTitle:@"开始测量" forState:UIControlStateNormal];
    self.startbtn2.backgroundColor = SYColor(249, 130, 186);
    self.startbtn2.layer.cornerRadius = 10;
    [self.startbtn2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.startbtn2 setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
    [self.view addSubview:self.startbtn2];
    
    _middleLabel = [[UILabel alloc] initWithFrame:CGRectMake(115,  0, 100, 60 * heightScale)];
    _middleLabel.numberOfLines = 0;
    _middleLabel.font = [UIFont systemFontOfSize:15];
    _middleLabel.textColor = SYColor(249, 130, 186);
    _middleLabel.textAlignment = NSTextAlignmentLeft;
    [self.textview addSubview:self.middleLabel];
    
    UIImageView *helpImage = [[UIImageView alloc] initWithFrame:CGRectMake(DeviceWidth - 110, 15, 30, 30)];
    [helpImage setImage:[UIImage imageNamed:@"record_help"]];
    
    
    UIButton *useAddvice = [UIButton buttonWithType:UIButtonTypeCustom];
    useAddvice.frame = CGRectMake(CGRectGetMaxX(helpImage.frame), 5, 85, 50 );
    
   
    
    [useAddvice setTitle:@"使用指导" forState:UIControlStateNormal];
    [useAddvice setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [useAddvice setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    useAddvice.titleLabel.textAlignment = NSTextAlignmentLeft;
    
    [useAddvice addTarget:self action:@selector(useaddviceView:) forControlEvents:UIControlEventTouchUpInside];
    _useAddviceWindow = [[UIView alloc] initWithFrame:CGRectMake(0 , SYNavigateHeight, DeviceWidth, DeviceHeight)];
    _useAddviceWindow.backgroundColor = SYColor(245, 245, 245);
    _useAddviceWindow.hidden = YES;

    [self.view addSubview:_useAddviceWindow];
     [self.textview addSubview:useAddvice];
    [self.textview addSubview:helpImage];

    UITextView *addviceTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight * 0.6 )];
    addviceTextView.editable = NO;
    addviceTextView.backgroundColor = SYColor(245, 245, 245);
    addviceTextView.alpha = 1.0;
    [addviceTextView setFont:[UIFont systemFontOfSize: 15]];
    [_useAddviceWindow addSubview:addviceTextView];
    
    UIButton *knowbtn = [UIButton buttonWithType:UIButtonTypeCustom];
    knowbtn.frame = CGRectMake(DeviceWidth * 0.5 - 50, DeviceHeight * 0.7, 100, 40);
    knowbtn.backgroundColor = SYColor(249, 130, 186);
    [knowbtn setTitle:@"我知道了" forState:UIControlStateNormal];
    knowbtn.layer.cornerRadius = 10;
    [knowbtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    [knowbtn addTarget:self action:@selector(knowbtnclick:) forControlEvents:UIControlEventTouchUpInside];
    [_useAddviceWindow addSubview:knowbtn];
    switch (index) {
        case 0:
        {
            [_instructionsImage setImage:[UIImage imageNamed:@"health_example2"]];
            NSString *tempStr = @"  电子体温计使用指引：\n  1、打开电子体温计的开关。\n  2、确保设备已蓝牙连接到软件上，点击软件中的体温计测量模块。\n  3、将体温计接触到所测部位，使测量头探测到温度。\n  4、点击测量模块界面的“开始测量”按钮，查看体温值。";
            addviceTextView.text = tempStr;
            break;
        }
        case 1:
        {
            [_instructionsImage setImage:[UIImage imageNamed:@"health_example4"]];
            NSString *tempStr = @"  脂肪秤使用指引：\n  1、使用前请将脂肪秤放在坚固、平坦的地方。\n  2、打开手机蓝牙。\n  3、请您脱下鞋袜，赤脚站在脂肪秤上，系统自动进行检测，等待检测完成接收数据。\n  4、数据接收后即可离开秤面。\n  5、适用人群10-65岁。";
            addviceTextView.text = tempStr;
        }
            break;
        case 2:
        {
            [_instructionsImage setImage:[UIImage imageNamed:@"health_example1"]];
            NSString *tempStr = @"  血压计使用流程：\n     请您在进行测量之前的30分钟内，不要进食、吸烟、运动、入浴，尽量在每天的同一时间测量血压。请在安静的地方以放松的坐姿进行测量。\n    1、请您裸露上臂或穿较薄的衬衫进行测量 \n    2、坐在座椅上，将手臂放在桌面上，使臂带与心脏同一高度。 \n    3、将臂带缠于上臂。空气管位于前臂内测并与中指在同一条直线上，袖带底部应在肘关节上方约1-2cm处。 \n    4、全身放松不说话、不活动。 \n    5、打开血压计开关，确保设备已蓝牙连接到软件上，点击软件中的血压计测量模块，点击“开始测量”按钮，查看血压变化值。";
            addviceTextView.text = tempStr;
        }
            break;
        case 3:
        {
            //血糖仪使用说明以及示例图。
            [_instructionsImage setImage:[UIImage imageNamed:@"health_example5"]];
            NSString *tempStr = @"  血糖仪使用流程：\n   1、准备测试用品，包括血糖仪、血糖试纸、采血笔、一次性使用无菌采血针。\n   2、打开血糖试纸瓶，取出拟用血糖试纸并立即盖好瓶盖。 \n   3、将血糖试纸插入血糖仪的测试条插入口，插入时应使测试条正面朝上，取血口朝外。 \n   4、血糖仪自动开启。待血糖仪显示血滴符号时开始检测。 \n   5、用采血装置采集血样。 \n   6、使血样轻触血糖试纸的取血口，血样被自动吸入，5秒钟后显示测试结果。 \n   7、滑动退试纸键，弹出测试条(勿对人),血糖仪自动关机。 \n   8、按医用废弃物处理规定妥善废弃测试条。";
            addviceTextView.text = tempStr;
            break;
        }
        case 4:
        {
            [_instructionsImage setImage:[UIImage imageNamed:@"health_example5"]];
           NSString *tempStr = @"  血压计使用流程：\n     请您在进行测量之前的30分钟内，不要进食、吸烟、运动、入浴，尽量在每天的同一时间测量血压。请在安静的地方以放松的坐姿进行测量。\n    1、请您裸露上臂或穿较薄的衬衫进行测量 \n    2、坐在座椅上，将手臂放在桌面上，使臂带与心脏同一高度。 \n    3、将臂带缠于上臂。空气管位于前臂内测并与中指在同一条直线上，袖带底部应在肘关节上方约1-2cm处。 \n    4、全身放松不说话、不活动。 \n    5、打开血压计开关，确保设备已蓝牙连接到软件上，点击软件中的血压计测量模块，点击“开始测量”按钮，查看血压变化值。";
            addviceTextView.text = tempStr;
            break;
        }
        case 5:
        {
            [_instructionsImage setImage:[UIImage imageNamed:@"health_example3"]];
            NSString *tempStr = @"  胎心仪使用指引：\n  1、请先确保胎心仪充满电，长按开关按钮开机\n  2、确保设备已蓝牙连接到软件上，点击软件中的胎心测量模块。\n  3、孕妇平躺，将耦合剂涂在胎心仪探头上，按孕周找到胎心并用绑带固定胎心仪。\n  4、一般24周前胎心位置通常在脐下、腹中线两侧；24周后随着胎儿的长大，胎心位置会上移一般在频繁胎动的另一侧；孕周越大，位置越往两侧移；32周后一般胎心位置：头位（头在下）在脐下两侧，臀位（臀在下）在脐上两侧，橫位在脐附近。\n   5、点击测量模块界面的“开始测量”按钮，对正胎心位置，探头稍用力按压，就能找到连续有节奏的胎心声";
            addviceTextView.text = tempStr;
            break;
        }
        default:
            break;
    }
    
    
    UIView *hongseLabel = [[UIView alloc] initWithFrame:CGRectMake(0, DeviceHeight - 10, DeviceWidth, 10)];
    hongseLabel.backgroundColor = SYColor(253, 207, 230);
    [self.view addSubview:hongseLabel];
}

- (void)useaddviceView:(UIButton *)btn
{
    self.rightBtn.enabled = NO;
    _useAddviceWindow.hidden = NO;
}

- (void)knowbtnclick:(UIButton *)btn
{
    self.rightBtn.enabled = YES;
    _useAddviceWindow.hidden = YES;
}
- (void)createLabel:(NSString *)tiwen
{
    _middleLabel.text = tiwen;
    [self.view setNeedsDisplay];
}

- (void)buyBtnclick:(UIButton *)btn
{
    UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, SYNavigateHeight, DeviceWidth, DeviceHeight)];
    self.rightBtn.hidden = YES;
    self.title = nil;
    NSString *tempStr = @"";
    switch (btn.tag) {
        case 10:
            break;
        case 11:
            tempStr = weixin_url;
            break;
        case 12:
            tempStr = weixin_url;
            break;
        case 13:
            break;
        case 14:
            tempStr = weixin_url;
            break;
        case 15:
            break;
        default:
            break;
    }
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:tempStr]];
    [webView loadRequest:request];
    [self.view addSubview:webView];
}

//上传数据
- (void)postParmaterData:(NSString *)nowtimeValue
{
    NSDateComponents *iphoneDateComp = [self iphoneDateAndTime];
    AFHTTPRequestOperationManager *mgr = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:[passValue passValue].passValueName forKey:@"account"];
    [params setObject:[passValue passValue].saveDataUserName forKey:@"username"];
    [params setObject:@"phealth" forKey:@"devName"];
    //上传的数据
    NSString *tempStr = [NSString stringWithFormat:@"time_%ld%02ld%02ld%02ld%02ld%02ld-%@",(long)iphoneDateComp.year, (long)iphoneDateComp.month, (long)iphoneDateComp.day, (long)iphoneDateComp.hour, (long)iphoneDateComp.minute, (long)iphoneDateComp.second, nowtimeValue];
    [params setObject:tempStr forKey:@"data"];
    
//    time_20160418190513-weight_77.0-bloodG_5-bloodPD_100-bloodPS_111-heartRate_81-temperature_39.9
    
    NSLog(@"上传params ＝ %@", params);
    [mgr POST:URL_Health_baby_saveUserData parameters:params success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        NSDictionary *responseDict = (NSDictionary *)responseObject;
        NSString *messageStr = responseDict[@"message"];
        if ([messageStr isEqualToString:@""]) {
//            [MBProgressHUD showSuccess:@"数据上传成功"];
//            NSLog(@"上传成功");
        } else {
            [MBProgressHUD showError:messageStr];
        }
        
        
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [MBProgressHUD showError:@"上传失败"];
    }];
    [self postParItemData];
    
}

- (void)postParItemData
{
    NSDateComponents *iphoneDateComp = [self iphoneDateAndTime];
    AFHTTPRequestOperationManager *mgr = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:[passValue passValue].passValueName forKey:@"account"];
    [params setObject:[passValue passValue].saveDataUserName forKey:@"username"];
    NSString *tempdateStr = [NSString stringWithFormat:@"%ld%02ld%02ld000000",(long)iphoneDateComp.year, (long)iphoneDateComp.month, (long)iphoneDateComp.day];
    [params setObject:tempdateStr forKey:@"date"];
    
    NSInteger tempInt = 0;
    for (NSInteger i = 0; i < self.itemChooseStr.length; i++) {
        char cc = [self.itemChooseStr characterAtIndex:i];
        if (cc == '1') {
            tempInt++;
        }
    }
    [params setObject:[NSString stringWithFormat:@"%ld", (long)tempInt] forKey:@"choosenumber"];
    [params setObject:self.itemChooseStr forKey:@"completed"];
    [mgr POST:URL_Health_baby_setNumberOfTests parameters:params success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        NSDictionary *responseDict = (NSDictionary *)responseObject;
        NSString *messageStr = responseDict[@"message"];
        if (responseObject[@"response"]) {
            [MBProgressHUD showSuccess:@"数据上传成功"];
            NSLog(@"上传成功");
        } else {
           [MBProgressHUD showError:@"请求数据失败,请检查网络"];
        }
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [MBProgressHUD showError:@"上传失败"];
    }];
}

//请求已完善设备
- (void)getItemTests
{
    _itemChooseStr = @"000000";
    NSDateComponents *iphoneDateComp = [self iphoneDateAndTime];
    AFHTTPRequestOperationManager *mgr = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:[passValue passValue].passValueName forKey:@"account"];
    [params setObject:[passValue passValue].saveDataUserName forKey:@"username"];
    NSString *tempdateStr = [NSString stringWithFormat:@"%ld%02ld%02ld000000",(long)iphoneDateComp.year, (long)iphoneDateComp.month, (long)iphoneDateComp.day];
    [params setObject:tempdateStr forKey:@"date"];
    [mgr GET:URL_Health_baby_getNumberofTests parameters:params success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        NSLog(@"responseObject:%@", responseObject);
        if (responseObject[@"response"]) {
            NSArray *tempArray = [responseObject objectForKey:@"data"];
            if (tempArray.firstObject) {
                for (NSDictionary *dic in tempArray) {
                    NSString *tempStr = [dic objectForKey:@"completed"];
                    if (tempStr.length == 6) {
                        self.itemChooseStr = [NSString stringWithString:tempStr];
                    }
                    
                }
            }
           
        } else {
            [MBProgressHUD showError:@"请求数据失败,请检查网络"];
        }
        

    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [MBProgressHUD showError:@"请求数据失败,请检查网络"];
    }];
}

//请求设备测量数据
- (void)getUserParmaterData:(NSString *)value selectionValue:(NSInteger)index gaoxueya:(NSString *)gaovalue
{
    dbp = @"0";
    sbp = @"0";
    weight = @"0";
    tem = @"0";
    bloodG = @"0";
    heartRate = @"0";
    self.theDayValue = nil;
    // 1.创建请求管理对象
    AFHTTPRequestOperationManager *mgr = [AFHTTPRequestOperationManager manager];
    // 2.封装请求参数
    NSDateComponents *tempComp  = [self convertFromDateToDateComp:[NSDate date]];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:[passValue passValue].passValueName forKey:@"account"];
    [params setObject:[passValue passValue].saveDataUserName forKey:@"username"];
    params[@"devicename"] = @"phealth";
    params[@"date"] = [NSString stringWithFormat:@"%ld%02ld%02ld000000", (long)tempComp.year, (long)tempComp.month, (long)tempComp.day];
    
    // 3.发送请求
    [mgr GET:URL_Health_baby_getDevUseInfo parameters:params
     success:^(AFHTTPRequestOperation *operation, id responseObject) {
         NSLog(@"responseObject:%@", responseObject);
         if (responseObject[@"response"]) {
             // 取出data字典数组
             NSArray *dataDictArray = responseObject[@"data"];
             self.theDayValue = [YBHealthParmater mj_objectArrayWithKeyValuesArray:dataDictArray];
             NSString *tempStr = value;
             switch (index) {
                 case 0:
                     [self returnTiWenStr:self.theDayValue value:tempStr];
                     break;
                 case 1:
                     [self returnWeightStr:self.theDayValue value:tempStr];
                     break;
                 case 2:
                     [self returnXueYaStr:self.theDayValue value:tempStr gaoxueya:gaovalue];
                     break;
                 case 3:
                     [self returnXueTangStr:self.theDayValue value:tempStr];
                     break;
                 case 4:
                     [self returnHeartStr:self.theDayValue value:tempStr];
                     break;
                 case 5:
//                     [self returnHeartStr:self.theDayValue value:tempStr];
                     break;
                 default:
                     break;
             }
             
             NSLog(@"%@", self.theDayValue);
             // 请求成功
             //                 NSLog(@"请求321的数据：%@", responseObject);
         } else {
             [MBProgressHUD showError:@"请求数据失败,请检查网络"];
         }
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         [MBProgressHUD showError:@"请求数据失败,请检查网络"];
     }];
}

- (void)returnTiWenStr:(NSArray *)array value:(NSString *)valuet
{
    
    YBHealthParmater *tempParmater = self.theDayValue.firstObject;
    tem = valuet;
    if (tempParmater.weight ) {
        weight = [NSString stringWithFormat:@"%0.1f", tempParmater.weight ];
    }
    if (tempParmater.bloodG) {
        bloodG = [NSString stringWithFormat:@"%ld", (long)tempParmater.bloodG] ;
    }
    if (tempParmater.heartRate) {
        heartRate = [NSString stringWithFormat:@"%ld", (long)tempParmater.heartRate ];
    }
    if (tempParmater.dbp) {
        dbp = [NSString stringWithFormat:@"%ld", (long)tempParmater.dbp];
    }
    if (tempParmater.sbp) {
        sbp = [NSString stringWithFormat:@"%ld", (long)tempParmater.sbp];
    }
    NSString * tempStr = [NSString stringWithFormat:@"%@", [self replaceStr:self.itemChooseStr index:0]];
    self.itemChooseStr = [NSString stringWithString:tempStr];
    
    self.uploadData = [NSString stringWithFormat:@"weight_%@-bloodG_%@-bloodPD_%@-bloodPS_%@-heartRate_%@-temperature_%@", weight, bloodG, dbp, sbp, heartRate, tem];
    [self postParmaterData:self.uploadData];
}

- (void)returnWeightStr:(NSArray *)array value:(NSString *)valuet
{
    
    YBHealthParmater *tempParmater = self.theDayValue.firstObject;
    weight = valuet;
    if (tempParmater.tem) {
        tem = [NSString stringWithFormat:@"%0.1f", tempParmater.tem ];
    }
    if (tempParmater.bloodG) {
        bloodG = [NSString stringWithFormat:@"%ld", (long)tempParmater.bloodG] ;
    }
    if (tempParmater.heartRate) {
        heartRate = [NSString stringWithFormat:@"%ld", (long)tempParmater.heartRate ];
    }
    if (tempParmater.dbp) {
        dbp = [NSString stringWithFormat:@"%ld", (long)tempParmater.dbp];
    }
    if (tempParmater.sbp) {
        sbp = [NSString stringWithFormat:@"%ld", (long)tempParmater.sbp];
    }
    NSString * tempStr = [NSString stringWithFormat:@"%@", [self replaceStr:self.itemChooseStr index:1]];
    self.itemChooseStr = [NSString stringWithString:tempStr];
    
    self.uploadData = [NSString stringWithFormat:@"weight_%@-bloodG_%@-bloodPD_%@-bloodPS_%@-heartRate_%@-temperature_%@", weight, bloodG, dbp, sbp, heartRate, tem];
    [self postParmaterData:self.uploadData];
}

- (void)returnXueYaStr:(NSArray *)array value:(NSString *)valuet gaoxueya:(NSString*)gaoxueya
{
  
    YBHealthParmater *tempParmater = self.theDayValue.firstObject;
    dbp = valuet;
    sbp = gaoxueya;
    
    if (tempParmater.tem) {
        tem = [NSString stringWithFormat:@"%0.1f", tempParmater.tem ];
    }
    if (tempParmater.bloodG) {
        bloodG = [NSString stringWithFormat:@"%ld", (long)tempParmater.bloodG] ;
    }
    if (tempParmater.heartRate) {
        heartRate = [NSString stringWithFormat:@"%ld", (long)tempParmater.heartRate ];
    }
    if (tempParmater.weight) {
        weight = [NSString stringWithFormat:@"%ld", (long)tempParmater.weight];
    }
    NSString * tempStr = [NSString stringWithFormat:@"%@", [self replaceStr:self.itemChooseStr index:2]];
    self.itemChooseStr = [NSString stringWithString:tempStr];
    
    self.uploadData = [NSString stringWithFormat:@"weight_%@-bloodG_%@-bloodPD_%@-bloodPS_%@-heartRate_%@-temperature_%@", weight, bloodG, dbp, sbp, heartRate, tem];
    [self postParmaterData:self.uploadData];
    
}

- (void)returnXueTangStr:(NSArray *)array value:(NSString *)valuet
{
    
    YBHealthParmater *tempParmater = self.theDayValue.firstObject;
    bloodG = valuet;
    if (tempParmater.tem) {
        tem = [NSString stringWithFormat:@"%0.1f", tempParmater.tem ];
    }
    if (tempParmater.weight) {
        weight = [NSString stringWithFormat:@"%ld", (long)tempParmater.weight] ;
    }
    if (tempParmater.heartRate) {
        heartRate = [NSString stringWithFormat:@"%ld", (long)tempParmater.heartRate ];
    }
    if (tempParmater.dbp) {
        dbp = [NSString stringWithFormat:@"%ld", (long)tempParmater.dbp];
    }
    if (tempParmater.sbp) {
        sbp = [NSString stringWithFormat:@"%ld", (long)tempParmater.sbp];
    }
    NSString * tempStr = [NSString stringWithFormat:@"%@", [self replaceStr:self.itemChooseStr index:3]];
    self.itemChooseStr = [NSString stringWithString:tempStr];
    
    self.uploadData = [NSString stringWithFormat:@"weight_%@-bloodG_%@-bloodPD_%@-bloodPS_%@-heartRate_%@-temperature_%@", weight, bloodG, dbp, sbp, heartRate, tem];
    YMLog(@"uplo = %@",self.uploadData);
    [self postParmaterData:self.uploadData];
}

- (void)returnHeartStr:(NSArray *)array value:(NSString *)valuet
{
    YBHealthParmater *tempParmater = self.theDayValue.firstObject;
    heartRate = valuet;
    
    if (tempParmater.tem) {
        tem = [NSString stringWithFormat:@"%0.1f", tempParmater.tem ];
    }
    if (tempParmater.bloodG) {
        bloodG = [NSString stringWithFormat:@"%ld", (long)tempParmater.bloodG] ;
    }
    if (tempParmater.weight) {
        weight = [NSString stringWithFormat:@"%ld", (long)tempParmater.weight ];
    }
    if (tempParmater.dbp) {
        dbp = [NSString stringWithFormat:@"%ld", (long)tempParmater.dbp];
    }
    if (tempParmater.sbp) {
        sbp = [NSString stringWithFormat:@"%ld", (long)tempParmater.sbp];
    }
    NSString * tempStr = [NSString stringWithFormat:@"%@", [self replaceStr:self.itemChooseStr index:4]];
    self.itemChooseStr = [NSString stringWithString:tempStr];
    self.uploadData = [NSString stringWithFormat:@"weight_%@-bloodG_%@-bloodPD_%@-bloodPS_%@-heartRate_%@-temperature_%@", weight, bloodG, dbp, sbp, heartRate, tem];
    [self postParmaterData:self.uploadData];
}

- (NSMutableString *)replaceStr:(NSString *)str index:(NSInteger)index
{
    NSMutableString *tempMStr = [NSMutableString string];
    for (NSInteger i = 0; i < str.length; i ++) {
      char  cc = [str characterAtIndex:i];
        if (i == index) {
            cc = '1';
        }
        [tempMStr appendString:[NSString stringWithFormat:@"%c", cc]];
    }
    return tempMStr;
}

- (NSDateComponents *)convertFromDateToDateComp:(NSDate *)tempDate
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    int unit = NSCalendarUnitDay | NSCalendarUnitMonth |  NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitWeekday;
    
    // 1.获得当前时间的年月日
    NSDateComponents *nowCmps = [calendar components:unit fromDate:tempDate];
    
    return nowCmps;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSDateComponents *)iphoneDateAndTime
{
    NSDate *now = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSUInteger unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    NSDateComponents *todayDateComp = [calendar components:unitFlags fromDate:now];
    
    return todayDateComp;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    self.useAddviceWindow.hidden = YES;
    self.useAddviceWindow = nil;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
@end
