//
//  YBChildBaseController.h
//  TTT
//
//  Created by Raoy on 16/3/28.
//  Copyright © 2016年 Raoy. All rights reserved.
//

#import <UIKit/UIKit.h>

#define PageBtn .0675 * DeviceHeight

@interface YBChildBaseController : UIViewController

@property(strong, nonatomic) UIButton *buyBtn;
@property (strong, nonatomic) UIImageView  *buletoothImageView;
@property (strong,nonatomic) UIButton *startbtn2; //自动测量开始button
@property(strong, nonatomic)UIButton  *rightBtn; //手动测量开始button
@property(copy, nonatomic)NSString *uploadData; //上传的数据
//使用指导界面
@property(strong, nonatomic)UIView *useAddviceWindow;

//覆盖的View
@property (nonatomic, weak)UIView *fuGaiView;

- (void) createLabel:(NSString *)tiwen;
//购买设备
- (void)setTitle:(NSString *)titleName buyTitle:(NSString *)buytitle indexImage:(NSInteger)index;
//上传数据
- (void)postParmaterData:(NSString *)nowtimeValue;
//请求
- (void)getUserParmaterData:(NSString *)value selectionValue:(NSInteger)index gaoxueya:(NSString *)gaovalue;
@end
