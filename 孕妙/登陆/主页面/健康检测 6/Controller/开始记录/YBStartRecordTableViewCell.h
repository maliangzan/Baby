//
//  YBStartRecordTableViewCell.h
//  孕妙
//
//  Created by Raoy on 16/4/11.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YBStartRecordTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageViewOne;
@property (weak, nonatomic) IBOutlet UILabel *itemL;
@property (weak, nonatomic) IBOutlet UILabel *itemValueL;
@property (weak, nonatomic) IBOutlet UILabel *recoredFlagL;
@property (weak, nonatomic) IBOutlet UIButton *inputBtn;
@end
