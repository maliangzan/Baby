//
//  YBStartRecordTableViewCell.m
//  孕妙
//
//  Created by Raoy on 16/4/11.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "YBStartRecordTableViewCell.h"

@implementation YBStartRecordTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    _recoredFlagL.text = @"未检测";
    [_recoredFlagL setTextColor:SYColor(252, 138, 37)];
    _recoredFlagL.textAlignment = NSTextAlignmentRight;
    
    _itemValueL.text = @"----";
    _itemValueL.textAlignment = NSTextAlignmentLeft;
    [_itemValueL setTextColor:SYColor(89, 89, 89)];
    
    [_itemL setTextColor:SYColor(200, 100, 140)];
    _itemL.textAlignment = NSTextAlignmentLeft;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
