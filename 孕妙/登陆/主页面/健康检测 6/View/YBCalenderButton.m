//
//  YBCalenderButton.m
//  健康监测
//
//  Created by sayes1 on 16/1/11.
//  Copyright © 2016年 sayes1. All rights reserved.
//
#define YBCalenderButtonImageW 20
#define YBNavigateHeight 64

#define HealthLayoutHeight (DeviceHeight - YBNavigateHeight)
#import "YBCalenderButton.h"
#import "UIImage+ZF.h"

@implementation YBCalenderButton

+ (instancetype)calenderButton
{
    return [[self alloc] init];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // 高亮的时候不要自动调整图标
//        self.adjustsImageWhenHighlighted = NO;
        self.titleLabel.font = [UIFont systemFontOfSize:19];
        self.imageView.contentMode = UIViewContentModeCenter;
        self.titleLabel.textAlignment = NSTextAlignmentRight;
        [self setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
    return self;
}

- (CGRect)imageRectForContentRect:(CGRect)contentRect
{
    CGFloat imageY = 0;
    CGFloat imageW = YBCalenderButtonImageW;
    CGFloat imageX = contentRect.size.width - imageW;
    CGFloat imageH = contentRect.size.height;
    return CGRectMake(imageX, imageY, imageW, imageH);
}

- (CGRect)titleRectForContentRect:(CGRect)contentRect
{
    CGFloat titleY = 0;
    CGFloat titleX = 0;
    CGFloat titleW = contentRect.size.width - YBCalenderButtonImageW;
    CGFloat titleH = contentRect.size.height;
    return CGRectMake(titleX, titleY, titleW, titleH);
}

- (void)setTitle:(NSString *)title forState:(UIControlState)state
{
    // 根据title计算自己的宽度
    CGFloat titleW = [title sizeWithFont:self.titleLabel.font].width;
    
    CGRect frame = self.frame;
    frame.size.width = titleW + YBCalenderButtonImageW + 5;
    frame.size.height = HealthLayoutHeight * 0.04455;
    frame.origin.y = 0;
    frame.origin.x = (DeviceWidth - frame.size.width) * 0.5;
    self.frame = frame;
    
    [super setTitle:title forState:state];
}


@end
