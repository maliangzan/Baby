//
//  YBSelectParmatView.m
//  孕妙
//
//  Created by sayes1 on 16/1/19.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "YBSelectParmatView.h"

@interface YBSelectParmatView () <UIPickerViewDataSource, UIPickerViewDelegate>
/**
 
 */
@property (nonatomic, strong) UILabel *titleL;
/**
 
 */
@property (nonatomic, strong) UIPickerView *pickerView;
@end

@implementation YBSelectParmatView

+ (instancetype)selectedGoalViewWithframe:(CGRect)frame titleStr:(NSString *)titleStr number:(NSInteger)num
{
    YBSelectParmatView *selectedNumView = [[YBSelectParmatView alloc] initWithFrame:frame];
    selectedNumView.titleL.text = titleStr;
//    selectedNumView.num = num;
    return selectedNumView;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        //
        self.backgroundColor = [UIColor whiteColor];
        [self setupSubview];
    }
    return self;
}

- (void)setupSubview
{
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 40)];
    titleView.backgroundColor = SYColor(55, 186, 199);
    [self addSubview:titleView];
    
    UILabel *titleL = [[UILabel alloc] initWithFrame:CGRectMake(60, 0, DeviceWidth - 120, 40)];
    self.titleL = titleL;
    titleL.textColor = [UIColor whiteColor];
    titleL.textAlignment = NSTextAlignmentCenter;
    [titleView addSubview:titleL];
    
    UIButton *sureBtn = [[UIButton alloc] initWithFrame:CGRectMake(DeviceWidth - 60, 0, 60, 40)];
    //    [sureBtn setImage:[UIImage imageNamed:@"sure"] forState:UIControlStateNormal];
    [sureBtn setTitle:@"保存" forState:UIControlStateNormal];
    [titleView addSubview:sureBtn];
 
}


#pragma mark -pickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}




@end
