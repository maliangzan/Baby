//
//  YBBodyFatView.h
//  孕妙
//
//  Created by Raoy on 16/6/8.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YBBodyFatView : UIView
@property (assign, nonatomic)float fat;
@property (assign, nonatomic)float guGe;
@property (assign, nonatomic)float jiRou;
@property (assign, nonatomic)float neiZang;
@property (assign, nonatomic)float shuiFen;
@property (assign, nonatomic)float reLiang;
@property (assign, nonatomic)float bmi;
@property (assign, nonatomic)float weight;
@property (assign, nonatomic)float height;

- (instancetype)initWithFrame:(CGRect)frame;
- (void)getUserWeightFatData;
- (void)reloadText;
@end
