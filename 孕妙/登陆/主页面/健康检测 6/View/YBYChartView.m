//
//  YBYChartView.m
//  孕妙
//
//  Created by sayes1 on 16/1/25.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "YBYChartView.h"

@implementation YBYChartView

- (void)setCanshuValueArray:(NSArray *)canshuValueArray
{
    _canshuValueArray = canshuValueArray;
    [self setNeedsDisplay];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    //    CGContextSaveGState(ctx);
    //    CGContextMoveToPoint(ctx, SYChartLeftMargin, 0);
    //    CGContextAddLineToPoint(ctx, SYChartLeftMargin, self.frame.size.height - SYChartLeftMargin);
    //    SYLog(@"view height :%f", self.frame.size.height);
    //    [[UIColor blackColor] set];
    //
    //    CGContextStrokePath(ctx);
    //    CGContextRestoreGState(ctx);
    
    CGFloat sectionHeight = (self.frame.size.height - SYMargin20) / (float)(self.canshuValueArray.count - 1);
    // 坐标0点的高度
    CGFloat zeroLoad = self.frame.size.height - SYMargin20;
    // 取出数组中的数值对象
    NSNumber *arrayData;
    // 属性字典
    NSMutableDictionary *dictAttr = [NSMutableDictionary dictionary];
    dictAttr[NSFontAttributeName] = [UIFont systemFontOfSize:8];
    dictAttr[NSForegroundColorAttributeName] = SYColor(95, 95, 95);
    SYLog(@"sectionHeight:%f", sectionHeight);
    for (int index = 1; index < self.canshuValueArray.count; index++) {
        // 纵坐标的点0 40 60 80 100
        CGPoint point = CGPointMake(SYMargin15, zeroLoad - (sectionHeight * (index - 1)));
        arrayData = self.canshuValueArray[index - 1];
        NSString *dataStr = [NSString stringWithFormat:@"%d", [arrayData intValue]];
        
        SYLog(@"dataStr:%@", dataStr);
        
        [dataStr drawInRect:CGRectMake(0, point.y - SYMargin5, 40, SYMargin20) withAttributes:dictAttr];
        SYLog(@"pointl:%@", NSStringFromCGPoint(point));
    }
    [SYColor(83, 83, 83) set];
    CGContextMoveToPoint(ctx, SYMargin20, 0);
    CGContextAddLineToPoint(ctx, SYMargin20, self.frame.size.height - SYMargin20);
    CGContextStrokePath(ctx);
}


@end
