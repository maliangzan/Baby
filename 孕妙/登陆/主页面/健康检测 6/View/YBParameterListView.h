//
//  YBParameterListView.h
//  健康监测
//
//  Created by sayes1 on 16/1/11.
//  Copyright © 2016年 sayes1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YBParameterListView : UIView

/**
 *
 */
@property (nonatomic, strong) UIButton *clickBtn;
/**
 *
 */
@property (nonatomic, strong) UIImageView *iconImage;
/**
 
 */
@property (nonatomic, strong) UILabel *nameL;
/**
 
 */
@property (nonatomic, strong) UILabel *parmeterValue;
/**
 
 */
@property (nonatomic, strong) UIImageView *rightArrowImage;
/**
 
 */
@property (nonatomic, strong) UIView *underLine;

+ (instancetype)parameterListViewIconImage:(NSString *)iconName name:(NSString *)name parmeterValue:(NSString *)parmeterVStr frame:(CGRect)frame hasUnderlined:(BOOL)isHas;
@end
