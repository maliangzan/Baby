//
//  YBChartScrollView.m
//  孕妙
//
//  Created by sayes1 on 16/1/25.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "YBChartScrollView.h"
#import "NSDateComponents+ZF.h"
#import "YBHealthParmater.h"
#import "YBBodyFatModel.h"

@interface YBChartScrollView ()
// 当前星期的数据
@property (nonatomic, strong) NSMutableArray *weekMeasureResult;
/**
 需要画在屏幕上的当前星期的数据
 */
@property (nonatomic, strong) NSMutableArray *weekChartData;
/**
 当前月的数据
 */
@property (nonatomic, strong) NSMutableArray *monthMeasureResult;
/**
 需要画在屏幕上的当前月的数据
 */
@property (nonatomic, strong) NSMutableArray *monthChartData;

/**
 当年的所有数据
 */
@property (nonatomic, strong) NSMutableArray *yearMeasureResult;
/**
 需要画在屏幕上的当前年的数据
 */
@property (nonatomic, strong) NSMutableArray *yearChartData;
/**
 当前显示的第几周
 */
@property (nonatomic, assign) NSInteger weekNumber;
/**
 是否到了第一个星期或最后一个星期
 */
@property (nonatomic, assign) BOOL isLastWeek ;
/**
 当前显示的第几个月
 */
@property (nonatomic, assign) NSInteger monthNumber;
/**
 是否到了第一个月或最后一个月
 */
@property (nonatomic, assign) BOOL isLastMonth ;
/**
 当前显示哪一年的数据
 */
@property (nonatomic, assign) NSInteger yearNumber;
/**
 提示信息label
 */
@property (nonatomic, strong) UILabel *messageL;



@end

@implementation YBChartScrollView

- (UILabel *)messageL
{
    if (_messageL == nil) {
        _messageL = [[UILabel alloc] init];
        CGFloat messageLW = (DeviceWidth) * 0.6;
        CGFloat messageLX = messageLW * 0.3 - SYMargin20;
        CGFloat messageLY = self.frame.size.height - 50;//(DeviceHeight - 100) * 0.5;
        CGFloat messageLH = 30;
        _messageL.frame = CGRectMake(messageLX, messageLY, messageLW, messageLH);
        _messageL.alpha = 0;
        _messageL.textAlignment = NSTextAlignmentCenter;
        _messageL.font = [UIFont systemFontOfSize:15];
        _messageL.textColor = SYColor(83, 83, 83);
        _messageL.backgroundColor = SYColor(252, 170, 209);
        _messageL.layer.cornerRadius = 10;
        _messageL.layer.masksToBounds = YES;
        [self addSubview:_messageL];
    }
    return _messageL;
}

- (void)setIsWeekChart:(BOOL)isWeekChart
{
    _isWeekChart = isWeekChart;
    if (_isWeekChart) {
        [self setNeedsDisplay];
    }
    
}

- (void)setIsMonthChart:(BOOL)isMonthChart
{
    _isMonthChart = isMonthChart;
    if (_isMonthChart) {
        [self setNeedsDisplay];
    }
}

//- (void)setIsYearChart:(BOOL)isYearChart
//{
//    _isYearChart = isYearChart;
//    if (_isYearChart) {
//        [self setNeedsDisplay];
//    }
//    
//}

- (void)setIsSlideToLeft:(BOOL)isSlideToLeft
{
    _isSlideToLeft = isSlideToLeft;
    if (isSlideToLeft) {
        [self setNeedsDisplay];
    }
}

- (void)setIsSlideToRight:(BOOL)isSlideToRight
{
    _isSlideToRight = isSlideToRight;
    if (isSlideToRight) {
        [self setNeedsDisplay];
    }
}

- (void)setMeasureResult:(NSMutableArray *)measureResult
{
    _measureResult = measureResult;
    
    // 获得当前的日期信息
    NSDateComponents *testDate = [NSDateComponents dateComponentsWithNow];
    self.yearNumber = testDate.year;
    self.monthNumber = testDate.month;
    self.weekNumber = testDate.weekOfMonth;
    [self setNeedsDisplay];
}

- (NSMutableArray *)weekMeasureResult
{
    if (_weekMeasureResult == nil) {
        _weekMeasureResult = [NSMutableArray array];
    }
    return _weekMeasureResult;
}

- (NSMutableArray *)monthMeasureResult
{
    if (_monthMeasureResult == nil) {
        _monthMeasureResult = [NSMutableArray array];
    }
    return _monthMeasureResult;
}

- (NSMutableArray *)yearMeasureResult
{
    if (_yearMeasureResult == nil) {
        _yearMeasureResult = [NSMutableArray array];
    }
    return _yearMeasureResult;
}

- (NSMutableArray *)yearChartData
{
    if (_yearChartData == nil) {
        _yearChartData = [NSMutableArray array];
    }
    return _yearChartData;
}

//- (void)setWeekChartData:(NSMutableArray *)weekChartData
//{
//    _weekChartData = weekChartData;
//    if ([self.syDelegate respondsToSelector:@selector(chartScrollView:chartDataArray:)]) {
//        [self.syDelegate chartScrollView:self chartDataArray:self.weekChartData];
//    }
//}
//
//- (void)setYearChartData:(NSMutableArray *)yearChartData
//{
//    _yearChartData = yearChartData;
//    if ([self.syDelegate respondsToSelector:@selector(chartScrollView:chartDataArray:)]) {
//        [self.syDelegate chartScrollView:self chartDataArray:self.yearChartData];
//    }
//}
//
//- (void)setMonthChartData:(NSMutableArray *)monthChartData
//{
//    _monthChartData = monthChartData;
//    if ([self.syDelegate respondsToSelector:@selector(chartScrollView:chartDataArray:)]) {
//        [self.syDelegate chartScrollView:self chartDataArray:self.monthChartData];
//    }
//}

- (void)setWeightLine:(BOOL)weightLine
{
    _weightLine = weightLine;
    if (weightLine) {
        [self setNeedsDisplay];
    }
}

- (void)setTiwenLine:(BOOL)tiwenLine
{
    _tiwenLine = tiwenLine;
    if (tiwenLine) {
        [self setNeedsDisplay];
    }
}

- (void)setFatLine:(BOOL)fatLine
{
    _fatLine = fatLine;
    if (fatLine) {
        [self setNeedsDisplay];
    }
}

- (void)setXuetangLine:(BOOL)xuetangLine
{
    _xuetangLine = xuetangLine;
    if (xuetangLine) {
        [self setNeedsDisplay];
    }
}

- (void)setXueyaLine:(BOOL)xueyaLine
{
    _xueyaLine = xueyaLine;
    if (xueyaLine) {
        [self setNeedsDisplay];
    }
}

- (void)setHeartLine:(BOOL)heartLine
{
    _heartLine = heartLine;
    if (heartLine) {
        [self setNeedsDisplay];
    }
}


- (void)drawRect:(CGRect)rect {
    // 坐标0点的高度
    CGFloat zeroLoadY = self.frame.size.height - SYMargin20;
    // X轴的长度
    CGFloat xZhouW = (DeviceWidth - 2 * SYMargin20);
    // 把纵坐标分区
    CGFloat coordinateH = zeroLoadY / 5.0;
    // 每一区域5等分
//    CGFloat coordinateH_5 = coordinateH / 5.0;
    // 每一区域20等分
    CGFloat coordinateH_200 = coordinateH / 200.0;
    // 每一区域40等分
    CGFloat coordinateH_400 = coordinateH / 400.0;
    // 每一区域50等分
    CGFloat coordinateH_50 = coordinateH / 50.0;
//    // 每一区域100等分
//    CGFloat coordinateH_100 = coordinateH / 100.0;
    
    // 把纵坐标分区
    CGFloat coordinate7H = zeroLoadY / 7.0;
    // 每一区域10等分
    CGFloat coordinate7H_20 = coordinate7H / 20.0;
    // 把纵坐标分区
    CGFloat coordinate8H = zeroLoadY / 8.0;
    // 把纵坐标分区
    CGFloat coordinate8H_40 = coordinate8H / 40.0;
    // 把纵坐标分区
    CGFloat coordinate8H_20 = coordinate8H / 20.0;
    // 把纵坐标分区
    CGFloat coordinate12H = zeroLoadY / 12.0;
    CGFloat coordinate12H_20 = coordinate12H / 20.0;
    CGFloat coordinate12H_40 = coordinate12H / 40.0;
    
    SYLog(@"!!!!!self.frame:%@, %@", NSStringFromCGRect(self.frame), [self superview]);
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextSaveGState(ctx);
    //
    if (self.isWeekChart) {
        SYLog(@"周图");
        SYLog(@"zeroLoadY:%f", zeroLoadY);
        CGFloat sectionWidth = (xZhouW - SYMargin20) / 7.0;
        
        // 画x轴
        [[UIColor lightGrayColor] set];
        CGContextMoveToPoint(ctx, 0, zeroLoadY);
        
        SYLog(@"self.frame.size.height:%f", self.frame.size.height);
        CGContextAddLineToPoint(ctx,xZhouW, zeroLoadY);
//        
        CGContextMoveToPoint(ctx, xZhouW - SYMargin5, zeroLoadY - SYMargin5);
        CGContextAddLineToPoint(ctx, xZhouW, zeroLoadY);
        CGContextAddLineToPoint(ctx, xZhouW - SYMargin5, zeroLoadY + SYMargin5);
        CGContextStrokePath(ctx);
        
        // 画纵坐标刻度线
        if (self.isweightLine) {
            for (int index = 1; index < 5; index++) {
                CGContextMoveToPoint(ctx, 0, zeroLoadY - index * coordinateH);
                CGContextAddLineToPoint(ctx, xZhouW, zeroLoadY - index * coordinateH);
            }
        } else if (self.isxuetangLine) {
            for (int index = 1; index < 7; index++) {
                CGContextMoveToPoint(ctx, 0, zeroLoadY - index * coordinate7H);
                CGContextAddLineToPoint(ctx, xZhouW, zeroLoadY - index * coordinate7H);
            }
        } else if (self.isheartLine) {
            for (int index = 1; index < 8; index++) {
                CGContextMoveToPoint(ctx, 0, zeroLoadY - index * coordinate8H);
                CGContextAddLineToPoint(ctx, xZhouW, zeroLoadY - index * coordinate8H);
            }
        } else if (self.isxueyaLine) {
            for (int index = 1; index < 12; index++) {
//                if (index %2 == 0) {
                    CGContextMoveToPoint(ctx, 0, zeroLoadY - index * coordinate12H);
                    CGContextAddLineToPoint(ctx, xZhouW, zeroLoadY - index * coordinate12H);
//                }
            }
        }else if (self.istiwenLine){
            for (int index = 1; index < 8; index++) {
                CGContextMoveToPoint(ctx, 0, zeroLoadY - index * coordinate8H);
                CGContextAddLineToPoint(ctx, xZhouW, zeroLoadY - index * coordinate8H);
            }
        }else if (self.isfatLine){
        
            if (_yNumber == 9 || (_yNumber == 12)) {
                [self cgcontextpointMethod:ctx xZhouW:xZhouW zeroLoadY:zeroLoadY coorinateH:coordinate8H index:8];
            }else{
            
                [self cgcontextpointMethod:ctx xZhouW:xZhouW zeroLoadY:zeroLoadY coorinateH:coordinateH index:5];
            }
            
            
            
        }
        
        // 画刻度
        for (int index = 1; index <= 7; index++) {
            // 纵坐标的点 1 2 3 4 5 6 7 8
            CGPoint point = CGPointMake(index * sectionWidth, zeroLoadY);
            // 画x轴刻度线
            CGContextMoveToPoint(ctx, point.x, point.y);
            CGContextAddLineToPoint(ctx, point.x, point.y + SYMargin5); // 刻度线有5个像素长度
        }
        CGContextStrokePath(ctx);
        
        // 画x轴坐标值
        NSString *sunday = @"日";
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSFontAttributeName] = [UIFont systemFontOfSize:10];
        
        [sunday drawInRect:CGRectMake(sectionWidth - IWStatusTableBorder, zeroLoadY + IWStatusTableBorder, 20, 10) withAttributes:dict];
        for (int index = 1; index <= 6; index++) {
            // 纵坐标的点 1 2 3 4 5 6 7 8
            CGPoint point = CGPointMake((index + 1)* sectionWidth, zeroLoadY);
            
            NSString *xNum = nil;//
            if (index == 1) {
                xNum = [NSString stringWithFormat:@"一"];
            } else if (index == 2) {
                xNum = [NSString stringWithFormat:@"二"];
            } else if (index == 3) {
                xNum = [NSString stringWithFormat:@"三"];
            } else if (index == 4) {
                xNum = [NSString stringWithFormat:@"四"];
            } else if (index == 5) {
                xNum = [NSString stringWithFormat:@"五"];
            } else if (index == 6) {
                xNum = [NSString stringWithFormat:@"六"];
            }
            [xNum drawInRect:CGRectMake(point.x - IWStatusTableBorder, point.y + IWStatusTableBorder, 20, 10) withAttributes:dict];
        }

        
        
        NSMutableArray *tempResult = [NSMutableArray array];
        
        // 前一个元素的星期
        NSInteger weekLast = 0;
        SYLog(@"self.weekMeasureResult.count: %ld", (unsigned long)self.weekMeasureResult.count);
        
        // 获得当前的日期信息
        NSDateComponents *testDate = [NSDateComponents dateComponentsWithNow];
        
        if (self.isSlideToRight == NO && self.isSlideToLeft == NO) {
            
            // 清除weekMeasureResult中的数据
            [self.weekMeasureResult removeAllObjects];
            
            // 本周
            self.isLastWeek = NO;
            if (self.yearNumber == testDate.year && self.monthNumber == testDate.month) {
                self.weekNumber = testDate.weekOfMonth;
            } else {
                //                self.weekNumber = 1;
                // 切换周时，显示的是当前月的最早有数据的周
                for (int i = 0; i < self.measureResult.count; i++) {
                    YBHealthParmater *result = self.measureResult[i];
                    if (result.parmaterDateCom.year == self.yearNumber && result.parmaterDateCom.month == self.monthNumber) {
                        self.weekNumber = result.parmaterDateCom.weekOfMonth;
                        break;
                    }
                }
            }
            
            for (int i = 0; i < self.measureResult.count; i++) {
                YBHealthParmater *result = self.measureResult[i];
                SYLog(@"%ld-%ld-%ldWeek", (long)result.parmaterDateCom.year, (long)result.parmaterDateCom.month, (long)result.parmaterDateCom.weekOfMonth);
                if (result.parmaterDateCom.year == self.yearNumber && result.parmaterDateCom.month == self.monthNumber && result.parmaterDateCom.weekOfMonth == self.weekNumber) {
                    [self.weekMeasureResult addObject:self.measureResult[i]];
                }
            }
        } else if (self.isSlideToLeft) {
            self.isSlideToLeft = NO;
            // 下周
            if ((self.weekNumber + 1) <= ((testDate.year == self.yearNumber && testDate.month == self.monthNumber) ? testDate.weekOfMonth : 5)) {
                self.weekNumber += 1;
                self.isLastWeek = NO;
                // 清除weekMeasureResult中的数据
                [self.weekMeasureResult removeAllObjects];
                for (int i = 0; i < self.measureResult.count; i++) {
                    YBHealthParmater *result = self.measureResult[i];
                    SYLog(@"%ld-%ld-%ldWeek", (long)result.parmaterDateCom.year, (long)result.parmaterDateCom.month, (long)result.parmaterDateCom.weekOfMonth);
                    if (result.parmaterDateCom.year == self.yearNumber && result.parmaterDateCom.month == self.monthNumber && result.parmaterDateCom.weekOfMonth == self.weekNumber) {
                        [self.weekMeasureResult addObject:self.measureResult[i]];
                    }
                }
            } else {
                self.isLastWeek = YES;
                self.messageL.text = @"已是本月最后一周";
                [UIView animateWithDuration:1.0 animations:^{
                    self.messageL.alpha = 1.0;
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:1.0 animations:^{
                        self.messageL.alpha = 0;
                    }];
                }];
                //                [MBProgressHUD showError:@"已是本月最后一周"];
            }
        } else if (self.isSlideToRight) {
            self.isSlideToRight = NO;
            // 上周
            if ((self.weekNumber - 1) >= 1) {
                self.weekNumber -= 1;
                self.isLastWeek = NO;
                // 清除weekMeasureResult中的数据
                [self.weekMeasureResult removeAllObjects];
                for (int i = 0; i < self.measureResult.count; i++) {
                    YBHealthParmater *result = self.measureResult[i];
                    SYLog(@"%ld-%ld-%ldWeek", (long)result.parmaterDateCom.year, (long)result.parmaterDateCom.month, (long)result.parmaterDateCom.weekOfMonth);
                    if (result.parmaterDateCom.year == self.yearNumber && result.parmaterDateCom.month == self.monthNumber && result.parmaterDateCom.weekOfMonth == self.weekNumber) {
                        [self.weekMeasureResult addObject:self.measureResult[i]];
                    }
                }
            } else {
                self.isLastWeek = YES;
                self.messageL.text = @"已是本月第一周";
                [UIView animateWithDuration:1.0 animations:^{
                    self.messageL.alpha = 1.0;
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:1.0 animations:^{
                        self.messageL.alpha = 0;
                    }];
                }];
                //                [MBProgressHUD showError:@"已是本月第一周"];
            }
        }
        
        //        if (self.isLastWeek) return;
        
        for (int index = 0; index < self.weekMeasureResult.count; index++) {
            YBHealthParmater *result = self.weekMeasureResult[index];
            SYLog(@"weekLast:%ld, weekday:%ld", (long)weekLast, (long)result.parmaterDateCom.weekday);
            if (weekLast == 0) {
                //
            } else if ((result.parmaterDateCom.weekday) > weekLast){
                SYLog(@"加入需要画得数组");
                [tempResult addObject:self.weekMeasureResult[index - 1]];
            }
            
            if (index == (self.weekMeasureResult.count -1)) {
                [tempResult addObject:[self.weekMeasureResult lastObject]];
            }
            weekLast = result.parmaterDateCom.weekday;
        }
        
        [self.weekChartData removeAllObjects];
        self.weekChartData  = tempResult;
        
        if (self.weekChartData.count == 0 && self.isLastWeek == NO) {
            self.messageL.text = @"本周没有数据";
            [UIView animateWithDuration:1.0 animations:^{
                self.messageL.alpha = 1.0;
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:1.0 animations:^{
                    self.messageL.alpha = 0;
                }];
            }];
        }
        
        if (self.isweightLine) {
            // 保存各个点坐标数组
            NSMutableArray *pointWeight = [NSMutableArray array];
            // 保存各个点体重值的数组
            NSMutableArray *weightArray = [NSMutableArray array];
            
            [SYColor(224, 95, 154) set];
            for (int index = 0; index < self.weekChartData.count; index++) {
                YBHealthParmater *result = self.weekChartData[index];
                CGFloat pointX = (result.parmaterDateCom.weekday * sectionWidth);
                //        CGFloat pointX = 80 * (index + 1);
                CGFloat pointY;
                int weight = result.weight * 10;
                if (weight >= 0 && weight <= 400) {
                    pointY = zeroLoadY - coordinateH_400 * weight;
                } else if (weight > 400 && weight <= 600) {
                    pointY = zeroLoadY - (coordinateH + coordinateH_200 * (weight - 400));
                } else if (weight > 600 && weight <= 800) {
                    pointY = zeroLoadY - (coordinateH * 2 + coordinateH_200 * (weight - 600));
                } else if (weight > 800 && weight <= 1000) {
                    pointY = zeroLoadY - (coordinateH * 3 + coordinateH_200 * (weight - 800));
                } else if (weight > 1000 && weight <= 1200) {
                    pointY = zeroLoadY - (coordinateH * 4 + coordinateH_200 * (weight - 1000));
                } else {
                    pointY = 5;
                }
                
                [pointWeight addObject:[NSValue valueWithCGPoint:CGPointMake(pointX, pointY)]];
                [weightArray addObject:[NSString stringWithFormat:@"%.1f", result.weight]];
                if (index == 0) {
                    CGContextMoveToPoint(ctx, pointX, pointY);
                } else {
                    CGContextAddLineToPoint(ctx, pointX, pointY);
                }
            }
            CGContextStrokePath(ctx);
            CGContextRestoreGState(ctx);
            CGContextSaveGState(ctx);
            // 画点的小圆圈
            for (int index = 0; index < pointWeight.count; index++) {
                NSValue *valuePoint = pointWeight[index];
                CGPoint pointxy = [valuePoint CGPointValue];
                [SYColor(224, 95, 154) set];
                CGContextAddArc(ctx, pointxy.x, pointxy.y, 3, 0, M_PI * 2, 0);
                
                CGContextFillPath(ctx);
            }
            // 画点上方的体重值
            NSMutableDictionary *weightDict = [NSMutableDictionary dictionary];
            weightDict[NSFontAttributeName] = [UIFont systemFontOfSize:8];
            weightDict[NSForegroundColorAttributeName] = SYColor(224, 95, 154);
            for (int index = 0; index < pointWeight.count; index++) {
                NSValue *valuePoint = pointWeight[index];
                CGPoint pointxy = [valuePoint CGPointValue];
//                int weightInt = [weightArray[index] intValue];
//                CGFloat weightFloat = weightInt / 10.0;
                NSString *weightStr = weightArray[index];//[NSString stringWithFormat:@"%.1f", weightFloat];
                [weightStr drawAtPoint:CGPointMake(pointxy.x - 7, pointxy.y - 15) withAttributes:weightDict];
            }
            
            CGContextRestoreGState(ctx);
            CGContextSaveGState(ctx);
        } else if(self.isxueyaLine) {
            // 血压曲线
            {  // 保存各个点坐标数组
            NSMutableArray *pointXueyaGao = [NSMutableArray array];
            // 保存各个点体重值的数组
            NSMutableArray *xueyaGaoArray = [NSMutableArray array];
            [SYColor(224, 95, 154) set];
            for (int index = 0; index < self.weekChartData.count; index++) {
                YBHealthParmater *result = self.weekChartData[index];
                CGFloat pointX = (result.parmaterDateCom.weekday * sectionWidth);
                //        CGFloat pointX = 80 * (index + 1);
                CGFloat pointY;
                
                NSInteger xueyaValueCom = result.dbp;
                if (xueyaValueCom >= 0 && xueyaValueCom <= 40) {
                    pointY = zeroLoadY - coordinate12H_40 * xueyaValueCom;
                } else if (xueyaValueCom > 40 && xueyaValueCom <= 60) {
                    pointY = zeroLoadY - (coordinate12H + coordinate12H_20 * (xueyaValueCom - 40));
                } else if (xueyaValueCom > 60 && xueyaValueCom <= 80) {
                    pointY = zeroLoadY - (coordinate12H * 2 + coordinate12H_20 * (xueyaValueCom - 60));
                } else if (xueyaValueCom > 80 && xueyaValueCom <= 100) {
                    pointY = zeroLoadY - (coordinate12H * 3 + coordinate12H_20 * (xueyaValueCom - 80));
                } else if (xueyaValueCom > 100 && xueyaValueCom <= 120) {
                    pointY = zeroLoadY - (coordinate12H * 4 + coordinate12H_20 * (xueyaValueCom - 100));
                } else if (xueyaValueCom > 120 && xueyaValueCom <= 140) {
                    pointY = zeroLoadY - (coordinate12H * 5 + coordinate12H_20 * (xueyaValueCom - 120));
                } else if (xueyaValueCom > 140 && xueyaValueCom <= 160) {
                    pointY = zeroLoadY - (coordinate12H * 5 + coordinate12H_20 * (xueyaValueCom - 140));
                }  else if (xueyaValueCom > 160 && xueyaValueCom <= 180) {
                    pointY = zeroLoadY - (coordinate12H * 5 + coordinate12H_20 * (xueyaValueCom - 160));
                } else if (xueyaValueCom > 180 && xueyaValueCom <= 200) {
                    pointY = zeroLoadY - (coordinate12H * 5 + coordinate12H_20 * (xueyaValueCom - 180));
                }  else if (xueyaValueCom > 200 && xueyaValueCom <= 220) {
                    pointY = zeroLoadY - (coordinate12H * 5 + coordinate12H_20 * (xueyaValueCom - 200));
                }   else {
                    pointY = 5;
                }
                
                [pointXueyaGao addObject:[NSValue valueWithCGPoint:CGPointMake(pointX, pointY)]];
                [xueyaGaoArray addObject:[NSString stringWithFormat:@"%ld", (long)result.dbp]];
                if (index == 0) {
                    CGContextMoveToPoint(ctx, pointX, pointY);
                } else {
                    CGContextAddLineToPoint(ctx, pointX, pointY);
                }
            }
            CGContextStrokePath(ctx);
            CGContextRestoreGState(ctx);
            CGContextSaveGState(ctx);
            // 画点的小圆圈
            for (int index = 0; index < pointXueyaGao.count; index++) {
                NSValue *valuePoint = pointXueyaGao[index];
                CGPoint pointxy = [valuePoint CGPointValue];
                [SYColor(224, 95, 154) set];
                CGContextAddArc(ctx, pointxy.x, pointxy.y, 3, 0, M_PI * 2, 0);
                
                CGContextFillPath(ctx);
            }
            // 画点上方的BMI值
            NSMutableDictionary *xueyaDict = [NSMutableDictionary dictionary];
            xueyaDict[NSFontAttributeName] = [UIFont systemFontOfSize:8];
            xueyaDict[NSForegroundColorAttributeName] = SYColor(224, 95, 154);
            for (int index = 0; index < pointXueyaGao.count; index++) {
                NSValue *valuePoint = pointXueyaGao[index];
                CGPoint pointxy = [valuePoint CGPointValue];
//                CGFloat xuetangValue = [xueyaArray[index] floatValue];
                NSString *xueyaValueStr = xueyaGaoArray[index];
                [xueyaValueStr drawAtPoint:CGPointMake(pointxy.x - 7, pointxy.y - 15) withAttributes:xueyaDict];
            }
        }
        
            {
                // 保存各个点坐标数组
                NSMutableArray *pointXueyaDi = [NSMutableArray array];
                // 保存各个点体重值的数组
                NSMutableArray *xueyaDiArray = [NSMutableArray array];
                [SYColor(84, 189, 253) set];
                for (int index = 0; index < self.weekChartData.count; index++) {
                    YBHealthParmater *result = self.weekChartData[index];
                    CGFloat pointX = (result.parmaterDateCom.weekday * sectionWidth);
                    //        CGFloat pointX = 80 * (index + 1);
                    CGFloat pointY;
                    
                    NSInteger xueyaValueCom = result.sbp;
                    if (xueyaValueCom >= 0 && xueyaValueCom <= 40) {
                        pointY = zeroLoadY - coordinate12H_40 * xueyaValueCom;
                    } else if (xueyaValueCom > 40 && xueyaValueCom <= 60) {
                        pointY = zeroLoadY - (coordinate12H + coordinate12H_20 * (xueyaValueCom - 40));
                    } else if (xueyaValueCom > 60 && xueyaValueCom <= 80) {
                        pointY = zeroLoadY - (coordinate12H * 2 + coordinate12H_20 * (xueyaValueCom - 60));
                    } else if (xueyaValueCom > 80 && xueyaValueCom <= 100) {
                        pointY = zeroLoadY - (coordinate12H * 3 + coordinate12H_20 * (xueyaValueCom - 80));
                    } else if (xueyaValueCom > 100 && xueyaValueCom <= 120) {
                        pointY = zeroLoadY - (coordinate12H * 4 + coordinate12H_20 * (xueyaValueCom - 100));
                    } else if (xueyaValueCom > 120 && xueyaValueCom <= 140) {
                        pointY = zeroLoadY - (coordinate12H * 5 + coordinate12H_20 * (xueyaValueCom - 120));
                    } else if (xueyaValueCom > 140 && xueyaValueCom <= 160) {
                        pointY = zeroLoadY - (coordinate12H * 5 + coordinate12H_20 * (xueyaValueCom - 140));
                    }  else if (xueyaValueCom > 160 && xueyaValueCom <= 180) {
                        pointY = zeroLoadY - (coordinate12H * 5 + coordinate12H_20 * (xueyaValueCom - 160));
                    } else if (xueyaValueCom > 180 && xueyaValueCom <= 200) {
                        pointY = zeroLoadY - (coordinate12H * 5 + coordinate12H_20 * (xueyaValueCom - 180));
                    }  else if (xueyaValueCom > 200 && xueyaValueCom <= 220) {
                        pointY = zeroLoadY - (coordinate12H * 5 + coordinate12H_20 * (xueyaValueCom - 200));
                    }   else {
                        pointY = 5;
                    }
                    
                    [pointXueyaDi addObject:[NSValue valueWithCGPoint:CGPointMake(pointX, pointY)]];
                    [xueyaDiArray addObject:[NSString stringWithFormat:@"%ld", (long)result.sbp]];
                    if (index == 0) {
                        CGContextMoveToPoint(ctx, pointX, pointY);
                    } else {
                        CGContextAddLineToPoint(ctx, pointX, pointY);
                    }
                }
                CGContextStrokePath(ctx);
                CGContextRestoreGState(ctx);
                //        CGContextSaveGState(ctx);
                // 画点的小圆圈
                for (int index = 0; index < pointXueyaDi.count; index++) {
                    NSValue *valuePoint = pointXueyaDi[index];
                    CGPoint pointxy = [valuePoint CGPointValue];
                    [SYColor(84, 189, 253) set];
                    CGContextAddArc(ctx, pointxy.x, pointxy.y, 3, 0, M_PI * 2, 0);
                    
                    CGContextFillPath(ctx);
                }
                // 画点上方的BMI值
                NSMutableDictionary *xueyaDict = [NSMutableDictionary dictionary];
                xueyaDict[NSFontAttributeName] = [UIFont systemFontOfSize:8];
                xueyaDict[NSForegroundColorAttributeName] = SYColor(84, 189, 253);
                for (int index = 0; index < pointXueyaDi.count; index++) {
                    NSValue *valuePoint = pointXueyaDi[index];
                    CGPoint pointxy = [valuePoint CGPointValue];
                    //                CGFloat xuetangValue = [xueyaArray[index] floatValue];
                    NSString *xueyaValueStr = xueyaDiArray[index];
                    [xueyaValueStr drawAtPoint:CGPointMake(pointxy.x - 7, pointxy.y - 15) withAttributes:xueyaDict];
                }

            }
            

        } else if (self.isxuetangLine) {
            // 血糖曲线
            // 保存各个点坐标数组
            NSMutableArray *pointXuetang = [NSMutableArray array];
            // 保存各个点体重值的数组
            NSMutableArray *xuetangArray = [NSMutableArray array];
            [SYColor(224, 95, 154) set];
            for (int index = 0; index < self.weekChartData.count; index++) {
                YBHealthParmater *result = self.weekChartData[index];
                CGFloat pointX = (result.parmaterDateCom.weekday * sectionWidth);
                //        CGFloat pointX = 80 * (index + 1);
                CGFloat pointY;
                
                int xuetangValueCom = result.bloodG * 10;
                if (xuetangValueCom >= 0 && xuetangValueCom <= 20) {
                    pointY = zeroLoadY - coordinate7H_20 * xuetangValueCom;
                } else if (xuetangValueCom > 20 && xuetangValueCom <= 40) {
                    pointY = zeroLoadY - (coordinate7H + coordinate7H_20 * (xuetangValueCom - 20));
                } else if (xuetangValueCom > 40 && xuetangValueCom <= 60) {
                    pointY = zeroLoadY - (coordinate7H * 2 + coordinate7H_20 * (xuetangValueCom - 40));
                } else if (xuetangValueCom > 60 && xuetangValueCom <= 80) {
                    pointY = zeroLoadY - (coordinate7H * 3 + coordinate7H_20 * (xuetangValueCom - 60));
                } else if (xuetangValueCom > 80 && xuetangValueCom <= 100) {
                    pointY = zeroLoadY - (coordinate7H * 4 + coordinate7H_20 * (xuetangValueCom - 80));
                } else if (xuetangValueCom > 100 && xuetangValueCom <= 120) {
                    pointY = zeroLoadY - (coordinate7H * 5 + coordinate7H_20 * (xuetangValueCom - 100));
                } else {
                    pointY = 5;
                }
                
                [pointXuetang addObject:[NSValue valueWithCGPoint:CGPointMake(pointX, pointY)]];
                [xuetangArray addObject:[NSString stringWithFormat:@"%.1f", result.bloodG]];
                if (index == 0) {
                    CGContextMoveToPoint(ctx, pointX, pointY);
                } else {
                    CGContextAddLineToPoint(ctx, pointX, pointY);
                }
            }
            CGContextStrokePath(ctx);
            CGContextRestoreGState(ctx);
            //        CGContextSaveGState(ctx);
            // 画点的小圆圈
            for (int index = 0; index < pointXuetang.count; index++) {
                NSValue *valuePoint = pointXuetang[index];
                CGPoint pointxy = [valuePoint CGPointValue];
                [SYColor(224, 95, 154) set];
                CGContextAddArc(ctx, pointxy.x, pointxy.y, 3, 0, M_PI * 2, 0);
                
                CGContextFillPath(ctx);
            }
            // 画点上方的值
            NSMutableDictionary *xuetangDict = [NSMutableDictionary dictionary];
            xuetangDict[NSFontAttributeName] = [UIFont systemFontOfSize:8];
            xuetangDict[NSForegroundColorAttributeName] = SYColor(224, 95, 154);
            for (int index = 0; index < pointXuetang.count; index++) {
                NSValue *valuePoint = pointXuetang[index];
                CGPoint pointxy = [valuePoint CGPointValue];
                CGFloat xuetangValue = [xuetangArray[index] floatValue];
                NSString *xuetangValueStr = [NSString stringWithFormat:@"%.1f", xuetangValue];
                [xuetangValueStr drawAtPoint:CGPointMake(pointxy.x - 7, pointxy.y - 15) withAttributes:xuetangDict];
            }

        }else if (self.isheartLine) {
            // 心率曲线
            // 保存各个点坐标数组
            NSMutableArray *pointHeart = [NSMutableArray array];
            // 保存各个点体重值的数组
            NSMutableArray *heartArray = [NSMutableArray array];
            [SYColor(224, 95, 154) set];
            for (int index = 0; index < self.weekChartData.count; index++) {
                YBHealthParmater *result = self.weekChartData[index];
                CGFloat pointX = (result.parmaterDateCom.weekday * sectionWidth);
                //        CGFloat pointX = 80 * (index + 1);
                CGFloat pointY;
                
                NSInteger heartValueCom = result.heartRate;
                if (heartValueCom >= 0 && heartValueCom <= 40) {
                    pointY = zeroLoadY - coordinate8H_40 * heartValueCom;
                } else if (heartValueCom > 40 && heartValueCom <= 60) {
                    pointY = zeroLoadY - (coordinate8H + coordinate8H_20 * (heartValueCom - 40));
                } else if (heartValueCom > 60 && heartValueCom <= 80) {
                    pointY = zeroLoadY - (coordinate8H * 2 + coordinate8H_20 * (heartValueCom - 60));
                } else if (heartValueCom > 80 && heartValueCom <= 100) {
                    pointY = zeroLoadY - (coordinate8H * 3 + coordinate8H_20 * (heartValueCom - 80));
                } else if (heartValueCom > 100 && heartValueCom <= 120) {
                    pointY = zeroLoadY - (coordinate8H * 4 + coordinate8H_20 * (heartValueCom - 100));
                } else if (heartValueCom > 120 && heartValueCom <= 140) {
                    pointY = zeroLoadY - (coordinate8H * 5 + coordinate8H_20 * (heartValueCom - 120));
                } else if (heartValueCom > 140 && heartValueCom <= 160) {
                    pointY = zeroLoadY - (coordinate8H * 6 + coordinate8H_20 * (heartValueCom - 140));
                } else {
                    pointY = 5;
                }
                
                [pointHeart addObject:[NSValue valueWithCGPoint:CGPointMake(pointX, pointY)]];
                [heartArray addObject:[NSString stringWithFormat:@"%ld", (long)result.heartRate]];
                if (index == 0) {
                    CGContextMoveToPoint(ctx, pointX, pointY);
                } else {
                    CGContextAddLineToPoint(ctx, pointX, pointY);
                }
            }
            CGContextStrokePath(ctx);
            CGContextRestoreGState(ctx);
            //        CGContextSaveGState(ctx);
            // 画点的小圆圈
            for (int index = 0; index < pointHeart.count; index++) {
                NSValue *valuePoint = pointHeart[index];
                CGPoint pointxy = [valuePoint CGPointValue];
                [SYColor(224, 95, 154) set];
                CGContextAddArc(ctx, pointxy.x, pointxy.y, 3, 0, M_PI * 2, 0);
                
                CGContextFillPath(ctx);
            }
            // 画点上方的心率值
            NSMutableDictionary *heartDict = [NSMutableDictionary dictionary];
            heartDict[NSFontAttributeName] = [UIFont systemFontOfSize:8];
            heartDict[NSForegroundColorAttributeName] = SYColor(224, 95, 154);
            for (int index = 0; index < pointHeart.count; index++) {
                NSValue *valuePoint = pointHeart[index];
                CGPoint pointxy = [valuePoint CGPointValue];
//                CGFloat heartValue = [heartArray[index] floatValue];
                NSString *heartValueStr = heartArray[index];//[NSString stringWithFormat:@"%.1f", heartValue];
                [heartValueStr drawAtPoint:CGPointMake(pointxy.x - 7, pointxy.y - 15) withAttributes:heartDict];
            }
        }else if (self.istiwenLine) {
            // 保存各个点坐标数组
            NSMutableArray *pointTiwen = [NSMutableArray array];
            // 保存各个点体重值的数组
            NSMutableArray *tiwenArray = [NSMutableArray array];
            
            [SYColor(224, 95, 154) set];
            for (NSInteger index = 0; index < self.weekChartData.count; index++) {
                YBHealthParmater *result = self.weekChartData[index];
                CGFloat pointX = (result.parmaterDateCom.weekday * sectionWidth);
                //        CGFloat pointX = 80 * (index + 1);
                CGFloat pointY;
                NSInteger tiwenValueCom = result.tem * 10;
                if (tiwenValueCom >= 0 && tiwenValueCom <= 320) {
                    pointY = zeroLoadY - coordinate8H_40 * tiwenValueCom;
                } else if (tiwenValueCom > 320 && tiwenValueCom <= 340) {
                    pointY = zeroLoadY - (coordinate8H + coordinate8H_20 * (tiwenValueCom - 320));
                } else if (tiwenValueCom > 340 && tiwenValueCom <= 360) {
                    pointY = zeroLoadY - (coordinate8H * 2 + coordinate8H_20 * (tiwenValueCom - 340));
                } else if (tiwenValueCom > 360 && tiwenValueCom <= 380) {
                    pointY = zeroLoadY - (coordinate8H * 3 + coordinate8H_20 * (tiwenValueCom - 360));
                } else if (tiwenValueCom > 380 && tiwenValueCom <= 400) {
                    pointY = zeroLoadY - (coordinate8H * 4 + coordinate8H_20 * (tiwenValueCom - 380));
                } else if (tiwenValueCom > 400 && tiwenValueCom <= 420) {
                    pointY = zeroLoadY - (coordinate8H * 5 + coordinate8H_20 * (tiwenValueCom - 400));
                } else if (tiwenValueCom > 420 && tiwenValueCom <= 440) {
                    pointY = zeroLoadY - (coordinate8H * 6 + coordinate8H_20 * (tiwenValueCom - 420));
                } else {
                    pointY = 5;
                }
                
                [pointTiwen addObject:[NSValue valueWithCGPoint:CGPointMake(pointX, pointY)]];
                [tiwenArray addObject:[NSString stringWithFormat:@"%.1f", result.tem]];
                if (index == 0) {
                    CGContextMoveToPoint(ctx, pointX, pointY);
                } else {
                    CGContextAddLineToPoint(ctx, pointX, pointY);
                }
            }
            CGContextStrokePath(ctx);
            CGContextRestoreGState(ctx);
            CGContextSaveGState(ctx);
            // 画点的小圆圈
            for (int index = 0; index < pointTiwen.count; index++) {
                NSValue *valuePoint = pointTiwen[index];
                CGPoint pointxy = [valuePoint CGPointValue];
                [SYColor(224, 95, 154) set];
                CGContextAddArc(ctx, pointxy.x, pointxy.y, 3, 0, M_PI * 2, 0);
                
                CGContextFillPath(ctx);
            }
            // 画点上方的体重值
            NSMutableDictionary *tiwenDict = [NSMutableDictionary dictionary];
            tiwenDict[NSFontAttributeName] = [UIFont systemFontOfSize:8];
            tiwenDict[NSForegroundColorAttributeName] = SYColor(224, 95, 154);
            for (int index = 0; index < pointTiwen.count; index++) {
                NSValue *valuePoint = pointTiwen[index];
                CGPoint pointxy = [valuePoint CGPointValue];
                //                int weightInt = [weightArray[index] intValue];
                //                CGFloat weightFloat = weightInt / 10.0;
                NSString *weightStr = tiwenArray[index];//[NSString stringWithFormat:@"%.1f", weightFloat];
                [weightStr drawAtPoint:CGPointMake(pointxy.x - 7, pointxy.y - 15) withAttributes:tiwenDict];
            }
            
            CGContextRestoreGState(ctx);
            CGContextSaveGState(ctx);
        }else if(self.isfatLine){
        
            
            
            switch (_yNumber) {
                case 6:
                    [self fatLineBMI_week:ctx sectionW:sectionWidth zeroLoad:zeroLoadY coordinateH:coordinateH coordinate2H:coordinateH_50 coordinate4H:coordinateH_200];
                    break;
                case 7:
                    [self fatLineFat_week:ctx sectionW:sectionWidth zeroLoad:zeroLoadY coordinateH:coordinateH coordinate2H:coordinateH_50 coordinate4H:coordinateH_200*2];
                    break;
                case 8:
                    [self fatLineNeiZang_week:ctx sectionW:sectionWidth zeroLoad:zeroLoadY coordinateH:coordinateH coordinate2H:coordinateH / 30 coordinate4H:(coordinateH_200 * 6.666666) ];
                    break;
                case 9:
                    [self fatLineWater_week:ctx sectionW:sectionWidth zeroLoad:zeroLoadY coordinateH:coordinate8H coordinate2H:coordinate8H / 100 coordinate4H:coordinate8H / 400];
                    break;
                case 10:
                    [self fatLineMuscle_week:ctx sectionW:sectionWidth zeroLoad:zeroLoadY coordinateH:coordinateH coordinate2H:coordinateH / 10 coordinate4H:coordinateH_200 * 20 ];
                    break;
                case 11:
                    [self fatLineBone_week:ctx sectionW:sectionWidth zeroLoad:zeroLoadY coordinateH:coordinate8H coordinate2H:coordinateH / 10 coordinate4H:coordinateH_200 * 20];
                    break;
                case 12:
                     [self fatLineNengLiang_week:ctx sectionW:sectionWidth zeroLoad:zeroLoadY coordinateH:coordinate8H coordinate2H:coordinate8H / 50 coordinate4H:coordinate8H / 100];
                    break;
                default:
                    break;
            }
            
            
        }
        
        // 周图字样
        NSString *weekCh = [NSString stringWithFormat:@"%ld年%ld月第%ld周", (long)self.yearNumber, (long)self.monthNumber, (long)self.weekNumber];
        NSMutableDictionary *dictAttr = [NSMutableDictionary dictionary];
        //        dictAttr[NSFontAttributeName] = [UIFont systemFontOfSize:10];
        dictAttr[NSForegroundColorAttributeName] = SYColor(95, 95, 95);
        [weekCh drawInRect:SYRemindWordRect withAttributes:dictAttr];
        
    } else if (self.isMonthChart) {
        SYLog(@"月图");
        CGFloat sectionWidth = (xZhouW - SYMargin20) / 31;
        // 画x轴
        CGContextMoveToPoint(ctx, 0, zeroLoadY);
        
        SYLog(@"self.frame.size.height:%f", self.frame.size.height);
        CGContextAddLineToPoint(ctx,xZhouW, zeroLoadY);
        [[UIColor lightGrayColor] set];
        CGContextMoveToPoint(ctx, xZhouW - IWStatusTableBorder, zeroLoadY - IWStatusTableBorder);
        CGContextAddLineToPoint(ctx, xZhouW, zeroLoadY);
        CGContextAddLineToPoint(ctx, xZhouW - IWStatusTableBorder,  zeroLoadY + IWStatusTableBorder);
        CGContextStrokePath(ctx);
        
        CGContextStrokePath(ctx);
        
        // 画纵坐标刻度线
        if (self.isweightLine) {
            for (int index = 1; index < 5; index++) {
                CGContextMoveToPoint(ctx, 0, zeroLoadY - index * coordinateH);
                CGContextAddLineToPoint(ctx, xZhouW, zeroLoadY - index * coordinateH);
            }
        } else if (self.isxuetangLine) {
            for (int index = 1; index < 7; index++) {
                CGContextMoveToPoint(ctx, 0, zeroLoadY - index * coordinate7H);
                CGContextAddLineToPoint(ctx, xZhouW, zeroLoadY - index * coordinate7H);
            }
        } else if (self.isheartLine) {
            for (int index = 1; index < 8; index++) {
                CGContextMoveToPoint(ctx, 0, zeroLoadY - index * coordinate8H);
                CGContextAddLineToPoint(ctx, xZhouW, zeroLoadY - index * coordinate8H);
            }
        } else if (self.isxueyaLine) {
            for (int index = 1; index < 12; index++) {
                if (index %2 == 0) {
                    CGContextMoveToPoint(ctx, 0, zeroLoadY - index * coordinate12H);
                    CGContextAddLineToPoint(ctx, xZhouW, zeroLoadY - index * coordinate12H);
                }
            }
        }else if (self.istiwenLine) {
            for (int index = 1; index < 8; index++) {
                for (int index = 1; index < 8; index++) {
                    CGContextMoveToPoint(ctx, 0, zeroLoadY - index * coordinate8H);
                    CGContextAddLineToPoint(ctx, xZhouW, zeroLoadY - index * coordinate8H);
                }
            }
        }else if(self.isfatLine){
        
            if (_yNumber == 9 || (_yNumber == 12)) {
                [self cgcontextpointMethod:ctx xZhouW:xZhouW zeroLoadY:zeroLoadY coorinateH:coordinate8H index:8];
            }else{
                
                [self cgcontextpointMethod:ctx xZhouW:xZhouW zeroLoadY:zeroLoadY coorinateH:coordinateH index:5];
            }
            
        }
        
        
        // 画刻度
        for (int index = 1; index <= 31; index++) {
            // 纵坐标的点 1 2 3 4 5 6 7 8
            CGPoint point = CGPointMake(index* sectionWidth, zeroLoadY);
            // 画x轴刻度线
            CGContextMoveToPoint(ctx, point.x, point.y);
            CGContextAddLineToPoint(ctx, point.x, point.y + IWStatusTableBorder); // 刻度线有5个像素长度
        }
        CGContextStrokePath(ctx);
        
        // 画x轴坐标值
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSFontAttributeName] = [UIFont systemFontOfSize:8];
        
        //        [sunday drawInRect:CGRectMake(SYChartLeftMargin + sectionWidth, zeroLoadY + IWStatusTableBorder, 20, 10) withAttributes:dict];
        for (int index = 1; index <= 31;index += 2) {
            // 纵坐标的点 1 2 3 4 5 6 7 8
            CGPoint point = CGPointMake((index)* sectionWidth, zeroLoadY);
            
            NSString *xNum = [NSString stringWithFormat:@"%d", index];
            [xNum drawInRect:CGRectMake(point.x - IWStatusTableBorder, point.y + IWStatusTableBorder, 20, 10) withAttributes:dict];
        }
        
        
        
        // 获得当前的日期信息
        NSDateComponents *testDate = [NSDateComponents dateComponentsWithNow];
        
        if (self.isSlideToRight == NO && self.isSlideToLeft == NO) {
            
            // 清除weekMeasureResult中的数据
            [self.monthMeasureResult removeAllObjects];
            
            // 本月
            self.isLastMonth = NO;
            if (self.yearNumber == testDate.year) {
                self.monthNumber = testDate.month;
            } else {
                // 切换到非当前年，查看月份数据默认为有数据的最早月份
                for (int i = 0; i < self.measureResult.count; i++) {
                    YBHealthParmater *result = self.measureResult[i];
                    if (result.parmaterDateCom.year == self.yearNumber) {
                        self.monthNumber = result.parmaterDateCom.month;
                        break;
                    }
                }
            }
            
            for (int i = 0; i < self.measureResult.count; i++) {
                YBHealthParmater *result = self.measureResult[i];
                // 图表上的显示年份数据要对应着该年的月份数据
                if (result.parmaterDateCom.year == self.yearNumber && result.parmaterDateCom.month == self.monthNumber) {
                    [self.monthMeasureResult addObject:self.measureResult[i]];
                }
            }
        } else if (self.isSlideToLeft) {
            self.isSlideToLeft = NO;
            // 下月
            if ((self.monthNumber + 1) <= (self.yearNumber == testDate.year ? testDate.month : 12)) {
                self.monthNumber += 1;
                self.isLastMonth= NO;
                // 清除monthMeasureResult中的数据
                [self.monthMeasureResult removeAllObjects];
                for (int i = 0; i < self.measureResult.count; i++) {
                    YBHealthParmater *result = self.measureResult[i];
                    if (result.parmaterDateCom.year == self.yearNumber && result.parmaterDateCom.month == self.monthNumber) {
                        [self.monthMeasureResult addObject:self.measureResult[i]];
                    }
                }
            } else {
                self.isLastMonth = YES;
                NSString *infoStr = self.yearNumber == testDate.year ? @"已是当前月份了哦" : @"已是该年度最后一个月了哦";
                self.messageL.text = infoStr;
                [UIView animateWithDuration:1.0 animations:^{
                    self.messageL.alpha = 1.0;
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:1.0 animations:^{
                        self.messageL.alpha = 0;
                    }];
                }];
                //                [MBProgressHUD showError:infoStr];
            }
        } else if (self.isSlideToRight) {
            self.isSlideToRight = NO;
            // 上月
            if ((self.monthNumber - 1) >= 1) {
                self.monthNumber -= 1;
                self.isLastMonth = NO;
                // 清除monthMeasureResult中的数据
                [self.monthMeasureResult removeAllObjects];
                for (int i = 0; i < self.measureResult.count; i++) {
                    YBHealthParmater *result = self.measureResult[i];
                    if (result.parmaterDateCom.year == self.yearNumber && result.parmaterDateCom.month == self.monthNumber) {
                        [self.monthMeasureResult addObject:self.measureResult[i]];
                    }
                }
            } else {
                self.isLastMonth = YES;
                self.messageL.text = @"已是一月";
                [UIView animateWithDuration:1.0 animations:^{
                    self.messageL.alpha = 1.0;
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:1.0 animations:^{
                        self.messageL.alpha = 0;
                    }];
                }];
                //                [MBProgressHUD showError:@"已是一月"];
            }
        }
        
        NSMutableArray *tempResult = [NSMutableArray array];
        // 前一个元素的星期
        NSInteger daylast = 0;
        SYLog(@"self.weekMeasureResult.count: %ld", (unsigned long)self.monthMeasureResult.count);
        
        for (int index = 0; index < self.monthMeasureResult.count; index++) {
            YBHealthParmater *result = self.monthMeasureResult[index];
            SYLog(@"weekLast:%ld, weekday:%ld", (long)daylast, (long)result.parmaterDateCom.day);
            if (daylast == 0) {
                //
            } else if ((result.parmaterDateCom.day) > daylast){
                SYLog(@"加入需要画得数组");
                [tempResult addObject:self.monthMeasureResult[index - 1]];
            }
            
            if (index == (self.monthMeasureResult.count -1)) {
                [tempResult addObject:[self.monthMeasureResult lastObject]];
            }
            daylast = result.parmaterDateCom.day;
        }
        
        [self.monthChartData removeAllObjects];
        self.monthChartData  = tempResult;
        
        if (self.monthChartData.count == 0 && self.isLastMonth == NO) {
            self.messageL.text = @"本月没有数据";
            [UIView animateWithDuration:1.0 animations:^{
                self.messageL.alpha = 1.0;
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:1.0 animations:^{
                    self.messageL.alpha = 0;
                }];
            }];
        }
        
        if (self.isweightLine) {
            NSMutableArray *point = [NSMutableArray array];
            // 保存各个点体重值的数组
            NSMutableArray *weightArray = [NSMutableArray array];
            // 设置所画线为白色
            [SYColor(224, 95, 154) set];
            for (int index = 0; index < self.monthChartData.count; index++) {
                YBHealthParmater *result = self.monthChartData[index];
                CGFloat pointX = (result.parmaterDateCom.day * sectionWidth);
                //        CGFloat pointX = 80 * (index + 1);
                CGFloat pointY;
                int weight = result.weight * 10;
                if (weight >= 0 && weight <= 400) {
                    pointY = zeroLoadY - coordinateH_400 * weight;
                } else if (weight > 400 && weight <= 600) {
                    pointY = zeroLoadY - (coordinateH + coordinateH_200 * (weight - 400));
                } else if (weight > 600 && weight <= 800) {
                    pointY = zeroLoadY - (coordinateH * 2 + coordinateH_200 * (weight - 600));
                } else if (weight > 800 && weight <= 1000) {
                    pointY = zeroLoadY - (coordinateH * 3 + coordinateH_200 * (weight - 800));
                } else if (weight > 1000 && weight <= 1200) {
                    pointY = zeroLoadY - (coordinateH * 4 + coordinateH_200 * (weight - 1000));
                } else {
                    pointY = 5;
                }
                
                [point addObject:[NSValue valueWithCGPoint:CGPointMake(pointX, pointY)]];
                [weightArray addObject:[NSString stringWithFormat:@"%.1f", result.weight]];
                if (index == 0) {
                    CGContextMoveToPoint(ctx, pointX, pointY);
                } else {
                    CGContextAddLineToPoint(ctx, pointX, pointY);
                }
            }
            CGContextStrokePath(ctx);
            CGContextRestoreGState(ctx);
            CGContextSaveGState(ctx);
            // 画每个点上的小圆
            for (int index = 0; index < point.count; index++) {
                NSValue *valuePoint = point[index];
                CGPoint pointxy = [valuePoint CGPointValue];
                [SYColor(224, 95, 154) set];
                CGContextAddArc(ctx, pointxy.x, pointxy.y, 3, 0, M_PI * 2, 0);
                CGContextFillPath(ctx);
                
            }
            
            // 画点上方的体重值
            NSMutableDictionary *weightDict = [NSMutableDictionary dictionary];
            weightDict[NSFontAttributeName] = [UIFont systemFontOfSize:8];
            weightDict[NSForegroundColorAttributeName] = SYColor(224, 95, 154);
            for (int index = 0; index < point.count; index++) {
                NSValue *valuePoint = point[index];
                CGPoint pointxy = [valuePoint CGPointValue];
//                int weightInt = [weightArray[index] intValue];
//                CGFloat weightFloat = weightInt / 10.0;
                NSString *weightStr = weightArray[index];//[NSString stringWithFormat:@"%.1f", weightFloat];
                [weightStr drawAtPoint:CGPointMake(pointxy.x - 7, pointxy.y - 15) withAttributes:weightDict];
            }
            
            CGContextRestoreGState(ctx);
            CGContextSaveGState(ctx);
        } else if (self.isxueyaLine) {
            {
                NSMutableArray *pointXueyaGao = [NSMutableArray array];
                // 保存各个点体重值的数组
                NSMutableArray *xueyaGaoArray = [NSMutableArray array];
                // 设置所画线为白色
                [SYColor(224, 95, 154) set];
                for (int index = 0; index < self.monthChartData.count; index++) {
                    YBHealthParmater *result = self.monthChartData[index];
                    CGFloat pointX = (result.parmaterDateCom.day * sectionWidth);
                    //        CGFloat pointX = 80 * (index + 1);
                    CGFloat pointY;
                    NSInteger xueyaValueCom = result.dbp;
                    if (xueyaValueCom >= 0 && xueyaValueCom <= 40) {
                        pointY = zeroLoadY - coordinate12H_40 * xueyaValueCom;
                    } else if (xueyaValueCom > 40 && xueyaValueCom <= 60) {
                        pointY = zeroLoadY - (coordinate12H + coordinate12H_20 * (xueyaValueCom - 40));
                    } else if (xueyaValueCom > 60 && xueyaValueCom <= 80) {
                        pointY = zeroLoadY - (coordinate12H * 2 + coordinate12H_20 * (xueyaValueCom - 60));
                    } else if (xueyaValueCom > 80 && xueyaValueCom <= 100) {
                        pointY = zeroLoadY - (coordinate12H * 3 + coordinate12H_20 * (xueyaValueCom - 80));
                    } else if (xueyaValueCom > 100 && xueyaValueCom <= 120) {
                        pointY = zeroLoadY - (coordinate12H * 4 + coordinate12H_20 * (xueyaValueCom - 100));
                    } else if (xueyaValueCom > 120 && xueyaValueCom <= 140) {
                        pointY = zeroLoadY - (coordinate12H * 5 + coordinate12H_20 * (xueyaValueCom - 120));
                    } else if (xueyaValueCom > 140 && xueyaValueCom <= 160) {
                        pointY = zeroLoadY - (coordinate12H * 5 + coordinate12H_20 * (xueyaValueCom - 140));
                    }  else if (xueyaValueCom > 160 && xueyaValueCom <= 180) {
                        pointY = zeroLoadY - (coordinate12H * 5 + coordinate12H_20 * (xueyaValueCom - 160));
                    } else if (xueyaValueCom > 180 && xueyaValueCom <= 200) {
                        pointY = zeroLoadY - (coordinate12H * 5 + coordinate12H_20 * (xueyaValueCom - 180));
                    }  else if (xueyaValueCom > 200 && xueyaValueCom <= 220) {
                        pointY = zeroLoadY - (coordinate12H * 5 + coordinate12H_20 * (xueyaValueCom - 200));
                    }   else {
                        pointY = 5;
                    }
                    
                    [pointXueyaGao addObject:[NSValue valueWithCGPoint:CGPointMake(pointX, pointY)]];
                    [xueyaGaoArray addObject:[NSString stringWithFormat:@"%ld", (long)result.dbp]];
                    if (index == 0) {
                        CGContextMoveToPoint(ctx, pointX, pointY);
                    } else {
                        CGContextAddLineToPoint(ctx, pointX, pointY);
                    }
                }
                CGContextStrokePath(ctx);
                CGContextRestoreGState(ctx);
                CGContextSaveGState(ctx);
                // 画每个点上的小圆
                for (int index = 0; index < pointXueyaGao.count; index++) {
                    NSValue *valuePoint = pointXueyaGao[index];
                    CGPoint pointxy = [valuePoint CGPointValue];
                    [SYColor(224, 95, 154) set];
                    CGContextAddArc(ctx, pointxy.x, pointxy.y, 3, 0, M_PI * 2, 0);
                    CGContextFillPath(ctx);
                    
                }
                
                // 画点上方的体重值
                NSMutableDictionary *xueyaDict = [NSMutableDictionary dictionary];
                xueyaDict[NSFontAttributeName] = [UIFont systemFontOfSize:8];
                xueyaDict[NSForegroundColorAttributeName] = SYColor(224, 95, 154);
                for (int index = 0; index < pointXueyaGao.count; index++) {
                    NSValue *valuePoint = pointXueyaGao[index];
                    CGPoint pointxy = [valuePoint CGPointValue];
                    //                int weightInt = [weightArray[index] intValue];
                    //                CGFloat weightFloat = weightInt / 10.0;
                    NSString *xueyaStr = xueyaGaoArray[index];//[NSString stringWithFormat:@"%.1f", weightFloat];
                    [xueyaStr drawAtPoint:CGPointMake(pointxy.x - 7, pointxy.y - 15) withAttributes:xueyaDict];
                }
                
                CGContextRestoreGState(ctx);
                CGContextSaveGState(ctx);
            }
            {
                NSMutableArray *pointXueyaDi = [NSMutableArray array];
                // 保存各个点体重值的数组
                NSMutableArray *xueyaDiArray = [NSMutableArray array];
                // 设置所画线为白色
                [SYColor(84, 189, 253) set];
                for (int index = 0; index < self.monthChartData.count; index++) {
                    YBHealthParmater *result = self.monthChartData[index];
                    CGFloat pointX = (result.parmaterDateCom.day * sectionWidth);
                    //        CGFloat pointX = 80 * (index + 1);
                    CGFloat pointY;
                    NSInteger xueyaValueCom = result.sbp;
                    if (xueyaValueCom >= 0 && xueyaValueCom <= 40) {
                        pointY = zeroLoadY - coordinate12H_40 * xueyaValueCom;
                    } else if (xueyaValueCom > 40 && xueyaValueCom <= 60) {
                        pointY = zeroLoadY - (coordinate12H + coordinate12H_20 * (xueyaValueCom - 40));
                    } else if (xueyaValueCom > 60 && xueyaValueCom <= 80) {
                        pointY = zeroLoadY - (coordinate12H * 2 + coordinate12H_20 * (xueyaValueCom - 60));
                    } else if (xueyaValueCom > 80 && xueyaValueCom <= 100) {
                        pointY = zeroLoadY - (coordinate12H * 3 + coordinate12H_20 * (xueyaValueCom - 80));
                    } else if (xueyaValueCom > 100 && xueyaValueCom <= 120) {
                        pointY = zeroLoadY - (coordinate12H * 4 + coordinate12H_20 * (xueyaValueCom - 100));
                    } else if (xueyaValueCom > 120 && xueyaValueCom <= 140) {
                        pointY = zeroLoadY - (coordinate12H * 5 + coordinate12H_20 * (xueyaValueCom - 120));
                    } else if (xueyaValueCom > 140 && xueyaValueCom <= 160) {
                        pointY = zeroLoadY - (coordinate12H * 5 + coordinate12H_20 * (xueyaValueCom - 140));
                    }  else if (xueyaValueCom > 160 && xueyaValueCom <= 180) {
                        pointY = zeroLoadY - (coordinate12H * 5 + coordinate12H_20 * (xueyaValueCom - 160));
                    } else if (xueyaValueCom > 180 && xueyaValueCom <= 200) {
                        pointY = zeroLoadY - (coordinate12H * 5 + coordinate12H_20 * (xueyaValueCom - 180));
                    }  else if (xueyaValueCom > 200 && xueyaValueCom <= 220) {
                        pointY = zeroLoadY - (coordinate12H * 5 + coordinate12H_20 * (xueyaValueCom - 200));
                    }   else {
                        pointY = 5;
                    }
                    
                    [pointXueyaDi addObject:[NSValue valueWithCGPoint:CGPointMake(pointX, pointY)]];
                    [xueyaDiArray addObject:[NSString stringWithFormat:@"%ld", (long)result.sbp]];
                    if (index == 0) {
                        CGContextMoveToPoint(ctx, pointX, pointY);
                    } else {
                        CGContextAddLineToPoint(ctx, pointX, pointY);
                    }
                }
                CGContextStrokePath(ctx);
                CGContextRestoreGState(ctx);
                CGContextSaveGState(ctx);
                // 画每个点上的小圆
                for (int index = 0; index < pointXueyaDi.count; index++) {
                    NSValue *valuePoint = pointXueyaDi[index];
                    CGPoint pointxy = [valuePoint CGPointValue];
                    [SYColor(84, 189, 253) set];
                    CGContextAddArc(ctx, pointxy.x, pointxy.y, 3, 0, M_PI * 2, 0);
                    CGContextFillPath(ctx);
                    
                }
                
                // 画点上方的体重值
                NSMutableDictionary *xueyaDict = [NSMutableDictionary dictionary];
                xueyaDict[NSFontAttributeName] = [UIFont systemFontOfSize:8];
                xueyaDict[NSForegroundColorAttributeName] = SYColor(84, 189, 253);
                for (int index = 0; index < pointXueyaDi.count; index++) {
                    NSValue *valuePoint = pointXueyaDi[index];
                    CGPoint pointxy = [valuePoint CGPointValue];
                    //                int weightInt = [weightArray[index] intValue];
                    //                CGFloat weightFloat = weightInt / 10.0;
                    NSString *xueyaStr = xueyaDiArray[index];//[NSString stringWithFormat:@"%.1f", weightFloat];
                    [xueyaStr drawAtPoint:CGPointMake(pointxy.x - 7, pointxy.y - 15) withAttributes:xueyaDict];
                }
                
                CGContextRestoreGState(ctx);
                CGContextSaveGState(ctx);

            }
        } else if (self.isxuetangLine) {
            // 月图
            // 血糖曲线
            // 保存各个点坐标数组
            NSMutableArray *pointXuetang = [NSMutableArray array];
            // 保存各个点体重值的数组
            NSMutableArray *xuetangArray = [NSMutableArray array];
            [SYColor(224, 95, 154) set];
            for (int index = 0; index < self.monthChartData.count; index++) {
                YBHealthParmater *result = self.monthChartData[index];
                CGFloat pointX = (result.parmaterDateCom.day * sectionWidth);
                //        CGFloat pointX = 80 * (index + 1);
                CGFloat pointY;
                
                
                int xuetangValueCom = result.bloodG * 10;
                if (xuetangValueCom >= 0 && xuetangValueCom <= 20) {
                    pointY = zeroLoadY - coordinate7H_20 * xuetangValueCom;
                } else if (xuetangValueCom > 20 && xuetangValueCom <= 40) {
                    pointY = zeroLoadY - (coordinate7H + coordinate7H_20 * (xuetangValueCom - 20));
                } else if (xuetangValueCom > 40 && xuetangValueCom <= 60) {
                    pointY = zeroLoadY - (coordinate7H * 2 + coordinate7H_20 * (xuetangValueCom - 40));
                } else if (xuetangValueCom > 60 && xuetangValueCom <= 80) {
                    pointY = zeroLoadY - (coordinate7H * 3 + coordinate7H_20 * (xuetangValueCom - 60));
                } else if (xuetangValueCom > 80 && xuetangValueCom <= 100) {
                    pointY = zeroLoadY - (coordinate7H * 4 + coordinate7H_20 * (xuetangValueCom - 80));
                } else if (xuetangValueCom > 100 && xuetangValueCom <= 120) {
                    pointY = zeroLoadY - (coordinate7H * 5 + coordinate7H_20 * (xuetangValueCom - 100));
                } else {
                    pointY = 5;
                }
                
                
                [pointXuetang addObject:[NSValue valueWithCGPoint:CGPointMake(pointX, pointY)]];
                [xuetangArray addObject:[NSString stringWithFormat:@"%.1f", result.bloodG]];
                if (index == 0) {
                    CGContextMoveToPoint(ctx, pointX, pointY);
                } else {
                    CGContextAddLineToPoint(ctx, pointX, pointY);
                }
            }
            CGContextStrokePath(ctx);
            CGContextRestoreGState(ctx);
            //        CGContextSaveGState(ctx);
            // 画点的小圆圈
            for (int index = 0; index < pointXuetang.count; index++) {
                NSValue *valuePoint = pointXuetang[index];
                CGPoint pointxy = [valuePoint CGPointValue];
                [SYColor(224, 95, 154) set];
                CGContextAddArc(ctx, pointxy.x, pointxy.y, 3, 0, M_PI * 2, 0);
                
                CGContextFillPath(ctx);
            }
            // 画点上方的值
            NSMutableDictionary *xuetangDict = [NSMutableDictionary dictionary];
            xuetangDict[NSFontAttributeName] = [UIFont systemFontOfSize:8];
            xuetangDict[NSForegroundColorAttributeName] = SYColor(224, 95, 154);
            for (int index = 0; index < pointXuetang.count; index++) {
                NSValue *valuePoint = pointXuetang[index];
                CGPoint pointxy = [valuePoint CGPointValue];
//                CGFloat bmiValue = [xuetangArray[index] floatValue];
                NSString *xuetangValueStr = xuetangArray[index];//[NSString stringWithFormat:@"%.1f", bmiValue];
                [xuetangValueStr drawAtPoint:CGPointMake(pointxy.x - 7, pointxy.y - 15) withAttributes:xuetangDict];
            }
        } else if (self.heartLine) {
            // 月图
            // 心率曲线
            // 保存各个点坐标数组
            NSMutableArray *pointHeart = [NSMutableArray array];
            // 保存各个点体重值的数组
            NSMutableArray *heartArray = [NSMutableArray array];
            [SYColor(224, 95, 154) set];
            for (int index = 0; index < self.monthChartData.count; index++) {
                YBHealthParmater *result = self.monthChartData[index];
                CGFloat pointX = (result.parmaterDateCom.day * sectionWidth);
                //        CGFloat pointX = 80 * (index + 1);
                CGFloat pointY;
                
                NSInteger heartValueCom = result.heartRate;
                if (heartValueCom >= 0 && heartValueCom <= 40) {
                    pointY = zeroLoadY - coordinate8H_40 * heartValueCom;
                } else if (heartValueCom > 40 && heartValueCom <= 60) {
                    pointY = zeroLoadY - (coordinate8H + coordinate8H_20 * (heartValueCom - 40));
                } else if (heartValueCom > 60 && heartValueCom <= 80) {
                    pointY = zeroLoadY - (coordinate8H * 2 + coordinate8H_20 * (heartValueCom - 60));
                } else if (heartValueCom > 80 && heartValueCom <= 100) {
                    pointY = zeroLoadY - (coordinate8H * 3 + coordinate8H_20 * (heartValueCom - 80));
                } else if (heartValueCom > 100 && heartValueCom <= 120) {
                    pointY = zeroLoadY - (coordinate8H * 4 + coordinate8H_20 * (heartValueCom - 100));
                } else if (heartValueCom > 120 && heartValueCom <= 140) {
                    pointY = zeroLoadY - (coordinate8H * 5 + coordinate8H_20 * (heartValueCom - 120));
                } else if (heartValueCom > 140 && heartValueCom <= 160) {
                    pointY = zeroLoadY - (coordinate8H * 6 + coordinate8H_20 * (heartValueCom - 140));
                } else {
                    pointY = 5;
                }
                
                [pointHeart addObject:[NSValue valueWithCGPoint:CGPointMake(pointX, pointY)]];
                [heartArray addObject:[NSString stringWithFormat:@"%ld", (long)result.heartRate]];
                if (index == 0) {
                    CGContextMoveToPoint(ctx, pointX, pointY);
                } else {
                    CGContextAddLineToPoint(ctx, pointX, pointY);
                }
            }
            CGContextStrokePath(ctx);
            CGContextRestoreGState(ctx);
            //        CGContextSaveGState(ctx);
            // 画点的小圆圈
            for (int index = 0; index < pointHeart.count; index++) {
                NSValue *valuePoint = pointHeart[index];
                CGPoint pointxy = [valuePoint CGPointValue];
                [SYColor(224, 95, 154) set];
                CGContextAddArc(ctx, pointxy.x, pointxy.y, 3, 0, M_PI * 2, 0);
                
                CGContextFillPath(ctx);
            }
            // 画点上方的BMI值
            NSMutableDictionary *heartDict = [NSMutableDictionary dictionary];
            heartDict[NSFontAttributeName] = [UIFont systemFontOfSize:8];
            heartDict[NSForegroundColorAttributeName] = SYColor(224, 95, 154);
            for (int index = 0; index < pointHeart.count; index++) {
                NSValue *valuePoint = pointHeart[index];
                CGPoint pointxy = [valuePoint CGPointValue];
                //                CGFloat bmiValue = [xuetangArray[index] floatValue];
                NSString *heartValueStr = heartArray[index];//[NSString stringWithFormat:@"%.1f", bmiValue];
                [heartValueStr drawAtPoint:CGPointMake(pointxy.x - 7, pointxy.y - 15) withAttributes:heartDict];
            }
        }else if (self.istiwenLine){
        
            // 月图
            // 心率曲线
            // 保存各个点坐标数组
            NSMutableArray *pointTiwen = [NSMutableArray array];
            // 保存各个点体温值的数组
            NSMutableArray *tiwenArray = [NSMutableArray array];
            
            [SYColor(224, 95, 154) set];
            for (NSInteger index = 0; index < self.monthChartData.count; index++) {
                YBHealthParmater *result = self.monthChartData[index];
                CGFloat pointX = (result.parmaterDateCom.day * sectionWidth);
                //        CGFloat pointX = 80 * (index + 1);
                CGFloat pointY;
                NSInteger tiwenValueCom = result.tem * 10;
                if (tiwenValueCom >= 0 && tiwenValueCom <= 320) {
                    pointY = zeroLoadY - coordinate8H_40 * tiwenValueCom;
                } else if (tiwenValueCom > 320 && tiwenValueCom <= 340) {
                    pointY = zeroLoadY - (coordinate8H + coordinate8H_20 * (tiwenValueCom - 320));
                } else if (tiwenValueCom > 340 && tiwenValueCom <= 360) {
                    pointY = zeroLoadY - (coordinate8H * 2 + coordinate8H_20 * (tiwenValueCom - 340));
                } else if (tiwenValueCom > 360 && tiwenValueCom <= 380) {
                    pointY = zeroLoadY - (coordinate8H * 3 + coordinate8H_20 * (tiwenValueCom - 360));
                } else if (tiwenValueCom > 380 && tiwenValueCom <= 400) {
                    pointY = zeroLoadY - (coordinate8H * 4 + coordinate8H_20 * (tiwenValueCom - 380));
                } else if (tiwenValueCom > 400 && tiwenValueCom <= 420) {
                    pointY = zeroLoadY - (coordinate8H * 5 + coordinate8H_20 * (tiwenValueCom - 400));
                } else if (tiwenValueCom > 420 && tiwenValueCom <= 440) {
                    pointY = zeroLoadY - (coordinate8H * 6 + coordinate8H_20 * (tiwenValueCom - 420));
                } else {
                    pointY = 5;
                }
                
                [pointTiwen addObject:[NSValue valueWithCGPoint:CGPointMake(pointX, pointY)]];
                [tiwenArray addObject:[NSString stringWithFormat:@"%.1f", result.tem]];
                if (index == 0) {
                    CGContextMoveToPoint(ctx, pointX, pointY);
                } else {
                    CGContextAddLineToPoint(ctx, pointX, pointY);
                }
            }
            CGContextStrokePath(ctx);
            CGContextRestoreGState(ctx);
            CGContextSaveGState(ctx);
            // 画点的小圆圈
            for (int index = 0; index < pointTiwen.count; index++) {
                NSValue *valuePoint = pointTiwen[index];
                CGPoint pointxy = [valuePoint CGPointValue];
                [SYColor(224, 95, 154) set];
                CGContextAddArc(ctx, pointxy.x, pointxy.y, 3, 0, M_PI * 2, 0);
                
                CGContextFillPath(ctx);
            }
            // 画点上方的体重值
            NSMutableDictionary *tiwenDict = [NSMutableDictionary dictionary];
            tiwenDict[NSFontAttributeName] = [UIFont systemFontOfSize:8];
            tiwenDict[NSForegroundColorAttributeName] = SYColor(224, 95, 154);
            for (int index = 0; index < pointTiwen.count; index++) {
                NSValue *valuePoint = pointTiwen[index];
                CGPoint pointxy = [valuePoint CGPointValue];
                //                int weightInt = [weightArray[index] intValue];
                //                CGFloat weightFloat = weightInt / 10.0;
                NSString *weightStr = tiwenArray[index];//[NSString stringWithFormat:@"%.1f", weightFloat];
                [weightStr drawAtPoint:CGPointMake(pointxy.x - 7, pointxy.y - 15) withAttributes:tiwenDict];
            }
            
            CGContextRestoreGState(ctx);
            CGContextSaveGState(ctx);
        }else if(self.isfatLine)
        {
            switch (_yNumber) {
                case 6:
                    [self fatLineBMI_Month:ctx sectionW:sectionWidth zeroLoad:zeroLoadY coordinateH:coordinateH coordinate2H:coordinateH_50 coordinate4H:coordinateH_200];
                    break;
                case 7:
                    [self fatLineFat_Month:ctx sectionW:sectionWidth zeroLoad:zeroLoadY coordinateH:coordinateH coordinate2H:coordinateH_50 coordinate4H:coordinateH_200*2];
                    break;
                case 8:
                    [self fatLineNeiZang_Month:ctx sectionW:sectionWidth zeroLoad:zeroLoadY coordinateH:coordinateH coordinate2H:coordinateH / 30 coordinate4H:(coordinateH_200 * 6.666666) ];
                    break;
                case 9:
                    [self fatLineWater_Month:ctx sectionW:sectionWidth zeroLoad:zeroLoadY coordinateH:coordinate8H coordinate2H:coordinate8H / 100 coordinate4H:coordinate8H / 400];
                    break;
                case 10:
                    [self fatLineMuscle_Month:ctx sectionW:sectionWidth zeroLoad:zeroLoadY coordinateH:coordinateH coordinate2H:coordinateH / 10 coordinate4H:coordinateH_200 * 20 ];
                    break;
                case 11:
                    [self fatLineBone_Month:ctx sectionW:sectionWidth zeroLoad:zeroLoadY coordinateH:coordinate8H coordinate2H:coordinateH / 10 coordinate4H:coordinateH_200 * 20];
                    break;
                case 12:
                    [self fatLineNengLiang_Month:ctx sectionW:sectionWidth zeroLoad:zeroLoadY coordinateH:coordinate8H coordinate2H:coordinate8H / 50 coordinate4H:coordinate8H / 100];
                    break;
                default:
                    break;
            }

        }
        }
        
        
        // 月图字样
        NSString *monthCh = [NSString stringWithFormat:@"%ld年%ld月", (long)self.yearNumber, (long)self.monthNumber];
        NSMutableDictionary *dictAttr = [NSMutableDictionary dictionary];
        //        dictAttr[NSFontAttributeName] = [UIFont systemFontOfSize:10];
        dictAttr[NSForegroundColorAttributeName] = SYColor(95, 95, 95);
        [monthCh drawInRect:SYRemindWordRect withAttributes:dictAttr];

}

#pragma -mark private Method
- (void)cgcontextpointMethod:(CGContextRef)ctxP xZhouW:(CGFloat)xZhouWP zeroLoadY:(CGFloat)zeroLoadYP coorinateH:(CGFloat)coordinateHP index:(NSInteger)Num
{
    for (int index = 1; index < Num; index++) {
        CGContextMoveToPoint(ctxP, 0, zeroLoadYP - index * coordinateHP);
        CGContextAddLineToPoint(ctxP, xZhouWP, zeroLoadYP - index * coordinateHP);
    }
}

#pragma -mark 周图
- (void)fatLineBMI_week:(CGContextRef)ctx sectionW:(CGFloat)sectionWidth zeroLoad:(CGFloat)zeroLoadY coordinateH:(CGFloat)coordinateH coordinate2H:(CGFloat)coordinateH_200 coordinate4H:(CGFloat)coordinateH_400
{
    // 保存各个点坐标数组
    NSMutableArray *pointTiwen = [NSMutableArray array];
    // 保存各个点体重值的数组
    NSMutableArray *tiwenArray = [NSMutableArray array];
    
    [SYColor(224, 95, 154) set];
    for (NSInteger index = 0; index < self.weekChartData.count; index++) {
        YBBodyFatModel *result = self.weekChartData[index];
        CGFloat pointX = (result.parmaterDateCom.weekday * sectionWidth);
        //        CGFloat pointX = 80 * (index + 1);
        CGFloat pointY;
        NSInteger weight = result.bmi * 10;
        if (weight >= 0 && weight <= 200) {
            pointY = zeroLoadY - coordinateH_400 * weight;
        } else if (weight > 200 && weight <= 250) {
            pointY = zeroLoadY - (coordinateH + coordinateH_200 * (weight - 200));
        } else if (weight > 250 && weight <= 300) {
            pointY = zeroLoadY - (coordinateH * 2 + coordinateH_200 * (weight - 250));
        } else if (weight > 300 && weight <= 350) {
            pointY = zeroLoadY - (coordinateH * 3 + coordinateH_200 * (weight - 300));
        } else if (weight > 350 && weight <= 400) {
            pointY = zeroLoadY - (coordinateH * 4 + coordinateH_200 * (weight - 350));
        } else {
            pointY = 5;
        }
        
        [pointTiwen addObject:[NSValue valueWithCGPoint:CGPointMake(pointX, pointY)]];
        [tiwenArray addObject:[NSString stringWithFormat:@"%.1f", result.bmi]];
        if (index == 0) {
            CGContextMoveToPoint(ctx, pointX, pointY);
        } else {
            CGContextAddLineToPoint(ctx, pointX, pointY);
        }
    }
    CGContextStrokePath(ctx);
    CGContextRestoreGState(ctx);
    CGContextSaveGState(ctx);
    // 画点的小圆圈
    for (int index = 0; index < pointTiwen.count; index++) {
        NSValue *valuePoint = pointTiwen[index];
        CGPoint pointxy = [valuePoint CGPointValue];
        [SYColor(224, 95, 154) set];
        CGContextAddArc(ctx, pointxy.x, pointxy.y, 3, 0, M_PI * 2, 0);
        
        CGContextFillPath(ctx);
    }
    // 画点上方的体重值
    NSMutableDictionary *tiwenDict = [NSMutableDictionary dictionary];
    tiwenDict[NSFontAttributeName] = [UIFont systemFontOfSize:8];
    tiwenDict[NSForegroundColorAttributeName] = SYColor(224, 95, 154);
    for (int index = 0; index < pointTiwen.count; index++) {
        NSValue *valuePoint = pointTiwen[index];
        CGPoint pointxy = [valuePoint CGPointValue];
        //                int weightInt = [weightArray[index] intValue];
        //                CGFloat weightFloat = weightInt / 10.0;
        NSString *weightStr = tiwenArray[index];//[NSString stringWithFormat:@"%.1f", weightFloat];
        [weightStr drawAtPoint:CGPointMake(pointxy.x - 7, pointxy.y - 15) withAttributes:tiwenDict];
    }
    
    CGContextRestoreGState(ctx);
    CGContextSaveGState(ctx);
}

- (void)fatLineFat_week:(CGContextRef)ctx sectionW:(CGFloat)sectionWidth zeroLoad:(CGFloat)zeroLoadY coordinateH:(CGFloat)coordinateH coordinate2H:(CGFloat)coordinateH_200 coordinate4H:(CGFloat)coordinateH_400
{
    // 保存各个点坐标数组
    NSMutableArray *pointTiwen = [NSMutableArray array];
    // 保存各个点体重值的数组
    NSMutableArray *tiwenArray = [NSMutableArray array];
    
    [SYColor(224, 95, 154) set];
    for (NSInteger index = 0; index < self.weekChartData.count; index++) {
        YBBodyFatModel *result = self.weekChartData[index];
        CGFloat pointX = (result.parmaterDateCom.weekday * sectionWidth);
        //        CGFloat pointX = 80 * (index + 1);
        CGFloat pointY;
        NSInteger weight = result.fat * 10;
        if (weight >= 0 && weight <= 100) {
            pointY = zeroLoadY - coordinateH_400 * weight;
        } else if (weight > 100 && weight <= 150) {
            pointY = zeroLoadY - (coordinateH + coordinateH_200 * (weight - 100));
        } else if (weight > 150 && weight <= 200) {
            pointY = zeroLoadY - (coordinateH * 2 + coordinateH_200 * (weight - 150));
        } else if (weight > 200 && weight <= 250) {
            pointY = zeroLoadY - (coordinateH * 3 + coordinateH_200 * (weight - 200));
        } else if (weight > 250 && weight <= 300) {
            pointY = zeroLoadY - (coordinateH * 4 + coordinateH_200 * (weight - 250));
        } else {
            pointY = 5;
        }
        
        [pointTiwen addObject:[NSValue valueWithCGPoint:CGPointMake(pointX, pointY)]];
        [tiwenArray addObject:[NSString stringWithFormat:@"%.1f", result.fat]];
        if (index == 0) {
            CGContextMoveToPoint(ctx, pointX, pointY);
        } else {
            CGContextAddLineToPoint(ctx, pointX, pointY);
        }
    }
    CGContextStrokePath(ctx);
    CGContextRestoreGState(ctx);
    CGContextSaveGState(ctx);
    // 画点的小圆圈
    for (int index = 0; index < pointTiwen.count; index++) {
        NSValue *valuePoint = pointTiwen[index];
        CGPoint pointxy = [valuePoint CGPointValue];
        [SYColor(224, 95, 154) set];
        CGContextAddArc(ctx, pointxy.x, pointxy.y, 3, 0, M_PI * 2, 0);
        
        CGContextFillPath(ctx);
    }
    // 画点上方的体重值
    NSMutableDictionary *tiwenDict = [NSMutableDictionary dictionary];
    tiwenDict[NSFontAttributeName] = [UIFont systemFontOfSize:8];
    tiwenDict[NSForegroundColorAttributeName] = SYColor(224, 95, 154);
    for (int index = 0; index < pointTiwen.count; index++) {
        NSValue *valuePoint = pointTiwen[index];
        CGPoint pointxy = [valuePoint CGPointValue];
        //                int weightInt = [weightArray[index] intValue];
        //                CGFloat weightFloat = weightInt / 10.0;
        NSString *weightStr = tiwenArray[index];//[NSString stringWithFormat:@"%.1f", weightFloat];
        [weightStr drawAtPoint:CGPointMake(pointxy.x - 7, pointxy.y - 15) withAttributes:tiwenDict];
    }
    
    CGContextRestoreGState(ctx);
    CGContextSaveGState(ctx);
}

- (void)fatLineNeiZang_week:(CGContextRef)ctx sectionW:(CGFloat)sectionWidth zeroLoad:(CGFloat)zeroLoadY coordinateH:(CGFloat)coordinateH coordinate2H:(CGFloat)coordinateH_200 coordinate4H:(CGFloat)coordinateH_400
{
    // 保存各个点坐标数组
    NSMutableArray *pointTiwen = [NSMutableArray array];
    // 保存各个点体重值的数组
    NSMutableArray *tiwenArray = [NSMutableArray array];
    
    [SYColor(224, 95, 154) set];
    for (NSInteger index = 0; index < self.weekChartData.count; index++) {
        YBBodyFatModel *result = self.weekChartData[index];
        CGFloat pointX = (result.parmaterDateCom.weekday * sectionWidth);
        //        CGFloat pointX = 80 * (index + 1);
        CGFloat pointY;
        NSInteger weight = result.viscerafat * 10;
        if (weight >= 0 && weight <= 30) {
            pointY = zeroLoadY - coordinateH_400 * weight;
        } else if (weight > 30 && weight <= 60) {
            pointY = zeroLoadY - (coordinateH + coordinateH_200 * (weight - 30));
        } else if (weight > 60 && weight <= 90) {
            pointY = zeroLoadY - (coordinateH * 2 + coordinateH_200 * (weight - 60));
        } else if (weight > 90 && weight <= 120) {
            pointY = zeroLoadY - (coordinateH * 3 + coordinateH_200 * (weight - 90));
        } else if (weight > 120 && weight <= 150) {
            pointY = zeroLoadY - (coordinateH * 4 + coordinateH_200 * (weight - 120));
        } else {
            pointY = 5;
        }
        
        [pointTiwen addObject:[NSValue valueWithCGPoint:CGPointMake(pointX, pointY)]];
        [tiwenArray addObject:[NSString stringWithFormat:@"%.1f", result.viscerafat]];
        if (index == 0) {
            CGContextMoveToPoint(ctx, pointX, pointY);
        } else {
            CGContextAddLineToPoint(ctx, pointX, pointY);
        }
    }
    CGContextStrokePath(ctx);
    CGContextRestoreGState(ctx);
    CGContextSaveGState(ctx);
    // 画点的小圆圈
    for (int index = 0; index < pointTiwen.count; index++) {
        NSValue *valuePoint = pointTiwen[index];
        CGPoint pointxy = [valuePoint CGPointValue];
        [SYColor(224, 95, 154) set];
        CGContextAddArc(ctx, pointxy.x, pointxy.y, 3, 0, M_PI * 2, 0);
        
        CGContextFillPath(ctx);
    }
    // 画点上方的体重值
    NSMutableDictionary *tiwenDict = [NSMutableDictionary dictionary];
    tiwenDict[NSFontAttributeName] = [UIFont systemFontOfSize:8];
    tiwenDict[NSForegroundColorAttributeName] = SYColor(224, 95, 154);
    for (int index = 0; index < pointTiwen.count; index++) {
        NSValue *valuePoint = pointTiwen[index];
        CGPoint pointxy = [valuePoint CGPointValue];
        //                int weightInt = [weightArray[index] intValue];
        //                CGFloat weightFloat = weightInt / 10.0;
        NSString *weightStr = tiwenArray[index];//[NSString stringWithFormat:@"%.1f", weightFloat];
        [weightStr drawAtPoint:CGPointMake(pointxy.x - 7, pointxy.y - 15) withAttributes:tiwenDict];
    }
    
    CGContextRestoreGState(ctx);
    CGContextSaveGState(ctx);
}

- (void)fatLineMuscle_week:(CGContextRef)ctx sectionW:(CGFloat)sectionWidth zeroLoad:(CGFloat)zeroLoadY coordinateH:(CGFloat)coordinateH coordinate2H:(CGFloat)coordinateH_200 coordinate4H:(CGFloat)coordinateH_400
{
    // 保存各个点坐标数组
    NSMutableArray *pointTiwen = [NSMutableArray array];
    // 保存各个点体重值的数组
    NSMutableArray *tiwenArray = [NSMutableArray array];
    
    [SYColor(224, 95, 154) set];
    for (NSInteger index = 0; index < self.weekChartData.count; index++) {
        YBBodyFatModel *result = self.weekChartData[index];
        CGFloat pointX = (result.parmaterDateCom.weekday * sectionWidth);
        //        CGFloat pointX = 80 * (index + 1);
        CGFloat pointY;
        NSInteger weight = result.muscle * 10;
        if (weight >= 0 && weight <= 10) {
            pointY = zeroLoadY - coordinateH_400 * weight;
        } else if (weight > 10 && weight <= 20) {
            pointY = zeroLoadY - (coordinateH + coordinateH_200 * (weight - 10));
        } else if (weight > 20 && weight <= 30) {
            pointY = zeroLoadY - (coordinateH * 2 + coordinateH_200 * (weight - 20));
        } else if (weight > 30 && weight <= 40) {
            pointY = zeroLoadY - (coordinateH * 3 + coordinateH_200 * (weight - 30));
        } else if (weight > 40 && weight <= 50) {
            pointY = zeroLoadY - (coordinateH * 4 + coordinateH_200 * (weight - 40));
        } else {
            pointY = 5;
        }
        
        [pointTiwen addObject:[NSValue valueWithCGPoint:CGPointMake(pointX, pointY)]];
        [tiwenArray addObject:[NSString stringWithFormat:@"%.1f", result.muscle]];
        if (index == 0) {
            CGContextMoveToPoint(ctx, pointX, pointY);
        } else {
            CGContextAddLineToPoint(ctx, pointX, pointY);
        }
    }
    CGContextStrokePath(ctx);
    CGContextRestoreGState(ctx);
    CGContextSaveGState(ctx);
    // 画点的小圆圈
    for (int index = 0; index < pointTiwen.count; index++) {
        NSValue *valuePoint = pointTiwen[index];
        CGPoint pointxy = [valuePoint CGPointValue];
        [SYColor(224, 95, 154) set];
        CGContextAddArc(ctx, pointxy.x, pointxy.y, 3, 0, M_PI * 2, 0);
        
        CGContextFillPath(ctx);
    }
    // 画点上方的体重值
    NSMutableDictionary *tiwenDict = [NSMutableDictionary dictionary];
    tiwenDict[NSFontAttributeName] = [UIFont systemFontOfSize:8];
    tiwenDict[NSForegroundColorAttributeName] = SYColor(224, 95, 154);
    for (int index = 0; index < pointTiwen.count; index++) {
        NSValue *valuePoint = pointTiwen[index];
        CGPoint pointxy = [valuePoint CGPointValue];
        //                int weightInt = [weightArray[index] intValue];
        //                CGFloat weightFloat = weightInt / 10.0;
        NSString *weightStr = tiwenArray[index];//[NSString stringWithFormat:@"%.1f", weightFloat];
        [weightStr drawAtPoint:CGPointMake(pointxy.x - 7, pointxy.y - 15) withAttributes:tiwenDict];
    }
    
    CGContextRestoreGState(ctx);
    CGContextSaveGState(ctx);
}

- (void)fatLineBone_week:(CGContextRef)ctx sectionW:(CGFloat)sectionWidth zeroLoad:(CGFloat)zeroLoadY coordinateH:(CGFloat)coordinateH coordinate2H:(CGFloat)coordinateH_200 coordinate4H:(CGFloat)coordinateH_400
{
    // 保存各个点坐标数组
    NSMutableArray *pointTiwen = [NSMutableArray array];
    // 保存各个点体重值的数组
    NSMutableArray *tiwenArray = [NSMutableArray array];
    
    [SYColor(224, 95, 154) set];
    for (NSInteger index = 0; index < self.weekChartData.count; index++) {
        YBBodyFatModel *result = self.weekChartData[index];
        CGFloat pointX = (result.parmaterDateCom.weekday * sectionWidth);
        //        CGFloat pointX = 80 * (index + 1);
        CGFloat pointY;
        NSInteger weight = result.bone * 10;
        if (weight >= 0 && weight <= 10) {
            pointY = zeroLoadY - coordinateH_400 * weight;
        } else if (weight > 10 && weight <= 20) {
            pointY = zeroLoadY - (coordinateH + coordinateH_200 * (weight - 10));
        } else if (weight > 20 && weight <= 30) {
            pointY = zeroLoadY - (coordinateH * 2 + coordinateH_200 * (weight - 20));
        } else if (weight > 30 && weight <= 40) {
            pointY = zeroLoadY - (coordinateH * 3 + coordinateH_200 * (weight - 30));
        } else if (weight > 40 && weight <= 50) {
            pointY = zeroLoadY - (coordinateH * 4 + coordinateH_200 * (weight - 40));
        } else {
            pointY = 5;
        }
        
        [pointTiwen addObject:[NSValue valueWithCGPoint:CGPointMake(pointX, pointY)]];
        [tiwenArray addObject:[NSString stringWithFormat:@"%.1f", result.bone]];
        if (index == 0) {
            CGContextMoveToPoint(ctx, pointX, pointY);
        } else {
            CGContextAddLineToPoint(ctx, pointX, pointY);
        }
    }
    CGContextStrokePath(ctx);
    CGContextRestoreGState(ctx);
    CGContextSaveGState(ctx);
    // 画点的小圆圈
    for (int index = 0; index < pointTiwen.count; index++) {
        NSValue *valuePoint = pointTiwen[index];
        CGPoint pointxy = [valuePoint CGPointValue];
        [SYColor(224, 95, 154) set];
        CGContextAddArc(ctx, pointxy.x, pointxy.y, 3, 0, M_PI * 2, 0);
        
        CGContextFillPath(ctx);
    }
    // 画点上方的体重值
    NSMutableDictionary *tiwenDict = [NSMutableDictionary dictionary];
    tiwenDict[NSFontAttributeName] = [UIFont systemFontOfSize:8];
    tiwenDict[NSForegroundColorAttributeName] = SYColor(224, 95, 154);
    for (int index = 0; index < pointTiwen.count; index++) {
        NSValue *valuePoint = pointTiwen[index];
        CGPoint pointxy = [valuePoint CGPointValue];
        //                int weightInt = [weightArray[index] intValue];
        //                CGFloat weightFloat = weightInt / 10.0;
        NSString *weightStr = tiwenArray[index];//[NSString stringWithFormat:@"%.1f", weightFloat];
        [weightStr drawAtPoint:CGPointMake(pointxy.x - 7, pointxy.y - 15) withAttributes:tiwenDict];
    }
    
    CGContextRestoreGState(ctx);
    CGContextSaveGState(ctx);
}

- (void)fatLineWater_week:(CGContextRef)ctx sectionW:(CGFloat)sectionWidth zeroLoad:(CGFloat)zeroLoadY coordinateH:(CGFloat)coordinateH coordinate2H:(CGFloat)coordinateH_200 coordinate4H:(CGFloat)coordinateH_400
{
    // 保存各个点坐标数组
    NSMutableArray *pointTiwen = [NSMutableArray array];
    // 保存各个点体重值的数组
    NSMutableArray *tiwenArray = [NSMutableArray array];
    
    [SYColor(224, 95, 154) set];
    for (NSInteger index = 0; index < self.weekChartData.count; index++) {
        YBBodyFatModel *result = self.weekChartData[index];
        CGFloat pointX = (result.parmaterDateCom.weekday * sectionWidth);
        //        CGFloat pointX = 80 * (index + 1);
        CGFloat pointY;
        NSInteger tiwenValueCom = result.water * 10;
        if (tiwenValueCom >= 0 && tiwenValueCom <= 400) {
            pointY = zeroLoadY - coordinateH_400 * tiwenValueCom;
        } else if (tiwenValueCom > 400 && tiwenValueCom <= 500) {
            pointY = zeroLoadY - (coordinateH + coordinateH_200 * (tiwenValueCom - 400));
        } else if (tiwenValueCom > 500 && tiwenValueCom <= 600) {
            pointY = zeroLoadY - (coordinateH * 2 + coordinateH_200 * (tiwenValueCom - 500));
        } else if (tiwenValueCom > 600 && tiwenValueCom <= 700) {
            pointY = zeroLoadY - (coordinateH * 3 + coordinateH_200 * (tiwenValueCom - 600));
        } else if (tiwenValueCom > 700 && tiwenValueCom <= 800) {
            pointY = zeroLoadY - (coordinateH * 4 + coordinateH_200 * (tiwenValueCom - 700));
        } else if (tiwenValueCom > 800 && tiwenValueCom <= 900) {
            pointY = zeroLoadY - (coordinateH * 5 + coordinateH_200 * (tiwenValueCom - 800));
        } else if (tiwenValueCom > 900 && tiwenValueCom <= 1000) {
            pointY = zeroLoadY - (coordinateH * 6 + coordinateH_200 * (tiwenValueCom - 900));
        }
            else if (tiwenValueCom > 1000 && tiwenValueCom <= 1100) {
            pointY = zeroLoadY - (coordinateH * 7 + coordinateH_200 * (tiwenValueCom - 1000));
        }
        else {
            pointY = 5;
        }
        
        [pointTiwen addObject:[NSValue valueWithCGPoint:CGPointMake(pointX, pointY)]];
        [tiwenArray addObject:[NSString stringWithFormat:@"%.1f", result.water]];
        if (index == 0) {
            CGContextMoveToPoint(ctx, pointX, pointY);
        } else {
            CGContextAddLineToPoint(ctx, pointX, pointY);
        }
    }
    CGContextStrokePath(ctx);
    CGContextRestoreGState(ctx);
    CGContextSaveGState(ctx);
    // 画点的小圆圈
    for (int index = 0; index < pointTiwen.count; index++) {
        NSValue *valuePoint = pointTiwen[index];
        CGPoint pointxy = [valuePoint CGPointValue];
        [SYColor(224, 95, 154) set];
        CGContextAddArc(ctx, pointxy.x, pointxy.y, 3, 0, M_PI * 2, 0);
        
        CGContextFillPath(ctx);
    }
    // 画点上方的体重值
    NSMutableDictionary *tiwenDict = [NSMutableDictionary dictionary];
    tiwenDict[NSFontAttributeName] = [UIFont systemFontOfSize:8];
    tiwenDict[NSForegroundColorAttributeName] = SYColor(224, 95, 154);
    for (int index = 0; index < pointTiwen.count; index++) {
        NSValue *valuePoint = pointTiwen[index];
        CGPoint pointxy = [valuePoint CGPointValue];
        //                int weightInt = [weightArray[index] intValue];
        //                CGFloat weightFloat = weightInt / 10.0;
        NSString *weightStr = tiwenArray[index];//[NSString stringWithFormat:@"%.1f", weightFloat];
        [weightStr drawAtPoint:CGPointMake(pointxy.x - 7, pointxy.y - 15) withAttributes:tiwenDict];
    }
    
    CGContextRestoreGState(ctx);
    CGContextSaveGState(ctx);
}


- (void)fatLineNengLiang_week:(CGContextRef)ctx sectionW:(CGFloat)sectionWidth zeroLoad:(CGFloat)zeroLoadY coordinateH:(CGFloat)coordinateH coordinate2H:(CGFloat)coordinateH_200 coordinate4H:(CGFloat)coordinateH_400
{
    // 保存各个点坐标数组
    NSMutableArray *pointTiwen = [NSMutableArray array];
    // 保存各个点体重值的数组
    NSMutableArray *tiwenArray = [NSMutableArray array];
    
    [SYColor(224, 95, 154) set];
    for (NSInteger index = 0; index < self.weekChartData.count; index++) {
        YBBodyFatModel *result = self.weekChartData[index];
        CGFloat pointX = (result.parmaterDateCom.weekday * sectionWidth);
        //        CGFloat pointX = 80 * (index + 1);
        CGFloat pointY;
        NSInteger tiwenValueCom = result.kcal / 10 ;
        if (tiwenValueCom >= 0 && tiwenValueCom <= 100) {
            pointY = zeroLoadY - coordinateH_400 * tiwenValueCom;
        } else if (tiwenValueCom > 100 && tiwenValueCom <= 150) {
            pointY = zeroLoadY - (coordinateH + coordinateH_200 * (tiwenValueCom - 100));
        } else if (tiwenValueCom > 150 && tiwenValueCom <= 200) {
            pointY = zeroLoadY - (coordinateH * 2 + coordinateH_200 * (tiwenValueCom - 150));
        } else if (tiwenValueCom > 200 && tiwenValueCom <= 250) {
            pointY = zeroLoadY - (coordinateH * 3 + coordinateH_200 * (tiwenValueCom - 200));
        } else if (tiwenValueCom > 250 && tiwenValueCom <= 300) {
            pointY = zeroLoadY - (coordinateH * 4 + coordinateH_200 * (tiwenValueCom - 250));
        } else if (tiwenValueCom > 300 && tiwenValueCom <= 350) {
            pointY = zeroLoadY - (coordinateH * 5 + coordinateH_200 * (tiwenValueCom - 300));
        } else if (tiwenValueCom > 350 && tiwenValueCom <= 400) {
            pointY = zeroLoadY - (coordinateH * 6 + coordinateH_200 * (tiwenValueCom - 350));
        }else if (tiwenValueCom > 400 && tiwenValueCom <= 450) {
            pointY = zeroLoadY - (coordinateH * 7 + coordinateH_200 * (tiwenValueCom - 400));
        }
        else {
            pointY = 5;
        }
        [pointTiwen addObject:[NSValue valueWithCGPoint:CGPointMake(pointX, pointY)]];
        [tiwenArray addObject:[NSString stringWithFormat:@"%ld", (long)result.kcal]];
        if (index == 0) {
            CGContextMoveToPoint(ctx, pointX, pointY);
        } else {
            CGContextAddLineToPoint(ctx, pointX, pointY);
        }
    }
    CGContextStrokePath(ctx);
    CGContextRestoreGState(ctx);
    CGContextSaveGState(ctx);
    // 画点的小圆圈
    for (int index = 0; index < pointTiwen.count; index++) {
        NSValue *valuePoint = pointTiwen[index];
        CGPoint pointxy = [valuePoint CGPointValue];
        [SYColor(224, 95, 154) set];
        CGContextAddArc(ctx, pointxy.x, pointxy.y, 3, 0, M_PI * 2, 0);
        
        CGContextFillPath(ctx);
    }
    // 画点上方的体重值
    NSMutableDictionary *tiwenDict = [NSMutableDictionary dictionary];
    tiwenDict[NSFontAttributeName] = [UIFont systemFontOfSize:8];
    tiwenDict[NSForegroundColorAttributeName] = SYColor(224, 95, 154);
    for (int index = 0; index < pointTiwen.count; index++) {
        NSValue *valuePoint = pointTiwen[index];
        CGPoint pointxy = [valuePoint CGPointValue];
        //                int weightInt = [weightArray[index] intValue];
        //                CGFloat weightFloat = weightInt / 10.0;
        NSString *weightStr = tiwenArray[index];//[NSString stringWithFormat:@"%.1f", weightFloat];
        [weightStr drawAtPoint:CGPointMake(pointxy.x - 7, pointxy.y - 15) withAttributes:tiwenDict];
    }
    
    CGContextRestoreGState(ctx);
    CGContextSaveGState(ctx);
}

#pragma -mark 月图
- (void)fatLineBMI_Month:(CGContextRef)ctx sectionW:(CGFloat)sectionWidth zeroLoad:(CGFloat)zeroLoadY coordinateH:(CGFloat)coordinateH coordinate2H:(CGFloat)coordinateH_200 coordinate4H:(CGFloat)coordinateH_400
{
    // 保存各个点坐标数组
    NSMutableArray *pointTiwen = [NSMutableArray array];
    // 保存各个点体重值的数组
    NSMutableArray *tiwenArray = [NSMutableArray array];
    
    [SYColor(224, 95, 154) set];
    for (NSInteger index = 0; index < self.monthChartData.count; index++) {
        YBBodyFatModel *result = self.monthChartData[index];
        CGFloat pointX = (result.parmaterDateCom.day * sectionWidth);
        //        CGFloat pointX = 80 * (index + 1);
        CGFloat pointY;
        NSInteger weight = result.bmi * 10;
        if (weight > 0 && weight <= 200) {
            pointY = zeroLoadY - coordinateH_400 * weight;
        } else if (weight > 200 && weight <= 250) {
            pointY = zeroLoadY - (coordinateH + coordinateH_200 * (weight - 200));
        } else if (weight > 250 && weight <= 300) {
            pointY = zeroLoadY - (coordinateH * 2 + coordinateH_200 * (weight - 250));
        } else if (weight > 300 && weight <= 350) {
            pointY = zeroLoadY - (coordinateH * 3 + coordinateH_200 * (weight - 300));
        } else if (weight > 350 && weight <= 400) {
            pointY = zeroLoadY - (coordinateH * 4 + coordinateH_200 * (weight - 350));
        } else {
            pointY = 5;
        }
        
        [pointTiwen addObject:[NSValue valueWithCGPoint:CGPointMake(pointX, pointY)]];
        [tiwenArray addObject:[NSString stringWithFormat:@"%.1f", result.bmi]];
        if (index == 0) {
            CGContextMoveToPoint(ctx, pointX, pointY);
        } else {
            CGContextAddLineToPoint(ctx, pointX, pointY);
        }
    }
    CGContextStrokePath(ctx);
    CGContextRestoreGState(ctx);
    CGContextSaveGState(ctx);
    // 画点的小圆圈
    for (int index = 0; index < pointTiwen.count; index++) {
        NSValue *valuePoint = pointTiwen[index];
        CGPoint pointxy = [valuePoint CGPointValue];
        [SYColor(224, 95, 154) set];
        CGContextAddArc(ctx, pointxy.x, pointxy.y, 3, 0, M_PI * 2, 0);
        
        CGContextFillPath(ctx);
    }
    // 画点上方的体重值
    NSMutableDictionary *tiwenDict = [NSMutableDictionary dictionary];
    tiwenDict[NSFontAttributeName] = [UIFont systemFontOfSize:8];
    tiwenDict[NSForegroundColorAttributeName] = SYColor(224, 95, 154);
    for (int index = 0; index < pointTiwen.count; index++) {
        NSValue *valuePoint = pointTiwen[index];
        CGPoint pointxy = [valuePoint CGPointValue];
        //                int weightInt = [weightArray[index] intValue];
        //                CGFloat weightFloat = weightInt / 10.0;
        NSString *weightStr = tiwenArray[index];//[NSString stringWithFormat:@"%.1f", weightFloat];
        [weightStr drawAtPoint:CGPointMake(pointxy.x - 7, pointxy.y - 15) withAttributes:tiwenDict];
    }
    
    CGContextRestoreGState(ctx);
    CGContextSaveGState(ctx);
}

- (void)fatLineFat_Month:(CGContextRef)ctx sectionW:(CGFloat)sectionWidth zeroLoad:(CGFloat)zeroLoadY coordinateH:(CGFloat)coordinateH coordinate2H:(CGFloat)coordinateH_200 coordinate4H:(CGFloat)coordinateH_400
{
    // 保存各个点坐标数组
    NSMutableArray *pointTiwen = [NSMutableArray array];
    // 保存各个点体重值的数组
    NSMutableArray *tiwenArray = [NSMutableArray array];
    
    [SYColor(224, 95, 154) set];
    for (NSInteger index = 0; index < self.monthChartData.count; index++) {
        YBBodyFatModel *result = self.monthChartData[index];
        CGFloat pointX = (result.parmaterDateCom.day * sectionWidth);
        //        CGFloat pointX = 80 * (index + 1);
        CGFloat pointY;
        NSInteger weight = result.fat * 10;
        if (weight >= 0 && weight <= 100) {
            pointY = zeroLoadY - coordinateH_400 * weight;
        } else if (weight > 100 && weight <= 150) {
            pointY = zeroLoadY - (coordinateH + coordinateH_200 * (weight - 100));
        } else if (weight > 150 && weight <= 200) {
            pointY = zeroLoadY - (coordinateH * 2 + coordinateH_200 * (weight - 150));
        } else if (weight > 200 && weight <= 250) {
            pointY = zeroLoadY - (coordinateH * 3 + coordinateH_200 * (weight - 200));
        } else if (weight > 250 && weight <= 300) {
            pointY = zeroLoadY - (coordinateH * 4 + coordinateH_200 * (weight - 250));
        } else {
            pointY = 5;
        }
        
        [pointTiwen addObject:[NSValue valueWithCGPoint:CGPointMake(pointX, pointY)]];
        [tiwenArray addObject:[NSString stringWithFormat:@"%.1f", result.fat]];
        if (index == 0) {
            CGContextMoveToPoint(ctx, pointX, pointY);
        } else {
            CGContextAddLineToPoint(ctx, pointX, pointY);
        }
    }
    CGContextStrokePath(ctx);
    CGContextRestoreGState(ctx);
    CGContextSaveGState(ctx);
    // 画点的小圆圈
    for (int index = 0; index < pointTiwen.count; index++) {
        NSValue *valuePoint = pointTiwen[index];
        CGPoint pointxy = [valuePoint CGPointValue];
        [SYColor(224, 95, 154) set];
        CGContextAddArc(ctx, pointxy.x, pointxy.y, 3, 0, M_PI * 2, 0);
        
        CGContextFillPath(ctx);
    }
    // 画点上方的体重值
    NSMutableDictionary *tiwenDict = [NSMutableDictionary dictionary];
    tiwenDict[NSFontAttributeName] = [UIFont systemFontOfSize:8];
    tiwenDict[NSForegroundColorAttributeName] = SYColor(224, 95, 154);
    for (int index = 0; index < pointTiwen.count; index++) {
        NSValue *valuePoint = pointTiwen[index];
        CGPoint pointxy = [valuePoint CGPointValue];
        //                int weightInt = [weightArray[index] intValue];
        //                CGFloat weightFloat = weightInt / 10.0;
        NSString *weightStr = tiwenArray[index];//[NSString stringWithFormat:@"%.1f", weightFloat];
        [weightStr drawAtPoint:CGPointMake(pointxy.x - 7, pointxy.y - 15) withAttributes:tiwenDict];
    }
    
    CGContextRestoreGState(ctx);
    CGContextSaveGState(ctx);
}

- (void)fatLineNeiZang_Month:(CGContextRef)ctx sectionW:(CGFloat)sectionWidth zeroLoad:(CGFloat)zeroLoadY coordinateH:(CGFloat)coordinateH coordinate2H:(CGFloat)coordinateH_200 coordinate4H:(CGFloat)coordinateH_400
{
    // 保存各个点坐标数组
    NSMutableArray *pointTiwen = [NSMutableArray array];
    // 保存各个点体重值的数组
    NSMutableArray *tiwenArray = [NSMutableArray array];
    
    [SYColor(224, 95, 154) set];
    for (NSInteger index = 0; index < self.monthChartData.count; index++) {
        YBBodyFatModel *result = self.monthChartData[index];
        CGFloat pointX = (result.parmaterDateCom.day * sectionWidth);
        //        CGFloat pointX = 80 * (index + 1);
        CGFloat pointY;
        NSInteger weight = result.viscerafat * 10;
        if (weight >= 0 && weight <= 30) {
            pointY = zeroLoadY - coordinateH_400 * weight;
        } else if (weight > 30 && weight <= 60) {
            pointY = zeroLoadY - (coordinateH + coordinateH_200 * (weight - 30));
        } else if (weight > 60 && weight <= 90) {
            pointY = zeroLoadY - (coordinateH * 2 + coordinateH_200 * (weight - 60));
        } else if (weight > 90 && weight <= 120) {
            pointY = zeroLoadY - (coordinateH * 3 + coordinateH_200 * (weight - 90));
        } else if (weight > 120 && weight <= 150) {
            pointY = zeroLoadY - (coordinateH * 4 + coordinateH_200 * (weight - 120));
        } else {
            pointY = 5;
        }
        
        [pointTiwen addObject:[NSValue valueWithCGPoint:CGPointMake(pointX, pointY)]];
        [tiwenArray addObject:[NSString stringWithFormat:@"%.1f", result.viscerafat]];
        if (index == 0) {
            CGContextMoveToPoint(ctx, pointX, pointY);
        } else {
            CGContextAddLineToPoint(ctx, pointX, pointY);
        }
    }
    CGContextStrokePath(ctx);
    CGContextRestoreGState(ctx);
    CGContextSaveGState(ctx);
    // 画点的小圆圈
    for (int index = 0; index < pointTiwen.count; index++) {
        NSValue *valuePoint = pointTiwen[index];
        CGPoint pointxy = [valuePoint CGPointValue];
        [SYColor(224, 95, 154) set];
        CGContextAddArc(ctx, pointxy.x, pointxy.y, 3, 0, M_PI * 2, 0);
        
        CGContextFillPath(ctx);
    }
    // 画点上方的体重值
    NSMutableDictionary *tiwenDict = [NSMutableDictionary dictionary];
    tiwenDict[NSFontAttributeName] = [UIFont systemFontOfSize:8];
    tiwenDict[NSForegroundColorAttributeName] = SYColor(224, 95, 154);
    for (int index = 0; index < pointTiwen.count; index++) {
        NSValue *valuePoint = pointTiwen[index];
        CGPoint pointxy = [valuePoint CGPointValue];
        //                int weightInt = [weightArray[index] intValue];
        //                CGFloat weightFloat = weightInt / 10.0;
        NSString *weightStr = tiwenArray[index];//[NSString stringWithFormat:@"%.1f", weightFloat];
        [weightStr drawAtPoint:CGPointMake(pointxy.x - 7, pointxy.y - 15) withAttributes:tiwenDict];
    }
    
    CGContextRestoreGState(ctx);
    CGContextSaveGState(ctx);
}

- (void)fatLineMuscle_Month:(CGContextRef)ctx sectionW:(CGFloat)sectionWidth zeroLoad:(CGFloat)zeroLoadY coordinateH:(CGFloat)coordinateH coordinate2H:(CGFloat)coordinateH_200 coordinate4H:(CGFloat)coordinateH_400
{
    // 保存各个点坐标数组
    NSMutableArray *pointTiwen = [NSMutableArray array];
    // 保存各个点体重值的数组
    NSMutableArray *tiwenArray = [NSMutableArray array];
    
    [SYColor(224, 95, 154) set];
    for (NSInteger index = 0; index < self.monthChartData.count; index++) {
        YBBodyFatModel *result = self.monthChartData[index];
        CGFloat pointX = (result.parmaterDateCom.day * sectionWidth);
        //        CGFloat pointX = 80 * (index + 1);
        CGFloat pointY;
        NSInteger weight = result.muscle * 10;
        if (weight >= 0 && weight <= 10) {
            pointY = zeroLoadY - coordinateH_400 * weight;
        } else if (weight > 10 && weight <= 20) {
            pointY = zeroLoadY - (coordinateH + coordinateH_200 * (weight - 10));
        } else if (weight > 20 && weight <= 30) {
            pointY = zeroLoadY - (coordinateH * 2 + coordinateH_200 * (weight - 20));
        } else if (weight > 30 && weight <= 40) {
            pointY = zeroLoadY - (coordinateH * 3 + coordinateH_200 * (weight - 30));
        } else if (weight > 40 && weight <= 50) {
            pointY = zeroLoadY - (coordinateH * 4 + coordinateH_200 * (weight - 40));
        } else {
            pointY = 5;
        }
        
        [pointTiwen addObject:[NSValue valueWithCGPoint:CGPointMake(pointX, pointY)]];
        [tiwenArray addObject:[NSString stringWithFormat:@"%.1f", result.muscle]];
        if (index == 0) {
            CGContextMoveToPoint(ctx, pointX, pointY);
        } else {
            CGContextAddLineToPoint(ctx, pointX, pointY);
        }
    }
    CGContextStrokePath(ctx);
    CGContextRestoreGState(ctx);
    CGContextSaveGState(ctx);
    // 画点的小圆圈
    for (int index = 0; index < pointTiwen.count; index++) {
        NSValue *valuePoint = pointTiwen[index];
        CGPoint pointxy = [valuePoint CGPointValue];
        [SYColor(224, 95, 154) set];
        CGContextAddArc(ctx, pointxy.x, pointxy.y, 3, 0, M_PI * 2, 0);
        
        CGContextFillPath(ctx);
    }
    // 画点上方的体重值
    NSMutableDictionary *tiwenDict = [NSMutableDictionary dictionary];
    tiwenDict[NSFontAttributeName] = [UIFont systemFontOfSize:8];
    tiwenDict[NSForegroundColorAttributeName] = SYColor(224, 95, 154);
    for (int index = 0; index < pointTiwen.count; index++) {
        NSValue *valuePoint = pointTiwen[index];
        CGPoint pointxy = [valuePoint CGPointValue];
        //                int weightInt = [weightArray[index] intValue];
        //                CGFloat weightFloat = weightInt / 10.0;
        NSString *weightStr = tiwenArray[index];//[NSString stringWithFormat:@"%.1f", weightFloat];
        [weightStr drawAtPoint:CGPointMake(pointxy.x - 7, pointxy.y - 15) withAttributes:tiwenDict];
    }
    
    CGContextRestoreGState(ctx);
    CGContextSaveGState(ctx);
}

- (void)fatLineBone_Month:(CGContextRef)ctx sectionW:(CGFloat)sectionWidth zeroLoad:(CGFloat)zeroLoadY coordinateH:(CGFloat)coordinateH coordinate2H:(CGFloat)coordinateH_200 coordinate4H:(CGFloat)coordinateH_400
{
    // 保存各个点坐标数组
    NSMutableArray *pointTiwen = [NSMutableArray array];
    // 保存各个点体重值的数组
    NSMutableArray *tiwenArray = [NSMutableArray array];
    
    [SYColor(224, 95, 154) set];
    for (NSInteger index = 0; index < self.monthChartData.count; index++) {
        YBBodyFatModel *result = self.monthChartData[index];
        CGFloat pointX = (result.parmaterDateCom.day * sectionWidth);
        //        CGFloat pointX = 80 * (index + 1);
        CGFloat pointY;
        NSInteger weight = result.bone * 10;
        if (weight >= 0 && weight <= 10) {
            pointY = zeroLoadY - coordinateH_400 * weight;
        } else if (weight > 10 && weight <= 20) {
            pointY = zeroLoadY - (coordinateH + coordinateH_200 * (weight - 10));
        } else if (weight > 20 && weight <= 30) {
            pointY = zeroLoadY - (coordinateH * 2 + coordinateH_200 * (weight - 20));
        } else if (weight > 30 && weight <= 40) {
            pointY = zeroLoadY - (coordinateH * 3 + coordinateH_200 * (weight - 30));
        } else if (weight > 40 && weight <= 50) {
            pointY = zeroLoadY - (coordinateH * 4 + coordinateH_200 * (weight - 40));
        } else {
            pointY = 5;
        }
        
        [pointTiwen addObject:[NSValue valueWithCGPoint:CGPointMake(pointX, pointY)]];
        [tiwenArray addObject:[NSString stringWithFormat:@"%.1f", result.bone]];
        if (index == 0) {
            CGContextMoveToPoint(ctx, pointX, pointY);
        } else {
            CGContextAddLineToPoint(ctx, pointX, pointY);
        }
    }
    CGContextStrokePath(ctx);
    CGContextRestoreGState(ctx);
    CGContextSaveGState(ctx);
    // 画点的小圆圈
    for (int index = 0; index < pointTiwen.count; index++) {
        NSValue *valuePoint = pointTiwen[index];
        CGPoint pointxy = [valuePoint CGPointValue];
        [SYColor(224, 95, 154) set];
        CGContextAddArc(ctx, pointxy.x, pointxy.y, 3, 0, M_PI * 2, 0);
        
        CGContextFillPath(ctx);
    }
    // 画点上方的体重值
    NSMutableDictionary *tiwenDict = [NSMutableDictionary dictionary];
    tiwenDict[NSFontAttributeName] = [UIFont systemFontOfSize:8];
    tiwenDict[NSForegroundColorAttributeName] = SYColor(224, 95, 154);
    for (int index = 0; index < pointTiwen.count; index++) {
        NSValue *valuePoint = pointTiwen[index];
        CGPoint pointxy = [valuePoint CGPointValue];
        //                int weightInt = [weightArray[index] intValue];
        //                CGFloat weightFloat = weightInt / 10.0;
        NSString *weightStr = tiwenArray[index];//[NSString stringWithFormat:@"%.1f", weightFloat];
        [weightStr drawAtPoint:CGPointMake(pointxy.x - 7, pointxy.y - 15) withAttributes:tiwenDict];
    }
    
    CGContextRestoreGState(ctx);
    CGContextSaveGState(ctx);
}

- (void)fatLineWater_Month:(CGContextRef)ctx sectionW:(CGFloat)sectionWidth zeroLoad:(CGFloat)zeroLoadY coordinateH:(CGFloat)coordinateH coordinate2H:(CGFloat)coordinateH_200 coordinate4H:(CGFloat)coordinateH_400
{
    // 保存各个点坐标数组
    NSMutableArray *pointTiwen = [NSMutableArray array];
    // 保存各个点体重值的数组
    NSMutableArray *tiwenArray = [NSMutableArray array];
    
    [SYColor(224, 95, 154) set];
    for (NSInteger index = 0; index < self.monthChartData.count; index++) {
        YBBodyFatModel *result = self.monthChartData[index];
        CGFloat pointX = (result.parmaterDateCom.day * sectionWidth);
        //        CGFloat pointX = 80 * (index + 1);
        CGFloat pointY;
        NSInteger tiwenValueCom = result.water * 10;
        if (tiwenValueCom >= 0 && tiwenValueCom <= 400) {
            pointY = zeroLoadY - coordinateH_400 * tiwenValueCom;
        } else if (tiwenValueCom > 400 && tiwenValueCom <= 500) {
            pointY = zeroLoadY - (coordinateH + coordinateH_200 * (tiwenValueCom - 400));
        } else if (tiwenValueCom > 500 && tiwenValueCom <= 600) {
            pointY = zeroLoadY - (coordinateH * 2 + coordinateH_200 * (tiwenValueCom - 500));
        } else if (tiwenValueCom > 600 && tiwenValueCom <= 700) {
            pointY = zeroLoadY - (coordinateH * 3 + coordinateH_200 * (tiwenValueCom - 600));
        } else if (tiwenValueCom > 700 && tiwenValueCom <= 800) {
            pointY = zeroLoadY - (coordinateH * 4 + coordinateH_200 * (tiwenValueCom - 700));
        } else if (tiwenValueCom > 800 && tiwenValueCom <= 900) {
            pointY = zeroLoadY - (coordinateH * 5 + coordinateH_200 * (tiwenValueCom - 800));
        } else if (tiwenValueCom > 900 && tiwenValueCom <= 1000) {
            pointY = zeroLoadY - (coordinateH * 6 + coordinateH_200 * (tiwenValueCom - 900));
        }else if (tiwenValueCom > 1000 && tiwenValueCom <= 1200) {
            pointY = zeroLoadY - (coordinateH * 7 + coordinateH_200 * (tiwenValueCom - 1000));
        }else {
            pointY = 5;
        }
        
        [pointTiwen addObject:[NSValue valueWithCGPoint:CGPointMake(pointX, pointY)]];
        [tiwenArray addObject:[NSString stringWithFormat:@"%.1f", result.water]];
        if (index == 0) {
            CGContextMoveToPoint(ctx, pointX, pointY);
        } else {
            CGContextAddLineToPoint(ctx, pointX, pointY);
        }
    }
    CGContextStrokePath(ctx);
    CGContextRestoreGState(ctx);
    CGContextSaveGState(ctx);
    // 画点的小圆圈
    for (int index = 0; index < pointTiwen.count; index++) {
        NSValue *valuePoint = pointTiwen[index];
        CGPoint pointxy = [valuePoint CGPointValue];
        [SYColor(224, 95, 154) set];
        CGContextAddArc(ctx, pointxy.x, pointxy.y, 3, 0, M_PI * 2, 0);
        
        CGContextFillPath(ctx);
    }
    // 画点上方的体重值
    NSMutableDictionary *tiwenDict = [NSMutableDictionary dictionary];
    tiwenDict[NSFontAttributeName] = [UIFont systemFontOfSize:8];
    tiwenDict[NSForegroundColorAttributeName] = SYColor(224, 95, 154);
    for (int index = 0; index < pointTiwen.count; index++) {
        NSValue *valuePoint = pointTiwen[index];
        CGPoint pointxy = [valuePoint CGPointValue];
        //                int weightInt = [weightArray[index] intValue];
        //                CGFloat weightFloat = weightInt / 10.0;
        NSString *weightStr = tiwenArray[index];//[NSString stringWithFormat:@"%.1f", weightFloat];
        [weightStr drawAtPoint:CGPointMake(pointxy.x - 7, pointxy.y - 15) withAttributes:tiwenDict];
    }
    
    CGContextRestoreGState(ctx);
    CGContextSaveGState(ctx);
}


- (void)fatLineNengLiang_Month:(CGContextRef)ctx sectionW:(CGFloat)sectionWidth zeroLoad:(CGFloat)zeroLoadY coordinateH:(CGFloat)coordinateH coordinate2H:(CGFloat)coordinateH_200 coordinate4H:(CGFloat)coordinateH_400
{
    // 保存各个点坐标数组
    NSMutableArray *pointTiwen = [NSMutableArray array];
    // 保存各个点体重值的数组
    NSMutableArray *tiwenArray = [NSMutableArray array];
    
    [SYColor(224, 95, 154) set];
    for (NSInteger index = 0; index < self.monthChartData.count; index++) {
        YBBodyFatModel *result = self.monthChartData[index];
        CGFloat pointX = (result.parmaterDateCom.day * sectionWidth);
        //        CGFloat pointX = 80 * (index + 1);
        CGFloat pointY;
        NSInteger tiwenValueCom = result.kcal / 10 ;
        if (tiwenValueCom >= 0 && tiwenValueCom <= 100) {
            pointY = zeroLoadY - coordinateH_400 * tiwenValueCom;
        } else if (tiwenValueCom > 100 && tiwenValueCom <= 150) {
            pointY = zeroLoadY - (coordinateH + coordinateH_200 * (tiwenValueCom - 100));
        } else if (tiwenValueCom > 150 && tiwenValueCom <= 200) {
            pointY = zeroLoadY - (coordinateH * 2 + coordinateH_200 * (tiwenValueCom - 150));
        } else if (tiwenValueCom > 200 && tiwenValueCom <= 250) {
            pointY = zeroLoadY - (coordinateH * 3 + coordinateH_200 * (tiwenValueCom - 200));
        } else if (tiwenValueCom > 250 && tiwenValueCom <= 300) {
            pointY = zeroLoadY - (coordinateH * 4 + coordinateH_200 * (tiwenValueCom - 250));
        } else if (tiwenValueCom > 300 && tiwenValueCom <= 350) {
            pointY = zeroLoadY - (coordinateH * 5 + coordinateH_200 * (tiwenValueCom - 300));
        } else if (tiwenValueCom > 350 && tiwenValueCom <= 400) {
            pointY = zeroLoadY - (coordinateH * 6 + coordinateH_200 * (tiwenValueCom - 350));
        }else if (tiwenValueCom > 400 && tiwenValueCom <= 450) {
            pointY = zeroLoadY - (coordinateH * 7 + coordinateH_200 * (tiwenValueCom - 400));
        }
        else {
            pointY = 5;
        }
        [pointTiwen addObject:[NSValue valueWithCGPoint:CGPointMake(pointX, pointY)]];
        [tiwenArray addObject:[NSString stringWithFormat:@"%ld", (long)result.kcal]];
        if (index == 0) {
            CGContextMoveToPoint(ctx, pointX, pointY);
        } else {
            CGContextAddLineToPoint(ctx, pointX, pointY);
        }
    }
    CGContextStrokePath(ctx);
    CGContextRestoreGState(ctx);
    CGContextSaveGState(ctx);
    // 画点的小圆圈
    for (int index = 0; index < pointTiwen.count; index++) {
        NSValue *valuePoint = pointTiwen[index];
        CGPoint pointxy = [valuePoint CGPointValue];
        [SYColor(224, 95, 154) set];
        CGContextAddArc(ctx, pointxy.x, pointxy.y, 3, 0, M_PI * 2, 0);
        
        CGContextFillPath(ctx);
    }
    // 画点上方的体重值
    NSMutableDictionary *tiwenDict = [NSMutableDictionary dictionary];
    tiwenDict[NSFontAttributeName] = [UIFont systemFontOfSize:8];
    tiwenDict[NSForegroundColorAttributeName] = SYColor(224, 95, 154);
    for (int index = 0; index < pointTiwen.count; index++) {
        NSValue *valuePoint = pointTiwen[index];
        CGPoint pointxy = [valuePoint CGPointValue];
        //                int weightInt = [weightArray[index] intValue];
        //                CGFloat weightFloat = weightInt / 10.0;
        NSString *weightStr = tiwenArray[index];//[NSString stringWithFormat:@"%.1f", weightFloat];
        [weightStr drawAtPoint:CGPointMake(pointxy.x - 7, pointxy.y - 15) withAttributes:tiwenDict];
    }
    
    CGContextRestoreGState(ctx);
    CGContextSaveGState(ctx);
}

@end
