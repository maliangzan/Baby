//
//  YBSelectPickerWindow.h
//  孕妙
//
//  Created by Raoy on 16/4/12.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface YBSelectPickerWindow : UIWindow

@property (strong, nonatomic)UIButton      *sureBtn;
@property (strong, nonatomic)UIButton      *cancelBtn;
@property (copy, nonatomic)UIPickerView  *selectPickerView;
@property (strong, nonatomic)UILabel *dianL;

- (void)setInterFace:(NSString *)title;
@end
