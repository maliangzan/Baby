//
//  YBParmaterListView.m
//  孕妙
//
//  Created by sayes1 on 16/1/22.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "YBParmaterListView.h"
#import "YBParmaterListCell.h"

@interface YBParmaterListView () <UITableViewDataSource>
/**
 
 */
@property (nonatomic, strong) UILabel *titleL;
/**
 
 */
@property (nonatomic, strong) UILabel *pamaterTextL;
/**
 
 */
@property (nonatomic, strong) UITableView *pamaterTableView;
/**
 * 参数的名字
 */
@property (nonatomic, copy) NSString *parmaterName;
@end

@implementation YBParmaterListView

- (void)setPamaterArray:(NSArray *)pamaterArray
{
    if (_pamaterArray == nil) {
        _pamaterArray = [NSArray array];
    }
    _pamaterArray = [[pamaterArray reverseObjectEnumerator] allObjects];
    
    [self.pamaterTableView reloadData];
}

+ (instancetype)parmaterListViewWithframe:(CGRect)frame titleStr:(NSString *)titleStr parmaterName:(NSString *)parmaterName
{
    YBParmaterListView *parmaterListView = [[YBParmaterListView alloc] initWithFrame:frame];
    parmaterListView.titleL.text = titleStr;
//    parmaterListView.titleL.font = [UIFont systemFontOfSize:16];
    parmaterListView.parmaterName = parmaterName;
    parmaterListView.pamaterTextL.text = parmaterName;
    return parmaterListView;
}

+ (instancetype)parmaterListViewWithframe:(CGRect)frame titleStr:(NSString *)titleStr parmaterName:(NSString *)parmaterName index:(NSInteger)index
{
    YBParmaterListView *parmaterListView = [[YBParmaterListView alloc] initWithFrame:frame];
    parmaterListView.titleL.text = titleStr;
    //    parmaterListView.titleL.font = [UIFont systemFontOfSize:16];
    parmaterListView.parmaterName = parmaterName;
    parmaterListView.indexF = index;
    parmaterListView.pamaterTextL.text = parmaterName;
    return parmaterListView;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
//        self.backgroundColor = [UIColor whiteColor];
        [self setupSubview];
    }
    return self;
}

- (void)setupSubview
{
    self.titleL = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, self.frame.size.height * 0.1813)];
    self.titleL.textAlignment = NSTextAlignmentCenter;
    self.titleL.textColor = SYColor(106, 66, 102);
    self.titleL.backgroundColor = SYColor(252, 175, 212);
    [self addSubview:self.titleL];
    
    UIView *tableViewTitleView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.titleL.frame), DeviceWidth, self.frame.size.height * 0.1813)];
    tableViewTitleView.backgroundColor = [UIColor whiteColor];
    [self addSubview:tableViewTitleView];
    
    UILabel *dateTextL = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth * 0.5, tableViewTitleView.frame.size.height)];
    dateTextL.text = @"日期";
//    dateTextL.font = [UIFont systemFontOfSize:15];
    dateTextL.textColor = SYColor(83, 83, 83);
    dateTextL.textAlignment = NSTextAlignmentCenter;
    [tableViewTitleView addSubview:dateTextL];
    
    self.pamaterTextL = [[UILabel alloc] initWithFrame:CGRectMake(DeviceWidth * 0.5, 0, DeviceWidth * 0.5, tableViewTitleView.frame.size.height)];
    self.pamaterTextL.textColor = SYColor(83, 83, 83);
//    self.pamaterTextL.font = [UIFont systemFontOfSize:15];
    self.pamaterTextL.textAlignment = NSTextAlignmentCenter;
    [tableViewTitleView addSubview:self.pamaterTextL];
    
    self.pamaterTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(tableViewTitleView.frame), DeviceWidth, self.frame.size.height - CGRectGetMaxY(tableViewTitleView.frame))];
    self.pamaterTableView.backgroundColor = SYColor(241, 241, 241);
    self.pamaterTableView.dataSource = self;
    self.pamaterTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self addSubview:self.pamaterTableView];
    
}

- (void)setIndexF:(NSInteger)indexF
{
    _indexF = indexF;
}

#pragma mark -数据源方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.pamaterArray.count;
}

- (YBParmaterListCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    YBParmaterListCell *cell = [YBParmaterListCell cellWithTableView:tableView parmater:self.parmaterName];
    if (self.indexF) {
        cell.FatIndex = self.indexF;
        cell.bodyfatmodel = self.pamaterArray[indexPath.row];
    }else{
        cell.healthParmater = self.pamaterArray[indexPath.row];
    }
    
    
    return cell;
}
@end
