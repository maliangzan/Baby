//
//  YBParmaterListCell.h
//  孕妙
//
//  Created by sayes1 on 16/1/22.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@class YBPamaterList, YBHealthParmater, YBBodyFatModel;
@interface YBParmaterListCell : UITableViewCell

@property (nonatomic, strong) YBHealthParmater *healthParmater;
@property (nonatomic, strong) YBBodyFatModel *bodyfatmodel;
@property (nonatomic, assign)NSInteger FatIndex;

+ (instancetype)cellWithTableView:(UITableView *)tableView parmater:(NSString *)parmaterStr;
@end
