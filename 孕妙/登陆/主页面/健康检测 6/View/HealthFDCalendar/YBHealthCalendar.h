//
//  FDCalendar.h
//  FDCalendarDemo
//
//  Created by fergusding on 15/8/20.
//  Copyright (c) 2015年 fergusding. All rights reserved.
//

#import <UIKit/UIKit.h>

@class YBHealthCalendarItem;
@interface YBHealthCalendar : UIView

@property (strong, nonatomic) NSDate *date;


- (instancetype)initWithCurrentDate:(NSDate *)date;
- (void)setCurrentDate:(NSDate *)date;
@end
