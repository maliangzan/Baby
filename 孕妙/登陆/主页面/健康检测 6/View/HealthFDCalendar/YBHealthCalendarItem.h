//
//  FDCalendarItem.h
//  FDCalendarDemo
//
//  Created by fergusding on 15/8/20.
//  Copyright (c) 2015年 fergusding. All rights reserved.
//

#import <UIKit/UIKit.h>

#define DeviceWidth [UIScreen mainScreen].bounds.size.width

@protocol YBHealthCalendarItemDelegate;

@interface YBHealthCalendarItem : UIView

@property (strong, nonatomic) NSDate *date;
@property (weak, nonatomic) id<YBHealthCalendarItemDelegate> delegate;

- (NSDate *)nextMonthDate;
- (NSDate *)previousMonthDate;

@end

@protocol YBHealthCalendarItemDelegate <NSObject>

- (void)calendarItem:(YBHealthCalendarItem *)item didSelectedDate:(NSDate *)date;

@end
