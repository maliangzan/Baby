//
//  DVLineChartView.m
//  DVLineChart
//
//  Created by Fire on 15/10/16.
//  Copyright © 2015年 DuoLaiDian. All rights reserved.
//

#import "DVLineChartView.h"
#import "DVXAxisView.h"
#import "DVYAxisView.h"
#import "UIView+Extension.h"
#import "UIColor+Hex.h"

@interface DVLineChartView () <DVXAxisViewDelegate>

@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) DVXAxisView *xAxisView;
@property (strong, nonatomic) DVYAxisView *yAxisView;
@property (assign, nonatomic) CGFloat gap;

//体重上限值数组
@property (nonatomic, strong)NSArray *shangPlotArray;
//体重下限数组
@property (nonatomic, strong)NSArray *xiaPlotArray;
//体重实际值数组
@property (nonatomic, strong)NSArray *testPlotArray;

//当前月的测量天数
@property (nonatomic, strong)NSArray *testdayData2;

@end

@implementation DVLineChartView
{
    BOOL _FlagInt;
    NSInteger _monthInt;
    NSInteger _ifInt;
    float BMI;
}

- (NSMutableArray *)plots {
    
    if (_plots == nil) {
        _plots = [NSMutableArray array];
    }
    return _plots;
}


- (NSArray *)shangPlotArray
{
    if (_shangPlotArray == nil) {
        _shangPlotArray = [NSMutableArray array];
    }
    return _shangPlotArray;
}

- (NSArray *)xiaPlotArray
{
    if (_xiaPlotArray == nil) {
        _xiaPlotArray = [NSMutableArray array];
    }
    return _xiaPlotArray;
}

- (NSArray *)testPlotArray
{
    if (_testPlotArray == nil) {
        _testPlotArray = [NSMutableArray array];
    }
    return _testPlotArray;
}

- (void)commonInit {
    
    // 初始化某些属性值
    self.xAxisTextGap = 10;
    self.yAxisTextGap = 10;
    self.pointGap = 60;
    self.axisColor = [UIColor blackColor];
    self.textColor = [UIColor blackColor];
    self.textFont = [UIFont systemFontOfSize:12];
    //++
    _monthInt = 1;
    self.numberOfYAxisElements = 5;
    self.percent = NO;
    self.showSeparate = NO;
//    self.chartViewFill = YES;
    self.showPointLabel = YES;
    self.backColor = [UIColor whiteColor];
    self.pointUserInteractionEnabled = YES;
    self.index = -1;
    
    // 添加x轴与y轴视图
    DVYAxisView *yAxisView = [[DVYAxisView alloc] init];
    
    [self addSubview:yAxisView];
    
    UIScrollView *scrollView = [[UIScrollView alloc] init];
    //++++
    //在ScrollView添加滑动手势
//    UISwipeGestureRecognizer *sliderRecongizer = [[UISwipeGestureRecognizer alloc] init];
//    [sliderRecongizer setDirection:UISwipeGestureRecognizerDirectionRight];
//    [scrollView addGestureRecognizer:sliderRecongizer];
//    [sliderRecongizer addTarget:self action:@selector(handleSwipeFrom:)];
//    
//    sliderRecongizer = [[UISwipeGestureRecognizer alloc] init];
//    [sliderRecongizer setDirection:UISwipeGestureRecognizerDirectionLeft];
//    [scrollView addGestureRecognizer:sliderRecongizer];
//    [sliderRecongizer addTarget:self action:@selector(handleSwipeFrom:)];
    
    DVXAxisView *xAxisView = [[DVXAxisView alloc] init];
    
    [scrollView addSubview:xAxisView];
    
    [self addSubview:scrollView];
    
    self.scrollView = scrollView;
    self.xAxisView = xAxisView;
    self.xAxisView.delegate = self;
    self.yAxisView = yAxisView;
    
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.bounces = NO;
    
    NSNotificationCenter * center =[NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(testNotfoution:) name:@"BMIFloat" object:@"testttt"];
    
    // 1. 创建一个"轻触手势对象"
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture:)];
    
    // 点击几次
//    tap.numberOfTapsRequired = 2;
//    [self.xAxisView addGestureRecognizer:tap];
    
    // 2. 捏合手势
    UIPinchGestureRecognizer *pinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchGesture:)];
    [self.xAxisView addGestureRecognizer:pinch];
    
    
    
    
}


- (instancetype)initWithFrame:(CGRect)frame {
    
    if (self == [super initWithFrame:frame]) {
        
        [self commonInit];
        
    }
    return self;
}

- (void)awakeFromNib {
    
    [self commonInit];
}

+ (instancetype)lineChartView {
    
    return [[self alloc] init];
}

- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    
}

- (void)addPlot:(DVPlot *)plot {
    
    if (plot == nil) return;
    if (plot.pointArray.count == 0) return;
    
    [self.plots addObject:plot];
    
}

- (void)testNotfoution:(NSNotification *)notification{
    
    NSLog(@"fdskjhfsdkjfhsdkjhwetiuyiuy = %@",notification.name);
    BMI = [[notification.userInfo objectForKey:@"BMIKEY"]floatValue];
}

- (void)setLineDraw:(NSString *)bmi
{
    self.yAxisMaxValue = 20;
    self.numberOfYAxisElements = 10;
    self.pointGap = 50;
    self.showSeparate = YES;
//    self.separateColor = [UIColor colorWithHexString:@"67707c"];
    self.separateColor = [UIColor grayColor];
    self.textColor = [UIColor colorWithHexString:@"9aafc1"];
//    self.backColor = [UIColor colorWithHexString:@"3e4a59"];
    //    ccc.backColor = [UIColor whiteColor];
//    self.axisColor = [UIColor colorWithHexString:@"67707c"];
    float bmiStr = [bmi floatValue];
//    bmiStr = (float)roundf(bmiStr);
    
    NSLog(@"daslkjdslkajieworiu9899898 = %.2lf",bmiStr);

    NSMutableArray *tempArray = [NSMutableArray array];
    //上限
    NSMutableArray *plot1Array = [NSMutableArray array];
    //下限
    NSMutableArray *plot2Array = [NSMutableArray array];
    //标准
    NSMutableArray *plot3Array = [NSMutableArray array];
    
    
    //+++++
//    self.weightData1 = @[@1.1,@9.5,@0,@0,@0,@7.6,@0,@7.6,@0,@10.1];
//    self.dayData2 = @[@153,@155,@157,@160,@161,@162,@164,@168,@183,@184];
    
    for (NSInteger i = 1; i < 41; i ++) {
        [tempArray addObject:[NSString stringWithFormat:@"%ld", (long)i]];
        float f1 = 0.00;//上限
        float f2 = 0.00;//下限
//        float f3 = 0.00;
     
        
        /*
         郭丰修改 2016-09-05
         */
        if (bmiStr > 25.0 && bmiStr<29.9) {//BMI值在25.0-29.9范围

            if (i < 13.5) {//判断上限1-13.5周
                    f1 = 0.272 * (i - 1);
                }else if (i == 13.5){//13.5-40周
                    f1 = 0.314 * i -0.839;
                }else{
                    f1 = 0.314 * i -0.839;
                }
            
            if (i < 12.5) {//判断上限1-12.5周
                    f2 = 0.087 * (i - 1);
                    
                }else if (i == 12.5){//12.5-40周
                    f2 = 0.214 * 12.6 -1.675;
                }else
                {
                    f2 = 0.214 * i -1.675;
                }

        }else if (bmiStr >= 30.0){//BMI值大于或者等于30.0

            if (i<12) {
                    f1 = 0.164 * (i -1);
                }else if (i == 12){//12.1-40周
                    f1 = 0.259 * i - 1.308;
                }else{
                    f1 = 0.259 * i - 1.308;
                }

            if (i <12.5) {
                    f2 = 0.043 * (i - 1);
                }else if (i == 12.5){
                    f2 = 0.170 * 12.6 - 1.625;
                }else{
                    f2 = 0.170 * i - 1.625;
                }

        }else if (bmiStr >= 18.5 && bmiStr <= 24.9){//BMI值在18.5-24.9范围

            if (i<12.5) {
                    f1 = 0.261 * (i -1);
                }else if (i == 12.5){
                    f1 = 0.491 * i - 3.1375;
                }else{
                    f1 = 0.491 * i - 3.1375;
                }

            if (i < 12.5) {
                    f2 = 0.087 * (i -1);
                }else if (i == 12.5){
                    f2 = 0.381 * 12.6 - 3.7625;
                }else{
                   f2 = 0.381 * i - 3.7625;
                }
            
        }else if (bmiStr < 18.5){//BMI值小于18.5的范围

            if (i < 12) {
                    f1 = 0.25 * (i -1);
                }else if(i == 12){
                    f1 = 0.5625 * i - 4;
                }else{
                   f1 = 0.5625 * i - 4;
                }
            
                if (i<12.5) {
                    f2 = 0.070 * (i -1);
                }else if (i == 12.5){
                    f2 = 0.423 * 12.6 - 4.4875;
                }else{
                    f2 = 0.423 * i - 4.4875;
                }
        }
        
        [plot1Array addObject:[NSString stringWithFormat:@"%.3f", f1]];
        [plot2Array addObject:[NSString stringWithFormat:@"%.3f", f2]];
        
    }
    
    self.xAxisTitleArray = [NSArray arrayWithArray:tempArray];
    
    DVPlot *plot = [[DVPlot alloc] init];
    plot.pointArray = [NSArray arrayWithArray:_weightData1];
    
    //++++
    plot.testdayArray = self.dayData2;
    
    plot.lineColor = [UIColor colorWithHexString:@"3FBE0D"];
    plot.pointColor = [UIColor colorWithHexString:@"14b9d6"];
  
    
    DVPlot *plot1 = [[DVPlot alloc] init];
    plot1.pointArray = [NSArray arrayWithArray:plot1Array];
    plot1.lineColor = [SYColor(249, 130, 186) colorWithAlphaComponent:0.2];
    plot1.pointColor = [UIColor whiteColor];
    plot1.chartViewFill = YES;
    
    DVPlot *plot2 = [[DVPlot alloc] init];
    plot2.pointArray = [NSArray arrayWithArray:plot2Array];
//    plot2.lineColor = [[UIColor colorWithHexString:@"3e4a59"] colorWithAlphaComponent:1];
    plot2.lineColor = [[UIColor whiteColor] colorWithAlphaComponent:1];
    plot2.pointColor = [UIColor redColor];
    plot2.chartViewFill = YES;
    //不显示坐标线
//    self.showSeparate = NO;
        self.testPlotArray = _weightData1;
    self.shangPlotArray = plot1Array;
    self.xiaPlotArray = plot2Array;

    
    

    [self addPlot:plot1];
    [self addPlot:plot2];
    [self addPlot:plot];
    [self draw];
    
    //展示全图
    self.xAxisView.showTailAndHead = YES;
    
    CGFloat scale = self.scrollView.width / (self.xAxisTitleArray.count * self.gap + 100);
    
    self.pointGap *= scale;
    
    //++++
    self.yAxisView.numberOfYAxisElements = 10;
    self.yAxisView.yAxisMaxValue = 20;
    [self.yAxisView draw];
    [self.xAxisView draw];
    
    
    
    [UIView animateWithDuration:0.25 animations:^{
        
        self.xAxisView.width = self.scrollView.width;
    }];
    
    self.xAxisView.pointGap = self.pointGap;
    
    self.scrollView.contentSize = CGSizeMake(self.xAxisView.width, 0);

}



- (void)draw {
    
    if (self.plots.count == 0) return;
    
    // 设置y轴视图的尺寸
    self.yAxisView.x = 0;
    self.yAxisView.y = 0;
    self.yAxisView.width = self.yAxisViewWidth;
    self.yAxisView.height = self.height;
    
    
    // 设置scrollView的尺寸
    self.scrollView.x = self.yAxisView.width;
    self.scrollView.y = 0;
    
    self.scrollView.width = self.width - self.scrollView.x;
    self.scrollView.height = self.height;
    
    // 设置x轴视图的尺寸
    self.xAxisView.x = 0;
    self.xAxisView.y = 0;
    self.xAxisView.height = self.scrollView.height;
    self.xAxisView.width = self.xAxisTitleArray.count * self.pointGap + 400;
    
    self.scrollView.contentSize = self.xAxisView.frame.size;
    
    self.gap = self.pointGap;
    
    // 给y轴视图传递参数
    self.yAxisView.xAxisTextGap = self.xAxisTextGap;
    self.yAxisView.yAxisTextGap = self.yAxisTextGap;
    self.yAxisView.textColor = self.textColor;
    self.yAxisView.textFont = self.textFont;
    self.yAxisView.percent = self.isPercent;
    self.yAxisView.showSeparate = self.isShowSeparate;
    self.yAxisView.axisColor = self.axisColor;
    self.yAxisView.numberOfYAxisElements = self.numberOfYAxisElements;
    self.yAxisView.yAxisMaxValue = self.yAxisMaxValue;
    self.yAxisView.backColor = self.backColor;
    [self.yAxisView draw];
    
    self.xAxisView.xAxisTitleArray = self.xAxisTitleArray;
    self.xAxisView.pointGap = self.pointGap;
    self.xAxisView.xAxisTextGap = self.xAxisTextGap;
    self.xAxisView.axisColor = self.axisColor;
    self.xAxisView.showSeparate = self.isShowSeparate;
    self.xAxisView.numberOfYAxisElements = self.numberOfYAxisElements;
    self.xAxisView.plots = self.plots;
    self.xAxisView.yAxisMaxValue = self.yAxisMaxValue;
    //    self.xAxisView.chartViewFill = self.isChartViewFill;
    self.xAxisView.showPointLabel = self.isShowPointLabel;
    self.xAxisView.backColor = self.backColor;
    self.xAxisView.textFont = self.textFont;
    self.xAxisView.textColor = self.textColor;
    self.xAxisView.percent = self.isPercent;
    self.xAxisView.pointUserInteractionEnabled = self.isPointUserInteractionEnabled;
    self.xAxisView.separateColor = self.separateColor;
    [self.xAxisView draw];
    
    if (self.index < 0) {
        
        if (self.index * self.pointGap > self.scrollView.width * 0.5) {
            [self.scrollView setContentOffset:CGPointMake(self.index * self.pointGap - self.scrollView.width * 0.5 + self.pointGap * 0.8, 0) animated:YES];
        }else{
            
            [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
        }
    }
    
    
}


- (NSArray *)testArray:(NSInteger)month
{
    NSMutableArray *tempArray1 = [NSMutableArray array];
    NSMutableArray *tempArray2 = [NSMutableArray array];
    NSInteger i = 0;
    for (NSString *str in self.dayData2) {
        if ([str integerValue] >= (4* (month-1)* 7) && ([str integerValue] <=(4*(month))* 7)) {
            [tempArray1 addObject:self.testPlotArray[i]];
            [tempArray2 addObject:self.dayData2[i]];
        }
        i++;
    }
    
    self.testdayData2 = tempArray2;
    return tempArray1;
}


// 捏合手势监听方法
- (void)pinchGesture:(UIPinchGestureRecognizer *)recognizer
{
   
    
    if (recognizer.state == 3) {
        
        if (self.xAxisView.width <= self.scrollView.width) {
            
            CGFloat scale = self.scrollView.width / self.xAxisView.width;
            
            self.pointGap *= scale;
            
            [UIView animateWithDuration:0.25 animations:^{
                
                self.xAxisView.width = self.scrollView.width;
            }];
            
            self.xAxisView.showTailAndHead = YES;
            self.yAxisView.showTailAndHead = YES;
            
            self.xAxisView.pointGap = self.pointGap;
            //            self.yAxisView.yAxisTextGap = self.pointGap;
            self.yAxisView.pointGap = self.pointGap;
            
        }else if (self.xAxisView.width >= self.xAxisTitleArray.count * self.gap + 180){
            
            //            [UIView animateWithDuration:0.25 animations:^{
            //                self.xAxisView.width = self.xAxisTitleArray.count * self.gap + 100;
            //
            //            }];
            
            self.xAxisView.showTailAndHead = NO;
            self.yAxisView.showTailAndHead = NO;
            
            self.pointGap = self.gap;
            
            self.xAxisView.pointGap = self.pointGap;
            self.yAxisView.pointGap = self.pointGap;
        }
    }else{
        
        self.xAxisView.width *= recognizer.scale;
        
        self.xAxisView.showTailAndHead = NO;
        self.yAxisView.showTailAndHead = NO;
        
        self.pointGap *= recognizer.scale;
        //            self.yAxisTextGap *= recognizer.scale;
        //            self.yAxisView.yAxisTextGap = self.yAxisTextGap;
        self.yAxisView.pointGap = self.pointGap;
        self.xAxisView.pointGap = self.pointGap;
        
        recognizer.scale = 1.0;
        
        
        
    }
    
    self.scrollView.contentSize = CGSizeMake(self.xAxisView.width,100);
    
  }

- (void)xAxisView:(DVXAxisView *)xAxisView didClickButtonAtIndex:(NSInteger)index {
    
    if ([self.delegate respondsToSelector:@selector(lineChartView:DidClickPointAtIndex:)]) {
        [self.delegate lineChartView:self DidClickPointAtIndex:index];
        
        
    }
    
}

-(void)dealloc
{
    _scrollView.delegate = nil;
}
@end
