//
//  YBParmaterListCell.m
//  孕妙
//
//  Created by sayes1 on 16/1/22.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "YBParmaterListCell.h"
#import "YBHealthParmater.h"
#import "YBBodyFatModel.h"


@interface YBParmaterListCell ()
/**
 
 */
@property (nonatomic, strong) UILabel *dateL;
/**
 
 */
@property (nonatomic, strong) UILabel  *pamaterL;
/**
 * icon图片名
 */
@property (nonatomic, copy) NSString *parmaterStr;
@end

@implementation YBParmaterListCell

- (void)setHealthParmater:(YBHealthParmater *)healthParmater
{
    _dateL.text = [NSString stringWithFormat:@"%ld年%02ld月%02ld日", (long)healthParmater.parmaterDateCom.year, (long)healthParmater.parmaterDateCom.month, (long)healthParmater.parmaterDateCom.day];
//    _dateL.font = [UIFont systemFontOfSize:13];
//    _pamaterL.font = [UIFont systemFontOfSize:13];
    if ([self.parmaterStr containsString:@"kg"]) {
        _pamaterL.text = [NSString stringWithFormat:@"%.1f", healthParmater.weight];
    } else if ([self.parmaterStr containsString:@"mmol/L"]) {
        _pamaterL.text = [NSString stringWithFormat:@"%.1f", healthParmater.bloodG];
    } else if ([self.parmaterStr containsString:@"次/min"]) {
        _pamaterL.text = [NSString stringWithFormat:@"%ld", (long)healthParmater.heartRate];
    } else if ([self.parmaterStr containsString:@"mmHg"]) {
        _pamaterL.text = [NSString stringWithFormat:@"%ld/%ld", (long)healthParmater.dbp, (long)healthParmater.sbp];
    }else if ([self.parmaterStr containsString:@"°C"]){
        _pamaterL.text = [NSString stringWithFormat:@"%.1f", healthParmater.tem];
    }
}

- (void)setBodyfatmodel:(YBBodyFatModel *)bodyfatmodel
{
    _dateL.text = [NSString stringWithFormat:@"%ld年%02ld月%02ld日", (long)bodyfatmodel.parmaterDateCom.year, (long)bodyfatmodel.parmaterDateCom.month, (long)bodyfatmodel.parmaterDateCom.day];
    if (_FatIndex == 1) {
        _pamaterL.text = [NSString stringWithFormat:@"%.1f", bodyfatmodel.bmi];
        return;
    }
    
    if (_FatIndex == 2) {
        _pamaterL.text = [NSString stringWithFormat:@"%.1f", bodyfatmodel.fat];
        return;
    }
    if (_FatIndex == 3) {
        _pamaterL.text = [NSString stringWithFormat:@"%.1f", bodyfatmodel.viscerafat];
        return;
    }
    if (_FatIndex == 4) {
        _pamaterL.text = [NSString stringWithFormat:@"%.1f", bodyfatmodel.water];
        return;
    }
    if (_FatIndex == 5) {
        _pamaterL.text = [NSString stringWithFormat:@"%.1f", bodyfatmodel.muscle];
        return;
    }
    if (_FatIndex == 6) {
        _pamaterL.text = [NSString stringWithFormat:@"%.1f", bodyfatmodel.bone];
        return;
    }
    if (_FatIndex == 7) {
        _pamaterL.text = [NSString stringWithFormat:@"%ld", (long)bodyfatmodel.kcal];
        return;
    }
    
    
}


#pragma mark - 初始化
+ (instancetype)cellWithTableView:(UITableView *)tableView parmater:(NSString *)parmaterStr
{
    static NSString *ID = @"parmaterListCell";
    YBParmaterListCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (cell == nil) {
        cell = [[YBParmaterListCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:ID];
    }
    cell.parmaterStr = parmaterStr;
    return cell;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        // 初始化子控件
        [self setupSubCell];
        self.backgroundColor = SYColor(237, 237, 237);
//        self.selectedBackgroundView = [[UIView alloc] initWithFrame:self.frame];
//        self.selectedBackgroundView.backgroundColor = SYColor(87, 210, 242);
        //        self.backgroundColor = [UIColor blueColor];
        
        // 添加分隔线
//        [self addDeviderView];
    }
    return self;
}

//// 添加分隔线
//- (void)addDeviderView
//{
//    UIView *divider = [[UIView alloc] init];
//    divider.backgroundColor = [UIColor blackColor];
//    divider.alpha = 0.2;
//    [self.contentView addSubview:divider];
//    
//    self.deiver = divider;
//    
//}

- (void)layoutSubviews
{
    [super layoutSubviews];
    // 设置控件的frame
    CGFloat dateLX = 0;
    CGFloat dateLY = 0;
    CGFloat dateLW = (self.frame.size.width) * 0.5;
    CGFloat dateLH = self.frame.size.height;
    self.dateL.frame = CGRectMake(dateLX, dateLY, dateLW, dateLH);
    
    CGFloat pamaterLX = CGRectGetMaxX(self.dateL.frame);
    CGFloat pamaterLY = 0;
    CGFloat pamaterLW = dateLW;
    CGFloat pamaterLH = self.frame.size.height;
    self.pamaterL.frame = CGRectMake(pamaterLX, pamaterLY, pamaterLW, pamaterLH);
    
//    CGFloat dividerX = 0;
//    CGFloat dividerH = 1;
//    CGFloat dividerY = self.frame.size.height - dividerH;
//    CGFloat dividerW = self.frame.size.width;
//    self.deiver.frame = CGRectMake(dividerX, dividerY, dividerW, dividerH);
}

- (void)setupSubCell
{
    // 日期
    UILabel *dateL = [[UILabel alloc] init];
    self.dateL = dateL;
    self.dateL.textColor = SYColor(102, 102, 102);
    dateL.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:dateL];
    
    self.pamaterL = [[UILabel alloc] init];
    self.pamaterL.textColor = SYColor(102, 102, 102);
    self.pamaterL.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:self.pamaterL];
}

//- (void)setFrame:(CGRect)frame
//{
//    frame.origin.x = 5;
//    frame.size.width -= 10;
//    [super setFrame:frame];
//}




@end
