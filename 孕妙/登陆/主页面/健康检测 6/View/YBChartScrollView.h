//
//  YBChartScrollView.h
//  孕妙
//
//  Created by sayes1 on 16/1/25.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@class YBChartScrollView;

@protocol YBChartScrollViewDelegate <NSObject>

@optional
- (void)chartScrollView:(YBChartScrollView *)chartScrollView chartDataArray:(NSArray *)chartArray;

@end

@interface YBChartScrollView : UIScrollView
@property (nonatomic, strong) NSArray *measureResult;
@property (nonatomic, assign) BOOL isWeekChart;
@property (nonatomic, assign) BOOL isMonthChart;
//@property (nonatomic, assign) BOOL isYearChart;
/**
 向右滑动
 */
@property (nonatomic, assign) BOOL isSlideToRight;
/**
 向左滑动
 */
@property (nonatomic, assign) BOOL isSlideToLeft;

@property (nonatomic, assign) id<YBChartScrollViewDelegate> YBDelegate;
//@property (nonatomic, getter=isbleLink) BOOL bleLink;
/**
 当前显示体重曲线
 */
@property (nonatomic, getter=isweightLine) BOOL weightLine;
/**
 当前显示血压曲线
 */
@property (nonatomic, getter=isxueyaLine) BOOL xueyaLine;
/**
 当前显示血糖曲线
 */
@property (nonatomic, getter=isxuetangLine) BOOL xuetangLine;
/**
 当前显示心率曲线
 */
@property (nonatomic, getter=isheartLine) BOOL heartLine;

/**
 当前显示体温曲线
 */
@property (nonatomic, getter=istiwenLine) BOOL tiwenLine;

//当前体脂曲线
@property (nonatomic, getter=isfatLine) BOOL fatLine;

@property (nonatomic, assign)NSInteger yNumber;

@end
