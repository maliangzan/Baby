//
//  YBSelectPickerWindow.m
//  孕妙
//
//  Created by Raoy on 16/4/12.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "YBSelectPickerWindow.h"
#define StartLayoutHeight (DeviceHeight - 64)

@implementation YBSelectPickerWindow
{
    UILabel *titleL;
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.frame = CGRectMake(SYMargin20, SYNavigateHeight * 2, DeviceWidth - 2 * SYMargin20, StartLayoutHeight * 0.7);;
        self.backgroundColor = [UIColor whiteColor];
        self.hidden = NO;//不隐藏
//        self.windowLevel = 100;
        if (_selectPickerView == nil) {
            _selectPickerView = [[UIPickerView alloc] init];
        }
        
    }
    return self;
}


- (void)setInterFace:(NSString *)title
{
    
    titleL = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height * 0.1)];
    titleL.text = title;
    titleL.backgroundColor = SYColor(249, 130, 186);
    titleL.textColor = [UIColor whiteColor];
    titleL.textAlignment = NSTextAlignmentCenter;
    [self addSubview:titleL];
    UIPickerView *xuetangPicker = [[UIPickerView alloc] init];
    CGFloat xuetangPickerH = xuetangPicker.frame.size.height;
    CGFloat marginTotitle = (self.frame.size.height * 0.7 - xuetangPickerH) * 0.5;
    xuetangPicker.frame = CGRectMake(0, CGRectGetMaxY(titleL.frame) + marginTotitle, self.frame.size.width, 216);
    _selectPickerView = xuetangPicker;
    [self addSubview:_selectPickerView];
    
    
    CGFloat dianLX = self.frame.size.width * 0.5;
    CGFloat dianLY = xuetangPicker.frame.origin.y + xuetangPickerH * 0.5;
    CGFloat dianLW = 15;
    CGFloat dianLH = 15;
    _dianL = [[UILabel alloc] initWithFrame:CGRectMake(dianLX, dianLY, dianLW, dianLH)];
    _dianL.text = @".";
    _dianL.textColor = [UIColor blackColor];
    
    CGFloat btnViewH = self.frame.size.height * 0.2;
    CGFloat cancleBtnX = self.frame.size.width * 0.5 * 0.2;
    CGFloat cancleBtnY = self.frame.size.height * 0.8 + btnViewH * 0.2;
    CGFloat cancleBtnW = self.frame.size.width * 0.5 * 0.6;
    CGFloat cancleBtnH = btnViewH * 0.6;
    
    _cancelBtn = [[UIButton alloc] initWithFrame:CGRectMake(cancleBtnX, cancleBtnY, cancleBtnW, cancleBtnH)];
    [_cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    [_cancelBtn  setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_cancelBtn setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
    [_cancelBtn setBackgroundColor:SYColor(249, 130, 186)];
    _cancelBtn.layer.cornerRadius = 5;
    _cancelBtn.layer.masksToBounds = YES;
    [self addSubview:self.cancelBtn];
    
    
    _sureBtn = [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width * 0.5 + cancleBtnX, cancleBtnY, cancleBtnW, cancleBtnH)];
    [_sureBtn setTitle:@"确定" forState:UIControlStateNormal];
    [_sureBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_sureBtn setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
    [_sureBtn setBackgroundColor:SYColor(249, 130, 186)];
    _sureBtn.layer.cornerRadius = 5;
    _sureBtn.layer.masksToBounds = YES;
    _sureBtn.backgroundColor = SYColor(249, 130, 186);
    
    [self addSubview:self.sureBtn];
    
    [_cancelBtn addTarget:self action:@selector(cancleSaveDataBtn:) forControlEvents:UIControlEventTouchUpInside];
    
    
}

- (void)cancleSaveDataBtn:(UIButton *)button
{
//    [_pickerWindow removeFromSuperview];
    self.hidden = YES;
    
  
}

//- (void)dealloc
//{
//    self.selectPickerView = nil;
//    titleL = nil;
//}



@end
