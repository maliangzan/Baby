//
//  YBBodyFatView.m
//  孕妙
//
//  Created by Raoy on 16/6/8.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "YBBodyFatView.h"
#import "AFNetworking.h"

// 工具栏高度
#define SYTabBarHeight 49
/**
 转盘的比例
 */
#define SYZhuanpanHRatio (39 / 90.0)
/**
 转盘的比例
 */
#define SYMeasureHRatio (51 / 90.0)

@interface YBBodyFatView ()
/**
 转盘view
 */
@property (weak, nonatomic) UIView *zhuanpanView;
/**
 BMI的状态副本
 */
@property (weak, nonatomic) UIButton *bmiStateBtn;
@property (weak, nonatomic) UIButton *bmiStateBtn_2;
/**
 BMI值副本
 */
@property (weak, nonatomic) UILabel *bmiValueL_2;
/**
 体重
 */
@property (weak, nonatomic) UILabel *weightValueL;
/**
 建议view
 */
@property (nonatomic, weak) UIView *adviceView;
/**
 BMI值
 */
@property (weak, nonatomic) UILabel *bmiValueL;
@property (weak, nonatomic) UILabel *bodyFatL;
@property (weak, nonatomic) UIButton *bodyFatStateBtn;
@property (nonatomic, weak) UILabel *adviceL;
/**
 显示测量结果View
 */
@property (nonatomic, weak) UIView *mearsureView;
@property (nonatomic, weak) UIButton *sureButton;
@property (weak, nonatomic) UILabel *neizangValueL;
@property (weak, nonatomic) UIButton *neizangStateBtn;
@property (weak, nonatomic) UILabel *waterValueL;
/**
 水分参数值判断
 */
@property (weak, nonatomic) UIButton *waterStateBtn;
@property (weak, nonatomic) UILabel *gugeValueL;
@property (weak, nonatomic) UIButton *gugeStateBtn;
@property (weak, nonatomic) UILabel *jirouValueL;
@property (weak, nonatomic) UIButton *jirouStateBtn;

@property (weak, nonatomic) UIView *jirouView;
@property (weak, nonatomic) UIImageView *jirouImage;
@property (weak, nonatomic) UILabel *reliangValueL;

@property (weak, nonatomic) UIButton *reliangStateBtn;


@end

@implementation YBBodyFatView


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = SYColor(245, 245, 245);
        // 圆盘view
        [self setupZhuanPan];
        
        // 测量信息View
        [self setupMearsureView];
    }
    
    return self;
}



#pragma mark - priveateMethod
- (void)setupZhuanPan
{
    UIView *zhuanpanView = [[UIView alloc] init];
    CGFloat zhuangpanViewX = 0;
    CGFloat zhuangpanViewY = 0;
    CGFloat zhuangpanViewW = DeviceWidth;
    CGFloat zhuangpanViewH = (DeviceHeight  - SYTabBarHeight - SYMargin5) * SYZhuanpanHRatio;
    zhuanpanView.frame = CGRectMake(zhuangpanViewX, zhuangpanViewY, zhuangpanViewW, zhuangpanViewH);
    //    zhuanpanView.backgroundColor = [UIColor redColor];
    self.zhuanpanView = zhuanpanView;
    [self addSubview:zhuanpanView];
    
    UIImageView *zhuanpanImage = [[UIImageView alloc] init];
    CGFloat zhuanpanImageY = 0;
    CGFloat zhuanpanImageH = zhuangpanViewH;
    CGFloat zhuanpanImageW = zhuangpanViewH;
    CGFloat zhuanpanImageX = (DeviceWidth - zhuangpanViewH) * 0.5;
    zhuanpanImage.frame = CGRectMake(zhuanpanImageX, zhuanpanImageY, zhuanpanImageW, zhuanpanImageH);
    zhuanpanImage.image = [UIImage imageNamed:@"圆底"];
    [zhuanpanView addSubview:zhuanpanImage];
    
    UIButton *backMeasure = [UIButton buttonWithType:UIButtonTypeCustom];
    backMeasure.backgroundColor = SYColor(249, 150, 200);
    backMeasure.frame = CGRectMake(0, 0, 85, 30);
    [backMeasure setTitle:@"返回检测" forState:UIControlStateNormal];
    backMeasure.layer.cornerRadius = 10;
    [backMeasure setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [backMeasure setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
    [self.zhuanpanView addSubview:backMeasure];
    
    [backMeasure addTarget:self action:@selector(backMeasureClick:) forControlEvents:UIControlEventTouchUpInside];
    // 图片点击
    zhuanpanImage.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(zhuanpanTapped)];
    [zhuanpanImage addGestureRecognizer:singleTap];//点击图片事件
    
    // bmi的状态
    NSString *bmiStateStr = @"标准";
    NSMutableDictionary *dictAttrbmiState = [NSMutableDictionary dictionary];
    dictAttrbmiState[NSFontAttributeName] = [UIFont systemFontOfSize:17];
    CGSize bmiStateSize = [bmiStateStr sizeWithAttributes:dictAttrbmiState];
    
    NSInteger weightFont = 27;
    if (SYDevice4s) {
        weightFont = 27;
        SYLog(@"4s");
    } else if (SYDevice5) {
        weightFont = 37;
        SYLog(@"5");
    } else if (SYDevice6p) {
        weightFont = 45;
        SYLog(@"6p");
    } else if (SYDevice6) {
        weightFont = 40;
        SYLog(@"6");
    }
    NSString *weightValueStr = @"000.0";
    NSMutableDictionary *dictAttrweightValue = [NSMutableDictionary dictionary];
    dictAttrweightValue[NSFontAttributeName] = [UIFont systemFontOfSize:weightFont];
    CGSize weightValueStrSize = [weightValueStr sizeWithAttributes: dictAttrweightValue];
    // bmi值
    NSString *bmiStr = @"BMI";
    NSMutableDictionary *dictAttrBmi = [NSMutableDictionary dictionary];
    dictAttrBmi[NSFontAttributeName] = [UIFont systemFontOfSize:15];
    CGSize bmiStrSize = [bmiStr sizeWithAttributes: dictAttrBmi];
    
    UILabel *bmiL = [[UILabel alloc] init];
    CGFloat bmiLW = bmiStrSize.width;
    CGFloat bmiLX = DeviceWidth * 0.5 - bmiLW;
    CGFloat bmiLY = zhuanpanImageH / 4.0;
    CGFloat bmiLH = bmiStrSize.height;
    bmiL.frame = CGRectMake(bmiLX, bmiLY, bmiLW, bmiLH);
    bmiL.text = bmiStr;
    bmiL.font = [UIFont systemFontOfSize:15];
    bmiL.textColor = SYColor(29, 125, 183);
    [self.zhuanpanView addSubview:bmiL];
    
    NSString *bmiValueStr = @"00.0";
    NSMutableDictionary *dictAttrBmiValue = [NSMutableDictionary dictionary];
    dictAttrBmiValue[NSFontAttributeName] = [UIFont systemFontOfSize:15];
    CGSize bmiValueStrSize = [bmiValueStr sizeWithAttributes: dictAttrBmiValue];
    UILabel *bmiValueL = [[UILabel alloc] init];
    self.bmiValueL_2 = bmiValueL;
    bmiValueL.text = @"0";
    CGFloat bmiValueLW = bmiValueStrSize.width+10;
    CGFloat bmiValueLX = DeviceWidth * 0.5;
    CGFloat bmiValueLY = bmiLY;
    CGFloat bmiValueLH = bmiLH;
    bmiValueL.frame = CGRectMake(bmiValueLX, bmiValueLY, bmiValueLW, bmiValueLH);
    bmiValueL.font = [UIFont systemFontOfSize:15];
    bmiValueL.textAlignment = NSTextAlignmentCenter;
    bmiValueL.textColor = SYColor(183, 92, 169);
    [self.zhuanpanView addSubview:bmiValueL];
    
    // bmi状态
    UIButton *bmiStateBtn = [[UIButton alloc] init];
    self.bmiStateBtn_2 = bmiStateBtn;
    CGFloat bmiStateBtnW = bmiStateSize.width + SYMargin10;
    CGFloat bmiStateBtnX = DeviceWidth * 0.5 - bmiStateBtnW * 0.5;
    CGFloat bmiStateBtnH = bmiStateSize.height;
    CGFloat bmiStateBtnY = zhuanpanImageH * 0.75 - bmiStateBtnH;
    bmiStateBtn.frame = CGRectMake(bmiStateBtnX, bmiStateBtnY, bmiStateBtnW, bmiStateBtnH);
    [bmiStateBtn setBackgroundImage:[UIImage resizedImageWithName:@"标准"] forState:UIControlStateNormal];
    [bmiStateBtn setTitle:@"----" forState:UIControlStateNormal];
    bmiStateBtn.userInteractionEnabled = NO;
    [self.zhuanpanView addSubview:bmiStateBtn];
    
    // 体重值
    
    UILabel *weightValueL = [[UILabel alloc] init];
    self.weightValueL = weightValueL;
    CGFloat weightValueLW = weightValueStrSize.width;
    CGFloat weightValueLX = DeviceWidth * 0.5 - weightValueLW * 0.5 - SYMargin5;
    CGFloat weightValueLY = CGRectGetMaxY(bmiL.frame);
    CGFloat weightValueLH = bmiStateBtnY - weightValueLY;
    weightValueL.frame = CGRectMake(weightValueLX, weightValueLY, weightValueLW, weightValueLH);
    weightValueL.text = @"00.0";
    weightValueL.textColor = SYColor(243, 109, 23);
    weightValueL.font = [UIFont systemFontOfSize:weightFont];
    //    weightValueL.backgroundColor = [UIColor redColor];
    weightValueL.textAlignment = NSTextAlignmentCenter;
    [self.zhuanpanView addSubview:weightValueL];
    
    NSString *kgStr = @"KG";
    NSMutableDictionary *dictAttrKg = [NSMutableDictionary dictionary];
    dictAttrKg[NSFontAttributeName] = [UIFont systemFontOfSize:11];
    CGSize kgStrSize = [kgStr sizeWithAttributes: dictAttrKg];
    UILabel *kgL = [[UILabel alloc] init];
    kgL.text = kgStr;
    CGFloat kgLW = kgStrSize.width;
    CGFloat kgLX = CGRectGetMaxX(weightValueL.frame);
    CGFloat kgLY = weightValueLY + weightValueLH * 0.5;
    CGFloat kgLH = kgStrSize.height;
    kgL.frame = CGRectMake(kgLX, kgLY, kgLW, kgLH);
    kgL.font = [UIFont systemFontOfSize:11];
    [self.zhuanpanView addSubview:kgL];
}

- (void)zhuanpanTapped
{
    SYLog(@"转盘点击");
    
    if (self.adviceView) {
        return;
    }
    
    NSString *adviceStr = nil;
    int bmiValueCom = (int)([self.bmiValueL.text floatValue] * 10);
    int bodyFatCom = (int)([self.bodyFatL.text floatValue] * 10);
    if (bmiValueCom == 0 && bodyFatCom == 0) {
        adviceStr = @"亲，没有测量到您身体的其他参数，请在测一次";
    } else if (bmiValueCom < 185) {
        adviceStr = @"瘦成一道闪电啦，请注意不要挑食，保证营养均衡，尤其要加强对蛋白质和奶制品的摄入，保持充足而良好的睡眠和愉悦的心情，适当运动，使肌肉强壮、体魄健美。";
    } else if (bmiValueCom >= 185 && bmiValueCom < 240) {
        adviceStr = @"哇塞，您的身材太标准了，您是健康使者，请继续保持，再接再厉，更加健康，轻松保持好身材!";
    } else if (bmiValueCom >= 240 && bmiValueCom < 280) {
        adviceStr = @"亲，您有婴儿肥，建议多吃水果，选择有氧运动进行锻炼；调节好日常饮食，加强运动，提高代谢率，倾国倾城指日可待。";
    } else if (bmiValueCom >= 280) {
        adviceStr = @"您要Hold住体重哦，“管住嘴迈开腿”，多动，少吃，特别是少吃含脂肪多的油腻食物，细嚼慢咽，生活要有规律，并注重生活质量，加强日常锻炼，这样做保准瘦到您尖叫。";
    }
    
    UIView *adviceView = [[UIView alloc] init];
    self.adviceView = adviceView;
    
    NSMutableDictionary *dictAttrAdvice = [NSMutableDictionary dictionary];
    dictAttrAdvice[NSFontAttributeName] = [UIFont systemFontOfSize:17];
    UILabel *adviceL = [[UILabel alloc] init];
    self.adviceL = adviceL;
    CGFloat adviceLW = (DeviceWidth) * 0.8 - SYMargin20;
    CGFloat adviceLX = SYMargin10;
    CGFloat adviceLY = 0;
    CGSize adviceLSize = [adviceStr boundingRectWithSize:CGSizeMake(adviceLW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:dictAttrAdvice context:nil].size;
    //    CGSize adviceLSize = [adviceStr sizeWithFont:[UIFont systemFontOfSize:13] constrainedToSize:CGSizeMake(adviceLW, MAXFLOAT)];
    CGFloat adviceLH = adviceLSize.height;
    adviceL.frame = CGRectMake(adviceLX, adviceLY, adviceLW, adviceLH);
    adviceL.textAlignment = NSTextAlignmentLeft;
    //    adviceL.backgroundColor = SYColor(187, 187, 187);
    //    adviceL.layer.cornerRadius = 10;
    //    adviceL.layer.masksToBounds = YES;
    adviceL.numberOfLines = 0;
    adviceL.text = adviceStr;
    [self.adviceView addSubview:adviceL];
    
    UIButton *sureButton = [[UIButton alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(adviceL.frame) + SYMargin20, DeviceWidth * 0.8, 2 * SYMargin20)];
    self.sureButton = sureButton;
    [sureButton setBackgroundColor:SYColor(30, 166, 255)];
    [sureButton setTitle:@"确  定" forState:UIControlStateNormal];
    [self.adviceView addSubview:sureButton];
    // 监听关闭按键点击
    [sureButton addTarget:self action:@selector(clickCloseBtn) forControlEvents:UIControlEventTouchUpInside];
    
    
    CGFloat adviceViewW = (DeviceWidth) * 0.8;
    CGFloat adviceViewX = (DeviceWidth - adviceViewW) * 0.5;
    CGFloat adviceViewY = 0;
    CGFloat adviceViewH = adviceLH + sureButton.frame.size.height + SYMargin20;
    adviceView.frame = CGRectMake(adviceViewX, adviceViewY, adviceViewW, adviceViewH);
    adviceView.alpha = 0;
    adviceView.backgroundColor = SYColor(244, 244, 244);
    adviceView.layer.cornerRadius = 10;
    adviceView.layer.masksToBounds = YES;
    [self.mearsureView addSubview:adviceView];
    [UIView animateWithDuration:0.2 animations:^{
        self.adviceView.alpha = 1.0;
    }];
}

- (void)clickCloseBtn
{
    [UIView animateWithDuration:0.5 animations:^{
        self.adviceView.alpha = 0;
    } completion:^(BOOL finished) {
        [self.adviceView removeFromSuperview];
        self.adviceView = nil;
    }];
    
}

- (void)backMeasureClick:(id)sender
{
    self.hidden = YES;
}

- (void)setupMearsureView
{
    UIView *mearsureView = [[UIView alloc] init];
   
    self.mearsureView = mearsureView;
    CGFloat mearsureViewW = DeviceWidth;
    CGFloat mearsureViewH = (DeviceHeight  - SYTabBarHeight - 180 * (DeviceWidth / 480) - SYMargin5) * SYMeasureHRatio;
    CGFloat mearsureViewX = 0;
    CGFloat mearsureViewY = CGRectGetMaxY(self.zhuanpanView.frame);
    mearsureView.frame = CGRectMake(mearsureViewX, mearsureViewY, mearsureViewW, mearsureViewH);
    [self addSubview:mearsureView];
    // 各个参数的高度
    CGFloat heightUnit = mearsureViewH / 7.0;
    SYLog(@"heightUnit:%f", heightUnit);
    
    UIView *bmiView = [[UIView alloc] init];
    CGFloat bmiViewX = 0;
    CGFloat bmiViewY = 0;
    CGFloat bmiViewW = DeviceWidth;
    CGFloat bmiViewH = heightUnit;
    bmiView.frame = CGRectMake(bmiViewX, bmiViewY, bmiViewW, bmiViewH);
    [mearsureView addSubview:bmiView];
    
    UIView *bodyFatView = [[UIView alloc] init];
    CGFloat bodyFatViewX = 0;
    CGFloat bodyFatViewY = heightUnit;
    CGFloat bodyFatViewW = DeviceWidth;
    CGFloat bodyFatViewH = heightUnit;
    bodyFatView.frame = CGRectMake(bodyFatViewX, bodyFatViewY, bodyFatViewW, bodyFatViewH);
    [mearsureView addSubview:bodyFatView];
    
    UIView *neizangView = [[UIView alloc] init];
    CGFloat neizangViewX = 0;
    CGFloat neizangViewY = 2 * heightUnit;
    CGFloat neizangViewW = DeviceWidth;
    CGFloat neizangViewH = heightUnit;
    neizangView.frame = CGRectMake(neizangViewX, neizangViewY, neizangViewW, neizangViewH);
    [mearsureView addSubview:neizangView];
    
    UIView *waterView = [[UIView alloc] init];
    CGFloat waterViewX = 0;
    CGFloat waterViewY = 3 * heightUnit;
    CGFloat waterViewW = DeviceWidth;
    CGFloat waterViewH = heightUnit;
    waterView.frame = CGRectMake(waterViewX, waterViewY, waterViewW, waterViewH);
    [mearsureView addSubview:waterView];
    
    UIView *jirouView = [[UIView alloc] init];
    CGFloat jirouViewX = 0;
    CGFloat jirouViewY = 4 * heightUnit;
    CGFloat jirouViewW = DeviceWidth;
    CGFloat jirouViewH = heightUnit;
    jirouView.frame = CGRectMake(jirouViewX, jirouViewY, jirouViewW, jirouViewH);
    [mearsureView addSubview:jirouView];
    
    UIView *gugeView = [[UIView alloc] init];
    CGFloat gugeViewX = 0;
    CGFloat gugeViewY = 5 * heightUnit;
    CGFloat gugeViewW = DeviceWidth;
    CGFloat gugeViewH = heightUnit;
    gugeView.frame = CGRectMake(gugeViewX, gugeViewY, gugeViewW, gugeViewH);
    [mearsureView addSubview:gugeView];
    
    UIView *reliangView = [[UIView alloc] init];
    CGFloat reliangViewX = 0;
    CGFloat reliangViewY = 6 * heightUnit;
    CGFloat reliangViewW = DeviceWidth;
    CGFloat reliangViewH = heightUnit;
    reliangView.frame = CGRectMake(reliangViewX, reliangViewY, reliangViewW, reliangViewH);
    [mearsureView addSubview:reliangView];
    
    // 计算两个字的size
    NSString *twoWordStr = @"肌肉";
    NSMutableDictionary *dictAttrTwoWord = [NSMutableDictionary dictionary];
    dictAttrTwoWord[NSFontAttributeName] = [UIFont systemFontOfSize:17];
    CGSize twoWordStrSize = [twoWordStr sizeWithAttributes: dictAttrTwoWord];
    
    
    // BMI
    UIImageView *bmiImage = [[UIImageView alloc] init];
    CGFloat bmiImageW = heightUnit - SYMargin5;
    CGFloat bmiImageH = heightUnit - SYMargin5;
    CGFloat bmiImageX = SYMargin20;
    CGFloat bmiImageY = SYMargin5 / 2;
    bmiImage.frame = CGRectMake(bmiImageX, bmiImageY, bmiImageW, bmiImageH);
    [bmiView addSubview:bmiImage];
    bmiImage.image = [UIImage imageNamed:@"scale_bmi"];
    
    UILabel *bmiL = [[UILabel alloc] init];
    bmiL.text = @"BMI指数";
    NSMutableDictionary *dictAttrbmi = [NSMutableDictionary dictionary];
    dictAttrbmi[NSFontAttributeName] = [UIFont systemFontOfSize:17];
    CGSize bmiSize = [bmiL.text sizeWithAttributes: dictAttrbmi];
    CGFloat bmiLW = bmiSize.width;
    CGFloat bmiLH = heightUnit;
    CGFloat bmiLX = CGRectGetMaxX(bmiImage.frame) + SYMargin5;
    CGFloat bmiLY = 0;
    bmiL.frame = CGRectMake(bmiLX, bmiLY, bmiLW, bmiLH);
    [bmiView addSubview:bmiL];
    // 状态
    UIButton *bmiStateBtn = [[UIButton alloc] init];
    bmiStateBtn.userInteractionEnabled = NO;
    self.bmiStateBtn = bmiStateBtn;
    CGFloat bmiStateBtnW = twoWordStrSize.width + SYMargin10;
    CGFloat bmiStateBtnH = bmiViewH * 0.7;
    CGFloat bmiStateBtnX = bmiViewW - SYMargin20 - bmiStateBtnW;
    CGFloat bmiStateBtnY = bmiViewH * 0.5 * 0.3;
    bmiStateBtn.frame = CGRectMake(bmiStateBtnX, bmiStateBtnY, bmiStateBtnW, bmiStateBtnH);
    [bmiView addSubview:bmiStateBtn];
    [bmiStateBtn setTitle:@"----" forState:UIControlStateNormal];
    [bmiStateBtn setBackgroundImage:[UIImage resizedImageWithName:@"标准"] forState:UIControlStateNormal];
    [bmiStateBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    UILabel *bmiValueL = [[UILabel alloc] init];
    self.bmiValueL = bmiValueL;
    CGFloat bmiValueLW = bmiStateBtnX - CGRectGetMaxX(bmiL.frame) - SYMargin20;
    CGFloat bmiValueLH = bmiViewH;
    CGFloat bmiValueLX = CGRectGetMaxX(bmiL.frame);
    CGFloat bmiValueLY = 0;
    bmiValueL.frame = CGRectMake(bmiValueLX, bmiValueLY, bmiValueLW, bmiValueLH);
    [bmiView addSubview:bmiValueL];
    bmiValueL.textAlignment = NSTextAlignmentRight;
    bmiValueL.text = @"0";
    
    // 分割线
    UIView *marginBmiView = [[UIView alloc] init];
    CGFloat marginBmiViewX = SYMargin20;
    CGFloat marginBmiViewH = 1;
    CGFloat marginBmiViewY = bmiView.frame.size.height -  marginBmiViewH;
    CGFloat marginBmiViewW = bmiView.frame.size.width - 2 * SYMargin20;
    marginBmiView.frame = CGRectMake(marginBmiViewX, marginBmiViewY, marginBmiViewW, marginBmiViewH);
    marginBmiView.backgroundColor = [UIColor blackColor];
    marginBmiView.alpha = 0.1;
    [bmiView addSubview:marginBmiView];
    
    // 体脂率
    UIImageView *bodyFatImage = [[UIImageView alloc] init];
    CGFloat bodyFatImageW = heightUnit - SYMargin5;
    CGFloat bodyFatImageH = heightUnit - SYMargin5;
    CGFloat bodyFatImageX = SYMargin20;
    CGFloat bodyFatImageY = SYMargin5 / 2;
    bodyFatImage.frame = CGRectMake(bodyFatImageX, bodyFatImageY, bodyFatImageW, bodyFatImageH);
    [bodyFatView addSubview:bodyFatImage];
    bodyFatImage.image = [UIImage imageNamed:@"scale_fat"];
    
    UILabel *bodyFatL = [[UILabel alloc] init];
    bodyFatL.text = @"脂肪率";
    NSMutableDictionary *dictAttrFat = [NSMutableDictionary dictionary];
    dictAttrFat[NSFontAttributeName] = [UIFont systemFontOfSize:17];
    CGSize bodyFatSize = [bodyFatL.text sizeWithAttributes: dictAttrFat];
    CGFloat bodyFatLW = bodyFatSize.width;
    CGFloat bodyFatLH = bodyFatViewH;
    CGFloat bodyFatLX = CGRectGetMaxX(bodyFatImage.frame) + SYMargin5;
    CGFloat bodyFatLY = 0;
    bodyFatL.frame = CGRectMake(bodyFatLX, bodyFatLY, bodyFatLW, bodyFatLH);
    [bodyFatView addSubview:bodyFatL];
    // 状态
    UIButton *bodyFatStateBtn = [[UIButton alloc] init];
    bodyFatStateBtn.userInteractionEnabled = NO;
    self.bodyFatStateBtn = bodyFatStateBtn;
    CGFloat bodyFatStateBtnW = twoWordStrSize.width + SYMargin10;
    CGFloat bodyFatStateBtnH = bodyFatViewH * 0.7;
    CGFloat bodyFatStateBtnX = bodyFatViewW - SYMargin20 - bodyFatStateBtnW;
    CGFloat bodyFatStateBtnY = bodyFatViewH * 0.3 * 0.5;
    bodyFatStateBtn.frame = CGRectMake(bodyFatStateBtnX, bodyFatStateBtnY, bodyFatStateBtnW, bodyFatStateBtnH);
    [bodyFatView addSubview:bodyFatStateBtn];
    
    [bodyFatStateBtn setTitle:@"----" forState:UIControlStateNormal];
    [bodyFatStateBtn setBackgroundImage:[UIImage resizedImageWithName:@"标准"] forState:UIControlStateNormal];
    //    [bodyFatStateBtn setImage:[UIImage imageNamed:@"标准"] forState:UIControlStateNormal];
    [bodyFatStateBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    UILabel *bodyFatValueL = [[UILabel alloc] init];
    self.bodyFatL = bodyFatValueL;
    CGFloat bodyFatValueLW = bodyFatStateBtnX - CGRectGetMaxX(bodyFatValueL.frame) - SYMargin20;
    CGFloat bodyFatValueLH = bodyFatViewH;
    CGFloat bodyFatValueLX = CGRectGetMaxX(bodyFatValueL.frame);
    CGFloat bodyFatValueLY = 0;
    bodyFatValueL.frame = CGRectMake(bodyFatValueLX, bodyFatValueLY, bodyFatValueLW, bodyFatValueLH);
    [bodyFatView addSubview:bodyFatValueL];
    bodyFatValueL.textAlignment = NSTextAlignmentRight;
    bodyFatValueL.text = [NSString stringWithFormat:@"%.1f", _fat];
    
    // 分割线
    UIView *marginFatView = [[UIView alloc] init];
    CGFloat marginFatViewX = SYMargin20;
    CGFloat marginFatViewH = 1;
    CGFloat marginFatViewY = bodyFatView.frame.size.height -  marginFatViewH;
    CGFloat marginFatViewW = bodyFatView.frame.size.width - 2 * SYMargin20;
    marginFatView.frame = CGRectMake(marginFatViewX, marginFatViewY, marginFatViewW, marginFatViewH);
    marginFatView.backgroundColor = [UIColor blackColor];
    marginFatView.alpha = 0.1;
    [bodyFatView addSubview:marginFatView];
    
    // 内脏脂肪指数
    UIImageView *neizangImage = [[UIImageView alloc] init];
    CGFloat neizangImageW = heightUnit - SYMargin5;
    CGFloat neizangImageH = heightUnit - SYMargin5;
    CGFloat neizangImageX = SYMargin20;
    CGFloat neizangImageY = SYMargin5 / 2;
    neizangImage.frame = CGRectMake(neizangImageX, neizangImageY, neizangImageW, neizangImageH);
    [neizangView addSubview:neizangImage];
    neizangImage.image = [UIImage imageNamed:@"scale_fat2"];
    
    UILabel *neizangL = [[UILabel alloc] init];
    neizangL.text = @"内脏脂肪指数";
    NSMutableDictionary *dictAttrneizang = [NSMutableDictionary dictionary];
    dictAttrneizang[NSFontAttributeName] = [UIFont systemFontOfSize:17];
    CGSize neizangLSize = [neizangL.text sizeWithAttributes: dictAttrneizang];
    CGFloat neizangLW = neizangLSize.width;
    CGFloat neizangLH = neizangViewH;
    CGFloat neizangLX = CGRectGetMaxX(neizangImage.frame) + SYMargin5;
    CGFloat neizangLY = 0;
    neizangL.frame = CGRectMake(neizangLX, neizangLY, neizangLW, neizangLH);
    [neizangView addSubview:neizangL];
    // 状态
    UIButton *neizangStateBtn = [[UIButton alloc] init];
    neizangStateBtn.userInteractionEnabled = NO;
    self.neizangStateBtn = neizangStateBtn;
    CGFloat neizangStateBtnW = twoWordStrSize.width + SYMargin10;
    CGFloat neizangStateBtnH = neizangViewH * 0.7;
    CGFloat neizangStateBtnX = neizangViewW - SYMargin20 - neizangStateBtnW;
    CGFloat neizangStateBtnY = neizangViewH * 0.3 * 0.5;
    neizangStateBtn.frame = CGRectMake(neizangStateBtnX, neizangStateBtnY, neizangStateBtnW, neizangStateBtnH);
    [neizangView addSubview:neizangStateBtn];
    
    [neizangStateBtn setTitle:@"----" forState:UIControlStateNormal];
    [neizangStateBtn setBackgroundImage:[UIImage resizedImageWithName:@"标准"] forState:UIControlStateNormal];
    [neizangStateBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    UILabel *neizangValueL = [[UILabel alloc] init];
    self.neizangValueL = neizangValueL;
    CGFloat neizangValueLW = neizangStateBtnX - CGRectGetMaxX(neizangL.frame) - SYMargin20;
    CGFloat neizangValueLH = neizangViewH;
    CGFloat neizangValueLX = CGRectGetMaxX(neizangL.frame);
    CGFloat neizangValueLY = 0;
    neizangValueL.frame = CGRectMake(neizangValueLX, neizangValueLY, neizangValueLW, neizangValueLH);
    [neizangView addSubview:neizangValueL];
    neizangValueL.textAlignment = NSTextAlignmentRight;
    neizangValueL.text = bodyFatValueL.text = [NSString stringWithFormat:@"%.1f", _neiZang];;
    
    // 分割线
    UIView *marginNeizangView = [[UIView alloc] init];
    CGFloat marginNeizangViewX = SYMargin20;
    CGFloat marginNeizangViewH = 1;
    CGFloat marginNeizangViewY = neizangView.frame.size.height -  marginFatViewH;
    CGFloat marginNeizangViewW = neizangView.frame.size.width - 2 * SYMargin20;
    marginNeizangView.frame = CGRectMake(marginNeizangViewX, marginNeizangViewY, marginNeizangViewW, marginNeizangViewH);
    marginNeizangView.backgroundColor = [UIColor blackColor];
    marginNeizangView.alpha = 0.1;
    [neizangView addSubview:marginNeizangView];
    
    // 水分率
    UIImageView *waterImage = [[UIImageView alloc] init];
    CGFloat waterImageW = heightUnit - SYMargin5;
    CGFloat waterImageH = heightUnit - SYMargin5;
    CGFloat waterImageX = SYMargin20;
    CGFloat waterImageY = SYMargin5 / 2;
    waterImage.frame = CGRectMake(waterImageX, waterImageY, waterImageW, waterImageH);
    [waterView addSubview:waterImage];
    waterImage.image = [UIImage imageNamed:@"scale_water"];
    
    UILabel *waterL = [[UILabel alloc] init];
    waterL.text = @"水分率";
    NSMutableDictionary *dictAttrWater = [NSMutableDictionary dictionary];
    dictAttrWater[NSFontAttributeName] = [UIFont systemFontOfSize:17];
    CGSize waterLSize = [waterL.text sizeWithAttributes: dictAttrWater];
    CGFloat waterLW = waterLSize.width;
    CGFloat waterLH = waterViewH;
    CGFloat waterLX = CGRectGetMaxX(waterImage.frame) + SYMargin5;
    CGFloat waterLY = 0;
    waterL.frame = CGRectMake(waterLX, waterLY, waterLW, waterLH);
    [waterView addSubview:waterL];
    // 状态
    UIButton *waterStateBtn = [[UIButton alloc] init];
    waterStateBtn.userInteractionEnabled = NO;
    self.waterStateBtn = waterStateBtn;
    CGFloat waterStateBtnW = twoWordStrSize.width + SYMargin10;
    CGFloat waterStateBtnH = waterViewH * 0.7;
    CGFloat waterStateBtnX = waterViewW - SYMargin20 - waterStateBtnW;
    CGFloat waterStateBtnY = waterViewH * 0.3 * 0.5;
    waterStateBtn.frame = CGRectMake(waterStateBtnX, waterStateBtnY, waterStateBtnW, waterStateBtnH);
    [waterView addSubview:waterStateBtn];
    
    [waterStateBtn setTitle:@"----" forState:UIControlStateNormal];
    [waterStateBtn setBackgroundImage:[UIImage resizedImageWithName:@"标准"] forState:UIControlStateNormal];
    [waterStateBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    UILabel *waterValueL = [[UILabel alloc] init];
    self.waterValueL = waterValueL;
    CGFloat waterValueLW = waterStateBtnX - CGRectGetMaxX(waterL.frame) - SYMargin20;
    CGFloat waterValueLH = waterViewH;
    CGFloat waterValueLX = CGRectGetMaxX(waterL.frame);
    CGFloat waterValueLY = 0;
    waterValueL.frame = CGRectMake(waterValueLX, waterValueLY, waterValueLW, waterValueLH);
    [waterView addSubview:waterValueL];
    waterValueL.textAlignment = NSTextAlignmentRight;
    waterValueL.text = bodyFatValueL.text = [NSString stringWithFormat:@"%.1f", _shuiFen];;
    
    // 分割线
    UIView *marginWaterView = [[UIView alloc] init];
    CGFloat marginWaterViewX = SYMargin20;
    CGFloat marginWaterViewH = 1;
    CGFloat marginWaterViewY = waterView.frame.size.height -  marginFatViewH;
    CGFloat marginWaterViewW = waterView.frame.size.width - 2 * SYMargin20;
    marginWaterView.frame = CGRectMake(marginWaterViewX, marginWaterViewY, marginWaterViewW, marginWaterViewH);
    marginWaterView.backgroundColor = [UIColor blackColor];
    marginWaterView.alpha = 0.1;
    [waterView addSubview:marginWaterView];
    
    // 肌肉率
    UIImageView *jirouImage = [[UIImageView alloc] init];
    CGFloat jirouImageW = heightUnit - SYMargin5;
    CGFloat jirouImageH = heightUnit - SYMargin5;
    CGFloat jirouImageX = SYMargin20;
    CGFloat jirouImageY = SYMargin5 / 2;
    jirouImage.frame = CGRectMake(jirouImageX, jirouImageY, jirouImageW, jirouImageH);
    [jirouView addSubview:jirouImage];
    jirouImage.image = [UIImage imageNamed:@"scale_muscle"];
    
    UILabel *jirouL = [[UILabel alloc] init];
    jirouL.text = @"肌肉含量";
    NSMutableDictionary *dictAttrjirou = [NSMutableDictionary dictionary];
    dictAttrjirou[NSFontAttributeName] = [UIFont systemFontOfSize:17];
    CGSize jirouLSize = [jirouL.text sizeWithAttributes: dictAttrjirou];
    CGFloat jirouLW = jirouLSize.width;
    CGFloat jirouLH = jirouViewH;
    CGFloat jirouLX = CGRectGetMaxX(jirouImage.frame) + SYMargin5;
    CGFloat jirouLY = 0;
    jirouL.frame = CGRectMake(jirouLX, jirouLY, jirouLW, jirouLH);
    [jirouView addSubview:jirouL];
    // 状态
    UIButton *jirouStateBtn = [[UIButton alloc] init];
    jirouStateBtn.userInteractionEnabled = NO;
    self.jirouStateBtn = jirouStateBtn;
    CGFloat jirouStateBtnW = twoWordStrSize.width + SYMargin10;
    CGFloat jirouStateBtnH = jirouViewH * 0.7;
    CGFloat jirouStateBtnX = jirouViewW - SYMargin20 - jirouStateBtnW;
    CGFloat jirouStateBtnY = jirouViewH * 0.3 * 0.5;
    jirouStateBtn.frame = CGRectMake(jirouStateBtnX, jirouStateBtnY, jirouStateBtnW, jirouStateBtnH);
    [jirouView addSubview:jirouStateBtn];
    
    [jirouStateBtn setTitle:@"----" forState:UIControlStateNormal];
    [jirouStateBtn setBackgroundImage:[UIImage resizedImageWithName:@"标准"] forState:UIControlStateNormal];
    [jirouStateBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    UILabel *jirouValueL = [[UILabel alloc] init];
    self.jirouValueL = jirouValueL;
    CGFloat jirouValueLW = jirouStateBtnX - CGRectGetMaxX(jirouL.frame) - SYMargin20;
    CGFloat jirouValueLH = jirouViewH;
    CGFloat jirouValueLX = CGRectGetMaxX(jirouL.frame);
    CGFloat jirouValueLY = 0;
    jirouValueL.frame = CGRectMake(jirouValueLX, jirouValueLY, jirouValueLW, jirouValueLH);
    [jirouView addSubview:jirouValueL];
    jirouValueL.textAlignment = NSTextAlignmentRight;
    jirouValueL.text = bodyFatValueL.text = [NSString stringWithFormat:@"%.1f", _jiRou];;
    
    // 分割线
    UIView *marginJirouView = [[UIView alloc] init];
    CGFloat marginJirouViewX = SYMargin20;
    CGFloat marginJirouViewH = 1;
    CGFloat marginJirouViewY = jirouView.frame.size.height -  marginFatViewH;
    CGFloat marginJirouViewW = jirouView.frame.size.width - 2 * SYMargin20;
    marginJirouView.frame = CGRectMake(marginJirouViewX, marginJirouViewY, marginJirouViewW, marginJirouViewH);
    marginJirouView.backgroundColor = [UIColor blackColor];
    marginJirouView.alpha = 0.1;
    [jirouView addSubview:marginJirouView];
    
    // 骨含量
    UIImageView *gugeImage = [[UIImageView alloc] init];
    CGFloat gugeImageW = heightUnit - SYMargin5;
    CGFloat gugeImageH = heightUnit - SYMargin5;
    CGFloat gugeImageX = SYMargin20;
    CGFloat gugeImageY = SYMargin5 / 2;
    gugeImage.frame = CGRectMake(gugeImageX, gugeImageY, gugeImageW, gugeImageH);
    [gugeView addSubview:gugeImage];
    gugeImage.image = [UIImage imageNamed:@"scale_bones"];
    
    UILabel *gugeL = [[UILabel alloc] init];
    gugeL.text = @"骨含量";
    NSMutableDictionary *dictAttrguge = [NSMutableDictionary dictionary];
    dictAttrguge[NSFontAttributeName] = [UIFont systemFontOfSize:17];
    CGSize gugeLSize = [gugeL.text sizeWithAttributes: dictAttrguge];
    CGFloat gugeLW = gugeLSize.width;
    CGFloat gugeLH = gugeViewH;
    CGFloat gugeLX = CGRectGetMaxX(gugeImage.frame) + SYMargin5;
    CGFloat gugeLY = 0;
    gugeL.frame = CGRectMake(gugeLX, gugeLY, gugeLW, gugeLH);
    [gugeView addSubview:gugeL];
    // 状态
    UIButton *gugeStateBtn = [[UIButton alloc] init];
    gugeStateBtn.userInteractionEnabled = NO;
    self.gugeStateBtn = gugeStateBtn;
    CGFloat gugeStateBtnW = twoWordStrSize.width + SYMargin10;
    CGFloat gugeStateBtnH = gugeViewH * 0.7;
    CGFloat gugeStateBtnX = gugeViewW - SYMargin20 - gugeStateBtnW;
    CGFloat gugeStateBtnY = gugeViewH * 0.3 * 0.5;
    gugeStateBtn.frame = CGRectMake(gugeStateBtnX, gugeStateBtnY, gugeStateBtnW, gugeStateBtnH);
    [gugeView addSubview:gugeStateBtn];
    
    [gugeStateBtn setTitle:@"----" forState:UIControlStateNormal];
    [gugeStateBtn setBackgroundImage:[UIImage resizedImageWithName:@"标准"] forState:UIControlStateNormal];
    [gugeStateBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    UILabel *gugeValueL = [[UILabel alloc] init];
    self.gugeValueL = gugeValueL;
    CGFloat gugeValueLW = gugeStateBtnX - CGRectGetMaxX(gugeL.frame) - SYMargin20;
    CGFloat gugeValueLH = gugeViewH;
    CGFloat gugeValueLX = CGRectGetMaxX(gugeL.frame);
    CGFloat gugeValueLY = 0;
    gugeValueL.frame = CGRectMake(gugeValueLX, gugeValueLY, gugeValueLW, gugeValueLH);
    [gugeView addSubview:gugeValueL];
    gugeValueL.textAlignment = NSTextAlignmentRight;
    gugeValueL.text = bodyFatValueL.text = [NSString stringWithFormat:@"%.1f", _guGe];;
    
    // 分割线
    UIView *marginGugeView = [[UIView alloc] init];
    CGFloat marginGugeViewX = 0;
    CGFloat marginGugeViewH = 1;
    CGFloat marginGugeViewY = gugeView.frame.size.height -  marginFatViewH;
    CGFloat marginGugeViewW = gugeView.frame.size.width - 2 * SYMargin20;
    marginGugeView.frame = CGRectMake(marginGugeViewX, marginGugeViewY, marginGugeViewW, marginGugeViewH);
    marginGugeView.backgroundColor = [UIColor blackColor];
    marginGugeView.alpha = 0.1;
    [gugeImage addSubview:marginGugeView];
    
    
    // 基础代谢率
    UIImageView *reliangImage = [[UIImageView alloc] init];
    CGFloat reliangImageW = heightUnit - SYMargin5;
    CGFloat reliangImageH = heightUnit - SYMargin5;
    CGFloat reliangImageX = SYMargin20;
    CGFloat reliangImageY = SYMargin5 / 2;
    reliangImage.frame = CGRectMake(reliangImageX, reliangImageY, reliangImageW, reliangImageH);
    [reliangView addSubview:reliangImage];
    reliangImage.image = [UIImage imageNamed:@"scale_metabolism"];
    
    UILabel *reliangL = [[UILabel alloc] init];
    reliangL.text = @"基础代谢率";
    NSMutableDictionary *dictAttrre = [NSMutableDictionary dictionary];
    dictAttrre[NSFontAttributeName] = [UIFont systemFontOfSize:17];
    CGSize reliangLSize = [reliangL.text sizeWithAttributes: dictAttrre];
    CGFloat reliangLW = reliangLSize.width;
    CGFloat reliangLH = reliangViewH;
    CGFloat reliangLX = CGRectGetMaxX(reliangImage.frame) + SYMargin5;
    CGFloat reliangLY = 0;
    reliangL.frame = CGRectMake(reliangLX, reliangLY, reliangLW, reliangLH);
    [reliangView addSubview:reliangL];
    // 状态
    UIButton *reliangStateBtn = [[UIButton alloc] init];
    reliangStateBtn.userInteractionEnabled = NO;
    self.reliangStateBtn = reliangStateBtn;
    CGFloat reliangStateBtnW = twoWordStrSize.width + SYMargin10;
    CGFloat reliangStateBtnH = reliangViewH * 0.7;
    CGFloat reliangStateBtnX = reliangViewW - SYMargin20 - reliangStateBtnW;
    CGFloat reliangStateBtnY = reliangViewH * 0.3 * 0.5;
    reliangStateBtn.frame = CGRectMake(reliangStateBtnX, reliangStateBtnY, reliangStateBtnW, reliangStateBtnH);
    [reliangView addSubview:reliangStateBtn];
    
    [reliangStateBtn setTitle:@"----" forState:UIControlStateNormal];
    [reliangStateBtn setBackgroundImage:[UIImage resizedImageWithName:@"标准"] forState:UIControlStateNormal];
    [reliangStateBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    UILabel *reliangValueL = [[UILabel alloc] init];
    self.reliangValueL = reliangValueL;
    CGFloat reliangValueLW = reliangStateBtnX - CGRectGetMaxX(reliangL.frame) - SYMargin20;
    CGFloat reliangValueLH = reliangViewH;
    CGFloat reliangValueLX = CGRectGetMaxX(reliangL.frame) ;
    CGFloat reliangValueLY = 0;
    reliangValueL.frame = CGRectMake(reliangValueLX, reliangValueLY, reliangValueLW, reliangValueLH);
    [reliangView addSubview:reliangValueL];
    reliangValueL.textAlignment = NSTextAlignmentRight;
    reliangValueL.text = bodyFatValueL.text = [NSString stringWithFormat:@"%.1f", _reLiang];
}

// 初始化首页数据
- (void)setupData
{
//    self.resultDict = [NSKeyedUnarchiver unarchiveObjectWithFile:SYPathTestResult];
}

- (void)reloadText
{
    self.reliangValueL.text  = [NSString stringWithFormat:@"%.1f kcal", _reLiang];
    self.jirouValueL.text  = [NSString stringWithFormat:@"%.1f kg", _jiRou];
    self.bmiValueL_2.text = [NSString stringWithFormat:@"%.1f", _bmi];
    self.weightValueL.text = [NSString stringWithFormat:@"%.1f", _weight];
    self.gugeValueL.text = [NSString stringWithFormat:@"%.1f kg", _guGe];
    self.waterValueL.text = [NSString stringWithFormat:@"%.1f％", _shuiFen];
    self.neizangValueL.text = [NSString stringWithFormat:@"%.1f", _neiZang];
    self.bodyFatL.text = [NSString stringWithFormat:@"%.1f％", _fat];
    self.bmiValueL.text = [NSString stringWithFormat:@"%.1f", _bmi];
    
    //判断状况
    [self judgeState];
}

- (void)judgeState
{
    if (_reLiang != 0.0) {
        [self.reliangStateBtn setTitle:@"标准" forState:UIControlStateNormal];
        [self.reliangStateBtn setBackgroundImage:[UIImage resizedImageWithName:@"标准"] forState:UIControlStateNormal];
   
    
    if (_shuiFen >= 450 && _shuiFen <= 600) {
        [self.waterStateBtn setTitle:@"标准" forState:UIControlStateNormal];
        [self.waterStateBtn setBackgroundImage:[UIImage resizedImageWithName:@"标准"] forState:UIControlStateNormal];
    } else if (_shuiFen < 450) {
        [self.waterStateBtn setTitle:@"偏低" forState:UIControlStateNormal];
        [self.waterStateBtn setBackgroundImage:[UIImage resizedImageWithName:@"偏瘦"] forState:UIControlStateNormal];
    } else if (_shuiFen > 600) {
        [self.waterStateBtn setTitle:@"偏高" forState:UIControlStateNormal];
        [self.waterStateBtn setBackgroundImage:[UIImage resizedImageWithName:@"偏高"] forState:UIControlStateNormal];
    }
    
    
    if (_neiZang <= 9) {
        // 正常 <0.49
        [self.neizangStateBtn setTitle:@"标准" forState:UIControlStateNormal];
        [self.neizangStateBtn setBackgroundImage:[UIImage resizedImageWithName:@"标准"] forState:UIControlStateNormal];
    } else if (_neiZang >= 10 && _neiZang <= 14) {
        //            // 偏高 < 0.77
        [self.neizangStateBtn setTitle:@"偏高" forState:UIControlStateNormal];
        [self.neizangStateBtn setBackgroundImage:[UIImage resizedImageWithName:@"偏高"] forState:UIControlStateNormal];
    } else {
        //            CGAffineTransform
        // 严重偏高 > 0.77
        [self.neizangStateBtn setTitle:@"偏高" forState:UIControlStateNormal];
        [self.neizangStateBtn setBackgroundImage:[UIImage resizedImageWithName:@"偏高"] forState:UIControlStateNormal];
    }
    
    SYLog(@"肌肉含量  女");
        float jiRouPercent = _jiRou / _weight * 100;
    if (_height < 150) {
        SYLog(@"身高<1.60");
        if (jiRouPercent <= 29.1) {
            SYLog(@"低标准");
            [self.jirouStateBtn setTitle:@"偏低" forState:UIControlStateNormal];
            [self.jirouStateBtn setBackgroundImage:[UIImage resizedImageWithName:@"偏瘦"] forState:UIControlStateNormal];
            
        } else if (jiRouPercent > 29.1 && jiRouPercent <= 34.7) {
            SYLog(@"正常");
            [self.jirouStateBtn setTitle:@"标准" forState:UIControlStateNormal];
            [self.jirouStateBtn setBackgroundImage:[UIImage resizedImageWithName:@"标准"] forState:UIControlStateNormal];
        } else {
            SYLog(@"高标准");
            [self.jirouStateBtn setTitle:@"偏高" forState:UIControlStateNormal];
            [self.jirouStateBtn setBackgroundImage:[UIImage resizedImageWithName:@"偏高"] forState:UIControlStateNormal];
        }
    } else if (_height >= 150 && _height <= 160) {
        SYLog(@"身高> 1.60 ,<=1.70");
        if (jiRouPercent <=  32.9) {
            SYLog(@"低标准");
            [self.jirouStateBtn setTitle:@"偏低" forState:UIControlStateNormal];
            [self.jirouStateBtn setBackgroundImage:[UIImage resizedImageWithName:@"偏瘦"] forState:UIControlStateNormal];
            
        } else if (jiRouPercent > 32.9 && jiRouPercent <= 37.5) {
            SYLog(@"正常");
            [self.jirouStateBtn setTitle:@"标准" forState:UIControlStateNormal];
            [self.jirouStateBtn setBackgroundImage:[UIImage resizedImageWithName:@"标准"] forState:UIControlStateNormal];
        } else {
            SYLog(@"高标准");
            [self.jirouStateBtn setTitle:@"偏高" forState:UIControlStateNormal];
            [self.jirouStateBtn setBackgroundImage:[UIImage resizedImageWithName:@"偏高"] forState:UIControlStateNormal];
        }
        
    } else {
        SYLog(@"身高>1.60");
        if (jiRouPercent <=  36.5) {
            SYLog(@"低标准");
            [self.jirouStateBtn setTitle:@"偏低" forState:UIControlStateNormal];
            [self.jirouStateBtn setBackgroundImage:[UIImage resizedImageWithName:@"偏瘦"] forState:UIControlStateNormal];
            
        } else if (jiRouPercent > 36.5 && jiRouPercent <= 42.5) {
            SYLog(@"正常");
            [self.jirouStateBtn setTitle:@"标准" forState:UIControlStateNormal];
            [self.jirouStateBtn setBackgroundImage:[UIImage resizedImageWithName:@"标准"] forState:UIControlStateNormal];
        } else {
            SYLog(@"高标准");
            [self.jirouStateBtn setTitle:@"偏高" forState:UIControlStateNormal];
            [self.jirouStateBtn setBackgroundImage:[UIImage resizedImageWithName:@"偏高"] forState:UIControlStateNormal];
        }
    }
    
    SYLog(@"骨骼  女");
    if (_weight <= 45) {
        SYLog(@"体重<60");
        if (_guGe <= 1.3) {
            SYLog(@"低标准");
            [self.gugeStateBtn setTitle:@"偏低" forState:UIControlStateNormal];
            [self.gugeStateBtn setBackgroundImage:[UIImage resizedImageWithName:@"偏瘦"] forState:UIControlStateNormal];
            
        } else if (_guGe > 1.3 && _guGe <= 2.3) {
            SYLog(@"正常");
            [self.gugeStateBtn setTitle:@"标准" forState:UIControlStateNormal];
            [self.gugeStateBtn setBackgroundImage:[UIImage resizedImageWithName:@"标准"] forState:UIControlStateNormal];
        } else {
            SYLog(@"高标准");
            [self.gugeStateBtn setTitle:@"偏高" forState:UIControlStateNormal];
            [self.gugeStateBtn setBackgroundImage:[UIImage resizedImageWithName:@"偏高"] forState:UIControlStateNormal];
        }
    } else if (_weight > 45 && _weight <= 60) {
        SYLog(@"体重>60, <75");
        if (_guGe <= 1.7) {
            SYLog(@"低标准");
            [self.gugeStateBtn setTitle:@"偏低" forState:UIControlStateNormal];
            [self.gugeStateBtn setBackgroundImage:[UIImage resizedImageWithName:@"偏瘦"] forState:UIControlStateNormal];
            
        } else if (_guGe > 1.7 && _guGe <= 2.7) {
            SYLog(@"正常");
            [self.gugeStateBtn setTitle:@"标准" forState:UIControlStateNormal];
            [self.gugeStateBtn setBackgroundImage:[UIImage resizedImageWithName:@"标准"] forState:UIControlStateNormal];
        } else {
            SYLog(@"高标准");
            [self.gugeStateBtn setTitle:@"偏高" forState:UIControlStateNormal];
            [self.gugeStateBtn setBackgroundImage:[UIImage resizedImageWithName:@"偏高"] forState:UIControlStateNormal];
        }
    } else {
        SYLog(@"体重>75");
        if (_guGe <= 2.0) {
            SYLog(@"低标准");
            [self.gugeStateBtn setTitle:@"偏低" forState:UIControlStateNormal];
            [self.gugeStateBtn setBackgroundImage:[UIImage resizedImageWithName:@"偏瘦"] forState:UIControlStateNormal];
            //
        } else if (_guGe > 2.0 && _guGe <= 3.0) {
            SYLog(@"正常");
            [self.gugeStateBtn setTitle:@"标准" forState:UIControlStateNormal];
            [self.gugeStateBtn setBackgroundImage:[UIImage resizedImageWithName:@"标准"] forState:UIControlStateNormal];
        } else {
            SYLog(@"高标准");
            [self.gugeStateBtn setTitle:@"偏高" forState:UIControlStateNormal];
            [self.gugeStateBtn setBackgroundImage:[UIImage resizedImageWithName:@"偏高"] forState:UIControlStateNormal];
        }
    }
    
    
        if (_fat < 21) {
            // 偏瘦
            SYLog(@"39偏瘦");
            [self.bodyFatStateBtn setTitle:@"偏瘦" forState:UIControlStateNormal];
            [self.bodyFatStateBtn setBackgroundImage:[UIImage resizedImageWithName:@"偏瘦"] forState:UIControlStateNormal];
            
        } else if (_fat >= 21 && _fat < 28 ) {
            // 健康
            SYLog(@"39健康");
            [self.bodyFatStateBtn setTitle:@"标准" forState:UIControlStateNormal];
            [self.bodyFatStateBtn setBackgroundImage:[UIImage resizedImageWithName:@"标准"] forState:UIControlStateNormal];
        } else if (_fat >= 28) {
            // 爆表
            [self.bodyFatStateBtn setTitle:@"偏胖" forState:UIControlStateNormal];
            [self.bodyFatStateBtn setBackgroundImage:[UIImage resizedImageWithName:@"偏高"] forState:UIControlStateNormal];
            
        }
     }
    if (_bmi < 18.5) {
        [self.bmiStateBtn setTitle:@"偏瘦" forState:UIControlStateNormal];
        [self.bmiStateBtn_2 setTitle:@"偏瘦" forState:UIControlStateNormal];
        [self.bmiStateBtn setBackgroundImage:[UIImage resizedImageWithName:@"偏瘦"] forState:UIControlStateNormal];
        [self.bmiStateBtn_2 setBackgroundImage:[UIImage resizedImageWithName:@"偏瘦"] forState:UIControlStateNormal];
    } else if (_bmi < 24) {
        [self.bmiStateBtn setTitle:@"标准" forState:UIControlStateNormal];
        [self.bmiStateBtn_2 setTitle:@"标准" forState:UIControlStateNormal];
        [self.bmiStateBtn setBackgroundImage:[UIImage resizedImageWithName:@"标准"] forState:UIControlStateNormal];
        [self.bmiStateBtn_2 setBackgroundImage:[UIImage resizedImageWithName:@"标准"] forState:UIControlStateNormal];
    } else if (_bmi < 28) {
        [self.bmiStateBtn setTitle:@"偏胖" forState:UIControlStateNormal];
        [self.bmiStateBtn_2 setTitle:@"偏胖" forState:UIControlStateNormal];
        [self.bmiStateBtn setBackgroundImage:[UIImage resizedImageWithName:@"偏高"] forState:UIControlStateNormal];
        [self.bmiStateBtn_2 setBackgroundImage:[UIImage resizedImageWithName:@"偏高"] forState:UIControlStateNormal];
    } else {
        [self.bmiStateBtn setTitle:@"偏胖" forState:UIControlStateNormal];
        [self.bmiStateBtn_2 setTitle:@"偏胖" forState:UIControlStateNormal];
        [self.bmiStateBtn setBackgroundImage:[UIImage resizedImageWithName:@"偏高"] forState:UIControlStateNormal];
        [self.bmiStateBtn_2 setBackgroundImage:[UIImage resizedImageWithName:@"偏高"] forState:UIControlStateNormal];
    }
    
}


//上传设备测量数据
- (void)getUserWeightFatData
{
    
    
    AFHTTPRequestOperationManager *mgr = [AFHTTPRequestOperationManager manager];
    NSDateComponents *tempComp  = [self convertFromDateToDateComp:[NSDate date]];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:[passValue passValue].passValueName forKey:@"account"];
    [params setObject:[passValue passValue].saveDataUserName forKey:@"username"];
    params[@"devName"] = @"zfc";
    [params setObject:@"aabb" forKey:@"devNo"];
    [params setObject:@1 forKey:@"version"];
    [params setObject:@"hs" forKey:@"from"];
    
    
    params[@"data"] = [NSString stringWithFormat:@"time_%ld%02ld%02ld050000-bmi_%.1f-bone_%.1f-kcal_%.1f-muscle_%.1f-viscerafat_%.1f-water_%.1f-weight_%.1f-fat_%.1f", (long)tempComp.year, (long)tempComp.month, (long)tempComp.day, _bmi, _guGe, _reLiang, _jiRou, _neiZang, _shuiFen, _weight, _fat];
    
    [mgr POST:URL_Health_baby_saveUserData parameters:params success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        NSLog(@"response == %@", [responseObject objectForKey:@"response"]);
        if ([responseObject objectForKey:@"response"]) {
            [MBProgressHUD showSuccess:@"上传成功"];
        }else {
        
            [MBProgressHUD showError:@"上传失败"];
        }
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [MBProgressHUD showError:@"上传失败"];
    }];
}




- (NSDateComponents *)convertFromDateToDateComp:(NSDate *)tempDate
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    int unit = NSCalendarUnitDay | NSCalendarUnitMonth |  NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitWeekday;
    
    // 1.获得当前时间的年月日
    NSDateComponents *nowCmps = [calendar components:unit fromDate:tempDate];
    
    return nowCmps;
}


@end
