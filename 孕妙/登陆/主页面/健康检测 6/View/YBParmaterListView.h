//
//  YBParmaterListView.h
//  孕妙
//
//  Created by sayes1 on 16/1/22.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YBParmaterListView : UIView

/**
 * 传入的数据模型YBHealthParmater
 */
@property (nonatomic, strong) NSArray *pamaterArray;
@property (nonatomic, assign)NSInteger indexF;
+ (instancetype)parmaterListViewWithframe:(CGRect)frame titleStr:(NSString *)titleStr parmaterName:(NSString *)parmaterName;
+ (instancetype)parmaterListViewWithframe:(CGRect)frame titleStr:(NSString *)titleStr parmaterName:(NSString *)parmaterName index:(NSInteger)index;
@end
