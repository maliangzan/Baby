//
//  YBParameterListView.m
//  健康监测
//
//  Created by sayes1 on 16/1/11.
//  Copyright © 2016年 sayes1. All rights reserved.
//
#define DeviceHeight [UIScreen mainScreen].bounds.size.height
#define DeviceWidth  [UIScreen mainScreen].bounds.size.width
// 2.获得RGB颜色
#define SYColor(r, g, b) ([UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1.0])

#import "YBParameterListView.h"

@implementation YBParameterListView

+ (instancetype)parameterListViewIconImage:(NSString *)iconName name:(NSString *)name parmeterValue:(NSString *)parmeterVStr frame:(CGRect)frame hasUnderlined:(BOOL)isHas
{
    YBParameterListView *plView = [[self alloc] initWithFrame:frame];
    plView.iconImage.image = [UIImage imageNamed:iconName];
    plView.nameL.text = name;
    plView.parmeterValue.text = parmeterVStr;
    if (!isHas) {
        plView.underLine.hidden = YES;
    }
    return plView;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self setupSubView];
    }
    return self;
}

- (void)setupSubView
{
    self.clickBtn = [[UIButton alloc] init];
    [self addSubview:self.clickBtn];
    
    self.iconImage = [[UIImageView alloc] init];
    [self addSubview:self.iconImage];
    
    self.nameL = [[UILabel alloc] init];
    self.nameL.textColor = SYColor(151, 51, 98);
    [self addSubview:self.nameL];
    
    self.parmeterValue = [[UILabel alloc] init];
    self.parmeterValue.textAlignment = NSTextAlignmentRight;
    self.parmeterValue.textColor = SYColor(106, 66, 102);
    [self addSubview:self.parmeterValue];
    
    self.rightArrowImage = [[UIImageView alloc] init];
    self.rightArrowImage.image = [UIImage imageNamed:@"YB_rightArrow"];
//    self.rightArrowImage.backgroundColor = [UIColor redColor];
    self.rightArrowImage.contentMode = UIViewContentModeCenter;
    [self addSubview:self.rightArrowImage];
    
    self.underLine = [[UIView alloc] init];
    self.underLine.backgroundColor = [UIColor blackColor];
    self.underLine.alpha = 0.2;
    [self addSubview:self.underLine];
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    self.clickBtn.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);

    self.iconImage.frame = CGRectMake(20, 5, self.frame.size.height - 10, self.frame.size.height - 10);
    self.rightArrowImage.frame = CGRectMake(self.frame.size.width - 20 - self.frame.size.height, 0, self.frame.size.height, self.frame.size.height);
    self.nameL.frame = CGRectMake(CGRectGetMaxX(self.iconImage.frame) + 20, 0, 100, self.frame.size.height);
    self.parmeterValue.frame = CGRectMake(CGRectGetMaxX(self.nameL.frame), 0, self.rightArrowImage.frame.origin.x - CGRectGetMaxX(self.nameL.frame), self.frame.size.height);
    self.underLine.frame = CGRectMake(20, self.frame.size.height - 1, DeviceWidth - 40, 1);
}

@end
