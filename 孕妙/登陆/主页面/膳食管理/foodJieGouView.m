//
//  foodJieGouView.m
//  孕妙
//
//  Created by 是源科技 on 16/6/30.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "foodJieGouView.h"

@implementation foodJieGouView

/**首页今日膳食结构*/
-(void)setViewTitle:(NSString *)titleName andTotallabel:(NSString *)totalCount andSheruCount:(NSString *)sheruCount
{
    CGFloat w = self.frame.size.width;
    CGFloat h = self.frame.size.height;
    CGFloat y = h / 6.0;
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(8, y, w / 2.0, h / 4.0)];
    titleLabel.textColor = YMColor(123, 123, 123);
    titleLabel.text = titleName;
    [self addSubview:titleLabel];
    titleLabel.font = [UIFont systemFontOfSize:15.0f];
    
    UILabel *totalCountLabel = [[UILabel alloc]initWithFrame:CGRectMake(w / 2.0, y, w / 2.0 - 8, h / 4.0)];
    totalCountLabel.textColor = YMColor(123, 123, 123);
    totalCountLabel.text = [NSString stringWithFormat:@"(%@份)",totalCount];
    [self addSubview:totalCountLabel];
    totalCountLabel.adjustsFontSizeToFitWidth = YES;
    
    y += h / 5.0 + 5;
    UILabel *sheruLabel = [[UILabel alloc]initWithFrame:CGRectMake(8, y, w / 2.0 + 15, h / 4.0)];
    sheruLabel.textColor = YMColor(163, 163, 163);
    sheruLabel.text = @"已摄入";
    sheruLabel.font = [UIFont systemFontOfSize:14.5f];
    [self addSubview:sheruLabel];
    
    UILabel *sheRuCountLabel = [[UILabel alloc]initWithFrame:CGRectMake(w / 2.0 + 15, y, w / 2.0 - 23, h / 4.0)];
    sheRuCountLabel.textColor = YMColor(163, 163, 163);
    sheRuCountLabel.text = [NSString stringWithFormat:@"%@",sheruCount];
    sheRuCountLabel.textAlignment = NSTextAlignmentCenter;
    sheRuCountLabel.adjustsFontSizeToFitWidth = YES;
    [self addSubview:sheRuCountLabel];
    
    y += h / 5.0 + 5;
    UILabel *shengyuLabel = [[UILabel alloc]initWithFrame:CGRectMake(8, y, w / 2.0 + 15, h / 4.0)];
    shengyuLabel.textColor = YMColor(163, 163, 163);
    shengyuLabel.text = @"剩余";
    shengyuLabel.font = [UIFont systemFontOfSize:14.5f];
    [self addSubview:shengyuLabel];
    
    UILabel *shengyuCountLabel = [[UILabel alloc]initWithFrame:CGRectMake(w / 2.0 + 15, y, w / 2.0 - 23, h / 4.0)];
    shengyuCountLabel.textColor = YMColor(163, 163, 163);
    shengyuCountLabel.textAlignment = NSTextAlignmentCenter;
    shengyuCountLabel.font = [UIFont systemFontOfSize:14.0f];
    shengyuCountLabel.adjustsFontSizeToFitWidth = YES;
    [self addSubview:shengyuCountLabel];
    
    
    CGFloat totalInt = 0;
    CGFloat sheRuInt= 0;
    if(totalCount)
    {
       totalInt = [totalCount floatValue];
    }
    if(sheruCount)
    {
        sheRuInt = [sheruCount floatValue];
    }
    float shengyuInt = 0;
    if(totalInt >= sheRuInt)
    {
        shengyuInt = totalInt - sheRuInt;
    }
    else
    {
        shengyuInt = sheRuInt - totalInt;
        shengyuLabel.text = @"已超标";
        shengyuLabel.textColor = [UIColor redColor];
        shengyuCountLabel.textColor = [UIColor redColor];
    }
    shengyuCountLabel.text = [NSString stringWithFormat:@"%.1f",shengyuInt];
    
}

/**添加食物页面的图片按钮和文字*/
-(void)addButtonAndNameToView:(UIImage *)buttonImage andTitle:(NSString *)titleName
{
    CGFloat w = self.frame.size.width;
    CGFloat h = w;
    
    UIButton *imageButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, w, h)];
    [imageButton setImage:buttonImage forState:UIControlStateNormal];
    self.imageButton = imageButton;
    imageButton.tag = self.tag;
    [self addSubview:imageButton];
    
    UILabel *titlaLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, h, w, 30)];
    titlaLabel.textColor = YMColor(107, 107, 107);
    titlaLabel.text = titleName;
    titlaLabel.textAlignment = NSTextAlignmentCenter;
    titlaLabel.font = [UIFont systemFontOfSize:15.0f];
    [self addSubview:titlaLabel];
    
    imageButton.layer.borderWidth = 1.0f;
    imageButton.layer.borderColor = [YMColor(216, 216, 216)CGColor];
}

/**膳食原则界面布局*/
-(void)shanshiYuanzeLayoutaddImage:(NSString *)imageName andTitle:(NSString *)titleName
{
    CGFloat w = self.frame.size.width;
    CGFloat h = self.frame.size.height;
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(25, h / 4.0, h / 2.0 / 1.09375, h / 2.0)];
    imageView.image = [UIImage imageNamed:imageName];
    [self addSubview:imageView];
    
    UILabel *titlelabel = [[UILabel alloc]initWithFrame:CGRectMake(w / 4.0 - 8, 0, w , h)];
    titlelabel.text = titleName;
    titlelabel.font = [UIFont systemFontOfSize:16.0f];
    titlelabel.textColor = YMColor(193, 86, 144);
    [self addSubview:titlelabel];
    
    UIImageView *jiantouImage = [[UIImageView alloc]initWithFrame:CGRectMake(w - 15 - h / 3.0, h / 3.0, h / 3.0, h / 3.0)];
    jiantouImage.image = [UIImage imageNamed:@"personal_》small"];
    self.jiantouimage = jiantouImage;
    [self addSubview:jiantouImage];
    
    UIButton *btn = [[UIButton alloc]initWithFrame:self.bounds];
    btn.tag = self.tag;
    [self addSubview:btn];
    self.zhankaiButton = btn;    
}

@end
