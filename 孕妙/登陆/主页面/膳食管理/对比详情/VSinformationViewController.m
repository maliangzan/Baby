//
//  VSinformationViewController.m
//  孕妙
//
//  Created by 是源科技 on 16/7/5.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "VSinformationViewController.h"
#import "VSAddFoodViewController.h"
#import "foodExchange.h"

@interface VSinformationViewController ()
@property (nonatomic,strong)NSArray *yingyangArray;
@property (nonatomic,weak)UIView *leftView;
@property (nonatomic,weak)UIView *rightView;
@property (nonatomic,weak)UIButton *leftBtn;
@property (nonatomic,weak)UIButton *rightBtn;

@property (nonatomic,weak)UIButton *CloseBtnL;
@property (nonatomic,weak)UIButton *CloseBtnR;

@property (nonatomic,strong)NSMutableArray *leftArrayM;
@property (nonatomic,strong)NSMutableArray *rightArrayM;

@property (nonatomic,assign)BOOL flagL;
@property (nonatomic,assign)BOOL flagR;



@end

@implementation VSinformationViewController

-(NSArray *)yingyangArray
{
    if(!_yingyangArray)
    {
        _yingyangArray = @[@"热量",@"水分",@"蛋白质",@"脂肪",@"膳食纤维",@"碳水化合物",@"维生素A",@"维生素B1",@"维生素B2",@"维生素C",@"维生素E",@"烟酸",@"钠",@"钙",@"铁"];
    }
    return _yingyangArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"对比详情";
    self.view.backgroundColor = YMColor(245, 245, 245);
    [self controllViewLayout];
    self.leftArrayM = [NSMutableArray array];
    self.rightArrayM = [NSMutableArray array];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSMutableArray *dataArray = [NSMutableArray array];
    foodExchange *food = [passValue passValue].selectFood;
    if(food)
    {
        YMLog(@"food = %@",food);
        [dataArray addObject:[NSString stringWithFormat:@"%.1f千卡",[food.calori floatValue]]];
        [dataArray addObject:[NSString stringWithFormat:@"%.1f克",[food.water floatValue]]];
        [dataArray addObject:[NSString stringWithFormat:@"%.1f克",[food.protein floatValue]]];
        [dataArray addObject:[NSString stringWithFormat:@"%.1f克",[food.fat floatValue]]];
        [dataArray addObject:[NSString stringWithFormat:@"%.1f克",[food.df floatValue]]];
        [dataArray addObject:[NSString stringWithFormat:@"%.1f克",[food.carbohydrate floatValue]]];
        [dataArray addObject:[NSString stringWithFormat:@"%.1f毫克",[food.va floatValue]]];
        [dataArray addObject:[NSString stringWithFormat:@"%.1f毫克",[food.vb1 floatValue]]];
        [dataArray addObject:[NSString stringWithFormat:@"%.1f毫克",[food.vb2 floatValue]]];
        [dataArray addObject:[NSString stringWithFormat:@"%.1f毫克",[food.vc floatValue]]];
        [dataArray addObject:[NSString stringWithFormat:@"%.1f毫克",[food.ve floatValue]]];
        [dataArray addObject:[NSString stringWithFormat:@"%.1f毫克",[food.niacin floatValue]]];
        [dataArray addObject:[NSString stringWithFormat:@"%.1f毫克",[food.na floatValue]]];
        [dataArray addObject:[NSString stringWithFormat:@"%.1f毫克",[food.ca floatValue]]];
        [dataArray addObject:[NSString stringWithFormat:@"%.1f毫克",[food.fe floatValue]]];
        [self downLoadBabyImageFromInternet:food.imgSrc];
        if(self.flagL == YES)
        {
            self.leftArrayM = [dataArray copy];
            for (UILabel *label in self.leftView.subviews) {
                for(int i = 0;i < 15;i ++)
                {
                    if(i == label.tag)
                    {
                        label.text = [NSString stringWithFormat:@"%@",dataArray[i]];
                    }
                }
            }
        }
        if(self.flagR == YES)
        {
            self.rightArrayM = [dataArray copy];
            for (UILabel *label in self.rightView.subviews) {
                for(int i = 0;i < 15;i ++)
                {
                    if(i == label.tag)
                    {
                        label.text = [NSString stringWithFormat:@"%@",dataArray[i]];
                    }
                }
            }
        }
    }
}

//从网络下载用户头像
-(void)downLoadBabyImageFromInternet:(NSString *)str
{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *str1 = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url1 = [NSURL URLWithString:str1];
        NSData *resultData1 = [NSData dataWithContentsOfURL:url1];
        UIImage *img1 = [UIImage imageWithData:resultData1];
        dispatch_async(dispatch_get_main_queue(), ^{
            if(self.flagL == YES)
            {
                [self.leftBtn setImage:img1 forState:UIControlStateNormal];
                self.CloseBtnL.hidden = NO;
            }
            if(self.flagR == YES)
            {
                [self.rightBtn setImage:img1 forState:UIControlStateNormal];
                self.CloseBtnR.hidden = NO;
            }
        });
    });
}

-(void)controllViewLayout
{
    CGFloat y = 100.f;
    
    UIView *buttonView = [[UIView alloc]initWithFrame:CGRectMake(0, y, kDeviceWidth, 100)];
    [self.view addSubview:buttonView];
    
    [self addbuttonToView:buttonView];
    
    y += buttonView.frame.size.height + 34;
    
    UILabel *yingyangLabel = [[UILabel alloc]initWithFrame:CGRectMake(-1, y, kDeviceWidth + 2, 40)];
    yingyangLabel.backgroundColor = [UIColor whiteColor];
    yingyangLabel.textAlignment = NSTextAlignmentCenter;
    yingyangLabel.textColor = YMColor(191, 87, 140);
    yingyangLabel.text = @"营养元素";
    yingyangLabel.layer.borderWidth = 1.0f;
    yingyangLabel.layer.borderColor = [YMColor(224, 224, 224)CGColor];
    [self.view addSubview:yingyangLabel];
    
     y += yingyangLabel.frame.size.height;
    //表格View
    UIView *biaogeView = [[UIView alloc]initWithFrame:CGRectMake(-1, y, kDeviceWidth + 2, kDeviceHeight - y - 1)];
    [self.view addSubview:biaogeView];
    [self addLineToview:biaogeView];
    
    CGFloat viewW = kDeviceWidth / 3.0;
    CGFloat viewH = kDeviceHeight - y;
    for(int i = 0;i < 3;i ++)
    {
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(viewW * i, y, viewW, viewH)];
        [self.view addSubview:view];
        view.tag = i;
        if(0 == view.tag)
        {
            self.leftView = view;
        }
        if(2 == view.tag)
        {
            self.rightView = view;
        }
        [self addLabelToView:view];
    }
}
//表格线
-(void)addLineToview:(UIView *)view
{
    CGFloat w = view.frame.size.width;
    CGFloat h = view.frame.size.height;
    CGFloat labelW = w / 3.0;
    CGFloat labelH = h / 15.0;
    for(int i = 1;i < 3;i ++)
    {
        UILabel *shuLabel = [[UILabel alloc]initWithFrame:CGRectMake(labelW * i, 0, 1, h)];
        shuLabel.backgroundColor = YMColor(224, 224, 224);
        [view addSubview:shuLabel];
    }
    for(int i = 1;i < 16;i ++)
    {
        UILabel *crosslabel = [[UILabel alloc]initWithFrame:CGRectMake(0, labelH * i, w, 1)];
        crosslabel.backgroundColor = YMColor(224, 224, 224);
        [view addSubview:crosslabel];
    }
}



-(void)addLabelToView:(UIView *)view
{
    CGFloat w = view.frame.size.width;
    CGFloat h = view.frame.size.height;
    CGFloat labelH = h / 15.0;
    CGFloat labelW = w;
    for(int i = 0; i < 15;i ++)
    {
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, labelH * i, labelW, labelH)];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont systemFontOfSize:14.0f];
        label.tag = i;
        [view addSubview:label];
        if(view.tag == 1)
        {
            label.text = self.yingyangArray[i];
        }
        else
        {
            label.text = @"0";
        }
    }
}



/**添加按钮*/
-(void)addbuttonToView:(UIView *)view
{
    CGFloat w = view.frame.size.width;
    CGFloat h = view.frame.size.height;
    
    CGFloat spaceX = (w - h * 3) * 0.5;
    
    UIButton *btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn1.frame = CGRectMake(spaceX, 0, h, h);
    [btn1 setImage:[UIImage imageNamed:@"ssgl_addFrame"] forState:UIControlStateNormal];
    btn1.tag = 1;
    [view addSubview:btn1];
    
    UIButton *btn2 = [[UIButton alloc]initWithFrame:CGRectMake(w - h - spaceX, 0, h, h)];
    [btn2 setImage:[UIImage imageNamed:@"ssgl_addFrame"] forState:UIControlStateNormal];
    btn2.tag = 2;
    [view addSubview:btn2];
    
    self.leftBtn = btn1;
    self.rightBtn = btn2;
    
    CGFloat imageW = h / 2.0;
    UIImageView *imageVS = [[UIImageView alloc]initWithFrame:CGRectMake(w * 0.5 - imageW * 0.5, h / 4.0, imageW, imageW)];
    imageVS.image = [UIImage imageNamed:@"ssgl_foodVSicon"];
    [view addSubview:imageVS];
    
    [btn1 addTarget:self action:@selector(addFood:) forControlEvents:UIControlEventTouchUpInside];
    [btn2 addTarget:self action:@selector(addFood:) forControlEvents:UIControlEventTouchUpInside];
    
     CGFloat btnW = btn1.frame.size.width;
    for(int i = 0;i < 2; i++)
    {
        UIButton *closeButton = [[UIButton alloc]initWithFrame:CGRectMake(btnW - 22, -8, 30, 30)];
        [closeButton setImage:[UIImage imageNamed:@"ssgl_deltfood"] forState:UIControlStateNormal];
        closeButton.tag = i;
        closeButton.hidden = YES;
        [closeButton addTarget:self action:@selector(clickCloseButton:) forControlEvents:UIControlEventTouchUpInside];
        if(0 == closeButton.tag)
        {
            self.CloseBtnL = closeButton;
            [btn1 addSubview:closeButton];
        }
        if(1 == closeButton.tag)
        {
            self.CloseBtnR = closeButton;
             [btn2 addSubview:closeButton];
        }
    }
}


-(void)addFood:(UIButton *)btn
{
    if(1 == btn.tag)
    {
        self.flagL = YES;
        self.flagR = NO;
    }
    else
    {
        self.flagR = YES;
        self.flagL = NO;
    }
    [passValue passValue].selectFood = nil;
    VSAddFoodViewController *vsadd = [[VSAddFoodViewController alloc]init];
    [self.navigationController pushViewController:vsadd animated:YES];
}

-(void)clickCloseButton:(UIButton *)btn
{
    if(0 == btn.tag)
    {
        
        self.leftArrayM = [NSMutableArray array];
        [self.leftBtn setImage:[UIImage imageNamed:@"ssgl_addFrame"] forState:UIControlStateNormal];
        self.CloseBtnL.hidden = YES;
        for (UILabel *label in self.leftView.subviews) {
            label.text = @"0";
        }
    }
    else
    {
        self.rightArrayM = [NSMutableArray array];
        [self.rightBtn setImage:[UIImage imageNamed:@"ssgl_addFrame"] forState:UIControlStateNormal];
        self.CloseBtnR.hidden = YES;
        for (UILabel *label in self.rightView.subviews) {
            label.text = @"0";
        }

    }
    self.flagL = NO;
    self.flagR = NO;
    [passValue passValue].selectFood = nil;
}

@end
