//
//  foodExchange.h
//  孕妙
//
//  Created by 是源科技 on 16/7/14.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface foodExchange : NSObject
@property (nonatomic,copy)NSString *ca; //钙
@property (nonatomic,copy)NSString *calori; //能量
@property (nonatomic,copy)NSString *carbohydrate; //碳水化合物
@property (nonatomic,copy)NSString *category; //种类
@property (nonatomic,copy)NSString *cholesterol; // 胆固醇
@property (nonatomic,copy)NSString *comestible; // 重量
@property (nonatomic,copy)NSString *df; //膳食纤维
@property (nonatomic,copy)NSString *fat; // 脂肪
@property (nonatomic,copy)NSString *fe; // 铁
@property (nonatomic,copy)NSString *foodType; //
@property (nonatomic,copy)NSString *imgSrc; //
@property (nonatomic,copy)NSString *na; // 钠
@property (nonatomic,copy)NSString *name; // 名字
@property (nonatomic,copy)NSString *niacin; // 烟酸
@property (nonatomic,copy)NSString *protein; //蛋白质
@property (nonatomic,copy)NSString *va; // 维生素a
@property (nonatomic,copy)NSString *vb1; //维生素b1
@property (nonatomic,copy)NSString *vb2; //维生素b2
@property (nonatomic,copy)NSString *vc; //维生素c
@property (nonatomic,copy)NSString *ve; //维生素e
@property (nonatomic,copy)NSString *water; //水分
@property (nonatomic,copy)NSString *code;
@property (nonatomic,copy)NSString *foodID;

- (instancetype)initWithDict:(NSDictionary *)dict;
+ (instancetype)foodInfoWithDict:(NSDictionary *)dict;

@end
