//
//  exchangeFoodTableViewCell.m
//  孕妙
//
//  Created by 是源科技 on 16/7/15.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "exchangeFoodTableViewCell.h"

@implementation exchangeFoodTableViewCell


- (void)setFrame:(CGRect)frame {
    
    frame.size.width = kDeviceWidth;
    [super setFrame:frame];
    
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
//    CGFloat w = self.frame.size.width;
    CGFloat w = self.contentView.frame.size.width;
    
    CGFloat spaceX = 5.0f;
    CGFloat lw = (w - spaceX * 8) / 5.0;
    CGFloat lh = lw;
    for(int i = 0;i < 5;i ++)
    {
        CGFloat x = (i % 5) * (spaceX + lw) + spaceX * 2;
        UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(x, 0, lw, lh)];
        [self addSubview:button];
        
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(x, lh, lw, 30)];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont systemFontOfSize:14.0f];
        label.textColor = YMColor(95, 95, 95);
        [self addSubview:label];
        
        if(i == 0)
        {
            self.btn1 = button;
            self.label1 = label;
        }
        if(i == 1)
        {
            self.btn2 = button;
            self.label2 = label;
        }
        if(i == 2)
        {
            self.btn3 = button;
            self.label3 = label;
        }
        if(i == 3)
        {
            self.btn4 = button;
            self.label4 = label;
        }
        if(i == 4)
        {
            self.btn5 = button;
            self.label5 = label;
        }
        
        
//        button.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 30, 0);
//        button.titleEdgeInsets = UIEdgeInsetsMake(lw,0, 0, 0);
//        [button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
//        button.titleLabel.font = [UIFont systemFontOfSize:14.0f];
//        [button.titleLabel setBackgroundColor:[UIColor redColor]];
    }

    
    
    return self;
}

@end