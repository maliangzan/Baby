//
//  exchangeFoodTableViewCell.h
//  孕妙
//
//  Created by 是源科技 on 16/7/15.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface exchangeFoodTableViewCell : UITableViewCell
@property (nonatomic,weak)UIButton *btn1;
@property (nonatomic,weak)UIButton *btn2;
@property (nonatomic,weak)UIButton *btn3;
@property (nonatomic,weak)UIButton *btn4;
@property (nonatomic,weak)UIButton *btn5;

@property (nonatomic,weak)UILabel *label1;
@property (nonatomic,weak)UILabel *label2;
@property (nonatomic,weak)UILabel *label3;
@property (nonatomic,weak)UILabel *label4;
@property (nonatomic,weak)UILabel *label5;

@property(nonatomic,weak)UIButton *exChangeButton;

@end
