//
//  CollectionViewCell.h
//  collectionView
//
//  Created by 是源科技 on 16/7/19.
//  Copyright © 2016年 ceshi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
