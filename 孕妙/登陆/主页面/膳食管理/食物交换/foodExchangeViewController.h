//
//  foodExchangeViewController.h
//  孕妙
//
//  Created by 是源科技 on 16/7/5.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface foodExchangeViewController : UIViewController
@property (nonatomic,copy)NSString *foodType;
@property (nonatomic,copy)NSString *foodCode;
@property (nonatomic,copy)NSString *foodID;
@property (nonatomic,copy)NSString *foodWeight;

@end
