//
//  foodExchange.m
//  孕妙
//
//  Created by 是源科技 on 16/7/14.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "foodExchange.h"

@implementation foodExchange

- (instancetype)initWithDict:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        self.ca = dict[@"ca"];
        self.calori = dict[@"calori"];
        self.carbohydrate = dict[@"carbohydrate"];
        self.category = dict[@"category"];
        self.cholesterol = dict[@"cholesterol"];
        self.comestible = dict[@"comestible"];
        self.df = dict[@"df"];
        self.fat = dict[@"fat"];
        self.fe = dict[@"fe"];
        self.foodType = dict[@"foodType"];
        self.imgSrc = dict[@"imgSrc"];
        self.na = dict[@"na"];
        self.name = dict[@"name"];
        self.niacin = dict[@"niacin"];
        self.protein = dict[@"protein"];
        self.va = dict[@"va"];
        self.vb1 = dict[@"vb1"];
        self.vb2 = dict[@"vb2"];
        self.vc = dict[@"vc"];
        self.ve = dict[@"ve"];
        self.water = dict[@"water"];
        self.code = dict[@"code"];
        self.foodID = dict[@"id"];
//        [self setValuesForKeysWithDictionary:dict];
    }
    return self;
}

+ (instancetype)foodInfoWithDict:(NSDictionary *)dict
{
    return [[self alloc] initWithDict:dict];
}

@end
