//
//  foodJiegou.h
//  孕妙
//
//  Created by 是源科技 on 16/6/28.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface foodJiegou : NSObject
@property (nonatomic,copy)NSString *totalCount;
@property (nonatomic,copy)NSString *sheRuCount;
@property (nonatomic,strong)UILabel *shengyuLabel;
@property (nonatomic,copy)NSString *shengyuCount;

// instancetype只能用于返回类型，不能当做参数使用
- (instancetype)initWithDict:(NSDictionary *)dict;
/** 工厂方法 */
+ (instancetype)foodInfoWithDict:(NSDictionary *)dict;

@end
