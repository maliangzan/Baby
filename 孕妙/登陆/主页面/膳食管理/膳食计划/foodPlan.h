//
//  foodPlan.h
//  孕妙
//
//  Created by 是源科技 on 16/7/13.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface foodPlan : NSObject

@property (nonatomic,copy)NSString *foodName;
@property (nonatomic,copy)NSString *foodWeight;
@property (nonatomic,copy)NSString *foodCode;
@property (nonatomic,copy)NSString *foodType;
@property (nonatomic,copy)NSString *foodPic;
@property (nonatomic,copy)NSString *foodID;

-(instancetype)initWithDict:(NSDictionary *)dict;

+ (instancetype)foodInfoWithDict:(NSDictionary *)dict;
@end
