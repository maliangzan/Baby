//
//  foodPlan.m
//  孕妙
//
//  Created by 是源科技 on 16/7/13.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "foodPlan.h"

@implementation foodPlan

-(instancetype)initWithDict:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        self.foodName = [dict objectForKey:@"foodName"];
        self.foodWeight = [dict objectForKey:@"weight"];
        self.foodCode = [dict objectForKey:@"code"];
        self.foodType = [dict objectForKey:@"foodType"];
        self.foodPic = [dict objectForKey:@"foodPic"];
        self.foodID = [dict objectForKey:@"id"];
    }
    return self;
}

+ (instancetype)foodInfoWithDict:(NSDictionary *)dict
{
    return [[self alloc] initWithDict:dict];
}

@end
