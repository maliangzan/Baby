//
//  planFoodViewController.m
//  孕妙
//
//  Created by 是源科技 on 16/6/30.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "planFoodViewController.h"

@interface planFoodViewController ()
@property (nonatomic,weak)UILabel *sheruLabel;
@property (nonatomic,weak)UILabel *countLabel;
@property (nonatomic,assign)CGFloat y;
@property (nonatomic,strong)NSArray *leibieArray;
@property (nonatomic,weak)UIView *helpView;
@property (nonatomic,weak)UIView *tipsView;

@property (nonatomic,strong)NSMutableArray *arrayMA;
@property (nonatomic,strong)NSMutableArray *arrayMB;
@property (nonatomic,strong)NSMutableArray *arrayMC;
@property (nonatomic,strong)NSMutableArray *arrayMD;

@property (nonatomic,weak)UIView *xialaView;
@property (nonatomic,weak)UIButton *selectButton;
@property (nonatomic,strong)NSMutableArray *flagArrayM;

@property (nonatomic,copy)NSString *type0;
@property (nonatomic,copy)NSString *type1;
@property (nonatomic,copy)NSString *type2;
@property (nonatomic,copy)NSString *type3;


@end

@implementation planFoodViewController

-(NSArray *)leibieArray
{
    if(!_leibieArray)
    {
        _leibieArray = @[@"",@"谷类",@"奶类",@"肉蛋类",@"豆类",@"蔬菜类",@"水果类",@"硬果类",@"油脂类",@"总交换份"];
    }
    return _leibieArray;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"膳食计划";
    self.view.backgroundColor = YMColor(245, 245, 245);
    self.arrayMA = [NSMutableArray array];
    self.arrayMB = [NSMutableArray array];
    self.arrayMC = [NSMutableArray array];
    self.arrayMD = [NSMutableArray array];
    self.flagArrayM = [NSMutableArray array];
    [self setTitleAndFenshu];
    [self getFourFangAnFromServices];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    for(int i = 0 ;i < self.flagArrayM.count;i ++)
    {
        if([self.flagArrayM[i]intValue] == 1)
        {
            if(0 == i)
            {
                [self.selectButton setTitle:@"A" forState:UIControlStateNormal];
            }
            if(1 == i)
            {
                [self.selectButton setTitle:@"B" forState:UIControlStateNormal];
            }
            if(2 == i)
            {
                [self.selectButton setTitle:@"C" forState:UIControlStateNormal];
            }
            if(3 == i)
            {
                [self.selectButton setTitle:@"D" forState:UIControlStateNormal];
            }
        }
    }
}


/**从服务器上获取四种方案*/
-(void)getFourFangAnFromServices
{
    NSString *urlString = @"http://120.24.178.16:8080/Health/food/getFoodCases.do";
    NSURL *url = [[NSURL alloc]initWithString:urlString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:2];
    request.HTTPMethod = @"POST";
    NSString *str = [NSString stringWithFormat:@"account=%@&userName=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"userAccount"],[passValue passValue].saveDataUserName];
//    NSString *str = [NSString stringWithFormat:@"account=%@&userName=0",[[NSUserDefaults standardUserDefaults]objectForKey:@"userAccount"]];
    
    NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:data];
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        
        if (data)
        {
            // 请求成功
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
//            YMLog(@"dict = %@",dict);
            if([[dict objectForKey:@"code"] isEqualToString:@"0"])
            {
                NSDictionary *dataDic = [dict objectForKey:@"data"];
                NSDictionary *foodEnergy = [dataDic objectForKey:@"foodEnergy"];
                NSArray *foodCasesArray = [dataDic objectForKey:@"foodCases"];
                
                for (NSDictionary *ddd in foodCasesArray)
                {
                    if([[ddd objectForKey:@"type"]intValue] == 0)
                    {
                        self.type0 = [NSString stringWithFormat:@"%@",[ddd objectForKey:@"flag"]];
                        [self.arrayMA addObject:@"0"];
                        [self.arrayMA addObject:[ddd objectForKey:@"rice"]];
                        [self.arrayMA addObject:[ddd objectForKey:@"milk"]];
                        [self.arrayMA addObject:[ddd objectForKey:@"meat"]];
                        [self.arrayMA addObject:[ddd objectForKey:@"bean"]];
                        [self.arrayMA addObject:[ddd objectForKey:@"vegetable"]];
                        [self.arrayMA addObject:[ddd objectForKey:@"fruit"]];
                        [self.arrayMA addObject:[ddd objectForKey:@"nut"]];
                        [self.arrayMA addObject:[ddd objectForKey:@"fat"]];
                        [self.arrayMA addObject:[foodEnergy objectForKey:@"portion"]];

                    }
                    if([[ddd objectForKey:@"type"]intValue] == 1)
                    {
                        self.type1 = [NSString stringWithFormat:@"%@",[ddd objectForKey:@"flag"]];
                        [self.arrayMB addObject:@"0"];
                        [self.arrayMB addObject:[ddd objectForKey:@"rice"]];
                        [self.arrayMB addObject:[ddd objectForKey:@"milk"]];
                        [self.arrayMB addObject:[ddd objectForKey:@"meat"]];
                        [self.arrayMB addObject:[ddd objectForKey:@"bean"]];
                        [self.arrayMB addObject:[ddd objectForKey:@"vegetable"]];
                        [self.arrayMB addObject:[ddd objectForKey:@"fruit"]];
                        [self.arrayMB addObject:[ddd objectForKey:@"nut"]];
                        [self.arrayMB addObject:[ddd objectForKey:@"fat"]];
                        [self.arrayMB addObject:[foodEnergy objectForKey:@"portion"]];
                        
                    }
                    if([[ddd objectForKey:@"type"]intValue] == 2)
                    {
                        self.type2 = [NSString stringWithFormat:@"%@",[ddd objectForKey:@"flag"]];
                        [self.arrayMC addObject:@"0"];
                        [self.arrayMC addObject:[ddd objectForKey:@"rice"]];
                        [self.arrayMC addObject:[ddd objectForKey:@"milk"]];
                        [self.arrayMC addObject:[ddd objectForKey:@"meat"]];
                        [self.arrayMC addObject:[ddd objectForKey:@"bean"]];
                        [self.arrayMC addObject:[ddd objectForKey:@"vegetable"]];
                        [self.arrayMC addObject:[ddd objectForKey:@"fruit"]];
                        [self.arrayMC addObject:[ddd objectForKey:@"nut"]];
                        [self.arrayMC addObject:[ddd objectForKey:@"fat"]];
                        [self.arrayMC addObject:[foodEnergy objectForKey:@"portion"]];
                       
                    }
                    if([[ddd objectForKey:@"type"]intValue] == 3)
                    {
                        self.type3 = [NSString stringWithFormat:@"%@",[ddd objectForKey:@"flag"]];
                        [self.arrayMD addObject:@"0"];
                        [self.arrayMD addObject:[ddd objectForKey:@"rice"]];
                        [self.arrayMD addObject:[ddd objectForKey:@"milk"]];
                        [self.arrayMD addObject:[ddd objectForKey:@"meat"]];
                        [self.arrayMD addObject:[ddd objectForKey:@"bean"]];
                        [self.arrayMD addObject:[ddd objectForKey:@"vegetable"]];
                        [self.arrayMD addObject:[ddd objectForKey:@"fruit"]];
                        [self.arrayMD addObject:[ddd objectForKey:@"nut"]];
                        [self.arrayMD addObject:[ddd objectForKey:@"fat"]];
                        [self.arrayMD addObject:[foodEnergy objectForKey:@"portion"]];
                        
                    }
            }
                [self.flagArrayM addObject:self.type0];
                [self.flagArrayM addObject:self.type1];
                [self.flagArrayM addObject:self.type2];
                [self.flagArrayM addObject:self.type3];
                self.countLabel.text = [NSString stringWithFormat:@"可摄入份数：%@份",[foodEnergy objectForKey:@"portion"]];
                self.sheruLabel.text = [NSString stringWithFormat:@"预计摄入能量：%@kcal",[foodEnergy objectForKey:@"energy"]];
                
                [self chufangViewLayout];
            }
            else
            {
                [MBProgressHUD showError:[dict objectForKey:@"message"]];
            }

        }
        
        
    }];
}

/**摄入能量和摄入份数*/
-(void)setTitleAndFenshu
{
    self.y = 74.0f;
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, self.y, kDeviceWidth, kDeviceWidth / 4.57)];
    view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:view];
    CGFloat h = view.frame.size.height;
    
    UILabel *sherulabel = [[UILabel alloc]initWithFrame:CGRectMake(30, h / 6.4, kDeviceWidth / 4.0 * 3, 30)];
    sherulabel.textColor = YMColor(111, 111, 111);
    self.sheruLabel = sherulabel;
    self.sheruLabel.font = [UIFont systemFontOfSize:16.0f];
    [view addSubview:sherulabel];
    
    UILabel *counrLabel = [[UILabel alloc]initWithFrame:CGRectMake(30, h / 1.88, kDeviceWidth / 4.0 * 3, 30)];
    counrLabel.textColor = YMColor(111, 111, 111);
    self.countLabel = counrLabel;
    self.countLabel.font = [UIFont systemFontOfSize:16.0f];
    [view addSubview:counrLabel];
    
    UIButton *tipsBtn = [[UIButton alloc]initWithFrame:CGRectMake(kDeviceWidth - 80, h / 3.2, 60, 40)];
    [tipsBtn setImage:[UIImage imageNamed:@"ssgl_tips-icon"] forState:UIControlStateNormal];
    [tipsBtn setTitle:@"小贴示" forState:UIControlStateNormal];
    tipsBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 20, 20, 20);
    tipsBtn.titleEdgeInsets = UIEdgeInsetsMake(20, -43, 0, 0);
    tipsBtn.titleLabel.font = [UIFont systemFontOfSize:14.0];
    [tipsBtn setTitleColor:YMColor(193, 193, 193) forState:UIControlStateNormal];
    [view addSubview:tipsBtn];
    
    [tipsBtn addTarget:self action:@selector(checkTips) forControlEvents:UIControlEventTouchUpInside];
    self.y += view.frame.size.height;
    
    [self setLineStartX:0 andStartY:self.y andWidth:kDeviceWidth andHeight:1.0f ToView:self.view];
    self.y += 8.0f;
    [self setLineStartX:0 andStartY:self.y andWidth:kDeviceWidth andHeight:1.0f ToView:self.view];
    self.y += 1.0f;
    
    UILabel *chufangLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, self.y, kDeviceWidth, kDeviceWidth / 8.0)];
    chufangLabel.textColor = YMColor(195, 97, 146);
    chufangLabel.text = @"膳食处方";
    chufangLabel.backgroundColor = [UIColor whiteColor];
    chufangLabel.textAlignment = NSTextAlignmentCenter;
    chufangLabel.font = [UIFont systemFontOfSize:17.0f];
    [self.view addSubview:chufangLabel];
    
    self.y += chufangLabel.frame.size.height;
    [self setLineStartX:0 andStartY:self.y andWidth:kDeviceWidth andHeight:1.0f ToView:self.view];
    self.y += 1;
}

/**处方view布局*/
-(void)chufangViewLayout
{
    UILabel *nameView = [[UILabel alloc]initWithFrame:CGRectMake(0, self.y, kDeviceWidth / 3.76, kDeviceWidth / 1.28)];
    [self.view addSubview:nameView];
     [self addLabelToView:nameView andLabelString:self.leibieArray];
    
    CGFloat w = (kDeviceWidth - nameView.frame.size.width) * 0.25;
    CGFloat h = nameView.frame.size.height;
    for(int i = 0;i < 4;i ++)
    {
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(nameView.frame.size.width + i * w, self.y, w, h)];
        view.tag = i + 1;
        [self.view addSubview:view];
        if(view.tag == 1)
        {
            [self addLabelToView:view andLabelString:self.arrayMA];
        }
        if(view.tag == 2)
        {
            [self addLabelToView:view andLabelString:self.arrayMB];
        }
        if(view.tag == 3)
        {
            [self addLabelToView:view andLabelString:self.arrayMC];
        }
        if(view.tag == 4)
        {
            [self addLabelToView:view andLabelString:self.arrayMD];
        }
        [self setLineStartX:0 andStartY:0 andWidth:1 andHeight:view.frame.size.height ToView:view];
    }
    
    self.y += nameView.frame.size.height;
     [self setChufangView];
}

/**在view上布局*/
-(void)addLabelToView:(UIView *)view andLabelString:(NSArray *)array
{
    CGFloat w= view.frame.size.width;
    CGFloat h = view.frame.size.height * .1;
    for(int i = 0; i < array.count;i ++)
    {
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, i * h, w, h)];
        label.text = [NSString stringWithFormat:@"%@",array[i]];
        label.tag = i;
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = YMColor(111, 111, 111);
        label.font = [UIFont systemFontOfSize:15.0f];
        [view addSubview:label];
        if(view.tag == 1 && label.tag == 0)
        {
            label.text = @"A-标准";
        }
        if(view.tag == 2 && label.tag == 0)
        {
            label.text = @"B-高钙";
        }
        if(view.tag == 3 && label.tag == 0)
        {
            label.text = @"C-少油";
        }
        if(view.tag == 4 && label.tag == 0)
        {
            label.text = @"D-清淡";
        }
        
        [self setLineStartX:0 andStartY:(i + 1) * h andWidth:w andHeight:1 ToView:view];
    }
}

/**选择处方view*/
-(void)setChufangView
{
    self.y += 8;
    [self setLineStartX:0 andStartY:self.y andWidth:kDeviceWidth andHeight:1 ToView:self.view];
    self.y += 1;
    UIView *chufangView = [[UIView alloc]initWithFrame:CGRectMake(0, self.y, kDeviceWidth, kDeviceWidth / 6.4)];
    chufangView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:chufangView];
    
    UIView *xialaView = [[UIView alloc]initWithFrame:CGRectMake(105, self.y + (kDeviceWidth / 6.4 - 30) * 0.5 + 30, 110, kDeviceHeight - (self.y + (kDeviceWidth / 6.4 - 30) * 0.5 + 30))];
    xialaView.backgroundColor = [UIColor whiteColor];
    self.xialaView = xialaView;
    [self.view addSubview:xialaView];
    self.xialaView.hidden = YES;
    
    
    self.y += chufangView.frame.size.height;
    [self setLineStartX:0 andStartY:self.y andWidth:kDeviceWidth andHeight:1 ToView:self.view];
    
    [self selectChuFanglayoutToView:chufangView];
    
    //红色横杠
    UILabel *redLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, kDeviceHeight - 20, kDeviceWidth, 20)];
    redLabel.backgroundColor = YMColor(254, 206, 230);
    [self.view addSubview:redLabel];
    
    [self addFourButtonToView];
    
    [self tipsLayout];
    
}

-(void)addFourButtonToView
{
    CGFloat w = self.xialaView.frame.size.width;
    
    UIScrollView *scrollVieew = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, w, self.xialaView.frame.size.height)];
    scrollVieew.bounces = NO;
    [self.xialaView addSubview:scrollVieew];
   
    
    for(int i = 0;i < 4;i ++)
    {
        UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0,25 * i, w, 25)];
        [btn setTitleColor:YMColor(63, 63, 63) forState:UIControlStateNormal];
        btn.titleEdgeInsets = UIEdgeInsetsMake(0, -28, 0, 0);
        [scrollVieew addSubview:btn];
        btn.tag = i;
        if(btn.tag == 0)
        {
            [btn setTitle:@"A" forState:UIControlStateNormal];
        }
        if(btn.tag == 1)
        {
            [btn setTitle:@"B" forState:UIControlStateNormal];
        }
        if(btn.tag == 2)
        {
            [btn setTitle:@"C" forState:UIControlStateNormal];
        }
        if(btn.tag == 3)
        {
            [btn setTitle:@"D" forState:UIControlStateNormal];
        }
        [btn addTarget:self action:@selector(clickSureButton:) forControlEvents:UIControlEventTouchUpInside];
    }
     scrollVieew.contentSize = CGSizeMake(w, 100);
    
    
        
}

-(void)clickSureButton:(UIButton *)btn
{
    self.xialaView.hidden = YES;
    if(btn.tag == 0)
    {
        [self.selectButton setTitle:@"A" forState:UIControlStateNormal];
    }
    if(btn.tag == 1)
    {
        [self.selectButton setTitle:@"B" forState:UIControlStateNormal];
    }
    if(btn.tag == 2)
    {
        [self.selectButton setTitle:@"C" forState:UIControlStateNormal];
    }
    if(btn.tag == 3)
    {
        [self.selectButton setTitle:@"D" forState:UIControlStateNormal];
    }
}
/**选择处方view上的布局*/
-(void)selectChuFanglayoutToView:(UIView *)view
{
//    CGFloat w = view.frame.size.width;
    CGFloat h = view.frame.size.height;
    
    UILabel *selectLabel = [[UILabel alloc]initWithFrame:CGRectMake(20, 0, 100, h)];
    selectLabel.text = @"选择处方：";
    selectLabel.font = [UIFont systemFontOfSize:16.0f];
    selectLabel.textColor = YMColor(194, 97, 146);
    [view addSubview:selectLabel];
    
    //选择A B C D四个方案按钮
    UIButton *selectButton = [UIButton buttonWithType:UIButtonTypeCustom];
    selectButton.frame = CGRectMake(105, (h - 30) * 0.5,110, 30);
    selectButton.layer.borderWidth = 1.0f;
    selectButton.layer.cornerRadius = 8.0;
    selectButton.layer.borderColor = [YMColor(253, 206, 228)CGColor];
    [selectButton setTitle:@"A" forState:UIControlStateNormal];
    [selectButton setTitleColor:YMColor(63, 63, 63) forState:UIControlStateNormal];
    [selectButton setImage:[UIImage imageNamed:@"ssgl_arrowdown"] forState:UIControlStateNormal];
    selectButton.imageEdgeInsets = UIEdgeInsetsMake(8, 86, 8, 10);
    selectButton.titleEdgeInsets = UIEdgeInsetsMake(0, -45, 0, 0);
    self.selectButton = selectButton;
    [view addSubview:selectButton];
    
    //定制膳食按钮
    UIButton *dingzhiBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    dingzhiBtn.frame = CGRectMake(kDeviceWidth - 100, (h - 30) * 0.5, 80, 30);
    dingzhiBtn.layer.borderColor = [YMColor(253, 206, 228)CGColor];
    dingzhiBtn.layer.borderWidth = 1.0f;
    dingzhiBtn.layer.cornerRadius = 8.0;
    [dingzhiBtn setTitle:@"定制食谱" forState:UIControlStateNormal];
    [dingzhiBtn setTitleColor:YMColor(252, 167, 209) forState:UIControlStateNormal];
    dingzhiBtn.titleLabel.font = [UIFont systemFontOfSize:15.0f];
    [view addSubview:dingzhiBtn];
    
    [selectButton addTarget:self action:@selector(selectOneFangan) forControlEvents:UIControlEventTouchUpInside];
    [dingzhiBtn addTarget:self action:@selector(clickDingzhiShiPuButton) forControlEvents:UIControlEventTouchUpInside];
}
/**点击了定制食谱按钮*/
-(void)clickDingzhiShiPuButton
{
    [self postFanganToServices];
}

/**将选择的方案上传至服务器*/
-(void)postFanganToServices
{
    NSString *btnTitle = self.selectButton.titleLabel.text;
    NSString *type = nil;
    if([btnTitle isEqualToString:@"A"])
    {
        type = @"0";
    }
    if([btnTitle isEqualToString:@"B"])
    {
        type = @"1";
    }
    if([btnTitle isEqualToString:@"C"])
    {
        type = @"2";
    }if([btnTitle isEqualToString:@"D"])
    {
        type = @"3";
    }
    
    [MBProgressHUD showMessage:@"正在拼命上传，请稍后..."];
    NSString *urlString = @"http://120.24.178.16:8080/Health/food/selectFoodCase.do";
    NSURL *url = [[NSURL alloc]initWithString:urlString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:2];
    request.HTTPMethod = @"POST";
    NSString *str = [NSString stringWithFormat:@"account=%@&userName=%@&type=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"userAccount"],[passValue passValue].saveDataUserName,type];
    
    NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:data];
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (data)
        {
            [MBProgressHUD hideHUD];
            // 请求成功
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
//            YMLog(@"dict = %@",dict);
            if([[dict objectForKey:@"code"]isEqualToString:@"0"])
            {
                [MBProgressHUD showSuccess:@"上传成功"];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.navigationController popViewControllerAnimated:YES];
                });
            }
            else
            {
                [MBProgressHUD showError:@"上传失败，请重新上传"];
            }
        }
    }];
}

/**选择A B C D四个方案中的一种*/
-(void)selectOneFangan
{
    self.xialaView.hidden = NO;
    [self.view bringSubviewToFront:self.xialaView];
}

/**横线和竖线*/
-(void)setLineStartX:(CGFloat)x andStartY:(CGFloat)y andWidth:(CGFloat)w andHeight:(CGFloat)h ToView:(UIView *)view
{
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(x, y, w, h)];
    label.backgroundColor = YMColor(218, 218, 218);
    [view addSubview:label];
}

/**小贴士*/
-(void)tipsLayout
{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 64, kDeviceWidth, kDeviceHeight - 64)];
    view.backgroundColor = [UIColor blackColor];
    view.alpha = 0.5f;
    self.helpView = view;
    [self.view addSubview:view];
    view.hidden = YES;
    
    UIButton *hideButton = [[UIButton alloc]initWithFrame:view.bounds];
    [hideButton addTarget:self action:@selector(clickHidebutton) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:hideButton];
    
    //小贴士内容
    UIView *tipsView = [[UIView alloc]initWithFrame:CGRectMake(kDeviceWidth / 16.0, kDeviceHeight / 5.7, kDeviceWidth / 8.0 * 7, kDeviceHeight / 1.5)];
    tipsView.layer.cornerRadius = 12.0;
    tipsView.layer.borderWidth = 0.5;
    tipsView.layer.borderColor = [YMColor(252, 160, 207)CGColor];
    tipsView.backgroundColor = [UIColor whiteColor];
    self.tipsView = tipsView;
    [self.view addSubview:tipsView];
    self.tipsView.hidden = YES;
    [self addNeiRongToView:self.tipsView];
}

//点击隐藏小贴士内容
-(void)clickHidebutton
{
    self.helpView.hidden = YES;
    self.tipsView.hidden = YES;
}

/**点击了小贴士按钮*/
-(void)checkTips
{
    self.helpView.hidden = NO;
    self.tipsView.hidden = NO;
}

/**布局小贴士内容*/
-(void)addNeiRongToView:(UIView *)view
{
    CGFloat w = view.frame.size.width;
    CGFloat h = view.frame.size.height;
    CGFloat y = 10.0f;
    
    UIScrollView *scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, w, h)];
    [view addSubview:scrollView];
    scrollView.bounces = NO;
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, y, w, 40)];
    titleLabel.text = @"如何选择膳食处方？";
    titleLabel.font = [UIFont systemFontOfSize:18.0f];
    titleLabel.textColor = YMColor(238, 124, 176);
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [scrollView addSubview:titleLabel];
    
    y += titleLabel.frame.size.height + 15;
    UILabel *qingduLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
    qingduLabel.text = @"以孕中期为例，一天食物建议量：谷类200～250g,蔬菜类300～500g,水果类200～400g,奶类300～500g等，那从A/B/C/D四个膳食处方中，根据自己的口味偏好，选择合适自己的膳食处方，保证能量和营养素正常摄入范围的情况下，在不同食物分类中进行调整，系统会根据您选择的处方定制食谱。\n处方A为标准处方，适合各类人群\n处方B偏高钙，奶制品份数多，适合缺钙人群\n处方C少油，适合忌荤腥的人群\n处方D清淡，适合肥胖人群\n预计摄入能量：根据您的身高和体重，计算出体重指数，然后参照你的劳动强度为您指定每日所需要的能量。\n可摄入份数：将提供90kcal能量的各类食物重量叫做一个食物交换份，故每日各类食物的份数为 = 预计摄入能量 ÷ 90";
    qingduLabel.textAlignment = NSTextAlignmentLeft;
    qingduLabel.font = [UIFont systemFontOfSize:14.0];
    NSDictionary * attribute2 = @{NSFontAttributeName:[UIFont systemFontOfSize:15.0]};
    CGSize size2 = [qingduLabel.text boundingRectWithSize:CGSizeMake(kDeviceWidth - 30, kDeviceHeight) options:NSStringDrawingUsesLineFragmentOrigin attributes:attribute2 context:nil].size;
    qingduLabel.lineBreakMode = NSLineBreakByCharWrapping;
    qingduLabel.numberOfLines = 0;
    qingduLabel.frame = CGRectMake(15, y, w - 30, size2.height);
    qingduLabel.textColor = YMColor(95, 95, 95);
    [scrollView addSubview:qingduLabel];
    y += qingduLabel.frame.size.height + 25;
    CGFloat lw = w * 0.4;
    CGFloat lh = lw / 2.22;
    
    
    UIButton *closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    closeBtn.frame  = CGRectMake((w - lw) * 0.5, y, lw, lh);
    closeBtn.layer.cornerRadius = 8.0f;
    closeBtn.backgroundColor = YMColor(252, 160, 207);
    [scrollView addSubview:closeBtn];
    [closeBtn setTitle:@"我知道了" forState:UIControlStateNormal];
    [closeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [closeBtn addTarget:self action:@selector(clickHidebutton) forControlEvents:UIControlEventTouchUpInside];
    
    y += closeBtn.frame.size.height + 20;
    
    scrollView.contentSize = CGSizeMake(w, y);
    
    
}

@end
