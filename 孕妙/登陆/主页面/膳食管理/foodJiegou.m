//
//  foodJiegou.m
//  孕妙
//
//  Created by 是源科技 on 16/6/28.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "foodJiegou.h"

@implementation foodJiegou

// instancetype只能用于返回类型，不能当做参数使用
- (instancetype)initWithDict:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        self.totalCount = dict[@"total"];
        self.sheRuCount = dict[@"already"];
        
    }
    return self;
}
/** 工厂方法 */
+ (instancetype)foodInfoWithDict:(NSDictionary *)dict
{
    return [[self alloc] initWithDict:dict];
}


@end
