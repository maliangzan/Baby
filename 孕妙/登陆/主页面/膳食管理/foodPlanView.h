//
//  foodPlanView.h
//  孕妙
//
//  Created by 是源科技 on 16/6/28.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface foodPlanView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *foodImage;
@property (weak, nonatomic) IBOutlet UILabel *foodName;
@property (weak, nonatomic) IBOutlet UILabel *foodWeight;
@property (weak, nonatomic) IBOutlet UIButton *changeButton;
@property (nonatomic,strong)UISwipeGestureRecognizer *recognizer;


@end
