//
//  selectFoodfenshuViewController.h
//  孕妙
//
//  Created by 是源科技 on 16/7/5.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "foodExchange.h"

@interface selectFoodfenshuViewController : UIViewController
@property (nonatomic,strong)foodExchange *selectFood;
@property (nonatomic,copy)NSString *selectDateString;
@property (nonatomic,assign)NSInteger btnTag;

@end
