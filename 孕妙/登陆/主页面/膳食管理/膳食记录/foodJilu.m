//
//  foodJilu.m
//  孕妙
//
//  Created by 是源科技 on 16/7/25.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "foodJilu.h"

@implementation foodJilu

- (instancetype)initWithDict:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        self.createTime = dict[@"createTime"];
        self.energy = dict[@"energy"];
        self.foodId = dict[@"foodId"];
        self.foodName = dict[@"foodName"];
        self.foodTypeId = dict[@"foodTypeId"];
        self.foodnumber = dict[@"foodnumber"];
        self.imgSrc = dict[@"imgSrc"];
        self.orderId = dict[@"orderId"];
        self.userId = dict[@"userId"];
        self.weight = dict[@"weight"];
        self.jiluID = dict[@"id"];
    }
    return self;
}

+ (instancetype)foodInfoWithDict:(NSDictionary *)dict
{
    return [[self alloc] initWithDict:dict];
}


@end
