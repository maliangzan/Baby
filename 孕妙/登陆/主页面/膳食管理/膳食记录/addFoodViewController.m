//
//  addFoodViewController.m
//  孕妙
//
//  Created by 是源科技 on 16/6/29.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "addFoodViewController.h"
#import "foodJieGouView.h"
#import "selectFoodfenshuViewController.h"
#import "foodExchange.h"
#import "VSinformationViewController.h"


@interface addFoodViewController ()<UITextFieldDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (nonatomic,assign)CGFloat y;
@property (nonatomic,weak)UIView *leibieView;
@property (nonatomic,weak)UIView *selectView;
@property (nonatomic,weak)UITextField *searchTextField;

@property (nonatomic,strong)UIButton *starButton;

@property (nonatomic,strong)NSArray *leibieArray; //八大类
@property (nonatomic,weak)UIButton *firButton;

@property (nonatomic,assign)int indexCount;
@property (nonatomic,assign)int foodType;

@property (nonatomic,weak)UICollectionView *collcetionView;

@property (nonatomic,strong)NSMutableArray *foodArrayM;

@property (nonatomic,assign)BOOL huadongFlag;
@property (nonatomic,weak)UILabel *xialaLabel;

@property (nonatomic,assign)BOOL searchFlag;


@end

@implementation addFoodViewController

-(UILabel *)xialaLabel
{
    if(!_xialaLabel)
    {
        UILabel *xialaLabel = [[UILabel alloc]initWithFrame:CGRectMake(kDeviceWidth / 4.57, kDeviceHeight - 50, kDeviceWidth - kDeviceWidth / 4.57 , 30)];
        xialaLabel.text = @"松手,加载更多数据!!";
        xialaLabel.textAlignment = NSTextAlignmentCenter;
        xialaLabel.font = [UIFont systemFontOfSize:14.0f];
        xialaLabel.textColor = YMColor(91, 91, 91);
        _xialaLabel = xialaLabel;
        [self.view addSubview:xialaLabel];
        xialaLabel.hidden = YES;
    }
    return _xialaLabel;
}


-(NSArray *)leibieArray
{
    if(!_leibieArray)
    {
        _leibieArray = @[@"谷薯",@"蔬菜",@"水果",@"豆类",@"奶类",@"肉蛋",@"硬果",@"油脂"];
    }
    return _leibieArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"添加食物";
    self.view.backgroundColor = YMColor(245, 245, 245);
    
    self.huadongFlag = NO;
    self.searchFlag = NO;
    self.indexCount = 1;
    self.foodType = 1;
    self.automaticallyAdjustsScrollViewInsets = NO; //控件无故偏移
    
    self.foodArrayM = [NSMutableArray array];
    [self searchTextFieldAndSearchButtonLayout];
    [self foodVSfoodLayout];
    [self addButtonToView];
    
    //初始化
    UICollectionViewFlowLayout *flowLayout=[[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    UICollectionView *collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(kDeviceWidth / 4.57 + 2, self.y, kDeviceWidth - kDeviceWidth / 4.57 - 4, kDeviceHeight - self.y - 5) collectionViewLayout:flowLayout];
    self.collcetionView = collectionView;

    //注册
    //    [collectionView registerClass:[CollectionViewCell class]forCellWithReuseIdentifier:@"collectionCell"];
    [self.collcetionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"UICollectionViewCell"];
    collectionView.delegate = self;
    collectionView.dataSource = self;

    collectionView.delegate = self;
    collectionView.dataSource = self;
    [self.view addSubview:collectionView];
    //    collectionView.backgroundColor = YMColor(245, 245, 245);
    collectionView.backgroundColor = [UIColor whiteColor];
    flowLayout.minimumInteritemSpacing = 0;
    flowLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    [self getFoodImageFromServicesWithIndexCount:self.indexCount andFoodType:self.foodType];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [passValue passValue].selectFood = nil;
}

//点击空白处收缩键盘
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.searchTextField resignFirstResponder];
}

/**搜索条和搜索按钮布局*/
-(void)searchTextFieldAndSearchButtonLayout
{
    self.y = 74;
    UITextField *searchTextField = [[UITextField alloc]initWithFrame:CGRectMake(20, self.y, kDeviceWidth - 80, 30)];
    searchTextField.layer.cornerRadius = 8.0;
    searchTextField.layer.borderWidth = 1.0f;
    searchTextField.layer.borderColor = [YMColor(239, 180, 205)CGColor];
    searchTextField.textColor = YMColor(110, 110, 110);
    [searchTextField setValue:YMColor(206, 206, 206) forKeyPath:@"_placeholderLabel.textColor"];
    searchTextField.placeholder = @"请输入食物名称";
    searchTextField.delegate = self;
    self.searchTextField = searchTextField;
    searchTextField.clearButtonMode = UITextFieldViewModeAlways;
    searchTextField.borderStyle = UITextBorderStyleRoundedRect;
    [self.view addSubview:searchTextField];
    
    UIButton *searchButton = [[UIButton alloc]initWithFrame:CGRectMake(kDeviceWidth - 47, self.y + 4, 28, 28)];
    [searchButton setImage:[UIImage imageNamed:@"搜索btn"] forState:UIControlStateNormal];
    [self.view addSubview:searchButton];
    [searchButton addTarget:self action:@selector(clickSearchButton) forControlEvents:UIControlEventTouchUpInside];
    
    self.y += searchTextField.frame.size.height + 10;
    
}

/**点击搜索食物按钮*/
-(void)clickSearchButton
{
    [self.searchTextField resignFirstResponder];
    self.indexCount = 1;
    self.searchFlag = YES;
    [self getFoodImageFromServicesWithIndexCount:self.indexCount andFoodName:self.searchTextField.text];
}

/**食物对比布局*/
-(void)foodVSfoodLayout
{
    UIView *foodVsFoodView = [[UIView alloc]initWithFrame:CGRectMake(0, self.y, kDeviceWidth, 70)];
    foodVsFoodView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:foodVsFoodView];
    
    CGFloat w = foodVsFoodView.frame.size.width;
    CGFloat h = foodVsFoodView.frame.size.height;
    
    UIImageView *logoImage = [[UIImageView alloc]initWithFrame:CGRectMake(20, h / 3.0, h / 3.0, h / 3.0)];
    logoImage.image = [UIImage imageNamed:@"ssgl_VSicon"];
    [foodVsFoodView addSubview:logoImage];
    
    UILabel *foodLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(w / 5.0, 10, w / 3.0, h / 2.0)];
    foodLabel1.textColor = YMColor(190, 74, 136);
    foodLabel1.text = @"食物对比";
    foodLabel1.font = [UIFont systemFontOfSize:18.0f];
    [foodVsFoodView addSubview:foodLabel1];
    
    UILabel *foodLabel2 = [[UILabel alloc]initWithFrame:CGRectMake(w / 5.0, h / 3.0 + 10, w / 3.0, h / 2.0)];
    foodLabel2.text = @"食物营养素PK";
    foodLabel2.textColor = YMColor(162, 162, 162);
    foodLabel2.font = [UIFont systemFontOfSize:15.0f];
    [foodVsFoodView addSubview:foodLabel2];
    
    UIButton *vsButton = [[UIButton alloc]initWithFrame:CGRectMake(w / 3.0 * 2, 0, w / 3.0, h)];
    [vsButton setImage:[UIImage imageNamed:@"personal_》small"] forState:UIControlStateNormal];
    vsButton.imageEdgeInsets = UIEdgeInsetsMake(h / 3.0, w / 3.0 - h / 3.0 - 18, h / 3.0, 18);
    [foodVsFoodView addSubview:vsButton];
    [vsButton addTarget:self action:@selector(clickVSButton) forControlEvents:UIControlEventTouchUpInside];
    
    self.y += foodVsFoodView.frame.size.height + 10;
}

/**创建view以便放button*/
-(void)addButtonToView
{
    UIView *leibieView = [[UIView alloc]initWithFrame:CGRectMake(0, self.y, kDeviceWidth / 4.57, kDeviceHeight - self.y - 2)];
    self.leibieView = leibieView;
    [self.view addSubview:leibieView];
    [self addEightButtonToView:self.leibieView];
    
    
}

-(void)clickVSButton
{
    VSinformationViewController *vs = [[VSinformationViewController alloc]init];
    [self.navigationController pushViewController:vs animated:YES];
}

/**加八大类的按钮*/
-(void)addEightButtonToView:(UIView *)view
{
    CGFloat h = view.frame.size.height;
    CGFloat w = view.frame.size.width;
    for(int i = 0;i < 8;i ++)
    {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(-1, h / 8.0 * i - i, w + 1, h / 8.0);
        [btn setTitle:self.leibieArray[i] forState:UIControlStateNormal];
        btn.tag = i + 1;
        btn.backgroundColor = [UIColor whiteColor];
        [btn setTitleColor:YMColor(101, 101, 101) forState:UIControlStateNormal];
        btn.layer.borderWidth = 1.0f;
        btn.layer.borderColor = [YMColor(216, 216, 216)CGColor];
        [view addSubview:btn];
        [btn addTarget:self action:@selector(clickEightButton:) forControlEvents:UIControlEventTouchUpInside];
        //初始化
        if(i == 0)
        {
            [btn setBackgroundColor:YMColor(255, 233, 244)];
            [btn setTitleColor:YMColor(193, 90, 144) forState:UIControlStateNormal];
            self.firButton = btn;
        }
    }
}

/**选择八大类按钮*/
-(void)clickEightButton:(UIButton *)btn
{
    self.indexCount = 1;
//    self.collcetionView.contentOffset = CGPointMake(0, 0);
    self.foodArrayM = [NSMutableArray array];
    [self.firButton setBackgroundColor:[UIColor whiteColor]];
    [self.firButton setTitleColor:YMColor(101, 101, 101) forState:UIControlStateNormal];
    self.firButton.userInteractionEnabled = NO;
    if(btn.tag != 1)
    {
        self.firButton.userInteractionEnabled = YES;
    }
    if(btn != self.starButton)
    {
        self.starButton.selected = NO;
        [self.starButton setBackgroundColor:[UIColor whiteColor]];
        [self.starButton setTitleColor:YMColor(101, 101, 101) forState:UIControlStateNormal];
        self.starButton = btn;
        [btn setBackgroundColor:YMColor(255, 233, 244)];
        [btn setTitleColor:YMColor(193, 90, 144) forState:UIControlStateNormal];
    }
    self.starButton.selected = YES;
    self.huadongFlag = NO;
    self.searchFlag = NO;
    
    for(UIView *view in self.selectView.subviews)
    {
        [view removeFromSuperview];
    }
    if(btn.tag == 1)
    {
        self.foodType = 1;
       
    }
    if(btn.tag == 2)
    {
        self.foodType = 5;
        
    }
    if(btn.tag == 3)
    {
        self.foodType = 6;
    }
    if(btn.tag == 4)
    {
        self.foodType = 4;
    }
    if(btn.tag == 5)
    {
        self.foodType = 2;
    }
    if(btn.tag == 6)
    {
        self.foodType = 3;
    }
    if(btn.tag == 7)
    {
        self.foodType = 8;
    }
    if(btn.tag == 8)
    {
       self.foodType = 7;
    }
    
     [self getFoodImageFromServicesWithIndexCount:self.indexCount andFoodType:self.foodType];
}

/**从服务器上获取要交换的食物图片*/
-(void)getFoodImageFromServicesWithIndexCount:(int)indexCount andFoodType:(int)foodType
{
    self.indexCount ++;
    //    YMLog(@"self.indexCount = %d",self.indexCount);
    [MBProgressHUD showMessage:@"正在拼命加载中，请稍后..."];
    
    NSString *urlString = @"http://120.24.178.16:8080/Health/food/findexchangeFoods.do";
    NSURL *url = [[NSURL alloc]initWithString:urlString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:2];
    request.HTTPMethod = @"POST";
    NSString *str = [NSString stringWithFormat:@"pageSize=20&pageIndex=%@&foodType=%@",[NSString stringWithFormat:@"%d",indexCount],[NSString stringWithFormat:@"%d",foodType]];
    NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:data];
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (data)
        {
            [MBProgressHUD hideHUD];
            // 请求成功
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
//            YMLog(@"dict = %@",dict);
            if([[dict objectForKey:@"code"] isEqualToString:@"0"])
            {
                NSArray *dataarray = [dict objectForKey:@"data"];
                for (NSDictionary *food in dataarray) {
                    foodExchange *fff = [foodExchange foodInfoWithDict:food];
                    [self.foodArrayM addObject:fff];
                }
               
                [UIView animateWithDuration:0.01 animations:^{
                    [self.collcetionView reloadData];
                }];
            }
            else
            {
                [MBProgressHUD showError:[dict objectForKey:@"message"]];
            }
            if([[dict objectForKey:@"code"]isEqualToString:@"1"])
            {
                self.huadongFlag = YES;
            }
        }
    }];
}

/**从服务器上获取搜索到的食物*/
-(void)getFoodImageFromServicesWithIndexCount:(int)indexCount andFoodName:(NSString *)foodName;
{
    self.indexCount ++;
    //    YMLog(@"self.indexCount = %d",self.indexCount);
    [MBProgressHUD showMessage:@"正在拼命加载中，请稍后..."];
    
    NSString *urlString = @"http://120.24.178.16:8080/Health/food/findFoodsByName.do";
    NSURL *url = [[NSURL alloc]initWithString:urlString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:2];
    request.HTTPMethod = @"POST";
    NSString *str = [NSString stringWithFormat:@"pageSize=20&pageIndex=%@&foodName=%@",[NSString stringWithFormat:@"%d",indexCount],[NSString stringWithFormat:@"%@",foodName]];
    NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:data];
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (data)
        {
            [MBProgressHUD hideHUD];
            // 请求成功
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
//            YMLog(@"dict = %@",dict);
            if([[dict objectForKey:@"code"] isEqualToString:@"0"])
            {
                NSArray *dataarray = [dict objectForKey:@"data"];
                if(dataarray.count > 0)
                {
                    self.foodArrayM = [NSMutableArray array];
                    for (NSDictionary *food in dataarray) {
                        foodExchange *fff = [foodExchange foodInfoWithDict:food];
                        [self.foodArrayM addObject:fff];
                    }
                    
                    [UIView animateWithDuration:0.01 animations:^{
                        [self.collcetionView reloadData];
                    }];
                }
            }
            else
            {
                [MBProgressHUD showError:[dict objectForKey:@"message"]];
                self.indexCount --;
            }
            
            if([[dict objectForKey:@"code"]isEqualToString:@"1"])
            {
                self.huadongFlag = YES;
            }
            
        }
    }];
}



//每个section的item个数
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.foodArrayM.count;
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString * CellIdentifier = @"UICollectionViewCell";
    UICollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    foodExchange *fff = self.foodArrayM[indexPath.item];
    CGFloat w = cell.frame.size.width;
    CGFloat h = cell.frame.size.height;
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, w, w)];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, w, w, h - w)];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont systemFontOfSize:14.0f];
    label.textColor = YMColor(95, 95, 95);
    
    NSURL *url = [[NSURL alloc]initWithString:fff.imgSrc];
    [imageView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"zonghe"]];
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
//    UIImage *cachedImage = [manager imageWithURL:url]; // 将需要缓存的图片加载进来
    [manager downloadImageWithURL:url options:SDWebImageRetryFailed progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        
//        NSLog(@"显示当前进度");
        
    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
        
//        NSLog(@"下载完成");
//        imageView.image = image;
        
    }];
    

    
    
    label.text = fff.name;
    
    
    for (id subView in cell.contentView.subviews)
    {
        [subView removeFromSuperview];
    }
    [cell.contentView addSubview:label];
    [cell.contentView addSubview:imageView];
    
    [cell sizeToFit];
    
    return cell;
}



#pragma mark -- UICollectionViewDelegate
//设置每个 UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat w = self.collcetionView.frame.size.width;
    CGFloat spaceX = 2.0f;
    CGFloat lw = (w - spaceX * 5) / 4.0;
    CGFloat lh = lw + 25;
    return CGSizeMake(lw, lh);
}
//定义每个UICollectionView 的间距
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0,0);
}

//定义每个UICollectionView 的纵向间距
-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}


-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"%@",@(indexPath.row).description);
    

//    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    foodExchange *fff = self.foodArrayM[indexPath.row];

    selectFoodfenshuViewController *selectFood = [[selectFoodfenshuViewController alloc]init];
    [self.navigationController pushViewController:selectFood animated:YES];
    selectFood.selectFood = fff;
    selectFood.selectDateString = self.selectDateString;
    selectFood.btnTag = self.btnTag;
}

#pragma mark - UIScrollViewDelegate实现方法-
// scrollView滚动时执行
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.y >= fmaxf(.0f, scrollView.contentSize.height - scrollView.frame.size.height) + 50) //x是触发操作的阀值
    {
        
        //触发上拉刷新
        
        if(self.huadongFlag == NO)
        {
            self.xialaLabel.hidden = NO;
        }
    }
    else
    {
        self.xialaLabel.hidden = YES;
    }
    
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    //    NSLog(@"scrollViewWillBeginDragging");
    //    [self.foodMutableArray removeAllObjects];
    
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (scrollView.contentOffset.y >= fmaxf(.0f, scrollView.contentSize.height - scrollView.frame.size.height) + 50) //x是触发操作的阀值
    {
        if(self.huadongFlag == NO)
        {
            if(self.searchFlag == NO)
            {
                //触发上拉刷新
                [self getFoodImageFromServicesWithIndexCount:self.indexCount andFoodType:self.foodType];
                
                
            }
            else
            {
                [self getFoodImageFromServicesWithIndexCount:self.indexCount andFoodName:self.searchTextField.text];
            }
        }
    }
    
}




@end
