//
//  foodJilu.h
//  孕妙
//
//  Created by 是源科技 on 16/7/25.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface foodJilu : NSObject

@property (nonatomic,copy)NSString *createTime;
@property (nonatomic,copy)NSString *energy;
@property (nonatomic,copy)NSString *foodId;
@property (nonatomic,copy)NSString *foodName;
@property (nonatomic,copy)NSString *foodTypeId;
@property (nonatomic,copy)NSString *foodnumber;
@property (nonatomic,copy)NSString *imgSrc;
@property (nonatomic,copy)NSString *orderId;
@property (nonatomic,copy)NSString *userId;
@property (nonatomic,copy)NSString *weight;
@property (nonatomic,copy)NSString *jiluID;


- (instancetype)initWithDict:(NSDictionary *)dict;
+ (instancetype)foodInfoWithDict:(NSDictionary *)dict;

@end
