//
//  foodFenxiTableViewCell.h
//  孕妙
//
//  Created by 是源科技 on 16/7/26.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface foodFenxiTableViewCell : UITableViewCell
@property (nonatomic,strong)UILabel *yinyangsuLabel;
@property (nonatomic,strong)UILabel *danweiLabel;
@property (nonatomic,strong)UILabel *sheruliangLabel;
@property (nonatomic,strong)UILabel *tuijianzhiLabel;
@property (nonatomic,strong)UILabel *fenxiLabel;
@property (nonatomic,strong)UIImageView *jiantouImageView;

@end
