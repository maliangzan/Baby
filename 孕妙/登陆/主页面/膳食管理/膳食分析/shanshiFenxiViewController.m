//
//  shanshiFenxiViewController.m
//  孕妙
//
//  Created by 是源科技 on 16/1/14.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "shanshiFenxiViewController.h"
#import "AFNetworking.h"
#import "MJExtension.h"
#import "passValue.h"
#import "foodFenxiTableViewCell.h"


@interface shanshiFenxiViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UIButton *backButton;
@property (nonatomic,strong)UIImageView *fanhuiImageView;

@property (nonatomic,strong)UITableView *fenxiTableView;

@property (nonatomic,strong)NSMutableArray *dataArray;



@property (nonatomic,strong)NSArray *nameArray;
@property (nonatomic,strong)NSArray *danweiArray;
@property (nonatomic,strong)NSMutableArray *mubiaoMutableArray;

@property (nonatomic, strong)NSMutableArray *ADIarray;

@property (nonatomic,strong)NSMutableArray *goldArrayM;//推荐值数组
@property (nonatomic,strong)NSMutableArray *sheRuliangArrayM;
@property (nonatomic,strong)NSMutableArray *fenxiArrayM;
@property (nonatomic,strong)NSMutableArray *jiantouArrayM;



@end

@implementation shanshiFenxiViewController

-(NSArray *)nameArray
{
    if(!_nameArray)
    {
        _nameArray = @[@"总能量",@"水分",@"蛋白质",@"脂肪",@"膳食纤维",@"碳水化合物",@"维生素A",@"维生素B1",@"维生素B2",@"烟酸",@"维生素E",@"钠",@"钙",@"铁",@"维生素C",@"胆固醇"];
    }
    return _nameArray;
}

-(NSArray *)danweiArray
{
    if(!_danweiArray)
    {
        _danweiArray = @[@"千卡",@"g",@"g",@"g",@"g",@"g",@"mg",@"mg",@"mg",@"mg",@"mg",@"mg",@"mg",@"mg",@"mg",@"mg"];
    }
    return _danweiArray;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = YMColor(245, 245, 245);
    self.title = @"膳食分析";
    

    self.dataArray = [NSMutableArray array];
    self.fenxiArrayM = [NSMutableArray array];
    self.jiantouArrayM = [NSMutableArray array];
    self.sheRuliangArrayM = [NSMutableArray array];
    self.goldArrayM = [NSMutableArray array];
    
    [self allArrayLayout];
    
    //设置右按钮
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame =  CGRectMake(0, 0, 30.0f, 30.0f);
    [backButton setImage:[UIImage imageNamed:@"back_30X30_1"] forState:UIControlStateNormal];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = backButtonItem;
    [backButton addTarget:self action:@selector(ClickbackButton) forControlEvents:UIControlEventTouchUpInside];
    
}

-(void)ClickbackButton
{
    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1]animated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
   
}

//所有数组初始化
-(void)allArrayLayout
{
    CGFloat totalCalori = [[[NSUserDefaults standardUserDefaults]objectForKey:@"totalNengliang"]floatValue];
    NSString *total = [NSString stringWithFormat:@"%.1f",totalCalori];
    
    int yunqitimeInt = [[[NSUserDefaults standardUserDefaults]objectForKey:@"yunqiValue"]intValue];
    
    if(yunqitimeInt == 1)
    {
        
        NSArray *goldArray = @[total,@"1650.0",@"55.0",@"50.0",@"30.0",@"130.0",@"700.0",@"1.2",@"1.2",@"12.0",@"14.0",@"1500.0",@"800.0",@"20.0",@"100.0",@"200.0"];//孕早期
        for (NSString *string in goldArray) {
            [self.goldArrayM addObject:string];
        }
        
    }else if(yunqitimeInt == 2){
        
         NSArray *goldArray = @[total,@"1650.0",@"70.0",@"50.0",@"30.0",@"130.0",@"770.0",@"1.4",@"1.4",@"12.0",@"14.0",@"1500.0",@"1000.0",@"24.0",@"115.0",@"200.0"];//孕中期
        for (NSString *string in goldArray) {
            [self.goldArrayM addObject:string];
        }
    }else{
         NSArray *goldArray = @[total,@"1650.0",@"85.0",@"50.0",@"30.0",@"130.0",@"770.0",@"1.5",@"1.5",@"12.0",@"14.0",@"1500.0",@"1000.0",@"29.0",@"115.0",@"200.0"];//孕晚期
        for (NSString *string in goldArray) {
            [self.goldArrayM addObject:string];
        }
    }
    [self getShanshiFenxiInformationFromServices];
    
    
}


-(void)getShanshiFenxiInformationFromServices
{
    YMLog(@"userName = %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"userName"]);
    YMLog(@"account = %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"userAccount"]);
    YMLog(@"shijian = %@",[self getSystemTime]);
    YMLog(@"self.selectDateString = %@",self.selectDateString);
    
    NSRange yearRang = {0,4};
    NSRange monthRang = {5,2};
    NSRange dayRang = {8,2};
    NSString *yearStr = [self.selectDateString substringWithRange:yearRang];
    NSString *monStr = [self.selectDateString substringWithRange:monthRang];
    NSString *dayStr = [self.selectDateString substringWithRange:dayRang];
    NSString *date = [NSString stringWithFormat:@"%@%@%@000000",yearStr,monStr,dayStr];
    
    [MBProgressHUD showMessage:@"正在拼命加载，请稍后..."];
    NSString *urlString = @"http://120.24.178.16:8080/Health/baby/getFoodanalysisresultinfo.do";
    NSURL *url = [[NSURL alloc]initWithString:urlString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:2];
    request.HTTPMethod = @"POST";
    NSString *str = [NSString stringWithFormat:@"account=%@&date=%@&username=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"userAccount"],date,[[NSUserDefaults standardUserDefaults]objectForKey:@"userName"]];
    NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:data];
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (data)
        {
            [MBProgressHUD hideHUD];
            // 请求成功
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
//            YMLog(@"dict = %@",dict);
            if([[dict objectForKey:@"code"] isEqualToString:@"0"])
            {
                NSArray *dataArray = [dict objectForKey:@"data"];
               
                if(dataArray.count > 0)
                {
                    NSString *goldCalori = [dict objectForKey:@"data1"];
                    [self.goldArrayM replaceObjectAtIndex:0 withObject:goldCalori];
                    for (NSDictionary *dicccc in dataArray) {
                        
                        [self.sheRuliangArrayM addObject:[NSString stringWithFormat:@"%.1f",[[dicccc objectForKey:@"calori"]floatValue]]];
                        [self.sheRuliangArrayM addObject:[NSString stringWithFormat:@"%.1f",[[dicccc objectForKey:@"water"]floatValue]]];
                        [self.sheRuliangArrayM addObject:[NSString stringWithFormat:@"%.1f",[[dicccc objectForKey:@"protein"]floatValue]]];
                        [self.sheRuliangArrayM addObject:[NSString stringWithFormat:@"%.1f",[[dicccc objectForKey:@"fat"]floatValue]]];
                        [self.sheRuliangArrayM addObject:[NSString stringWithFormat:@"%.1f",[[dicccc objectForKey:@"df"]floatValue]]];
                        [self.sheRuliangArrayM addObject:[NSString stringWithFormat:@"%.1f",[[dicccc objectForKey:@"carbohydrate"]floatValue]]];
                        [self.sheRuliangArrayM addObject:[NSString stringWithFormat:@"%.1f",[[dicccc objectForKey:@"va"]floatValue]]];
                        [self.sheRuliangArrayM addObject:[NSString stringWithFormat:@"%.1f",[[dicccc objectForKey:@"vb1"]floatValue]]];
                        [self.sheRuliangArrayM addObject:[NSString stringWithFormat:@"%.1f",[[dicccc objectForKey:@"vb2"]floatValue]]];
                        [self.sheRuliangArrayM addObject:[NSString stringWithFormat:@"%.1f",[[dicccc objectForKey:@"niacin"]floatValue]]];
                        [self.sheRuliangArrayM addObject:[NSString stringWithFormat:@"%.1f",[[dicccc objectForKey:@"ve"]floatValue]]];
                        [self.sheRuliangArrayM addObject:[NSString stringWithFormat:@"%.1f",[[dicccc objectForKey:@"na"]floatValue]]];
                        [self.sheRuliangArrayM addObject:[NSString stringWithFormat:@"%.1f",[[dicccc objectForKey:@"ca"]floatValue]]];
                        [self.sheRuliangArrayM addObject:[NSString stringWithFormat:@"%.1f",[[dicccc objectForKey:@"fe"]floatValue]]];
                        [self.sheRuliangArrayM addObject:[NSString stringWithFormat:@"%.1f",[[dicccc objectForKey:@"vc"]floatValue]]];
                        [self.sheRuliangArrayM addObject:[NSString stringWithFormat:@"%.1f",[[dicccc objectForKey:@"cholesterol"]floatValue]]];
                    }
                    for(int i = 0;i < self.sheRuliangArrayM.count;i ++)
                    {
                        NSString *sheruStr = self.sheRuliangArrayM[i];
                        NSString *goldStr = self.goldArrayM[i];
                        CGFloat bizhi = [sheruStr floatValue] / [goldStr floatValue] * 100.0;
                        [self.fenxiArrayM addObject:[NSString stringWithFormat:@"%.1f",bizhi]];
                        //BabyDown   BabyUp
                        NSString *imageName = nil;
                        if(bizhi > 100)
                        {
                            imageName = @"BabyUp";
                        }
                        else if (bizhi < 100)
                        {
                            imageName = @"BabyDown";
                        }
                        else
                        {
                            imageName = @"";
                        }
                        [self.jiantouArrayM addObject:imageName];
                    }
                }
                else
                {
                    for(int i = 0;i < 16;i ++)
                    {
                        NSString *string = @"0";
                        NSString *imageName = @"BabyDown";
                        [self.sheRuliangArrayM addObject:string];
                        [self.fenxiArrayM addObject:string];
                        [self.jiantouArrayM addObject:imageName];
                    }
                }
                [self viewLayout];
            }
            else
            {
                [MBProgressHUD showError:[dict objectForKey:@"message"]];
            }
            
    }
    }];

}


-(void)viewLayout
{
    //红色小横杠
    UIView *honseView = [[UIView alloc]initWithFrame:CGRectMake(0, 64 + DeviceHeight / 15.5 + DeviceHeight / 2.4, DeviceWidth,DeviceHeight - (64 + DeviceHeight / 15.5 + DeviceHeight / 2.4))];
    [self.view addSubview:honseView];
    
    UILabel *hongseLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, honseView.frame.size.height - honseView.frame.size.height / 16.0,honseView.frame.size.width, honseView.frame.size.height / 16.0)];
    hongseLabel.backgroundColor = YMColor(251, 151, 204);
    [honseView addSubview:hongseLabel];
    
    UIView *view1 = [[UIView alloc]initWithFrame:CGRectMake(0, 64, DeviceWidth, DeviceHeight / 16.23)];
    view1.backgroundColor = [UIColor clearColor];
    [self.view addSubview:view1];
    
    UIView *view2 = [[UIView alloc]initWithFrame:CGRectMake(0, 64 + view1.frame.size.height, DeviceWidth, DeviceHeight / 2.524)];
    view2.backgroundColor = [UIColor clearColor];
    [self.view addSubview:view2];
    
    UIView *view3 = [[UIView alloc]initWithFrame:CGRectMake(0, 64 + view1.frame.size.height + view2.frame.size.height, DeviceWidth, DeviceHeight - 64 - hongseLabel.frame.size.height - view2.frame.size.height - view1.frame.size.height)];
    view3.backgroundColor = [UIColor clearColor];
    [self.view addSubview:view3];
    
    [self addTitleToView:view1];
    
    
    self.fenxiTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, view2.frame.size.width, view2.frame.size.height) style:UITableViewStyleGrouped];
    self.fenxiTableView.delegate = self;
    self.fenxiTableView.dataSource = self;
    self.fenxiTableView.backgroundColor = [UIColor clearColor];
    self.fenxiTableView.tag = 1;
    [view2 addSubview:self.fenxiTableView];
    
    UILabel *crossLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, view3.frame.size.height / 8.75, DeviceWidth, 1)];
    crossLabel.backgroundColor = [UIColor colorWithRed:224 / 255.0 green:224 / 255.0 blue:224 / 255.0 alpha:1.0];
    [view3 addSubview:crossLabel];
    
    UIImageView *jianyiImageView = [[UIImageView alloc]initWithFrame:CGRectMake((DeviceWidth - (view3.frame.size.height / 12.92 + 5.0) * 2.925) / 2.0, view3.frame.size.height / 13.286 - 2.5, (view3.frame.size.height / 12.92 + 5.0) * 2.925, view3.frame.size.height / 12.92 + 5.0)];
    jianyiImageView.image = [UIImage imageNamed:@"建议.png"];
    [view3 addSubview:jianyiImageView];
    
    
   UIScrollView *scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, view3.frame.size.height / 5.0 , DeviceWidth, view3.frame.size.height - jianyiImageView.frame.size.height - 8 - view3.frame.size.height / 13.286 - 2.5 - 10 )];
    scrollView.backgroundColor = [UIColor clearColor];
    scrollView.showsHorizontalScrollIndicator = YES;
    scrollView.showsVerticalScrollIndicator = YES;
    
    UILabel *jianyilabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
    [scrollView addSubview:jianyilabel];
    NSString *s33 = nil;
    
    
    
    
    //判断能量
    if([self.fenxiArrayM[0] floatValue] < 100)
    {
        s33 = @"       孕妇能量长期摄入不足，则导致生长发育迟缓、消瘦，长期下去会严重影响机体的生命活动，机体会动用自身的能量储备甚至消耗自身的组织以满足生命活动能量的需要，若长期处于饥饿状态则将导致生长发育迟缓、消瘦、活力消失，甚至生命运动停止而死亡。\n       建议：在平衡膳食的基础上，适当增加主食和畜禽类、奶类、豆类和鱼类进食量。每天测量并记录体重，通过检测孕期体重增加情况来判断能量摄入是否充足。";
    }
    else if([self.fenxiArrayM[0] floatValue] == 100)
    {
        s33 = @"       孕妇摄入的能量在平衡状态时，可以保证各种生理和体力活动的正常进行。";
    }
    else
    {
        s33 = @"       孕妇摄入过多的能量，多余的能量将转化为脂肪储存在体内，长期摄入过剩，将导致超重、肥胖及相关的慢性病如糖尿病、血脂异常、心脑血管病、某些退行性疾病等。";
    }
    //判断蛋白质
    if([self.fenxiArrayM[2] floatValue] < 100)
    {
        s33 = [s33 stringByAppendingString:@"\n       怀孕后孕妇对蛋白质的需求量会增加。蛋白质摄入不足，会导致孕妇进行性消瘦、体重减轻或水肿，并出现其他营养不良症状。也会影响胎儿的脑部发育，容易发生流产、早产、死产或畸胎。\n       建议：补充蛋白质时一定要适量，不能多也不能少。\n           食物中以豆类、蛋类、奶类和动物瘦肉中富含蛋白，谷类、蔬菜瓜果中含量较低。为改善膳食蛋白质的质量，在膳食中应保证有一定数量的优质蛋白质。一般要求动物蛋白质和大豆蛋白质应占膳食蛋白质总量的30%-50%。\n       与孕前相比，孕妇除了要增加动物瘦肉、禽类、蛋类、鱼及海产品类等优质高蛋白质摄入同时也可以增加饮用（食用）乳制品。"];
    }
    else if ([self.fenxiArrayM[2] floatValue] == 100)
    {
        s33 = [s33 stringByAppendingString:@"\n       正常的蛋白质摄入能有效地促进各种生命活动。能够构成人体内各种重要生命活性物质，包括人类赖以生存的无数酶类，以及各种微量营养素的载体等"];
    }
    else
    {
        s33 = [s33 stringByAppendingString:@"\n       研究显示，过多的摄入蛋白质，人体内可产生大量硫化氢、组织胺等有害物质，易引起腹胀、食欲减退、头晕、疲倦等现象。同时，摄入过量，不仅可造成血中氮质增高，而且也易导致胆固醇增高，加重肾脏的肾小球过滤的压力。"];
    }
    //判断脂肪
    if([self.fenxiArrayM[3] floatValue] < 100)
    {
        s33 = [s33 stringByAppendingString:@"\n       孕妇脂肪摄入不足，可能发生脂溶性维生素缺乏症，引起肝脏、肾脏、神经和视觉等多种疾病，还可影响胎儿心血管和神经系统的发育和成熟。\n建议：孕期要保证一定量的脂肪摄入，动物性食品和硬果类食品中脂肪含量较高，各种油类种子以及硬果类食品中亚油酸类油脂脂肪酸含量较高，如花生仁、黄豆、芝麻、核桃等，多为不饱和脂肪酸，更有利于胎儿的发育。鱼类中含有丰富DHA，是胎儿脑细胞分裂所必需，对促进胎儿脑细胞神经的发育具有重要的作用"];
    }
    else if([self.fenxiArrayM[3] floatValue] == 100)
    {
        s33 = [s33 stringByAppendingString:@"\n       脂肪是构成组织的重要营养物质，在大脑活动中起着重要的和不可替代的作用。它的摄入量充足，不仅有利于孕妇妊娠晚期、分娩以及产褥期作必要的能量储备。而且对胎儿的中枢神经系统发育也尤为重要。"];
    }
    else
    {
        s33 = [s33 stringByAppendingString:@"\n       过多的脂肪不仅会使产妇肥胖，引起孕妇妊娠性脂肪肝，也会增加死产风险。同时摄入过量脂肪会导致孕妇和胎儿脑血管的循环速度变慢，导致大脑供氧量不足，易造成胎儿智力障碍"];
    }
    
    jianyilabel.text = s33;
    jianyilabel.textAlignment = NSTextAlignmentLeft;
    jianyilabel.font = [UIFont systemFontOfSize:15.0];
    NSDictionary * attribute33 = @{NSFontAttributeName:[UIFont boldSystemFontOfSize:15.0]};
    CGSize size33 = [s33 boundingRectWithSize:CGSizeMake(DeviceWidth - 20, DeviceHeight) options:NSStringDrawingUsesLineFragmentOrigin attributes:attribute33 context:nil].size;
    jianyilabel.lineBreakMode = NSLineBreakByWordWrapping;
    jianyilabel.numberOfLines = 0;
    jianyilabel.frame = CGRectMake(10, 8, DeviceWidth - 20, size33.height);
    jianyilabel.textColor = [UIColor colorWithRed:72 / 255.0 green:73 / 255.0 blue:74 / 255.0 alpha:1.0];
    scrollView.contentSize = CGSizeMake(DeviceWidth, size33.height + 10);
    [view3 addSubview:scrollView];
    
}

/**添加标题*/
-(void)addTitleToView:(UIView *)view
{
    CGFloat w = view.frame.size.width;
    CGFloat h = view.frame.size.height;
    
    for(int i = 0; i < 5;i ++)
    {
        CGFloat lw = w / 5.0;
        CGFloat lh = h;
        CGFloat x = i * lw;
        CGFloat y = 0;
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(x, y, lw, lh)];
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = YMColor(152, 41, 97);
        [view addSubview:label];
        switch (i) {
            case 0:
                 label.text = @"营养素";
                break;
            case 1:
                label.text = @"单位";
                break;
            case 2:
                label.text = @"摄入量";
                break;
            case 3:
                label.text = @"推荐值";
                break;
            case 4:
                label.text = @"分析";
                break;
                
            default:
                break;
        }
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return self.nameArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identified = @"Cell";
    foodFenxiTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identified];
    if(tableView.tag == 1)
    {
        if(cell == nil)
        {
            cell = [[foodFenxiTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identified];
            
            CGFloat w = cell.frame.size.width;
            CGFloat h = cell.frame.size.height;
            cell.yinyangsuLabel.frame  =CGRectMake(0, 0, DeviceWidth / 5.0, cell.frame.size.height);
            cell.yinyangsuLabel.textAlignment = NSTextAlignmentCenter;
            cell.yinyangsuLabel.textColor = YMColor(65, 65, 65);
            cell.yinyangsuLabel.font = [UIFont systemFontOfSize:15.0];
            [cell.contentView addSubview:cell.yinyangsuLabel];
            
            
            cell.danweiLabel.frame = CGRectMake(DeviceWidth / 5.0, 0, DeviceWidth / 5.0 - 10, cell.frame.size.height);
            cell.danweiLabel.textAlignment = NSTextAlignmentCenter;
            cell.danweiLabel.textColor = [UIColor colorWithRed:65 / 255.0 green:65 / 255.0 blue:65 / 255.0 alpha:1.0];
            cell.danweiLabel.font = [UIFont systemFontOfSize:15.0];
            [cell.contentView addSubview:cell.danweiLabel];
            
            cell.sheruliangLabel.frame = CGRectMake(DeviceWidth / 5.0 * 2 - 10, 0, DeviceWidth / 5.0, cell.frame.size.height);
            cell.sheruliangLabel.textAlignment = NSTextAlignmentCenter;
            cell.sheruliangLabel.textColor = [UIColor colorWithRed:65 / 255.0 green:65 / 255.0 blue:65 / 255.0 alpha:1.0];
            cell.sheruliangLabel.font = [UIFont systemFontOfSize:15.0];
            [cell.contentView addSubview:cell.sheruliangLabel];
            
            cell.tuijianzhiLabel.frame = CGRectMake(DeviceWidth / 5.0 * 3 - 10, 0, DeviceWidth / 5.0, cell.frame.size.height);
            cell.tuijianzhiLabel.textAlignment = NSTextAlignmentCenter;
            cell.tuijianzhiLabel.textColor = [UIColor colorWithRed:65 / 255.0 green:65 / 255.0 blue:65 / 255.0 alpha:1.0];
            cell.tuijianzhiLabel.font = [UIFont systemFontOfSize:15.0];
            [cell.contentView addSubview:cell.tuijianzhiLabel];
            
            cell.fenxiLabel.frame = CGRectMake(DeviceWidth / 5.0 * 4 - 10, 0, DeviceWidth / 5.0, cell.frame.size.height);
            cell.fenxiLabel.textAlignment = NSTextAlignmentCenter;
            cell.fenxiLabel.textColor = [UIColor colorWithRed:65 / 255.0 green:65 / 255.0 blue:65 / 255.0 alpha:1.0];
            cell.fenxiLabel.font = [UIFont systemFontOfSize:15.0];
            [cell.contentView addSubview:cell.fenxiLabel];
            
            cell.jiantouImageView.frame = CGRectMake(w - 15, (h - 14.25) * 0.5, 12, 14.25);
            [cell.contentView addSubview:cell.jiantouImageView];
            
            
        }
        
        tableView.separatorStyle = UITableViewCellSelectionStyleNone;

        if(indexPath.row % 2 == 1)
        {
            cell.backgroundColor = YMColor(236, 236, 236);
        }
        else
        {
            cell.backgroundColor = [UIColor whiteColor];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
       
        cell.yinyangsuLabel .text = [NSString stringWithFormat:@"%@",self.nameArray[indexPath.row]];
        cell.danweiLabel.text = [NSString stringWithFormat:@"%@",self.danweiArray[indexPath.row]];
        cell.sheruliangLabel.text = [NSString stringWithFormat:@"%@",self.sheRuliangArrayM[indexPath.row]];
        cell.tuijianzhiLabel.text = [NSString stringWithFormat:@"%@",self.goldArrayM[indexPath.row]];
        cell.fenxiLabel.text = [NSString stringWithFormat:@"%@%%",self.fenxiArrayM[indexPath.row]];
        cell.jiantouImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",self.jiantouArrayM[indexPath.row]]];
       
        
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.001f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.001f;
}


//获取系统时间
-(NSString *)getSystemTime
{
    NSDate *senddate=[NSDate date];
    NSDateFormatter  *dateformatter=[[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"YYYYMMddHHmmss"];
    NSString *locationString=[dateformatter stringFromDate:senddate];
    return locationString;
}



@end
