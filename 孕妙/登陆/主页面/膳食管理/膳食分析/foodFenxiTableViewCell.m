//
//  foodFenxiTableViewCell.m
//  孕妙
//
//  Created by 是源科技 on 16/7/26.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "foodFenxiTableViewCell.h"

@implementation foodFenxiTableViewCell

- (void)setFrame:(CGRect)frame {
    
    frame.size.width = kDeviceWidth;
    [super setFrame:frame];
    
}


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.yinyangsuLabel = [[UILabel alloc]init];
        self.danweiLabel = [[UILabel alloc]init];
        self.sheruliangLabel = [[UILabel alloc]init];
        self.tuijianzhiLabel = [[UILabel alloc]init];
        self.fenxiLabel = [[UILabel alloc]init];
        self.jiantouImageView = [[UIImageView alloc]init];
    }
    return self;
}



@end
