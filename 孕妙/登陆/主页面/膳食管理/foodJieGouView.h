//
//  foodJieGouView.h
//  孕妙
//
//  Created by 是源科技 on 16/6/30.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface foodJieGouView : UIView
@property (nonatomic,weak)UIButton *imageButton;//添加食物图片按钮

// 膳食原则界面
@property (nonatomic,weak)UIImageView *jiantouimage;
@property (nonatomic,weak)UIButton *zhankaiButton;

/**首页今日膳食结构*/
-(void)setViewTitle:(NSString *)titleName andTotallabel:(NSString *)totalCount andSheruCount:(NSString *)sheruCount;

/**添加食物中的图片文字*/
-(void)addButtonAndNameToView:(UIImage *)buttonImage andTitle:(NSString *)titleName;

/**膳食原则界面布局*/
-(void)shanshiYuanzeLayoutaddImage:(NSString *)imageName andTitle:(NSString *)titleName;

@end
