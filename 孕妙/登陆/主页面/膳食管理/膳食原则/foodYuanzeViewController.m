//
//  foodYuanzeViewController.m
//  孕妙
//
//  Created by 是源科技 on 16/7/1.
//  Copyright © 2016年 是源科技. All rights reserved.
//

#import "foodYuanzeViewController.h"

@interface foodYuanzeViewController ()<UIWebViewDelegate>
@property (nonatomic,strong)UIWebView *webView;


@end

@implementation foodYuanzeViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = YMColor(245, 245, 245);
    self.title = @"膳食原则";
    
    self.webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, kDeviceHeight)];
    self.webView.backgroundColor = [UIColor whiteColor];
    [self.webView setUserInteractionEnabled:YES];//是否支持交互
    self.webView.delegate = self;
    self.webView.dataDetectorTypes = UIDataDetectorTypeAll;
    [self.webView setOpaque:NO];//opaque是不透明的意思
    [self.webView setScalesPageToFit:YES];//自动缩放以适应屏幕
    [self.view addSubview:self.webView];
//  NSString *path =  @"http://120.24.178.16:8080/Demo4/shanshiyuanze/index.html";
    NSString *path = @"http://120.24.178.16:8080/Demo4/mealsPrinciple/mealsPrinciple.html";
    NSURL *url = [NSURL URLWithString:path];
    [self.webView loadRequest:[NSURLRequest requestWithURL:url]];
}

//几个代理方法

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [MBProgressHUD showMessage:@"正在拼命加载，请耐心等待"];
}

- (void)webViewDidFinishLoad:(UIWebView *)web
{
    [MBProgressHUD hideHUD];
}

-(void)webView:(UIWebView*)webView  DidFailLoadWithError:(NSError*)error
{
    [MBProgressHUD showError:@"网页加载出错,请稍后再试！"];
}

@end
